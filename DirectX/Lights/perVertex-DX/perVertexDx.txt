// *******  perspectiveProjection from orthoGraphics projections***********
//per vertex...
//Steps to convert:
//1.change global variable
//2.make it identity @initialize()
//3.set vertices between 0&1
//4..use perspective() in resize() :: gPerspectiveProjectionMatrix = XMMatrixPerspectiveFovLH(45.0f, (float)width / (float)height, 0.1f, 100.0f);
//5.Do trranslation in display  ...+ve Z as it follows LHS:: worldMatrix = XMMatrixTranslation(0.0f, 0.0f, 6.0f);

//flat4*4==vec4

#include <windows.h>
#include<stdio.h>

#include <d3d11.h>
#include <d3dcompiler.h>    //for shader compilers...
#include "C:\Users\dvyavahare\source\repos\D3d11-Lights\D3d11-Lights\Sphere.h"
#pragma warning( disable: 4838 ) //to reduce warning from xnamath..
#include "C:\Users\dvyavahare\source\repos\ortho\XNAMATH\xnamath.h"  //download from https:\\blogs.msdn.microsoft.com\chuckw\2011\02\22\xna-math_version_2_04\XNAmath_204.zip
//it contains 5 files: 4inl-inline files & 1 header which includes .inl files..copy these files to your project and include header file in program
#pragma comment (lib,"d3d11.lib")
#pragma comment (lib,"D3dcompiler.lib")


#pragma comment(lib,"D3dcompiler.lib")
#pragma comment(lib,"Sphere.lib")

#define WIN_WIDTH	800
#define WIN_HEIGHT	600

//global variables
HWND ghwnd;
DWORD dwStyle;
bool gbActiveWindow = false;
bool gbEscapeKeyPressed = false;
bool gbFullscreen = false;
FILE *gpLogFile = NULL;
char gszLogFileName[] = "Log.txt";

WINDOWPLACEMENT wpPrev = { sizeof(WINDOWPLACEMENT) };
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);


//All Interfaces first...

float gClearColor[4];
IDXGISwapChain *gpIDXGISwapChain = NULL;
ID3D11Device *gpID3D11Device = NULL;
ID3D11DeviceContext *gpID3D11DeviceContext = NULL;
ID3D11RenderTargetView *gpID3D11RenderTargetView = NULL;
ID3D11DepthStencilView *gpID3D11DepthStencilView = NULL;

ID3D11VertexShader *gpID3D11VertexShaderPV = NULL;	// ogl gVertexShaderObjectPV
ID3D11PixelShader *gpID3D11PixelShaderPV = NULL;	//  ogl gFragmentShaderObjectPV

ID3D11VertexShader *gpID3D11VertexShaderPP = NULL;	// ogl gVertexShaderObjectPF
ID3D11PixelShader *gpID3D11PixelShaderPP = NULL;	// ogl gFragmentShaderObjectPF

ID3D11Buffer *gpID3D11BufferInterfacePositionBuffer = NULL;	// ogl vbo_position
ID3D11Buffer *gpID3D11BufferInterfaceNormalBuffer = NULL;	// ogl vbo_normal
ID3D11Buffer *gpID3D11BufferInterfaceIndexBuffer = NULL;	// ogl index/element buffer
ID3D11Buffer *gpID3D11BufferInterfaceConstantBuffer = NULL;	// ogl uniforms

ID3D11InputLayout *gpID3D11InterfaceInputLayoutPV = NULL;
ID3D11InputLayout *gpID3D11InterfaceInputLayoutPP = NULL;
ID3D11RasterizerState *gpID3D11InterfaceRasterizerState = NULL;

float sphere_vertices[1146];
float sphere_normals[1146];
float sphere_textures[764];
unsigned short sphere_elements[2280];
unsigned int gNumElements;
unsigned int gNumVertices;

struct CBUFFER
{
	XMMATRIX WorldViewMatrix;	// 4x4 matrix, ogl vmath::mat4 modelViewMatrix
	XMMATRIX ProjectionMatrix;  // 4x4 matrix, ogl vmath::mat4 ProjectionMatrix

								//Three lights, RGB
	XMVECTOR La[3];				// Ambient Light
	XMVECTOR Ld[3];				// Diffuse Light vector ogl GLuint Ld which points to uniform vec4 Ld
	XMVECTOR Ls[3];				// Specular light

	XMVECTOR Ka;				// Ambient Material
	XMVECTOR Kd;				// Diffuse Material
	XMVECTOR Ks;				// Specular Material
	float    MaterialShininess;	// Material Shininess
	float    MaterialShininess1;	// Material Shininess

	XMVECTOR LightPosition[3];	// 3 Light positions
};

//For light0 - Red light
float light0RedAmbient[] = { 0.0f, 0.0f, 0.0f, 1.0f };	// ambient decides general light
float light0RedDiffuse[] = { 1.0f, 1.0f, 1.0f, 1.0f };	// diffuse decides light color
float light0RedSpecular[] = { 1.0f, 1.0f, 1.0f, 1.0f };	// specular decides highlight color of light
float light0RedPosition[] = { 0.0f, 0.0f, 0.0f, 0.0f };	// manupulate direction in display

														//float light0RedAmbient[] = { 0.0f, 0.0f, 0.0f, 1.0f };	// ambient decides general light
														//float light0RedDiffuse[] = { 1.0f, 0.0f, 0.0f, 1.0f };	// diffuse decides light color
														//float light0RedSpecular[] = { 1.0f, 0.0f, 0.0f, 1.0f };	// specular decides highlight color of light
														//float light0RedPosition[] = { 0.0f, 0.0f, 0.0f, 0.0f };	// manupulate direction in display

float materialAmbient[] = { 0.0f, 0.0f,0.0f,1.0f };
float materialDiffuse[] = { 1.0f,1.0f,1.0f,1.0f };
float materialSpecular[] = { 1.0f, 1.0f,1.0f,1.0f };
float materialShininess = 20.0f;


float materialAmbient1[] = { 1.0f, 0.0f,0.0f,1.0f };
float materialDiffuse1[] = { 1.0f,0.0f,0.0f,1.0f };
float materialSpecular1[] = { 1.0f, 1.0f,1.0f,1.0f };
float materialShininess1 = 100.0f;


bool bAnimation = true;				// default animation on
bool bPerVertexLighting = true;		// default per vertex
bool bPerPixelLighting = false;

XMMATRIX gPerspectiveProjectionMatix;	//4x4 matrix, ogl vmath::mat4 gPerspectiveProjectionMatix

float gAngle = 0.0f;

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdline, int iCmdshow)
{
	WNDCLASSEX wndclass;
	MSG msg;
	TCHAR szAppName[] = TEXT("D3D11");
	HWND hwnd;
	RECT rc;
	bool bDone = false;
	HRESULT initialize(void);
	void uninitialize(void);
	void display(void);
	void spin(void);

	if (fopen_s(&gpLogFile, gszLogFileName, "w") < 0)
	{
		MessageBox(NULL, TEXT("Failed to open Log.txt\nExiting..."), TEXT("Error!"), MB_OK | MB_ICONSTOP);
		exit(0);
	}
	else
	{
		fprintf_s(gpLogFile, "Log.txt created successfully\n");
		fclose(gpLogFile);
	}

	wndclass.cbSize = sizeof(WNDCLASSEX);
	wndclass.style = CS_VREDRAW | CS_HREDRAW | CS_OWNDC;
	wndclass.cbClsExtra = 0;
	wndclass.cbWndExtra = 0;
	wndclass.hInstance = hInstance;
	wndclass.lpfnWndProc = WndProc;
	wndclass.hbrBackground = (HBRUSH)GetStockObject(WHITE_BRUSH);
	wndclass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndclass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);
	wndclass.lpszClassName = szAppName;
	wndclass.lpszMenuName = NULL;

	RegisterClassEx(&wndclass);
	
	hwnd = CreateWindowEx(WS_EX_APPWINDOW, szAppName,
		TEXT("lights"),
		(WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_CLIPCHILDREN | WS_VISIBLE),
		100,
		100,
		WIN_WIDTH,
		WIN_HEIGHT,
		NULL,			/*Parent window handle*/
		NULL,			/*HMENU*/
		hInstance,
		NULL);			/*Something extra info if you want to provide*/

	ghwnd = hwnd;

	ShowWindow(hwnd, iCmdshow);
	SetForegroundWindow(hwnd);		// bring window on top
	SetFocus(hwnd);					// bring focus on window to accept keyboard

									//Initialize D3D
	HRESULT hr;
	hr = initialize();
	if (FAILED(hr))
	{
		fopen_s(&gpLogFile, gszLogFileName, "a+");
		fprintf_s(gpLogFile, "initialize() failed exiting now...\n");
		fclose(gpLogFile);
		DestroyWindow(hwnd);
		hwnd = NULL;
	}
	else
	{
		fopen_s(&gpLogFile, gszLogFileName, "a+");
		fprintf_s(gpLogFile, "initialize() Succeeded.\n");
		fclose(gpLogFile);
	}
	//gameloop
	while (bDone != true)
	{
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE) == TRUE)
		{// message is there 
			if (msg.message == WM_QUIT)
				bDone = true;
			else
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
		else
		{
			if (gbActiveWindow == true)
			{
				if (gbEscapeKeyPressed == true)
					bDone = true;
			}
			if (bAnimation)
				spin();
			/*rendor here*/
			display();
		}
	}
	uninitialize();
	return((int)msg.wParam);
}

LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	//function declarations
	HRESULT resize(int, int);
	void ToggleFullScreen(void);

	//variable declarations
	HRESULT hr;

	switch (iMsg)
	{
	case WM_KEYDOWN:
		switch (wParam)
		{
		case VK_ESCAPE:
			gbEscapeKeyPressed = true;
			break;
		case 0x41:		//'A' or 'a'
			
			break;
		case 0x46:
			ToggleFullScreen();
			break;
		case 0x50:	//P
			break;
		case 0x56:	//V
			break;
		}
		break;

	case WM_ACTIVATE:
		if (HIWORD(wParam) == 0)
			gbActiveWindow = true;
		else
			gbActiveWindow = false;
		break;
	case WM_SIZE:
		if (gpID3D11DeviceContext)
		{
			hr = resize(LOWORD(lParam), HIWORD(lParam));
			if (FAILED(hr))
			{
				fopen_s(&gpLogFile, gszLogFileName, "a+");
				fprintf_s(gpLogFile, "resize() Failed...\n");
				fclose(gpLogFile);
				return(hr);
			}
			else
			{
				fopen_s(&gpLogFile, gszLogFileName, "a+");
				fprintf_s(gpLogFile, "resize() Succeeded.\n");
				fclose(gpLogFile);
			}
		}
		break;
	case WM_ERASEBKGND:
		return (0);
	case WM_DESTROY:
		PostQuitMessage(0);
		break;
	default:
		break;
	}
	return(DefWindowProc(hwnd, iMsg, wParam, lParam));
}

void ToggleFullScreen(void)
{
	// variable declarations
	MONITORINFO mi;

	//code
	if (gbFullscreen == false)
	{
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);
		if (dwStyle & WS_OVERLAPPEDWINDOW)
		{
			mi.cbSize = sizeof(MONITORINFO);
			if (GetWindowPlacement(ghwnd, &wpPrev) && GetMonitorInfo(MonitorFromWindow(ghwnd, MONITORINFOF_PRIMARY), &mi))
			{
				SetWindowLong(ghwnd, GWL_STYLE, (dwStyle & ~WS_OVERLAPPEDWINDOW));
				SetWindowPos(ghwnd, HWND_TOP, mi.rcMonitor.left, mi.rcMonitor.top,
					(mi.rcMonitor.right - mi.rcMonitor.left), (mi.rcMonitor.bottom - mi.rcMonitor.top),
					SWP_NOZORDER | SWP_FRAMECHANGED);
			}
		}
		ShowCursor(false);
		gbFullscreen = true;
	}
	else
	{
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED);

		ShowCursor(true);
		gbFullscreen = false;
	}
}




HRESULT initialize(void)
{
	//function declarations
	void uninitialize(void);
	HRESULT resize(int, int);


	
	//variable declarations
	HRESULT hr;
	D3D_DRIVER_TYPE d3dDriverType;
	D3D_DRIVER_TYPE d3dDriverTypes[] = { D3D_DRIVER_TYPE_HARDWARE, D3D_DRIVER_TYPE_WARP, D3D_DRIVER_TYPE_REFERENCE, };
	D3D_FEATURE_LEVEL d3dFeatureLevel_required = D3D_FEATURE_LEVEL_11_0;
	D3D_FEATURE_LEVEL d3dFeatureLevel_acquired = D3D_FEATURE_LEVEL_10_0; //minimum version required, ask it through acquired
	UINT createDeviceFlags = 0;
	UINT numDriverTypes = 0;
	UINT numFeatureLevels = 1;	//based on d3dFeatureLevel_required

								//code
	numDriverTypes = sizeof(d3dDriverTypes) / sizeof(d3dDriverTypes[0]);	//calculate elements in array

	DXGI_SWAP_CHAIN_DESC dxgiSwapChainDesc;
	ZeroMemory((void *)&dxgiSwapChainDesc, sizeof(DXGI_SWAP_CHAIN_DESC));
	dxgiSwapChainDesc.BufferCount = 1;	//Request one back buffer (D3D gives 1 front buffer by default)
	dxgiSwapChainDesc.BufferDesc.Width = WIN_WIDTH;		//buffer width
	dxgiSwapChainDesc.BufferDesc.Height = WIN_HEIGHT;	//buffer height
	dxgiSwapChainDesc.BufferDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
	dxgiSwapChainDesc.BufferDesc.RefreshRate.Numerator = 60;	//Vertical Sync Numerator --> Refresh Rate=N/D
	dxgiSwapChainDesc.BufferDesc.RefreshRate.Denominator = 1;	//Vertical Sync Denominator --> Refresh Rate=N/D
	dxgiSwapChainDesc.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;	//buffer used for rendering
	dxgiSwapChainDesc.OutputWindow = ghwnd;
	dxgiSwapChainDesc.SampleDesc.Count = 1;	//Default MSSA=1x, no of multisamples per pixels (Range: 1x, 2x, 4x, 8x)
	dxgiSwapChainDesc.SampleDesc.Quality = 0;	//Default quality
	dxgiSwapChainDesc.Windowed = TRUE;	//Windowed Mode, FALSE: FullScreen Mode

										//Search through drivers
	UINT driverTypeIndex;
	for (driverTypeIndex = 0; driverTypeIndex < numDriverTypes; driverTypeIndex++)
	{
		d3dDriverType = d3dDriverTypes[driverTypeIndex];

		hr = D3D11CreateDeviceAndSwapChain(
			NULL,						//Default Video Adapter
			d3dDriverType,				//Driver Type (Range: Hardware, WARP, Reference, Software)
			NULL,						//Software Rasterizer if you have your own
			createDeviceFlags,			//Flags
			&d3dFeatureLevel_required,	//Required Feature Level
			numFeatureLevels,			//Num of feature levels in above argument
			D3D11_SDK_VERSION,			//SDK Version
			&dxgiSwapChainDesc,			//[In] DXGI Swap Chain Description
			&gpIDXGISwapChain,			//[Out] Swap Chain pointer, used for rendering
			&gpID3D11Device,				//[Out] Device Adapter
			&d3dFeatureLevel_acquired,	//[Out] Feature level supported 
			&gpID3D11DeviceContext);

		if (SUCCEEDED(hr))
			break;			//Required Driver Type Found, break the loop
	}
	if (FAILED(hr))
	{
		fopen_s(&gpLogFile, gszLogFileName, "a+");
		fprintf_s(gpLogFile, "D3D11CreateDeviceAndSwapChain() Failed at %d...\n", driverTypeIndex);
		fclose(gpLogFile);
		return(hr);
	}
	else
	{
		fopen_s(&gpLogFile, gszLogFileName, "a+");
		fprintf_s(gpLogFile, "The Chosen Driver Type Is Of ");
		if (d3dDriverType == D3D_DRIVER_TYPE_HARDWARE)
			fprintf_s(gpLogFile, "Hardware Type\n");
		else if (d3dDriverType = D3D_DRIVER_TYPE_WARP)
			fprintf_s(gpLogFile, "WARP Type\n");
		else if (d3dDriverType = D3D_DRIVER_TYPE_REFERENCE)
			fprintf_s(gpLogFile, "Reference Type\n");
		else
			fprintf_s(gpLogFile, "Unknown Type\n");

		fprintf_s(gpLogFile, "The Supported Highest Feature Level Is ");
		if (d3dFeatureLevel_acquired == D3D_FEATURE_LEVEL_11_0)
			fprintf_s(gpLogFile, "11.0\n");
		else if (d3dFeatureLevel_acquired == D3D_FEATURE_LEVEL_10_1)
			fprintf_s(gpLogFile, "10.1\n");
		else if (d3dFeatureLevel_acquired == D3D_FEATURE_LEVEL_10_0)
			fprintf_s(gpLogFile, "10.0\n");
		else
			fprintf_s(gpLogFile, "Unknown\n");

		fclose(gpLogFile);
	}

	// shader
	hr = S_OK;
	const char *vertexShaderSourceCode =
		"cbuffer ConstBuffer" \
		"{" \
		"float4x4 worldViewMatrix;" \
		"float4x4 projectionMatrix;" \
		"float4 la[3];" \
		"float4 ld[3];" \
		"float4 ls[3];" \
		"float4 ka;" \
		"float4 kd;" \
		"float4 ks;" \
		"float  materialShininess;" \
		"float4 lightPosition[3];" \
		"uint   lightEnabled;" \
		"}" \
		"struct vertex_output" \
		"{" \
		"float4 position: SV_POSITION;" \
		"float3 phong_ads_color: COLOR;" \
		"};" \
		"vertex_output main(float4 pos: POSITION, float4 normal: NORMAL)" \
		"{" \
		"vertex_output output;" \
		"float4 ambient = la[0] * ka;" \

		"float4 eyeCoordinates = mul(worldViewMatrix, pos);" \
		"float3 tnorm = normalize( mul( (float3x3)worldViewMatrix, (float3)normal ) );" \
		"float3 lightDirection = (float3)normalize( lightPosition[0] - eyeCoordinates );" \
		"float4 diffuse = ld[0] * kd * max( dot(lightDirection, tnorm), 0.0 );" \

		"float3 reflectionVector = reflect(-lightDirection, tnorm);" \
		"float3 viewerVector = normalize(-eyeCoordinates.xyz);" \
		"float  r_dot_v = max( dot(reflectionVector, viewerVector), 0.0 );" \
		"float4 specular = ls[0] * ks * pow(r_dot_v, materialShininess);" \
		"output.phong_ads_color = ambient + diffuse + specular;" \

		"ambient = la[1] * ka;" \
		"lightDirection = (float3)normalize( lightPosition[1] - eyeCoordinates );" \
		"diffuse = ld[1] * kd * max( dot(lightDirection, tnorm), 0.0 );" \
		"reflectionVector = reflect(-lightDirection, tnorm);" \
		"r_dot_v = max( dot(reflectionVector, viewerVector), 0.0 );" \
		"specular = ls[1] * ks * pow(r_dot_v, materialShininess);" \
		"output.phong_ads_color += ambient + diffuse + specular;" \

		"ambient = la[2] * ka;" \
		"lightDirection = (float3)normalize( lightPosition[2] - eyeCoordinates );" \
		"diffuse = ld[2] * kd * max( dot(lightDirection, tnorm), 0.0 );" \
		"reflectionVector = reflect(-lightDirection, tnorm);" \
		"r_dot_v = max( dot(reflectionVector, viewerVector), 0.0 );" \
		"specular = ls[2] * ks * pow(r_dot_v, materialShininess);" \
		"output.phong_ads_color += ambient + diffuse + specular;" \

		"float4 transformedPosition = mul( worldViewMatrix, pos );" \
		"output.position = mul( projectionMatrix, transformedPosition);" \
		"return (output);" \
		"}";

	ID3DBlob *pID3DBlob_VertexShaderByteCode = NULL;
	ID3DBlob *pID3DBlob_Error = NULL;

	//02-Compile Shader Source Code
	hr = D3DCompile(vertexShaderSourceCode,
		lstrlenA(vertexShaderSourceCode) + 1,
		"VS",
		NULL,
		D3D_COMPILE_STANDARD_FILE_INCLUDE,
		"main",
		"vs_5_0",
		0,
		0,
		&pID3DBlob_VertexShaderByteCode,
		&pID3DBlob_Error);

	if (FAILED(hr))
	{
		if (pID3DBlob_Error != NULL)
		{
			fopen_s(&gpLogFile, gszLogFileName, "a+");
			fprintf_s(gpLogFile, "PV: D3DCompile() Failed For Vertex Shader : %s\n",
				(char*)pID3DBlob_Error->GetBufferPointer());
			fclose(gpLogFile);
			pID3DBlob_Error->Release();
			pID3DBlob_Error = NULL;
		}
		return (hr);
	}
	else
	{
		fopen_s(&gpLogFile, gszLogFileName, "a+");
		fprintf_s(gpLogFile, "PV: D3DCompile() Succeeded For Vertex Shader\n");
		fclose(gpLogFile);
	}

	//03-Create Shader
	hr = gpID3D11Device->CreateVertexShader(pID3DBlob_VertexShaderByteCode->GetBufferPointer(),
		pID3DBlob_VertexShaderByteCode->GetBufferSize(),
		NULL, &gpID3D11VertexShaderPV);

	if (FAILED(hr))
	{
		fopen_s(&gpLogFile, gszLogFileName, "a+");
		fprintf_s(gpLogFile, "PV: CreateVertexShader() Failed.\n");
		fclose(gpLogFile);
		return (hr);
	}
	else
	{
		fopen_s(&gpLogFile, gszLogFileName, "a+");
		fprintf_s(gpLogFile, "PV: CreateVertexShader() Succeeded.\n");
		fclose(gpLogFile);


	}

	//04-Set Shader in Pipeline
	gpID3D11DeviceContext->VSSetShader(gpID3D11VertexShaderPV, NULL, 0);

	/*************PIXEL SHADER********************/
	//01-Prepare Shader Source Code
	const char *pixelShaderSourceCode =
		"float4 main(float4 pos: SV_POSITION, float4 phong_ads_color: COLOR) : SV_TARGET" \
		"{" \
		"return (phong_ads_color);" \
		"}"; \
		ID3DBlob *pID3DBlob_PixelShaderByteCoce = NULL;
	pID3DBlob_Error = NULL;

	//02-Compile Shader
	hr = D3DCompile(pixelShaderSourceCode,
		lstrlenA(pixelShaderSourceCode) + 1,
		"PS",
		NULL,
		D3D_COMPILE_STANDARD_FILE_INCLUDE,
		"main",
		"ps_5_0",
		0,
		0,
		&pID3DBlob_PixelShaderByteCoce,
		&pID3DBlob_Error);

	if (FAILED(hr))
	{
		if (pID3DBlob_Error != NULL)
		{
			fopen_s(&gpLogFile, gszLogFileName, "a+");
			fprintf_s(gpLogFile, "PV: D3DCompile() Failed for Pixel Shader : %s\n",
				(char*)pID3DBlob_Error->GetBufferPointer());
			fclose(gpLogFile);
		}
		return (hr);
	}
	else
	{
		fopen_s(&gpLogFile, gszLogFileName, "a+");
		fprintf_s(gpLogFile, "PV: D3DCompile() Succeeded for Pixel Shader\n");
		fclose(gpLogFile);
	}

	//03-Create Shader
	hr = gpID3D11Device->CreatePixelShader(pID3DBlob_PixelShaderByteCoce->GetBufferPointer(),
		pID3DBlob_PixelShaderByteCoce->GetBufferSize(), NULL, &gpID3D11PixelShaderPV);
	if (FAILED(hr))
	{
		fopen_s(&gpLogFile, gszLogFileName, "a+");
		fprintf_s(gpLogFile, "PV: CreatePixelShader() Failed\n");
		fclose(gpLogFile);
	}
	else
	{
		fopen_s(&gpLogFile, gszLogFileName, "a+");
		fprintf_s(gpLogFile, "PV: CreatePixelShader() Succeeded\n");
		fclose(gpLogFile);
	}

	//04-Set Shader in Pipeline
	gpID3D11DeviceContext->PSSetShader(gpID3D11PixelShaderPV, NULL, 0);
	pID3DBlob_PixelShaderByteCoce->Release();
	pID3DBlob_PixelShaderByteCoce = NULL;

	//create and set input layout
	D3D11_INPUT_ELEMENT_DESC inputElementDesc[2];	//Position and Normal
	inputElementDesc[0].SemanticName = "POSITION";
	inputElementDesc[0].SemanticIndex = 0;			//this is used for interleaved array
	inputElementDesc[0].Format = DXGI_FORMAT_R32G32B32_FLOAT;
	inputElementDesc[0].InputSlot = 0;				//Position slot
	inputElementDesc[0].InputSlotClass = D3D11_INPUT_PER_VERTEX_DATA;
	inputElementDesc[0].AlignedByteOffset = 0;
	inputElementDesc[0].InstanceDataStepRate = 0;

	inputElementDesc[1].SemanticName = "NORMAL";
	inputElementDesc[1].SemanticIndex = 0;			//this is used for interleaved array
	inputElementDesc[1].Format = DXGI_FORMAT_R32G32B32_FLOAT;
	inputElementDesc[1].InputSlot = 1;				//Normal slot
	inputElementDesc[1].InputSlotClass = D3D11_INPUT_PER_VERTEX_DATA;
	inputElementDesc[1].AlignedByteOffset = 0;
	inputElementDesc[1].InstanceDataStepRate = 0;

	hr = gpID3D11Device->CreateInputLayout(inputElementDesc, 2,
		pID3DBlob_VertexShaderByteCode->GetBufferPointer(),
		pID3DBlob_VertexShaderByteCode->GetBufferSize(), &gpID3D11InterfaceInputLayoutPV);
	gpID3D11DeviceContext->IASetInputLayout(gpID3D11InterfaceInputLayoutPV);
	pID3DBlob_VertexShaderByteCode->Release();
	pID3DBlob_VertexShaderByteCode = NULL;





//Vertices, inputlayout, vetex buffers, and constant buffers

	//Sphere Geometry data
	getSphereVertexData(sphere_vertices, sphere_normals, sphere_textures, sphere_elements);
	gNumVertices = getNumberOfSphereVertices();
	gNumElements = getNumberOfSphereElements();

	//create vertex buffer for position
	D3D11_BUFFER_DESC bufferDesc;
	ZeroMemory(&bufferDesc, sizeof(D3D11_BUFFER_DESC));
	bufferDesc.Usage = D3D11_USAGE_DYNAMIC;
	bufferDesc.ByteWidth = sizeof(float) * ARRAYSIZE(sphere_vertices);
	bufferDesc.BindFlags = D3D11_BIND_VERTEX_BUFFER;
	bufferDesc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
	hr = gpID3D11Device->CreateBuffer(&bufferDesc, NULL, &gpID3D11BufferInterfacePositionBuffer);
	if (FAILED(hr))
	{
		fopen_s(&gpLogFile, gszLogFileName, "a+");
		fprintf_s(gpLogFile, "ID3D11Device::CreateBuffer() Failed For Vertex Buffer\n");
		fclose(gpLogFile);
	}
	else
	{
		fopen_s(&gpLogFile, gszLogFileName, "a+");
		fprintf_s(gpLogFile, "ID3D11Device::CreateBuffer() Succeeded For Vertex Buffer\n");
		fclose(gpLogFile);
	}

	//copy vertices to above vertex buffer
	D3D11_MAPPED_SUBRESOURCE mappedSubresource;
	ZeroMemory(&mappedSubresource, sizeof(D3D11_MAPPED_SUBRESOURCE));
	gpID3D11DeviceContext->Map(gpID3D11BufferInterfacePositionBuffer, 0, D3D11_MAP_WRITE_DISCARD, 0, &mappedSubresource);
	memcpy(mappedSubresource.pData, sphere_vertices, sizeof(sphere_vertices));
	gpID3D11DeviceContext->Unmap(gpID3D11BufferInterfacePositionBuffer, 0);

	//create vertex buffer for normal
	ZeroMemory(&bufferDesc, sizeof(D3D11_BUFFER_DESC));
	bufferDesc.Usage = D3D11_USAGE_DYNAMIC;
	bufferDesc.ByteWidth = sizeof(float) * ARRAYSIZE(sphere_normals);
	bufferDesc.BindFlags = D3D11_BIND_VERTEX_BUFFER;
	bufferDesc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
	hr = gpID3D11Device->CreateBuffer(&bufferDesc, NULL, &gpID3D11BufferInterfaceNormalBuffer);
	if (FAILED(hr))
	{
		fopen_s(&gpLogFile, gszLogFileName, "a+");
		fprintf_s(gpLogFile, "ID3D11Device::CreateBuffer() Failed For Normal Buffer\n");
		fclose(gpLogFile);
	}
	else
	{
		fopen_s(&gpLogFile, gszLogFileName, "a+");
		fprintf_s(gpLogFile, "ID3D11Device::CreateBuffer() Succeeded For Normal Buffer\n");
		fclose(gpLogFile);
	}

	//copy vertices to above normal buffer
	ZeroMemory(&mappedSubresource, sizeof(D3D11_MAPPED_SUBRESOURCE));
	gpID3D11DeviceContext->Map(gpID3D11BufferInterfaceNormalBuffer, 0, D3D11_MAP_WRITE_DISCARD, 0, &mappedSubresource);
	memcpy(mappedSubresource.pData, sphere_normals, sizeof(sphere_normals));
	gpID3D11DeviceContext->Unmap(gpID3D11BufferInterfaceNormalBuffer, 0);

	//create vertex buffer for index
	ZeroMemory(&bufferDesc, sizeof(D3D11_BUFFER_DESC));
	bufferDesc.Usage = D3D11_USAGE_DYNAMIC;
	bufferDesc.ByteWidth = gNumElements * sizeof(unsigned short);
	bufferDesc.BindFlags = D3D11_BIND_INDEX_BUFFER;
	bufferDesc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
	hr = gpID3D11Device->CreateBuffer(&bufferDesc, NULL, &gpID3D11BufferInterfaceIndexBuffer);
	if (FAILED(hr))
	{
		fopen_s(&gpLogFile, gszLogFileName, "a+");
		fprintf_s(gpLogFile, "ID3D11Device::CreateBuffer() Failed For Index Buffer\n");
		fclose(gpLogFile);
	}
	else
	{
		fopen_s(&gpLogFile, gszLogFileName, "a+");
		fprintf_s(gpLogFile, "ID3D11Device::CreateBuffer() Succeeded For Index Buffer\n");
		fclose(gpLogFile);
	}

	//copy vertices to above index buffer
	ZeroMemory(&mappedSubresource, sizeof(D3D11_MAPPED_SUBRESOURCE));
	gpID3D11DeviceContext->Map(gpID3D11BufferInterfaceIndexBuffer, 0, D3D11_MAP_WRITE_DISCARD, 0, &mappedSubresource);
	memcpy(mappedSubresource.pData, sphere_elements, gNumElements * sizeof(unsigned short));
	gpID3D11DeviceContext->Unmap(gpID3D11BufferInterfaceIndexBuffer, 0);

	//Create Constant buffer
	D3D11_BUFFER_DESC bufferDesc_ConstBuffer;
	ZeroMemory(&bufferDesc_ConstBuffer, sizeof(D3D11_BUFFER_DESC));
	bufferDesc_ConstBuffer.Usage = D3D11_USAGE_DEFAULT;
	bufferDesc_ConstBuffer.ByteWidth = sizeof(struct CBUFFER);
	bufferDesc_ConstBuffer.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
	hr = gpID3D11Device->CreateBuffer(&bufferDesc_ConstBuffer, nullptr, &gpID3D11BufferInterfaceConstantBuffer);
	if (FAILED(hr))
	{
		fopen_s(&gpLogFile, gszLogFileName, "a+");
		fprintf_s(gpLogFile, "ID3D11Device::CreateBuffer() Failed For Constant Buffer\n");
		fclose(gpLogFile);
	}
	else
	{
		fopen_s(&gpLogFile, gszLogFileName, "a+");
		fprintf_s(gpLogFile, "ID3D11Device::CreateBuffer() Succeeded For Constant Buffer\n");
		fclose(gpLogFile);
	}
	gpID3D11DeviceContext->VSSetConstantBuffers(0, 1, &gpID3D11BufferInterfaceConstantBuffer);
	gpID3D11DeviceContext->PSSetConstantBuffers(0, 1, &gpID3D11BufferInterfaceConstantBuffer);

	//D3D Clearing color: Blue		//Analogous to glClearColor() in OpenGL
	gClearColor[0] = 0.0f;
	gClearColor[1] = 0.0f;
	gClearColor[2] = 0.0f;
	gClearColor[3] = 1.0f;

	//Other D3D11 settings
	//Disable back face culling mode
	D3D11_RASTERIZER_DESC rasterizerDesc;
	ZeroMemory(&rasterizerDesc, sizeof(D3D11_RASTERIZER_DESC));
	rasterizerDesc.AntialiasedLineEnable = FALSE;
	rasterizerDesc.MultisampleEnable = FALSE;
	rasterizerDesc.DepthBias = 0;
	rasterizerDesc.DepthBiasClamp = 0.0f;
	rasterizerDesc.SlopeScaledDepthBias = 0.0f;
	rasterizerDesc.CullMode = D3D11_CULL_NONE;
	rasterizerDesc.DepthClipEnable = TRUE;
	rasterizerDesc.FillMode = D3D11_FILL_SOLID;
	rasterizerDesc.FrontCounterClockwise = FALSE;
	rasterizerDesc.ScissorEnable = FALSE;
	hr = gpID3D11Device->CreateRasterizerState(&rasterizerDesc, &gpID3D11InterfaceRasterizerState);
	if (FAILED(hr))
	{
		fopen_s(&gpLogFile, gszLogFileName, "a+");
		fprintf_s(gpLogFile, "ID3D11Device::CreateRasterizerState() Failed \n");
		fclose(gpLogFile);
	}
	else
	{
		fopen_s(&gpLogFile, gszLogFileName, "a+");
		fprintf_s(gpLogFile, "ID3D11Device::CreateRasterizerState() Succeeded \n");
		fclose(gpLogFile);
	}

	gpID3D11DeviceContext->RSSetState(gpID3D11InterfaceRasterizerState);

	//Call resize for 1st time : Warm up call
	hr = resize(WIN_WIDTH, WIN_HEIGHT);
	if (FAILED(hr))
	{
		fopen_s(&gpLogFile, gszLogFileName, "a+");
		fprintf_s(gpLogFile, "resize() Failed...\n");
		fclose(gpLogFile);
		return(hr);
	}
	else
	{
		fopen_s(&gpLogFile, gszLogFileName, "a+");
		fprintf_s(gpLogFile, "resize() Succeeded.\n");
		fclose(gpLogFile);
	}

	return (S_OK);
}

HRESULT resize(int width, int height)
{
	//code 
	HRESULT hr = S_OK;

	//01-free size dependent resources
	if (gpID3D11DepthStencilView)
	{
		gpID3D11DepthStencilView->Release();
		gpID3D11DepthStencilView = NULL;
	}
	if (gpID3D11RenderTargetView)
	{
		gpID3D11RenderTargetView->Release();
		gpID3D11RenderTargetView = NULL;
	}

	//02-resize swap chain buffers as per width and height
	gpIDXGISwapChain->ResizeBuffers(1, width, height, DXGI_FORMAT_R8G8B8A8_UNORM, 0);


	//03_a-Get back resized buffer from swap chain
	ID3D11Texture2D *pID3D11Texture2D_BackBuffer;
	gpIDXGISwapChain->GetBuffer(0, __uuidof(ID3D11Texture2D), (LPVOID*)&pID3D11Texture2D_BackBuffer);

	//04-Regain Render Target View from D3D11 Device using above back buffer
	hr = gpID3D11Device->CreateRenderTargetView(pID3D11Texture2D_BackBuffer, NULL, &gpID3D11RenderTargetView);
	if (FAILED(hr))
	{
		fopen_s(&gpLogFile, gszLogFileName, "a+");
		fprintf_s(gpLogFile, "ID3D11Device::CreateRenderTargetView() Failed...\n");
		fclose(gpLogFile);
		return(hr);
	}
	else
	{
		fopen_s(&gpLogFile, gszLogFileName, "a+");
		fprintf_s(gpLogFile, "ID3D11Device::CreateRenderTargetView() Succeeded.\n");
		fclose(gpLogFile);
	}

	pID3D11Texture2D_BackBuffer->Release();
	pID3D11Texture2D_BackBuffer = NULL;

	//05-Create Depth buffer to enable depth
	//There is no direct depth buffer as such in D3D11,
	//so create empty Texture buffer and convert it to depth buffer
	D3D11_TEXTURE2D_DESC textureDesc;
	ZeroMemory(&textureDesc, sizeof(D3D11_TEXTURE2D_DESC));
	textureDesc.Width = width;		//depth buffer width equals to resized window width
	textureDesc.Height = height;	//depth buffer height equals to resized window height
	textureDesc.ArraySize = 1;		//number texture buffers, but here only one depth buffer
	textureDesc.MipLevels = 1;		//multisampled texture
	textureDesc.SampleDesc.Count = 1;	//MSAA
	textureDesc.SampleDesc.Quality = 0; //MSAA
	textureDesc.Format = DXGI_FORMAT_D32_FLOAT;		//Depth bits = 32 bit float 
	textureDesc.BindFlags = D3D11_BIND_DEPTH_STENCIL;	//buffer created for DEPTH or STENCIL
	textureDesc.CPUAccessFlags = 0;		//CPU access is not requred
	textureDesc.MiscFlags = 0;			//default

	ID3D11Texture2D *pID3D11Texture2D_DepthBuffer = NULL;
	hr = gpID3D11Device->CreateTexture2D(&textureDesc, NULL, &pID3D11Texture2D_DepthBuffer);
	if (FAILED(hr))
	{
		fopen_s(&gpLogFile, gszLogFileName, "a+");
		fprintf_s(gpLogFile, "ID3D11Device::CreateTexture2D() Failed...\n");
		fclose(gpLogFile);
		return(hr);
	}
	else
	{
		fopen_s(&gpLogFile, gszLogFileName, "a+");
		fprintf_s(gpLogFile, "ID3D11Device::CreateTexture2D() Succeeded.\n");
		fclose(gpLogFile);
	}

	D3D11_DEPTH_STENCIL_VIEW_DESC dsvDesc;
	ZeroMemory(&dsvDesc, sizeof(D3D11_DEPTH_STENCIL_VIEW_DESC));
	dsvDesc.Format = DXGI_FORMAT_D32_FLOAT;
	dsvDesc.ViewDimension = D3D11_DSV_DIMENSION_TEXTURE2DMS;	//multisampled texture2d

																//06-create depth stencil view
	hr = gpID3D11Device->CreateDepthStencilView(pID3D11Texture2D_DepthBuffer, &dsvDesc, &gpID3D11DepthStencilView);
	if (FAILED(hr))
	{
		fopen_s(&gpLogFile, gszLogFileName, "a+");
		fprintf_s(gpLogFile, "ID3D11Device::CreateDepthStencilView() Failed...\n");
		fclose(gpLogFile);
		return(hr);
	}
	else
	{
		fopen_s(&gpLogFile, gszLogFileName, "a+");
		fprintf_s(gpLogFile, "ID3D11Device::CreateDepthStencilView() Succeeded.\n");
		fclose(gpLogFile);
	}

	//release local interface
	pID3D11Texture2D_DepthBuffer->Release();
	pID3D11Texture2D_DepthBuffer = NULL;

	//07-Set Render Target View(RTV) and Depth Stencil View(DSV) as a render target
	gpID3D11DeviceContext->OMSetRenderTargets(1, &gpID3D11RenderTargetView, gpID3D11DepthStencilView);
	//gpID3D11DeviceContext->OMSetRenderTargets(1, &gpID3D11RenderTargetView, NULL);

	//08-Set Viewport
	D3D11_VIEWPORT d3dViewPort;
	d3dViewPort.TopLeftX = 0.0f;
	d3dViewPort.TopLeftY = 0.0f;
	d3dViewPort.Width = (float)width;
	d3dViewPort.Height = (float)height;
	d3dViewPort.MinDepth = 0.0f;
	d3dViewPort.MaxDepth = 1.0f;

	gpID3D11DeviceContext->RSSetViewports(1, &d3dViewPort);

	//set orthographic matrix
	gPerspectiveProjectionMatix = XMMatrixPerspectiveFovLH(XMConvertToRadians(45.0f), (float)width / (float)height, 0.1f, 100.0f);

	return(hr);
}

void display(void)
{
	//function declarations

	gpID3D11DeviceContext->IASetInputLayout(gpID3D11InterfaceInputLayoutPV);
	gpID3D11DeviceContext->VSSetShader(gpID3D11VertexShaderPV, nullptr, 0);
	gpID3D11DeviceContext->PSSetShader(gpID3D11PixelShaderPV, nullptr, 0);

	//code
	//Clear render target view to a chosen color
	gpID3D11DeviceContext->ClearRenderTargetView(gpID3D11RenderTargetView, gClearColor);
	//Clear depth view
	gpID3D11DeviceContext->ClearDepthStencilView(gpID3D11DepthStencilView, D3D11_CLEAR_DEPTH, 1.0f, 0);

	//01-Select which vertex buffer to display
	UINT stride = sizeof(float) * 3;	//x,y,z of float size
	UINT offset = 0;
	gpID3D11DeviceContext->IASetVertexBuffers(0, 1, &gpID3D11BufferInterfacePositionBuffer, &stride, &offset);

	stride = sizeof(float) * 3;	//nx,ny,nz of float size
	offset = 0;
	gpID3D11DeviceContext->IASetVertexBuffers(1, 1, &gpID3D11BufferInterfaceNormalBuffer, &stride, &offset);

	//set index buffer
	gpID3D11DeviceContext->IASetIndexBuffer(gpID3D11BufferInterfaceIndexBuffer, DXGI_FORMAT_R16_UINT, 0); // R16 maps with 'short'

																										  //02-Select Geometry primitive
	gpID3D11DeviceContext->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);

	//03-Translations: is concerned with world matrix transformation
	XMMATRIX worldViewMatrix = XMMatrixIdentity();
	XMMATRIX worldMatrix = XMMatrixIdentity();
	XMMATRIX viewMatrix = XMMatrixIdentity();
	XMMATRIX translationMatrix = XMMatrixIdentity();

	//translate along deep inside z (here +ve z is going inside screen)
	translationMatrix = XMMatrixTranslation(0.0f, 0.0f, 3.0f);

	//WM = TM		
	worldMatrix = translationMatrix;

	//final worldView matrix
	worldViewMatrix = worldMatrix * viewMatrix;

	//04-Load the data into constant buffer
	CBUFFER constantBuffer;
	ZeroMemory(&constantBuffer, sizeof(struct CBUFFER));

	float *pLight;
	float radius = 50.0f;
	// Red
	pLight = light0RedAmbient;
	constantBuffer.La[0] = XMVectorSet(pLight[0], pLight[1], pLight[2], pLight[3]);
	
	pLight = light0RedDiffuse;
	constantBuffer.Ld[0] = XMVectorSet(pLight[0], pLight[1], pLight[2], pLight[3]);
	pLight = light0RedSpecular;
	constantBuffer.Ls[0] = XMVectorSet(pLight[0], pLight[1], pLight[2], pLight[3]);
	//Red light move around x-axis
	light0RedPosition[0] = 0.0f;
	pLight = light0RedPosition;
	constantBuffer.LightPosition[0] = XMVectorSet(pLight[0], pLight[1], pLight[2], pLight[3]);;

	
	constantBuffer.Ka = XMVectorSet(materialAmbient[0], materialAmbient[1], materialAmbient[2], materialAmbient[3]);
	constantBuffer.Kd = XMVectorSet(materialDiffuse[0], materialDiffuse[1], materialDiffuse[2], materialDiffuse[3]);
	constantBuffer.Ks = XMVectorSet(materialSpecular[0], materialSpecular[1], materialSpecular[2], materialSpecular[3]);
	constantBuffer.MaterialShininess = materialShininess;

	constantBuffer.WorldViewMatrix = worldViewMatrix;
	constantBuffer.ProjectionMatrix = gPerspectiveProjectionMatix;
	gpID3D11DeviceContext->UpdateSubresource(gpID3D11BufferInterfaceConstantBuffer, 0, NULL, &constantBuffer, 0, 0);

	//05-Draw index buffer to render target
	gpID3D11DeviceContext->DrawIndexed(gNumElements, 0, 0);


	//switch between front and back buffers
	gpIDXGISwapChain->Present(0, 0);			//Analogous to SwapBuffers() in OpenGL on Windows
}

void spin(void)
{
	gAngle += 0.001f;
	if (gAngle >= 360.0f)
		gAngle = 0.0f;
}

void uninitialize(void)
{
	//code
	//Release resources
	if (gpID3D11InterfaceRasterizerState)
	{
		gpID3D11InterfaceRasterizerState->Release();
		gpID3D11InterfaceRasterizerState = NULL;
	}
	if (gpID3D11BufferInterfaceConstantBuffer)
	{
		gpID3D11BufferInterfaceConstantBuffer->Release();
		gpID3D11BufferInterfaceConstantBuffer = NULL;
	}

	if (gpID3D11BufferInterfacePositionBuffer)
	{
		gpID3D11BufferInterfacePositionBuffer->Release();
		gpID3D11BufferInterfacePositionBuffer = NULL;
	}
	if (gpID3D11BufferInterfaceNormalBuffer)
	{
		gpID3D11BufferInterfaceNormalBuffer->Release();
		gpID3D11BufferInterfaceNormalBuffer = NULL;
	}
	if (gpID3D11BufferInterfaceIndexBuffer)
	{
		gpID3D11BufferInterfaceIndexBuffer->Release();
		gpID3D11BufferInterfaceIndexBuffer = NULL;
	}

	if (gpID3D11InterfaceInputLayoutPV)
	{
		gpID3D11InterfaceInputLayoutPV->Release();
		gpID3D11InterfaceInputLayoutPV = NULL;
	}
	if (gpID3D11InterfaceInputLayoutPP)
	{
		gpID3D11InterfaceInputLayoutPP->Release();
		gpID3D11InterfaceInputLayoutPP = NULL;
	}

	if (gpID3D11PixelShaderPV)
	{
		gpID3D11PixelShaderPV->Release();
		gpID3D11PixelShaderPV = NULL;
	}
	if (gpID3D11VertexShaderPV)
	{
		gpID3D11VertexShaderPV->Release();
		gpID3D11VertexShaderPV = NULL;
	}

	if (gpID3D11DepthStencilView)
	{
		gpID3D11DepthStencilView->Release();
		gpID3D11DepthStencilView = NULL;
	}
	if (gpID3D11RenderTargetView)
	{
		gpID3D11RenderTargetView->Release();
		gpID3D11RenderTargetView = NULL;
	}
	if (gpIDXGISwapChain)
	{
		gpIDXGISwapChain->Release();
		gpIDXGISwapChain = NULL;
	}
	if (gpID3D11DeviceContext)
	{
		gpID3D11DeviceContext->Release();
		gpID3D11DeviceContext = NULL;
	}
	if (gpID3D11Device)
	{
		gpID3D11Device->Release();
		gpID3D11Device = NULL;
	}

	if (gbFullscreen == true)
	{
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED);
		ShowCursor(TRUE);
	}


	if (gpLogFile)
	{
		fopen_s(&gpLogFile, gszLogFileName, "a+");
		fprintf_s(gpLogFile, "uninitialize() Succeeded\n");
		fprintf_s(gpLogFile, "Log File is successfully closed\n");
		fclose(gpLogFile);
		gpLogFile = NULL;
	}
}/*void uninitialize(void)*/

