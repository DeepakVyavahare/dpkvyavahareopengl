#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <memory.h>
#include <math.h>	//sin() and cos()

#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/XKBlib.h>
#include <X11/keysym.h>

#include <GL/glew.h> 	
#include <GL/gl.h>
#include <GL/glx.h>	

#include "vmath.h"
#include "Sphere.h"
#include "MaterialCoordinatesArray.h"

using namespace vmath;

#define WIN_WIDTH	800
#define WIN_HEIGHT	600

//24 Sphere drawing margins and gap
#define LEFT_MARGIN     -5.0f
#define RIGHT_MARGIN    3.0f
#define HOR_GAP         3.2f
#define VER_GAP         1.2f
#define DEEPNESS        -10.0f

//global variable
Display 	*gpDisplay = NULL;
Colormap 	gColormap;
XVisualInfo	*gpXVisualInfo = NULL;
Window		gWindow;

GLXFBConfig	gGLXFBConfig;
GLXContext	gGLXContext;	// paralle to HGLRC
// extension API typedef since this API prototype or signature is not included in standard gl.h/glx.h include/header file
typedef	GLXContext (*glXCreateContextAttribsARBProc) (Display*, GLXFBConfig, GLXContext, Bool, const int*);
glXCreateContextAttribsARBProc	glXCreateContextAttribsARB = NULL;

bool gbFullscreen = false;
FILE *gpFile = NULL;

//libSphere.so for sphere draw
GLuint gNumElements;
GLuint gNumVertices;
float sphere_vertices[1146];
float sphere_normals[1146];
float sphere_textures[764];
unsigned short sphere_elements[2280];


//Shader Related
GLuint gVertexShaderObject;
GLuint gFragmentShaderObject;
GLuint gShaderProgramObject;

GLint gMUniform;
GLint gVUniform;
GLint gPUniform;
GLint gLKeyPressedUniform;

GLint gLaUniform;
GLint gLdUniform;
GLint gLsUniform;
GLint gKaUniform;
GLint gKdUniform;
GLint gKsUniform;
GLint gLightPositionUniform;
GLint gMaterialShininessUniform;

GLuint gVao_sphere;
GLuint gVbo_position;
GLuint gVbo_normal;
GLuint gVbo_element;

bool gbAnimate = true;
bool gbLight = true;
bool globalXPress = true;
bool globalYPress = false;
bool globalZPress = false;
GLfloat gAngleSphere = 0.0f;

GLfloat lightPosition[] = {0.0, 0.0, 0.0f, 0.0f};
GLfloat lightAmbient[] = {0.0f, 0.0f, 0.0f, 1.0f};
GLfloat lightDiffuse[] = {1.0f, 1.0f, 1.0f, 1.0f};
GLfloat lightSpecular[] = {1.0f, 1.0f, 1.0f, 1.0f};

GLfloat materialAmbient[] = {0.0f, 0.0f, 0.0f, 1.0f};
GLfloat materialDiffuse[] = {0.5f, 0.5f, 0.5f, 1.0f};
GLfloat materialSpecular[] = {1.0f, 1.0f, 1.0f, 1.0f};
GLfloat materialShininess = 50.0f;

mat4 gPerspectiveProjectionMatrix;

//RAM to VRAM binding indices
enum {
	dpk__ATTRIBUTE_POSITION=0,
	dpk__ATTRIBUTE_COLOR,
	dpk__ATTRIBUTE_NORMAL,
	dpk__ATTRIBUTE_TEXTURE0,
};

int main (void)
{
	//function declarations
	void CreateWindow (void);
	void uninitialize (void);
	void initialize (void);
	void ToggleFullscreen (void);
	void resize (int, int);
	void display (void);
	void update (void);

	//variable declarations
	bool 	bDone = false;
	char 	inputCharacter[26];
	KeySym 	keySym;
	int winWidth=WIN_WIDTH;
	int winHeight=WIN_HEIGHT;
	XEvent event;

	//code
	gpFile = fopen ("log.txt", "w");
	if (gpFile == NULL)
	{
		fprintf (stderr, "Log file creation failed...\n");
		exit (1);
	}
	else
	{
		fprintf (gpFile, "Log file created sucessfully\n");
	}

	CreateWindow ();
	initialize ();
	
	while (!bDone)
	{
		while (XPending (gpDisplay))
		{
			XNextEvent (gpDisplay, &event);	//blocking call
			switch (event.type)	// parellel iMsg
			{
				case MapNotify :	// parallel to wM_CREATE
					break;
				case KeyPress :		// parallel to WM_KEYDOWN
					keySym = XkbKeycodeToKeysym (gpDisplay, event.xkey.keycode, 0, 0);
					switch (keySym)
					{
						case XK_Escape:
							bDone = true;
							break;
						default:
							break;
					}
					XLookupString (&event.xkey, inputCharacter, sizeof (inputCharacter), NULL, NULL);
					switch (inputCharacter[0])
					{
						case 'A':
						case 'a':
							if (gbAnimate == true)
								gbAnimate = false;
							else
								gbAnimate = true;
							break;

						case 'L':
						case 'l':
							if (gbLight == true)
								gbLight = false;
							else
								gbLight = true;
							break;
						case 'F':
						case 'f': 
						if (gbFullscreen == true)
							{
								ToggleFullscreen ();
								gbFullscreen = false;
							}
							else
							{
								ToggleFullscreen ();
								gbFullscreen = true;
							}
							break;
						case 'X':
						case 'x':
							globalXPress = true;
							globalYPress = false;
							globalZPress = false;
							break;
						case 'Y':
						case 'y':
							globalXPress = false;
							globalYPress = true;
							globalZPress = false;
							break;
						case 'Z':
						case 'z':
							globalXPress = false;
							globalYPress = false;
							globalZPress = true;
							break;


					}
					break;
			
				case ButtonPress:	// mouse events
					switch (event.xbutton.button)
					{
						case 1:	// LBUTTONDOWN
							break;
						case 2: // MBUTTONDOWN
							break;
						case 3: // RBUTTONDOWN
							break;
						case 4: // WHEEL UP
							break;
						case 5: // WHEEL DOWN
							break;
						default:
							break;
					}
					break; 
				case MotionNotify : //WM_MOVE or mouse move
					break;
				case ConfigureNotify: // WM_SIZE
					winWidth = event.xconfigure.width;
					winHeight = event.xconfigure.height;
					resize (winWidth, winHeight);
					break;
				case Expose:
					break;
				case DestroyNotify:
					break;
				case 33:
					bDone=true;
					break;
				default:
					break;
			}
}
		display ();
		update ();
	
	}

	uninitialize ();
	return (0);
}

void CreateWindow (void)
{
	//function declarations
	void uninitialize (void);

	//variable declarations
	XSetWindowAttributes winAttribs;
	GLXFBConfig* pGLXFBConfigs = NULL;
	GLXFBConfig bestGLXFBConfig;
	XVisualInfo* pTempXVisualInfo = NULL;
	int iNumFBConfigs = 0;
	int styleMask;

	static int frameBufferAttributes [] = {
		GLX_X_RENDERABLE, 	True,
		GLX_DRAWABLE_TYPE, 	GLX_WINDOW_BIT,
		GLX_RENDER_TYPE, 	GLX_RGBA_BIT,
		GLX_X_VISUAL_TYPE, 	GLX_TRUE_COLOR,
		GLX_RED_SIZE, 		8,
		GLX_GREEN_SIZE,		8,
		GLX_BLUE_SIZE,		8,
		GLX_ALPHA_SIZE,		8,
		GLX_DEPTH_SIZE,		24,
		GLX_STENCIL_SIZE,	8,
		GLX_DOUBLEBUFFER, 	True,
		GLX_SAMPLE_BUFFERS,	1,
		GLX_SAMPLES,		4,
		None				
	};

	gpDisplay = XOpenDisplay (NULL);
	if (gpDisplay == NULL)
	{
		fprintf (stderr, "ERROR: Unable to open X Display. \nExiting Now...\n");
		uninitialize ();
		exit (1);
	}

	// get a new framebuffer config that meets out attrib requirement
	pGLXFBConfigs = glXChooseFBConfig (gpDisplay, XDefaultScreen (gpDisplay), frameBufferAttributes, &iNumFBConfigs);
	if (pGLXFBConfigs == NULL)
	{
		fprintf (stderr, "ERROR: Failed to valid Framebuffer configs. Exiting Now..\n");
		uninitialize ();
		exit (1);
	}	
	fprintf (gpFile, "%d Matching FB configs found.\n", iNumFBConfigs);

	//pick up that FB config/visual with the most samples per pixel
	int bestFramebufferConfig = -1, worstFramebufferConfig = -1, bestNumberOfSamples = -1, worstNumberOfSamples = 999;

	for (int i=0; i<iNumFBConfigs; i++)
	{
		pTempXVisualInfo = glXGetVisualFromFBConfig (gpDisplay, pGLXFBConfigs[i]);
		if (pTempXVisualInfo)
		{
			int sampleBuffer, samples;
			glXGetFBConfigAttrib (gpDisplay, pGLXFBConfigs[i], GLX_SAMPLE_BUFFERS, &sampleBuffer);
			glXGetFBConfigAttrib (gpDisplay, pGLXFBConfigs[i], GLX_SAMPLES, &samples);
			fprintf (gpFile, "Matching Framebuffer Config=%d : Visual ID=%lu : SAMPLE_BUFFERS=%d : SAMPLES=%d\n", \
						i, pTempXVisualInfo->visualid, sampleBuffer, samples);
			
			if (bestFramebufferConfig < 0 || sampleBuffer && samples > bestNumberOfSamples)
			{
				bestFramebufferConfig = i;
				bestNumberOfSamples = samples;
			}
			if (worstFramebufferConfig < 0 || !sampleBuffer || samples < worstNumberOfSamples)
			{
				worstFramebufferConfig = i;
				worstNumberOfSamples = samples;
			}
		}
		XFree (pTempXVisualInfo);
	}
	bestGLXFBConfig = pGLXFBConfigs [bestFramebufferConfig];
	//set global GLXFBConfig
	gGLXFBConfig = bestGLXFBConfig;
	 
	//be sure to free FBConfig list allocated by glXChooseFBConfig ()
	XFree (pGLXFBConfigs);

	gpXVisualInfo = glXGetVisualFromFBConfig (gpDisplay, bestGLXFBConfig);
	fprintf (gpFile, "Chosen Visual ID=0x%lx\n", gpXVisualInfo->visualid);

	//setting window's attributes 
	winAttribs.border_pixel = 0;
	winAttribs.background_pixmap = 0;
	winAttribs.colormap = XCreateColormap (gpDisplay, 
						RootWindow (gpDisplay, gpXVisualInfo->screen), 	// you can defaultscreen as well
						gpXVisualInfo->visual,
						AllocNone);	// for movable 	memory allocation

	winAttribs.event_mask = StructureNotifyMask | KeyPressMask | ButtonPressMask | 
				ExposureMask | VisibilityChangeMask | PointerMotionMask;
	styleMask = CWBorderPixel | CWEventMask | CWColormap;
	
	gWindow = XCreateWindow (gpDisplay, 
				RootWindow (gpDisplay, gpXVisualInfo->screen),
				100,
				100,
				WIN_WIDTH,
				WIN_HEIGHT,
				0,		//border width
				gpXVisualInfo->depth,	//depth of visual (depth of colormap)
				InputOutput,		//class (type)
				gpXVisualInfo->visual,
				styleMask,
				&winAttribs);

	if (!gWindow)
	{
		fprintf (stderr, "ERROR: Failure in window creation\n");
		uninitialize ();
		exit (1);
	}
	XStoreName (gpDisplay, gWindow, "xwindows- 24 Materials");
	
	Atom windowManagerDelete = XInternAtom (gpDisplay, "WM_WINDOW_DELETE", True);
	XSetWMProtocols (gpDisplay, gWindow, &windowManagerDelete, 1);

	XMapWindow (gpDisplay, gWindow);
}

void ToggleFullscreen (void)
{
	//variable declarations
	Atom wm_state;
	Atom fullscreen;
	XEvent xev = {0};

	//code 
	wm_state = XInternAtom (gpDisplay, "_NET_WM_STATE", False);
	memset (&xev, 0, sizeof (xev));
	
	xev.type = ClientMessage;
	xev.xclient.window = gWindow;
	xev.xclient.message_type = wm_state;
	xev.xclient.format = 32;
	xev.xclient.data.l[0] = gbFullscreen ? 0:1;

	fullscreen = XInternAtom (gpDisplay, "_NET_WM_STATE_FULLSCREEN", False);
	xev.xclient.data.l[1] = fullscreen;

	XSendEvent (gpDisplay, 
			RootWindow(gpDisplay, gpXVisualInfo->screen), 
			False, 
			StructureNotifyMask, 
			&xev);
}


void initialize (void)
{
	//function declarations
	void uninitialize (void);
	void resize (int, int);
	
	//code 
	/*Create a new GL context 3.0 for rendering*/
	glXCreateContextAttribsARB = (glXCreateContextAttribsARBProc) glXGetProcAddressARB (
								(GLubyte*) "glXCreateContextAttribsARB"); // parallel to GetProcAddress ()
	GLint attribs [] = {
		GLX_CONTEXT_MAJOR_VERSION_ARB,	3,	
		GLX_CONTEXT_MINOR_VERSION_ARB, 	0,	
		GLX_CONTEXT_PROFILE_MASK_ARB,	GLX_CONTEXT_COMPATIBILITY_PROFILE_BIT_ARB, 
		0	
	};

	gGLXContext = glXCreateContextAttribsARB (gpDisplay, gGLXFBConfig, 0, True, attribs);
	if (!gGLXContext)
	{
GLint attribs [] = {
			GLX_CONTEXT_MAJOR_VERSION_ARB,	1,
			GLX_CONTEXT_MINOR_VERSION_ARB,  0,
			0	// Array must be terminated by null
		};
		fprintf (stderr, "ERROR: Failed to create GLX 4.5 context. Hence using old-style GLX context\n");
		gGLXContext = glXCreateContextAttribsARB (gpDisplay, gGLXFBConfig, 0, True, attribs);
	}
	else
	{// successfuly created 3.0 context
		fprintf (gpFile, "OpenGL Context 3.0 created\n");
	}

	// verify context is direct or indirect
	if (!glXIsDirect (gpDisplay,  gGLXContext))
	{
		fprintf (gpFile, "Indirect GLX Rendering Context Obtained\n");
	}	
	else
	{
		fprintf (gpFile, "Direct GLX Rendering Context Obtained\n\n");
	}

	glXMakeCurrent (gpDisplay, gWindow, gGLXContext);

	GLenum glew_error = glewInit();
	if (glew_error != GLEW_OK)
	{
		fprintf (gpFile, "glewInit():failed\nExiting...\n");
		uninitialize();
		exit (1);	
	}
	fprintf (gpFile, "OpenGL Version: %s\n", glGetString (GL_VERSION));
	fprintf (gpFile, "GLSL Version: %s\n", glGetString (GL_SHADING_LANGUAGE_VERSION));

	/************VERTEX SHADER**********/
	gVertexShaderObject  = glCreateShader (GL_VERTEX_SHADER);
	
	const char* vertexShaderSourceCode = 
			"#version 130" \
			"\n" \
			"in vec4 vPosition;" \
			"in vec3 vNormal;" \
			"uniform mat4 u_model_matrix;" \
			"uniform mat4 u_view_matrix;" \
			"uniform mat4 u_projection_matrix;" \
			"uniform int u_LKeyPressed;" \
			"uniform vec4 u_Light_Position;" \
			"out vec3 oTransformed_normals;" \
			"out vec3 oLight_direction;" \
			"out vec3 oViewer_vector;" \
			"void main(void)" \
			"{" \
			"if (u_LKeyPressed == 1)" \
			"{" \
				"vec4 eyeCoordinates = u_view_matrix * u_model_matrix * vPosition;" \
				"oTransformed_normals = mat3(u_view_matrix * u_model_matrix) * vNormal;" \
				"oLight_direction = vec3(u_Light_Position) - eyeCoordinates.xyz;" \
				"oViewer_vector = -eyeCoordinates.xyz;" \
			"}" \
			"gl_Position = u_projection_matrix * u_view_matrix * u_model_matrix* vPosition;" \
			"}"; 
		
	glShaderSource (gVertexShaderObject, // Handle to shader object
			1, 		     // how many elemnets in next arg i.e. string
			(const GLchar**)&vertexShaderSourceCode,	// array pointer to source code string
			NULL);

	glCompileShader (gVertexShaderObject);

	GLint iShaderCompileStatus = 0;
	glGetShaderiv (gVertexShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);
	if (iShaderCompileStatus == GL_FALSE)
	{
		GLint iInfoLogLength = 0;
		glGetShaderiv (gVertexShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			char *szInfoLog = (char*) malloc (iInfoLogLength);
			if (szInfoLog)
			{
				GLsizei written;
				glGetShaderInfoLog (gVertexShaderObject, iInfoLogLength, &written, (GLchar*)szInfoLog);
				fprintf (gpFile, "Vertex Shader Compile Status Log: %s\n", szInfoLog);
				free (szInfoLog);
				uninitialize ();
				exit (1);
			}
		}
	}
	
	/********************FRAGMENT SHADER**********************/
	gFragmentShaderObject = glCreateShader (GL_FRAGMENT_SHADER);

	const char *fragmentShaderSourceCode = 
	"#version 130" \
	"\n" \
	"in vec3 phong_ads_color;" \
	"in vec3 oTransformed_normals;" \
	"in vec3 oLight_direction;" \
	"in vec3 oViewer_vector;" \
	"uniform vec3 u_La;" \
	"uniform vec3 u_Ld;" \
	"uniform vec3 u_Ls;" \
	"uniform vec3 u_Ka;" \
	"uniform vec3 u_Kd;" \
	"uniform vec3 u_Ks;" \
	"uniform float u_material_shininess;" \
	"out vec4 FragColor;" \
	"uniform int u_LKeyPressed;" \
	"void main (void)" \
	"{" \
		"FragColor = vec4(1.0, 1.0, 1.0, 1.0);" \
		"if (u_LKeyPressed == 1)" \
		"{" \
			"vec3 normalized_transformed_normals = normalize(oTransformed_normals);" \
			"vec3 normalized_light_direction = normalize(oLight_direction);" \
			"vec3 normalized_viewwer_vector = normalize(oViewer_vector);" \
			
			"vec3 ambient = u_La * u_Ka;" \
			"float ld_dot_tn = max(dot(normalized_light_direction, normalized_transformed_normals), 0.0);" \
			"vec3 diffuse = u_Ld * u_Kd * ld_dot_tn;" \
			"vec3 reflection_vector = reflect(-normalized_light_direction, normalized_transformed_normals);" \
			"float r_dot_v = max(dot(reflection_vector, normalized_viewwer_vector), 0.0);" \
			"vec3 specular = u_Ls * u_Ks * pow(r_dot_v, u_material_shininess);" \
			"vec3 ads_color = ambient + diffuse + specular;" \
			"FragColor = vec4(ads_color, 1.0);" \
		"}" \
	"}";

	glShaderSource (gFragmentShaderObject, //Shader object
			1, 	//no. of elements in source code string
			(const GLchar**)&fragmentShaderSourceCode, // source code string
			NULL);	
	glCompileShader (gFragmentShaderObject);
	
	iShaderCompileStatus = 0;
	glGetShaderiv (gFragmentShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);
	if (GL_FALSE == iShaderCompileStatus)
	{
		GLint iInfoLogLength = 0;
		glGetShaderiv (gFragmentShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			char *szInfoLog = (char*)malloc (iInfoLogLength);
			if (szInfoLog)
			{
				GLsizei written;
				glGetShaderInfoLog (gFragmentShaderObject, iInfoLogLength, &written, (GLchar*)szInfoLog);
				fprintf (gpFile, "Fragment Shader Compile Log: %s\n", szInfoLog);
				free (szInfoLog);
				uninitialize();
				exit (1);
			}
		}
	}

	/****************SHADER PROGRAM****************/
	gShaderProgramObject = glCreateProgram();

	glAttachShader (gShaderProgramObject, gVertexShaderObject);
	glAttachShader (gShaderProgramObject, gFragmentShaderObject);

	glBindAttribLocation (gShaderProgramObject, dpk__ATTRIBUTE_POSITION, "vPosition");
	glBindAttribLocation (gShaderProgramObject, dpk__ATTRIBUTE_NORMAL, "vNormal");

	glLinkProgram (gShaderProgramObject);

	GLint iProgramLinkStatus = 0;
	glGetProgramiv (gShaderProgramObject, GL_LINK_STATUS, &iProgramLinkStatus);
	if (GL_FALSE == iProgramLinkStatus)
	{
		GLint iInfoLogLength = 0;
		glGetProgramiv (gShaderProgramObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			char *szInfoLog = (char*) malloc (iInfoLogLength);
			if (szInfoLog)
			{
				GLsizei written;
				glGetProgramInfoLog (gShaderProgramObject, iInfoLogLength, &written, (GLchar*)szInfoLog);
				fprintf (gpFile, "Program Link Status Log: %s\n", szInfoLog);
				free (szInfoLog);
				uninitialize();
				exit (1);
			}
		}
	}

	gMUniform = glGetUniformLocation (gShaderProgramObject, "u_model_matrix");
	gVUniform = glGetUniformLocation (gShaderProgramObject, "u_view_matrix");
	gPUniform = glGetUniformLocation (gShaderProgramObject, "u_projection_matrix");

	gLKeyPressedUniform = glGetUniformLocation (gShaderProgramObject, "u_LKeyPressed");

	gKaUniform = glGetUniformLocation (gShaderProgramObject, "u_Ka");
	gKdUniform = glGetUniformLocation (gShaderProgramObject, "u_Kd");
	gKsUniform = glGetUniformLocation (gShaderProgramObject, "u_Ks");

	gLaUniform = glGetUniformLocation (gShaderProgramObject, "u_La");
	gLdUniform = glGetUniformLocation (gShaderProgramObject, "u_Ld");
	gLsUniform = glGetUniformLocation (gShaderProgramObject, "u_Ls");

	gLightPositionUniform = glGetUniformLocation (gShaderProgramObject, "u_Light_Position");

	gMaterialShininessUniform = glGetUniformLocation (gShaderProgramObject, "u_material_shininess");

	/*-----------Vertices, Color, Shader Attribs, VAO, VBO Initialization--------------*/

	getSphereVertexData(sphere_vertices, sphere_normals, sphere_textures, sphere_elements);
        gNumVertices = getNumberOfSphereVertices();
        gNumElements = getNumberOfSphereElements();
	
	
	glGenVertexArrays (1, &gVao_sphere);
	glBindVertexArray (gVao_sphere);

	glGenBuffers (1, &gVbo_position);
	glBindBuffer (GL_ARRAY_BUFFER, gVbo_position);
	glBufferData (GL_ARRAY_BUFFER, sizeof(sphere_vertices), sphere_vertices, GL_STATIC_DRAW);
	glVertexAttribPointer (dpk__ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray (dpk__ATTRIBUTE_POSITION);
	glBindBuffer (GL_ARRAY_BUFFER, 0);

	glGenBuffers (1, &gVbo_normal);
	glBindBuffer (GL_ARRAY_BUFFER, gVbo_normal);
	glBufferData (GL_ARRAY_BUFFER, sizeof(sphere_normals), sphere_normals, GL_STATIC_DRAW);
	glVertexAttribPointer (dpk__ATTRIBUTE_NORMAL, 3, GL_FLOAT, GL_FALSE, 0, 0);
	glEnableVertexAttribArray (dpk__ATTRIBUTE_NORMAL);
	glBindBuffer (GL_ARRAY_BUFFER, 0);

	glGenBuffers (1, &gVbo_element);
	glBindBuffer (GL_ELEMENT_ARRAY_BUFFER, gVbo_element);
	glBufferData (GL_ELEMENT_ARRAY_BUFFER, sizeof(sphere_elements), sphere_elements, GL_STATIC_DRAW);
	glBindBuffer (GL_ARRAY_BUFFER, 0);	//unbind element buffer

	glBindVertexArray (0); 

	//code
	glShadeModel 	(GL_SMOOTH);
	glClearDepth 	(1.0f);		// setup depth buffer
	glEnable 	(GL_DEPTH_TEST);	//enable depth test
	glDepthFunc 	(GL_LEQUAL);	// depth test to do
	glHint 		(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);	// Set really nice perspective calculations
	//glEnable 	(GL_CULL_FACE);	// we will always cull back faces for better performance
	
	glClearColor 	(0.25f, 0.25f, 0.25f, 0.0f);	// set background clearing color 

	//Set Perspective projection matrix to identity matrix
	gPerspectiveProjectionMatrix = mat4::identity ();

	resize 		(WIN_WIDTH, WIN_HEIGHT);	// resize window
}

void resize (int width, int height)
{
	if (height == 0)
		height = 1;
	if (width == 0)
		width = 1;

	glViewport (0, 0, (GLsizei)width, (GLsizei)height);
	gPerspectiveProjectionMatrix = perspective (45.0f, ((float)width/(float)height), 0.1f, 1000.0f);
}

void update (void)
{
}

void display (void)
{
	// code 
	glClear (GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	glUseProgram (gShaderProgramObject);
	if (gbLight == true)
	{
		glUniform1i (gLKeyPressedUniform, 1);		//signal shader that user wants to switch to per fragment lighting

		if (globalXPress == true)
		{
			//Light moving around X-Axis
			lightPosition[0] = 0.0f;
			lightPosition[1] = 1000.0f * cos(gAngleSphere);
			lightPosition[2] = 1000.0f * sin(gAngleSphere);
			glUniform4fv (gLightPositionUniform, 1, (GLfloat*)lightPosition);
		}
		else if (globalYPress == true)
		{
			lightPosition[0] = 9.0f * cos(gAngleSphere);
			lightPosition[2] = 9.0f * sin(gAngleSphere);
			glUniform4fv (gLightPositionUniform, 1, (GLfloat*)lightPosition);

		}
		else
		{
			lightPosition[0] = 9.0f * cos(gAngleSphere);
			lightPosition[1] = 9.0f * sin(gAngleSphere);
			glUniform4fv (gLightPositionUniform, 1, (GLfloat*)lightPosition);
		}

		glUniform3fv (gLaUniform, 1, lightAmbient);
		glUniform3fv (gLdUniform, 1, lightDiffuse);
		glUniform3fv (gLsUniform, 1, lightSpecular);
	}
	else
	{
		glUniform1i (gLKeyPressedUniform, 0);
	}
	
	
if (gAngleSphere < 2 * 3.1415f )
{
gAngleSphere=gAngleSphere+0.06f;		
		mat4 modelMatrix = mat4::identity ();	
			mat4 viewMatrix = mat4::identity();        
			//mat4 rotationMatrix = mat4::identity();

			modelMatrix = translate(1.0f,-3.0f,-10.0f);


			glUniformMatrix4fv (gMUniform, 1, GL_FALSE, modelMatrix);
			glUniformMatrix4fv (gVUniform, 1, GL_FALSE, viewMatrix);

			glUniformMatrix4fv (gPUniform, 1, GL_FALSE, gPerspectiveProjectionMatrix);

			glUniform3fv(gKaUniform, 1, material_ambient[0][0]);
			glUniform3fv(gKdUniform, 1, material_diffuse[0][0]);
			glUniform3fv(gKsUniform, 1, material_specular[0][0]);
			glUniform1f(gMaterialShininessUniform, material_shininess[0][0]);

			glBindVertexArray (gVao_sphere);

			glBindBuffer (GL_ELEMENT_ARRAY_BUFFER, gVbo_element);
			glDrawElements (GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);
			glBindBuffer (GL_ELEMENT_ARRAY_BUFFER, 0);	//unbind
			glBindVertexArray (0);
	
//2
			 modelMatrix = mat4::identity ();	//M
			 viewMatrix = mat4::identity();        //MV
			//mat4 rotationMatrix = mat4::identity();

			modelMatrix = translate(-1.0f,-3.0f,-10.0f);


			glUniformMatrix4fv (gMUniform, 1, GL_FALSE, modelMatrix);
			glUniformMatrix4fv (gVUniform, 1, GL_FALSE, viewMatrix);

			glUniformMatrix4fv (gPUniform, 1, GL_FALSE, gPerspectiveProjectionMatrix);

			glUniform3fv(gKaUniform, 1, material_ambient[0][1]);
			glUniform3fv(gKdUniform, 1, material_diffuse[0][1]);
			glUniform3fv(gKsUniform, 1, material_specular[0][1]);
			glUniform1f(gMaterialShininessUniform, material_shininess[0][1]);

			glBindVertexArray (gVao_sphere);

			glBindBuffer (GL_ELEMENT_ARRAY_BUFFER, gVbo_element);
			glDrawElements (GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);
			glBindBuffer (GL_ELEMENT_ARRAY_BUFFER, 0);	//unbind
			glBindVertexArray (0);
	
//3

			 modelMatrix = mat4::identity ();	//M
			 viewMatrix = mat4::identity();        //MV
			//mat4 rotationMatrix = mat4::identity();

			modelMatrix = translate(-3.0f,-3.0f,-10.0f);


			glUniformMatrix4fv (gMUniform, 1, GL_FALSE, modelMatrix);
			glUniformMatrix4fv (gVUniform, 1, GL_FALSE, viewMatrix);

			glUniformMatrix4fv (gPUniform, 1, GL_FALSE, gPerspectiveProjectionMatrix);

			glUniform3fv(gKaUniform, 1, material_ambient[0][2]);
			glUniform3fv(gKdUniform, 1, material_diffuse[0][2]);
			glUniform3fv(gKsUniform, 1, material_specular[0][2]);
			glUniform1f(gMaterialShininessUniform, material_shininess[0][2]);

			glBindVertexArray (gVao_sphere);

			glBindBuffer (GL_ELEMENT_ARRAY_BUFFER, gVbo_element);
			glDrawElements (GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);
			glBindBuffer (GL_ELEMENT_ARRAY_BUFFER, 0);	//unbind
			glBindVertexArray (0);

//4

			 modelMatrix = mat4::identity ();	//M
			 viewMatrix = mat4::identity();        //MV
			//mat4 rotationMatrix = mat4::identity();

			modelMatrix = translate(3.0f,-3.0f,-10.0f);


			glUniformMatrix4fv (gMUniform, 1, GL_FALSE, modelMatrix);
			glUniformMatrix4fv (gVUniform, 1, GL_FALSE, viewMatrix);

			glUniformMatrix4fv (gPUniform, 1, GL_FALSE, gPerspectiveProjectionMatrix);

			glUniform3fv(gKaUniform, 1, material_ambient[0][3]);
			glUniform3fv(gKdUniform, 1, material_diffuse[0][3]);
			glUniform3fv(gKsUniform, 1, material_specular[0][3]);
			glUniform1f(gMaterialShininessUniform, material_shininess[0][3]);

			glBindVertexArray (gVao_sphere);

			glBindBuffer (GL_ELEMENT_ARRAY_BUFFER, gVbo_element);
			glDrawElements (GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);
			glBindBuffer (GL_ELEMENT_ARRAY_BUFFER, 0);	//unbind
			glBindVertexArray (0);


//5
 modelMatrix = mat4::identity ();	//M
			 viewMatrix = mat4::identity();        //MV
			//mat4 rotationMatrix = mat4::identity();

			modelMatrix = translate(5.0f,-3.0f,-10.0f);


			glUniformMatrix4fv (gMUniform, 1, GL_FALSE, modelMatrix);
			glUniformMatrix4fv (gVUniform, 1, GL_FALSE, viewMatrix);

			glUniformMatrix4fv (gPUniform, 1, GL_FALSE, gPerspectiveProjectionMatrix);

			glUniform3fv(gKaUniform, 1, material_ambient[0][5]);
			glUniform3fv(gKdUniform, 1, material_diffuse[0][5]);
			glUniform3fv(gKsUniform, 1, material_specular[0][5]);
			glUniform1f(gMaterialShininessUniform, material_shininess[0][5]);

			glBindVertexArray (gVao_sphere);

			glBindBuffer (GL_ELEMENT_ARRAY_BUFFER, gVbo_element);
			glDrawElements (GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);
			glBindBuffer (GL_ELEMENT_ARRAY_BUFFER, 0);	//unbind

//6
 modelMatrix = mat4::identity ();	//M
			 viewMatrix = mat4::identity();        //MV
			//mat4 rotationMatrix = mat4::identity();

			modelMatrix = translate(-5.0f,-3.0f,-10.0f);


			glUniformMatrix4fv (gMUniform, 1, GL_FALSE, modelMatrix);
			glUniformMatrix4fv (gVUniform, 1, GL_FALSE, viewMatrix);

			glUniformMatrix4fv (gPUniform, 1, GL_FALSE, gPerspectiveProjectionMatrix);

			glUniform3fv(gKaUniform, 1, material_ambient[0][5]);
			glUniform3fv(gKdUniform, 1, material_diffuse[0][5]);
			glUniform3fv(gKsUniform, 1, material_specular[0][5]);
			glUniform1f(gMaterialShininessUniform, material_shininess[0][5]);

			glBindVertexArray (gVao_sphere);

			glBindBuffer (GL_ELEMENT_ARRAY_BUFFER, gVbo_element);
			glDrawElements (GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);
			glBindBuffer (GL_ELEMENT_ARRAY_BUFFER, 0);	//unbind
//6th -last row complete
//=========================================================
//5th

			 modelMatrix = mat4::identity ();	//M
			 viewMatrix = mat4::identity();        //MV
			//mat4 rotationMatrix = mat4::identity();

			modelMatrix = translate(1.0f,1.0f,-10.0f);


			glUniformMatrix4fv (gMUniform, 1, GL_FALSE, modelMatrix);
			glUniformMatrix4fv (gVUniform, 1, GL_FALSE, viewMatrix);

			glUniformMatrix4fv (gPUniform, 1, GL_FALSE, gPerspectiveProjectionMatrix);

			glUniform3fv(gKaUniform, 1, material_ambient[1][0]);
			glUniform3fv(gKdUniform, 1, material_diffuse[1][0]);
			glUniform3fv(gKsUniform, 1, material_specular[1][0]);
			glUniform1f(gMaterialShininessUniform, material_shininess[1][0]);

			glBindVertexArray (gVao_sphere);

			glBindBuffer (GL_ELEMENT_ARRAY_BUFFER, gVbo_element);
			glDrawElements (GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);
			glBindBuffer (GL_ELEMENT_ARRAY_BUFFER, 0);	//unbind
			glBindVertexArray (0);
	
//2
			 modelMatrix = mat4::identity ();	//M
			 viewMatrix = mat4::identity();        //MV
			//mat4 rotationMatrix = mat4::identity();

			modelMatrix = translate(-1.0f,1.0f,-10.0f);


			glUniformMatrix4fv (gMUniform, 1, GL_FALSE, modelMatrix);
			glUniformMatrix4fv (gVUniform, 1, GL_FALSE, viewMatrix);

			glUniformMatrix4fv (gPUniform, 1, GL_FALSE, gPerspectiveProjectionMatrix);

			glUniform3fv(gKaUniform, 1, material_ambient[1][1]);
			glUniform3fv(gKdUniform, 1, material_diffuse[1][1]);
			glUniform3fv(gKsUniform, 1, material_specular[1][1]);
			glUniform1f(gMaterialShininessUniform, material_shininess[1][1]);

			glBindVertexArray (gVao_sphere);

			glBindBuffer (GL_ELEMENT_ARRAY_BUFFER, gVbo_element);
			glDrawElements (GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);
			glBindBuffer (GL_ELEMENT_ARRAY_BUFFER, 0);	//unbind
			glBindVertexArray (0);
	
//3

			 modelMatrix = mat4::identity ();	//M
			 viewMatrix = mat4::identity();        //MV
			//mat4 rotationMatrix = mat4::identity();

			modelMatrix = translate(-3.0f,1.0f,-10.0f);


			glUniformMatrix4fv (gMUniform, 1, GL_FALSE, modelMatrix);
			glUniformMatrix4fv (gVUniform, 1, GL_FALSE, viewMatrix);

			glUniformMatrix4fv (gPUniform, 1, GL_FALSE, gPerspectiveProjectionMatrix);

			glUniform3fv(gKaUniform, 1, material_ambient[1][2]);
			glUniform3fv(gKdUniform, 1, material_diffuse[1][2]);
			glUniform3fv(gKsUniform, 1, material_specular[1][2]);
			glUniform1f(gMaterialShininessUniform, material_shininess[1][2]);

			glBindVertexArray (gVao_sphere);

			glBindBuffer (GL_ELEMENT_ARRAY_BUFFER, gVbo_element);
			glDrawElements (GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);
			glBindBuffer (GL_ELEMENT_ARRAY_BUFFER, 0);	//unbind
			glBindVertexArray (0);

//4

			 modelMatrix = mat4::identity ();	//M
			 viewMatrix = mat4::identity();        //MV
			//mat4 rotationMatrix = mat4::identity();

			modelMatrix = translate(3.0f,1.0f,-10.0f);


			glUniformMatrix4fv (gMUniform, 1, GL_FALSE, modelMatrix);
			glUniformMatrix4fv (gVUniform, 1, GL_FALSE, viewMatrix);

			glUniformMatrix4fv (gPUniform, 1, GL_FALSE, gPerspectiveProjectionMatrix);

			glUniform3fv(gKaUniform, 1, material_ambient[1][3]);
			glUniform3fv(gKdUniform, 1, material_diffuse[1][3]);
			glUniform3fv(gKsUniform, 1, material_specular[1][3]);
			glUniform1f(gMaterialShininessUniform, material_shininess[1][3]);

			glBindVertexArray (gVao_sphere);

			glBindBuffer (GL_ELEMENT_ARRAY_BUFFER, gVbo_element);
			glDrawElements (GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);
			glBindBuffer (GL_ELEMENT_ARRAY_BUFFER, 0);	//unbind
			glBindVertexArray (0);
//5
 modelMatrix = mat4::identity ();	//M
			 viewMatrix = mat4::identity();        //MV
			//mat4 rotationMatrix = mat4::identity();

			modelMatrix = translate(5.0f,1.0f,-10.0f);


			glUniformMatrix4fv (gMUniform, 1, GL_FALSE, modelMatrix);
			glUniformMatrix4fv (gVUniform, 1, GL_FALSE, viewMatrix);

			glUniformMatrix4fv (gPUniform, 1, GL_FALSE, gPerspectiveProjectionMatrix);

			glUniform3fv(gKaUniform, 1, material_ambient[1][4]);
			glUniform3fv(gKdUniform, 1, material_diffuse[1][4]);
			glUniform3fv(gKsUniform, 1, material_specular[1][4]);
			glUniform1f(gMaterialShininessUniform, material_shininess[1][4]);

			glBindVertexArray (gVao_sphere);

			glBindBuffer (GL_ELEMENT_ARRAY_BUFFER, gVbo_element);
			glDrawElements (GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);
			glBindBuffer (GL_ELEMENT_ARRAY_BUFFER, 0);	//unbind
			glBindVertexArray (0);
//6
 modelMatrix = mat4::identity ();	//M
			 viewMatrix = mat4::identity();        //MV
			//mat4 rotationMatrix = mat4::identity();

			modelMatrix = translate(-5.0f,1.0f,-10.0f);


			glUniformMatrix4fv (gMUniform, 1, GL_FALSE, modelMatrix);
			glUniformMatrix4fv (gVUniform, 1, GL_FALSE, viewMatrix);

			glUniformMatrix4fv (gPUniform, 1, GL_FALSE, gPerspectiveProjectionMatrix);

			glUniform3fv(gKaUniform, 1, material_ambient[1][5]);
			glUniform3fv(gKdUniform, 1, material_diffuse[1][5]);
			glUniform3fv(gKsUniform, 1, material_specular[1][5]);
			glUniform1f(gMaterialShininessUniform, material_shininess[1][5]);

			glBindVertexArray (gVao_sphere);

			glBindBuffer (GL_ELEMENT_ARRAY_BUFFER, gVbo_element);
			glDrawElements (GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);
			glBindBuffer (GL_ELEMENT_ARRAY_BUFFER, 0);	//unbind
			glBindVertexArray (0);
//=========================================================
//4th

			 modelMatrix = mat4::identity ();	//M
			 viewMatrix = mat4::identity();        //MV
			//mat4 rotationMatrix = mat4::identity();

			modelMatrix = translate(1.0f,-1.0f,-10.0f);


			glUniformMatrix4fv (gMUniform, 1, GL_FALSE, modelMatrix);
			glUniformMatrix4fv (gVUniform, 1, GL_FALSE, viewMatrix);

			glUniformMatrix4fv (gPUniform, 1, GL_FALSE, gPerspectiveProjectionMatrix);

			glUniform3fv(gKaUniform, 1, material_ambient[2][0]);
			glUniform3fv(gKdUniform, 1, material_diffuse[2][0]);
			glUniform3fv(gKsUniform, 1, material_specular[2][0]);
			glUniform1f(gMaterialShininessUniform, material_shininess[2][0]);

			glBindVertexArray (gVao_sphere);

			glBindBuffer (GL_ELEMENT_ARRAY_BUFFER, gVbo_element);
			glDrawElements (GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);
			glBindBuffer (GL_ELEMENT_ARRAY_BUFFER, 0);	//unbind
			glBindVertexArray (0);
	
//2
			 modelMatrix = mat4::identity ();	//M
			 viewMatrix = mat4::identity();        //MV
			//mat4 rotationMatrix = mat4::identity();

			modelMatrix = translate(-1.0f,-1.0f,-10.0f);


			glUniformMatrix4fv (gMUniform, 1, GL_FALSE, modelMatrix);
			glUniformMatrix4fv (gVUniform, 1, GL_FALSE, viewMatrix);

			glUniformMatrix4fv (gPUniform, 1, GL_FALSE, gPerspectiveProjectionMatrix);

			glUniform3fv(gKaUniform, 1, material_ambient[2][1]);
			glUniform3fv(gKdUniform, 1, material_diffuse[2][1]);
			glUniform3fv(gKsUniform, 1, material_specular[2][1]);
			glUniform1f(gMaterialShininessUniform, material_shininess[2][1]);

			glBindVertexArray (gVao_sphere);

			glBindBuffer (GL_ELEMENT_ARRAY_BUFFER, gVbo_element);
			glDrawElements (GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);
			glBindBuffer (GL_ELEMENT_ARRAY_BUFFER, 0);	//unbind
			glBindVertexArray (0);
	
//3

			 modelMatrix = mat4::identity ();	//M
			 viewMatrix = mat4::identity();        //MV
			//mat4 rotationMatrix = mat4::identity();

			modelMatrix = translate(-3.0f,-1.0f,-10.0f);


			glUniformMatrix4fv (gMUniform, 1, GL_FALSE, modelMatrix);
			glUniformMatrix4fv (gVUniform, 1, GL_FALSE, viewMatrix);

			glUniformMatrix4fv (gPUniform, 1, GL_FALSE, gPerspectiveProjectionMatrix);

			glUniform3fv(gKaUniform, 1, material_ambient[2][2]);
			glUniform3fv(gKdUniform, 1, material_diffuse[2][2]);
			glUniform3fv(gKsUniform, 1, material_specular[2][2]);
			glUniform1f(gMaterialShininessUniform, material_shininess[2][2]);

			glBindVertexArray (gVao_sphere);

			glBindBuffer (GL_ELEMENT_ARRAY_BUFFER, gVbo_element);
			glDrawElements (GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);
			glBindBuffer (GL_ELEMENT_ARRAY_BUFFER, 0);	//unbind
			glBindVertexArray (0);

//4

			 modelMatrix = mat4::identity ();	//M
			 viewMatrix = mat4::identity();        //MV
			//mat4 rotationMatrix = mat4::identity();

			modelMatrix = translate(3.0f,-1.0f,-10.0f);


			glUniformMatrix4fv (gMUniform, 1, GL_FALSE, modelMatrix);
			glUniformMatrix4fv (gVUniform, 1, GL_FALSE, viewMatrix);

			glUniformMatrix4fv (gPUniform, 1, GL_FALSE, gPerspectiveProjectionMatrix);

			glUniform3fv(gKaUniform, 1, material_ambient[2][3]);
			glUniform3fv(gKdUniform, 1, material_diffuse[2][3]);
			glUniform3fv(gKsUniform, 1, material_specular[2][3]);
			glUniform1f(gMaterialShininessUniform, material_shininess[2][3]);

			glBindVertexArray (gVao_sphere);

			glBindBuffer (GL_ELEMENT_ARRAY_BUFFER, gVbo_element);
			glDrawElements (GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);
			glBindBuffer (GL_ELEMENT_ARRAY_BUFFER, 0);	//unbind
			glBindVertexArray (0);


//5

			 modelMatrix = mat4::identity ();	//M
			 viewMatrix = mat4::identity();        //MV
			//mat4 rotationMatrix = mat4::identity();

			modelMatrix = translate(-5.0f,-1.0f,-10.0f);


			glUniformMatrix4fv (gMUniform, 1, GL_FALSE, modelMatrix);
			glUniformMatrix4fv (gVUniform, 1, GL_FALSE, viewMatrix);

			glUniformMatrix4fv (gPUniform, 1, GL_FALSE, gPerspectiveProjectionMatrix);

			glUniform3fv(gKaUniform, 1, material_ambient[2][5]);
			glUniform3fv(gKdUniform, 1, material_diffuse[2][5]);
			glUniform3fv(gKsUniform, 1, material_specular[2][5]);
			glUniform1f(gMaterialShininessUniform, material_shininess[2][5]);

			glBindVertexArray (gVao_sphere);

			glBindBuffer (GL_ELEMENT_ARRAY_BUFFER, gVbo_element);
			glDrawElements (GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);
			glBindBuffer (GL_ELEMENT_ARRAY_BUFFER, 0);	//unbind
			glBindVertexArray (0);
//6

			 modelMatrix = mat4::identity ();	//M
			 viewMatrix = mat4::identity();        //MV
			//mat4 rotationMatrix = mat4::identity();

			modelMatrix = translate(5.0f,-1.0f,-10.0f);


			glUniformMatrix4fv (gMUniform, 1, GL_FALSE, modelMatrix);
			glUniformMatrix4fv (gVUniform, 1, GL_FALSE, viewMatrix);

			glUniformMatrix4fv (gPUniform, 1, GL_FALSE, gPerspectiveProjectionMatrix);

			glUniform3fv(gKaUniform, 1, material_ambient[2][5]);
			glUniform3fv(gKdUniform, 1, material_diffuse[2][5]);
			glUniform3fv(gKsUniform, 1, material_specular[2][5]);
			glUniform1f(gMaterialShininessUniform, material_shininess[2][5]);

			glBindVertexArray (gVao_sphere);

			glBindBuffer (GL_ELEMENT_ARRAY_BUFFER, gVbo_element);
			glDrawElements (GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);
			glBindBuffer (GL_ELEMENT_ARRAY_BUFFER, 0);	//unbind
			glBindVertexArray (0);
//============================================================
//3rd

			
//===================================================
//===//

//6th -last row complete
//=========================================================
//5th

			 modelMatrix = mat4::identity ();	//M
			 viewMatrix = mat4::identity();        //MV
			//mat4 rotationMatrix = mat4::identity();

			modelMatrix = translate(1.0f,3.0f,-10.0f);


			glUniformMatrix4fv (gMUniform, 1, GL_FALSE, modelMatrix);
			glUniformMatrix4fv (gVUniform, 1, GL_FALSE, viewMatrix);

			glUniformMatrix4fv (gPUniform, 1, GL_FALSE, gPerspectiveProjectionMatrix);

			glUniform3fv(gKaUniform, 1, material_ambient[3][0]);
			glUniform3fv(gKdUniform, 1, material_diffuse[3][0]);
			glUniform3fv(gKsUniform, 1, material_specular[3][0]);
			glUniform1f(gMaterialShininessUniform, material_shininess[3][0]);

			glBindVertexArray (gVao_sphere);

			glBindBuffer (GL_ELEMENT_ARRAY_BUFFER, gVbo_element);
			glDrawElements (GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);
			glBindBuffer (GL_ELEMENT_ARRAY_BUFFER, 0);	//unbind
			glBindVertexArray (0);
	
//2
			 modelMatrix = mat4::identity ();	//M
			 viewMatrix = mat4::identity();        //MV
			//mat4 rotationMatrix = mat4::identity();

			modelMatrix = translate(-1.0f,3.0f,-10.0f);


			glUniformMatrix4fv (gMUniform, 1, GL_FALSE, modelMatrix);
			glUniformMatrix4fv (gVUniform, 1, GL_FALSE, viewMatrix);

			glUniformMatrix4fv (gPUniform, 1, GL_FALSE, gPerspectiveProjectionMatrix);

			glUniform3fv(gKaUniform, 1, material_ambient[3][1]);
			glUniform3fv(gKdUniform, 1, material_diffuse[3][1]);
			glUniform3fv(gKsUniform, 1, material_specular[3][1]);
			glUniform1f(gMaterialShininessUniform, material_shininess[3][1]);

			glBindVertexArray (gVao_sphere);

			glBindBuffer (GL_ELEMENT_ARRAY_BUFFER, gVbo_element);
			glDrawElements (GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);
			glBindBuffer (GL_ELEMENT_ARRAY_BUFFER, 0);	//unbind
			glBindVertexArray (0);
	
//3

			 modelMatrix = mat4::identity ();	//M
			 viewMatrix = mat4::identity();        //MV
			//mat4 rotationMatrix = mat4::identity();

			modelMatrix = translate(-3.0f,3.0f,-10.0f);


			glUniformMatrix4fv (gMUniform, 1, GL_FALSE, modelMatrix);
			glUniformMatrix4fv (gVUniform, 1, GL_FALSE, viewMatrix);

			glUniformMatrix4fv (gPUniform, 1, GL_FALSE, gPerspectiveProjectionMatrix);

			glUniform3fv(gKaUniform, 1, material_ambient[3][2]);
			glUniform3fv(gKdUniform, 1, material_diffuse[3][2]);
			glUniform3fv(gKsUniform, 1, material_specular[3][2]);
			glUniform1f(gMaterialShininessUniform, material_shininess[3][2]);

			glBindVertexArray (gVao_sphere);

			glBindBuffer (GL_ELEMENT_ARRAY_BUFFER, gVbo_element);
			glDrawElements (GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);
			glBindBuffer (GL_ELEMENT_ARRAY_BUFFER, 0);	//unbind
			glBindVertexArray (0);

//4

			 modelMatrix = mat4::identity ();	//M
			 viewMatrix = mat4::identity();        //MV
			//mat4 rotationMatrix = mat4::identity();

			modelMatrix = translate(3.0f,3.0f,-10.0f);


			glUniformMatrix4fv (gMUniform, 1, GL_FALSE, modelMatrix);
			glUniformMatrix4fv (gVUniform, 1, GL_FALSE, viewMatrix);

			glUniformMatrix4fv (gPUniform, 1, GL_FALSE, gPerspectiveProjectionMatrix);

			glUniform3fv(gKaUniform, 1, material_ambient[3][3]);
			glUniform3fv(gKdUniform, 1, material_diffuse[3][3]);
			glUniform3fv(gKsUniform, 1, material_specular[3][3]);
			glUniform1f(gMaterialShininessUniform, material_shininess[3][3]);

			glBindVertexArray (gVao_sphere);

			glBindBuffer (GL_ELEMENT_ARRAY_BUFFER, gVbo_element);
			glDrawElements (GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);
			glBindBuffer (GL_ELEMENT_ARRAY_BUFFER, 0);	//unbind
			glBindVertexArray (0);
//5
 modelMatrix = mat4::identity ();	//M
			 viewMatrix = mat4::identity();        //MV
			//mat4 rotationMatrix = mat4::identity();

			modelMatrix = translate(5.0f,3.0f,-10.0f);


			glUniformMatrix4fv (gMUniform, 1, GL_FALSE, modelMatrix);
			glUniformMatrix4fv (gVUniform, 1, GL_FALSE, viewMatrix);

			glUniformMatrix4fv (gPUniform, 1, GL_FALSE, gPerspectiveProjectionMatrix);

			glUniform3fv(gKaUniform, 1, material_ambient[3][4]);
			glUniform3fv(gKdUniform, 1, material_diffuse[3][4]);
			glUniform3fv(gKsUniform, 1, material_specular[3][4]);
			glUniform1f(gMaterialShininessUniform, material_shininess[3][4]);

			glBindVertexArray (gVao_sphere);

			glBindBuffer (GL_ELEMENT_ARRAY_BUFFER, gVbo_element);
			glDrawElements (GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);
			glBindBuffer (GL_ELEMENT_ARRAY_BUFFER, 0);	//unbind
			glBindVertexArray (0);
//6
 modelMatrix = mat4::identity ();	//M
			 viewMatrix = mat4::identity();        //MV
			//mat4 rotationMatrix = mat4::identity();

			modelMatrix = translate(-5.0f,3.0f,-10.0f);


			glUniformMatrix4fv (gMUniform, 1, GL_FALSE, modelMatrix);
			glUniformMatrix4fv (gVUniform, 1, GL_FALSE, viewMatrix);

			glUniformMatrix4fv (gPUniform, 1, GL_FALSE, gPerspectiveProjectionMatrix);

			glUniform3fv(gKaUniform, 1, material_ambient[3][5]);
			glUniform3fv(gKdUniform, 1, material_diffuse[3][5]);
			glUniform3fv(gKsUniform, 1, material_specular[3][5]);
			glUniform1f(gMaterialShininessUniform, material_shininess[3][5]);

			glBindVertexArray (gVao_sphere);

			glBindBuffer (GL_ELEMENT_ARRAY_BUFFER, gVbo_element);
			glDrawElements (GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);
			glBindBuffer (GL_ELEMENT_ARRAY_BUFFER, 0);	//unbind
			glBindVertexArray (0);
	glUseProgram (0);
}
	else 
		gAngleSphere = 0.0f;
	
glXSwapBuffers (gpDisplay, gWindow);
}

void uninitialize (void)
{
	// code

	//Releasiing Shader objects
        if (gShaderProgramObject)
        {
                if (gVertexShaderObject)
                        glDetachShader (gShaderProgramObject, gVertexShaderObject);

                if (gFragmentShaderObject)
                        glDetachShader (gShaderProgramObject, gFragmentShaderObject);
        }

        if (gVertexShaderObject)
        {
                glDeleteShader (gVertexShaderObject);
                gVertexShaderObject = 0;
        }
        if(gFragmentShaderObject)
        {
                glDeleteShader (gFragmentShaderObject);
                gFragmentShaderObject = 0;
        }
        if (gShaderProgramObject)
        {
                glDeleteProgram (gShaderProgramObject);
                gShaderProgramObject = 0;
        }

        if(gVao_sphere)
        {
                glDeleteVertexArrays (1, &gVao_sphere);
                gVao_sphere = 0;
        }
        if(gVbo_position)
        {
                glDeleteBuffers (1, &gVbo_position);
                gVbo_position = 0;
        }

        //Stop using program
        glUseProgram (0);

	// Releasing OpenGL and XWindow related objects
	GLXContext currentContext = glXGetCurrentContext ();
	
	if (currentContext != NULL && currentContext == gGLXContext)
		glXMakeCurrent (gpDisplay, 0, 0);

	if (gGLXContext)
		glXDestroyContext (gpDisplay, gGLXContext);

	if (gWindow)
		XDestroyWindow (gpDisplay, gWindow);
	
	if (gColormap)
		XFreeColormap (gpDisplay, gColormap);

	if (gpXVisualInfo)
	{
		free (gpXVisualInfo);
		gpXVisualInfo = NULL;
	}

	if (gpDisplay)
	{
		XCloseDisplay (gpDisplay);
		gpDisplay = NULL;
	}

	if (gpFile)
	{
		fprintf (gpFile, "Log file is successfuly closed\n");
		fclose (gpFile);
		gpFile = NULL;
	}
}

