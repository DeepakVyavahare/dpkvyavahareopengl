//2 light on pyramid

#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <memory.h>

#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/XKBlib.h>
#include <X11/keysym.h>

#include <GL/glew.h> 	
#include <GL/gl.h>
#include <GL/glx.h>	// glX___()

#include "vmath.h"

using namespace vmath;

#define WIN_WIDTH	800
#define WIN_HEIGHT	600

//global variable
Display 	*gpDisplay = NULL;
Colormap 	gColormap;
XVisualInfo	*gpXVisualInfo = NULL;
Window		gWindow;

GLXFBConfig	gGLXFBConfig;
GLXContext	gGLXContext;	// paralle to HGLRC

typedef	GLXContext (*glXCreateContextAttribsARBProc) (Display*, GLXFBConfig, GLXContext, Bool, const int*);
glXCreateContextAttribsARBProc	glXCreateContextAttribsARB = NULL;

bool gbFullscreen = false;
FILE *gpFile = NULL;

GLuint gVertexShaderObject;
GLuint gFragmentShaderObject;
GLuint gShaderProgramObject;

GLint modelUniform;
GLint viewUniform;
GLint projectionUniform;

GLint gLKeyPressedUniform;

GLint u_LaUniform;
GLint u_LdUniform;
GLint u_LsUniform;

GLint u_LaUniform1;
GLint u_LdUniform1;
GLint u_LsUniform1;

GLint gKaUniform;
GLint gKdUniform;
GLint gKsUniform;

GLint u_light_position_uniform;
GLint u_light_position_uniform1;
GLint gMaterialShininessUniform;

GLuint gVao_pyramid;
GLuint gVbo_position;
GLuint gVbo_normal;

GLfloat lightAmbient[] = { 0.0f,0.0f,0.0f,1.0f };
GLfloat lightDiffuse[] = { 1.0f,0.0f,0.0f,1.0f };
GLfloat lightSpecular[] = { 1.0f,0.0f,0.0f,1.0f };
GLfloat lightPosition[] = { -10.0f,0.0f,0.0f,0.0f }; 

GLfloat lightAmbient1[] = { 0.0f,0.0f,0.0f,1.0f };
GLfloat lightDiffuse1[] = { 0.0f,0.0f,1.0f,1.0f };
GLfloat lightSpecular1[] = { 0.0f,0.0f,1.0f,1.0f };
GLfloat lightPosition1[] = { 10.0f,0.0f,0.0f,0.0f };	

GLfloat materialAmbient[] = { 0.0f, 0.0f,0.0f,1.0f };
GLfloat materialDiffuse[] = { 1.0f,1.0f,1.0f,1.0f };
GLfloat materialSpecular[] = { 1.0f, 1.0f,1.0f,1.0f };
GLfloat materialShininess = 50.0f;

bool gbAnimate = false;
bool gbLight = false;
GLfloat gAngleCube = 0.0f;

mat4 gPerspectiveProjectionMatrix;

enum {
	dpk_ATTRIBUTE_POSITION=0,
	dpk_ATTRIBUTE_COLOR,
	dpk_ATTRIBUTE_NORMAL,
	dpk_ATTRIBUTE_TEXTURE0,
};

int main (void)
{
	//function declarations
	void CreateWindow (void);
	void uninitialize (void);
	void initialize (void);
	void ToggleFullscreen (void);
	void resize (int, int);
	void display (void);
	void spin (void);

	//variable declarations
	bool 	bDone = false;
	char 	inputCharacter[26];
	KeySym 	keySym;
	int winWidth=WIN_WIDTH;
	int winHeight=WIN_HEIGHT;
	XEvent event;

	//code
	gpFile = fopen ("log.txt", "w");
	if (gpFile == NULL)
	{
		fprintf (stderr, "Log file creation failed...\n");
		exit (1);
	}
	else
	{
		fprintf (gpFile, "Log file created sucessfully\n");
	}

	CreateWindow ();
	initialize ();
	
	while (!bDone)
	{
		while (XPending (gpDisplay))
		{
			XNextEvent (gpDisplay, &event);	//blocking call
			switch (event.type)	// parellel iMsg
			{
				case MapNotify :	// parallel to wM_CREATE
					break;
				case KeyPress :		// parallel to WM_KEYDOWN
					keySym = XkbKeycodeToKeysym (gpDisplay, event.xkey.keycode, 0, 0);
					switch (keySym)
					{
						case XK_Escape:
							bDone = true;
							break;
						default:
							break;
					}
					XLookupString (&event.xkey, inputCharacter, sizeof (inputCharacter), NULL, NULL);
					switch (inputCharacter[0])
					{
						case 'A':
						case 'a':
							if (gbAnimate == true)
								gbAnimate = false;
							else
								gbAnimate = true;
							break;

						case 'L':
						case 'l':
							if (gbLight == true)
								gbLight = false;
							else
								gbLight = true;
							break;
						case 'F':
						case 'f': //fallthrough
							if (gbFullscreen == true)
							{
								ToggleFullscreen ();
								gbFullscreen = false;
							}
							else
							{
								ToggleFullscreen ();
								gbFullscreen = true;
							}
							break;
							
					}
					break;
			
				case ButtonPress:	// mouse events
					switch (event.xbutton.button)
					{
						case 1:	// LBUTTONDOWN
							break;
						case 2: // MBUTTONDOWN
							break;
						case 3: // RBUTTONDOWN
							break;
						case 4: // WHEEL UP
							break;
						case 5: // WHEEL DOWN
							break;
						default:
							break;
					}
					break; 
				case MotionNotify : //WM_MOVE or mouse move
					break;
				case ConfigureNotify: // WM_SIZE
					winWidth = event.xconfigure.width;
					winHeight = event.xconfigure.height;
					resize (winWidth, winHeight);
					break;
				case Expose:
					break;
				case DestroyNotify:
					break;
				case 33:
					bDone=true;
					break;
				default:
					break;
			}/*switch(event.type)*/
		} /*while (XPending (gpDisplay)*/
	
		/*Rendor here*/
		display ();
		if (gbAnimate == true)
			spin ();
	
	}/*while(1)*/

	uninitialize ();
	return (0);
}

void CreateWindow (void)
{
	//function declarations
	void uninitialize (void);

	//variable declarations
	XSetWindowAttributes winAttribs;
	GLXFBConfig* pGLXFBConfigs = NULL;
	GLXFBConfig bestGLXFBConfig;
	XVisualInfo* pTempXVisualInfo = NULL;
	int iNumFBConfigs = 0;
	int styleMask;

	static int frameBufferAttributes [] = {
		GLX_X_RENDERABLE, 	True,
		GLX_DRAWABLE_TYPE, 	GLX_WINDOW_BIT,
		GLX_RENDER_TYPE, 	GLX_RGBA_BIT,
		GLX_X_VISUAL_TYPE, 	GLX_TRUE_COLOR,
		GLX_RED_SIZE, 		8,
		GLX_GREEN_SIZE,		8,
		GLX_BLUE_SIZE,		8,
		GLX_ALPHA_SIZE,		8,
		GLX_DEPTH_SIZE,		24,
		GLX_STENCIL_SIZE,	8,
		GLX_DOUBLEBUFFER, 	True,
		GLX_SAMPLE_BUFFERS,	1,
		GLX_SAMPLES,		4,
		None				//array must be terminated by 0 (None)
	};

	gpDisplay = XOpenDisplay (NULL);
	if (gpDisplay == NULL)
	{
		fprintf (stderr, "ERROR: Unable to open X Display. \nExiting Now...\n");
		uninitialize ();
		exit (1);
	}

	// get a new framebuffer config that meets out attrib requirement
	pGLXFBConfigs = glXChooseFBConfig (gpDisplay, XDefaultScreen (gpDisplay), frameBufferAttributes, &iNumFBConfigs);
	if (pGLXFBConfigs == NULL)
	{
		fprintf (stderr, "ERROR: Failed to valid Framebuffer configs. Exiting Now..\n");
		uninitialize ();
		exit (1);
	}	
	fprintf (gpFile, "%d Matching FB configs found.\n", iNumFBConfigs);

	//pick up that FB config/visual with the most samples per pixel
	int bestFramebufferConfig = -1, worstFramebufferConfig = -1, bestNumberOfSamples = -1, worstNumberOfSamples = 999;

	for (int i=0; i<iNumFBConfigs; i++)
	{
		pTempXVisualInfo = glXGetVisualFromFBConfig (gpDisplay, pGLXFBConfigs[i]);
		if (pTempXVisualInfo)
		{
			int sampleBuffer, samples;
			glXGetFBConfigAttrib (gpDisplay, pGLXFBConfigs[i], GLX_SAMPLE_BUFFERS, &sampleBuffer);
			glXGetFBConfigAttrib (gpDisplay, pGLXFBConfigs[i], GLX_SAMPLES, &samples);
			fprintf (gpFile, "Matching Framebuffer Config=%d : Visual ID=%lu : SAMPLE_BUFFERS=%d : SAMPLES=%d\n", \
						i, pTempXVisualInfo->visualid, sampleBuffer, samples);
			
			if (bestFramebufferConfig < 0 || sampleBuffer && samples > bestNumberOfSamples)
			{
				bestFramebufferConfig = i;
				bestNumberOfSamples = samples;
			}
			if (worstFramebufferConfig < 0 || !sampleBuffer || samples < worstNumberOfSamples)
			{
				worstFramebufferConfig = i;
				worstNumberOfSamples = samples;
			}
		}
		XFree (pTempXVisualInfo);
	}
	bestGLXFBConfig = pGLXFBConfigs [bestFramebufferConfig];
	//set global GLXFBConfig
	gGLXFBConfig = bestGLXFBConfig;
	 
	//be sure to free FBConfig list allocated by glXChooseFBConfig ()
	XFree (pGLXFBConfigs);

	gpXVisualInfo = glXGetVisualFromFBConfig (gpDisplay, bestGLXFBConfig);
	fprintf (gpFile, "Chosen Visual ID=0x%lx\n", gpXVisualInfo->visualid);

	//setting window's attributes 
	winAttribs.border_pixel = 0;
	winAttribs.background_pixmap = 0;
	winAttribs.colormap = XCreateColormap (gpDisplay, 
						RootWindow (gpDisplay, gpXVisualInfo->screen), 	// you can defaultscreen as well
						gpXVisualInfo->visual,
						AllocNone);	// for movable 	memory allocation

	winAttribs.event_mask = StructureNotifyMask | KeyPressMask | ButtonPressMask | 
				ExposureMask | VisibilityChangeMask | PointerMotionMask;
	styleMask = CWBorderPixel | CWEventMask | CWColormap;
	
	gWindow = XCreateWindow (gpDisplay, 
				RootWindow (gpDisplay, gpXVisualInfo->screen),
				100,
				100,
				WIN_WIDTH,
				WIN_HEIGHT,
				0,		//border width
				gpXVisualInfo->depth,	//depth of visual (depth of colormap)
				InputOutput,		//class (type)
				gpXVisualInfo->visual,
				styleMask,
				&winAttribs);

	if (!gWindow)
	{
		fprintf (stderr, "ERROR: Failure in window creation\n");
		uninitialize ();
		exit (1);
	}
	XStoreName (gpDisplay, gWindow, "xwindows-2 lights");
	
	Atom windowManagerDelete = XInternAtom (gpDisplay, "WM_WINDOW_DELETE", True);
	XSetWMProtocols (gpDisplay, gWindow, &windowManagerDelete, 1);

	XMapWindow (gpDisplay, gWindow);
}

void ToggleFullscreen (void)
{
	//variable declarations
	Atom wm_state;
	Atom fullscreen;
	XEvent xev = {0};

	//code 
	wm_state = XInternAtom (gpDisplay, "_NET_WM_STATE", False);
	memset (&xev, 0, sizeof (xev));
	
	xev.type = ClientMessage;
	xev.xclient.window = gWindow;
	xev.xclient.message_type = wm_state;
	xev.xclient.format = 32;
	xev.xclient.data.l[0] = gbFullscreen ? 0:1;

	fullscreen = XInternAtom (gpDisplay, "_NET_WM_STATE_FULLSCREEN", False);
	xev.xclient.data.l[1] = fullscreen;

	XSendEvent (gpDisplay, 
			RootWindow(gpDisplay, gpXVisualInfo->screen), 
			False, 
			StructureNotifyMask, 
			&xev);
}


void initialize (void)
{
	//function declarations
	void uninitialize (void);
	void resize (int, int);
	
	//code 
	/*Create a new GL context 3.0 for rendering*/
	glXCreateContextAttribsARB = (glXCreateContextAttribsARBProc) glXGetProcAddressARB (
								(GLubyte*) "glXCreateContextAttribsARB"); // parallel to GetProcAddress ()
	GLint attribs [] = {
		GLX_CONTEXT_MAJOR_VERSION_ARB,	3,	//4,
		GLX_CONTEXT_MINOR_VERSION_ARB, 	0,	//5,
		GLX_CONTEXT_PROFILE_MASK_ARB,	GLX_CONTEXT_COMPATIBILITY_PROFILE_BIT_ARB, 
		0	/*Array must be terminated by null*/
	};

	gGLXContext = glXCreateContextAttribsARB (gpDisplay, gGLXFBConfig, 0, True, attribs);
	if (!gGLXContext)
	{/*3.0 context not on this machine, fallback to old style 2.x context*/
		/*When a context version 3.0 is requested implementation will return 
		  the newest context version compatible with OpenGL version less than 3.0 */
		GLint attribs [] = {
			GLX_CONTEXT_MAJOR_VERSION_ARB,	1,
			GLX_CONTEXT_MINOR_VERSION_ARB,  0,
			0	// Array must be terminated by null
		};
		fprintf (stderr, "ERROR: Failed to create GLX 4.5 context. Hence using old-style GLX context\n");
		gGLXContext = glXCreateContextAttribsARB (gpDisplay, gGLXFBConfig, 0, True, attribs);
	}
	else
	{// successfuly created 3.0 context
		fprintf (gpFile, "OpenGL Context 3.0 created\n");
	}

	// verify context is direct or indirect
	if (!glXIsDirect (gpDisplay,  gGLXContext))
	{
		fprintf (gpFile, "Indirect GLX Rendering Context Obtained\n");
	}	
	else
	{
		fprintf (gpFile, "Direct GLX Rendering Context Obtained\n\n");
	}

	glXMakeCurrent (gpDisplay, gWindow, gGLXContext);

	GLenum glew_error = glewInit();
	if (glew_error != GLEW_OK)
	{
		fprintf (gpFile, "glewInit():failed\nExiting...\n");
		uninitialize();
		exit (1);	
	}
	fprintf (gpFile, "OpenGL Version: %s\n", glGetString (GL_VERSION));
	fprintf (gpFile, "GLSL Version: %s\n", glGetString (GL_SHADING_LANGUAGE_VERSION));

	/************VERTEX SHADER**********/
	gVertexShaderObject  = glCreateShader (GL_VERTEX_SHADER);
	
	const char* vertexShaderSourceCode = 
			"#version 130" \
			"\n" \
			"in vec4 vPosition;" \
			"in vec3 vNormal;" \
			"uniform mat4 u_model_matrix;" \
			"uniform mat4 u_view_matrix;" \
			"uniform mat4 u_projection_matrix;" \
			"uniform int u_LKeyPressed;" \
			"uniform vec3 u_red_La;" \
			"uniform vec3 u_red_Ld;" \
			"uniform vec3 u_red_Ls;" \
			"uniform vec3 u_blue_La;" \
			"uniform vec3 u_blue_Ld;" \
			"uniform vec3 u_blue_Ls;" \
			"uniform vec3 u_Ka;" \
			"uniform vec3 u_Kd;" \
			"uniform vec3 u_Ks;" \
			"uniform float u_material_shininess;" \
			"uniform vec4 u_RedLight_Position;" \
			"uniform vec4 u_BlueLight_Position;" \
			"out vec3 phong_ads_color;" \
			"void main(void)" \
			"{" \
			"if (u_LKeyPressed == 1)" \
			"{" \
			"vec4 eyeCoordinates = u_view_matrix * u_model_matrix * vPosition;" \
			"vec3 tnorm = normalize(mat3(u_view_matrix * u_model_matrix) * vNormal);" \
			"vec3 s = normalize(vec3(u_RedLight_Position) - eyeCoordinates.xyz);" \
			"vec3 reflection_vector = reflect (-s, tnorm);" \			
			"vec3 viewer_vector = normalize(-eyeCoordinates.xyz);" \
			
			"vec3 ambient = u_red_La * u_Ka;" \
			"vec3 diffuse = u_red_Ld * u_Kd * max(dot(s, tnorm), 0.0);" \
			"float r_dot_v = max(dot(reflection_vector, viewer_vector), 0.0);" \
			"vec3 specular = u_red_Ls * u_Ks * pow(r_dot_v, u_material_shininess);" \
			"phong_ads_color =  ambient + diffuse+specular;" \

			"s = normalize(vec3(u_BlueLight_Position) - eyeCoordinates.xyz);" \
			"ambient = u_blue_La * u_Ka;"
			"diffuse = u_blue_Ld * u_Kd * max(dot(s, tnorm), 0.0);"\
			"reflection_vector = reflect (-s, tnorm);" \
			"r_dot_v = max(dot(reflection_vector, viewer_vector), 0.0);" \
			"specular = u_red_Ls * u_Ks * pow(r_dot_v, u_material_shininess);" \
			"phong_ads_color += ambient + diffuse + specular;"\
			"}" \
			"gl_Position = u_projection_matrix * u_view_matrix * u_model_matrix * vPosition;" \
			"}"; 
		
	glShaderSource (gVertexShaderObject, // Handle to shader object
			1, 		     // how many elemnets in next arg i.e. string
			(const GLchar**)&vertexShaderSourceCode,	// array pointer to source code string
			NULL);

	glCompileShader (gVertexShaderObject);

	GLint iShaderCompileStatus = 0;
	glGetShaderiv (gVertexShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);
	if (iShaderCompileStatus == GL_FALSE)
	{
		GLint iInfoLogLength = 0;
		glGetShaderiv (gVertexShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			char *szInfoLog = (char*) malloc (iInfoLogLength);
			if (szInfoLog)
			{
				GLsizei written;
				glGetShaderInfoLog (gVertexShaderObject, iInfoLogLength, &written, (GLchar*)szInfoLog);
				fprintf (gpFile, "Vertex Shader Compile Status Log: %s\n", szInfoLog);
				free (szInfoLog);
				uninitialize ();
				exit (1);
			}
		}
	}
	
	/********************FRAGMENT SHADER**********************/
	gFragmentShaderObject = glCreateShader (GL_FRAGMENT_SHADER);

	const char *fragmentShaderSourceCode = 
	"#version 130" \
	"\n" \
	"in vec3 phong_ads_color;" \
	"out vec4 FragColor;" \
	"uniform int u_LKeyPressed;" \
	"void main (void)" \
	"{" \
	"vec4 color;" \
	"if (u_LKeyPressed == 1)" \
	"{" \
	"color = vec4(phong_ads_color, 1.0);" \
	"}" \
	"else" \
	"{" \
	"color = vec4(1.0, 1.0, 1.0, 1.0);" \
	"}" \
	"FragColor = color;" \
	"}";

	glShaderSource (gFragmentShaderObject, //Shader object
			1, 	//no. of elements in source code string
			(const GLchar**)&fragmentShaderSourceCode, // source code string
			NULL);	//if 2nd arg is more than 1, then this arg has index to each string start in 3rd arg

	glCompileShader (gFragmentShaderObject);
	
	iShaderCompileStatus = 0;
	glGetShaderiv (gFragmentShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);
	if (GL_FALSE == iShaderCompileStatus)
	{
		GLint iInfoLogLength = 0;
		glGetShaderiv (gFragmentShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			char *szInfoLog = (char*)malloc (iInfoLogLength);
			if (szInfoLog)
			{
				GLsizei written;
				glGetShaderInfoLog (gFragmentShaderObject, iInfoLogLength, &written, (GLchar*)szInfoLog);
				fprintf (gpFile, "Fragment Shader Compile Log: %s\n", szInfoLog);
				free (szInfoLog);
				uninitialize();
				exit (1);
			}
		}
	}

	/****************SHADER PROGRAM****************/
	gShaderProgramObject = glCreateProgram();

	glAttachShader (gShaderProgramObject, gVertexShaderObject);
	
glAttachShader (gShaderProgramObject, gFragmentShaderObject);

	glBindAttribLocation (gShaderProgramObject, dpk_ATTRIBUTE_POSITION, "vPosition");
	glBindAttribLocation (gShaderProgramObject, dpk_ATTRIBUTE_NORMAL, "vNormal");

	glLinkProgram (gShaderProgramObject);

	GLint iProgramLinkStatus = 0;
	glGetProgramiv (gShaderProgramObject, GL_LINK_STATUS, &iProgramLinkStatus);
	if (GL_FALSE == iProgramLinkStatus)
	{
		GLint iInfoLogLength = 0;
		glGetProgramiv (gShaderProgramObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			char *szInfoLog = (char*) malloc (iInfoLogLength);
			if (szInfoLog)
			{
				GLsizei written;
				glGetProgramInfoLog (gShaderProgramObject, iInfoLogLength, &written, (GLchar*)szInfoLog);
				fprintf (gpFile, "Program Link Status Log: %s\n", szInfoLog);
				free (szInfoLog);
				uninitialize();
				exit (1);
			}
		}
	}
	modelUniform = glGetUniformLocation (gShaderProgramObject, "u_model_matrix");
	viewUniform = glGetUniformLocation (gShaderProgramObject, "u_view_matrix");
	projectionUniform = glGetUniformLocation (gShaderProgramObject, "u_projection_matrix");
	gLKeyPressedUniform = glGetUniformLocation (gShaderProgramObject, "u_LKeyPressed");
	
	gKaUniform = glGetUniformLocation (gShaderProgramObject, "u_Ka");
	gKdUniform = glGetUniformLocation (gShaderProgramObject, "u_Kd");
	gKsUniform = glGetUniformLocation (gShaderProgramObject, "u_Ks");

	u_LaUniform = glGetUniformLocation (gShaderProgramObject, "u_red_La");
	u_LdUniform = glGetUniformLocation (gShaderProgramObject, "u_red_Ld");
	u_LsUniform = glGetUniformLocation (gShaderProgramObject, "u_red_Ls");

	u_LaUniform1 = glGetUniformLocation (gShaderProgramObject, "u_blue_La");
	u_LdUniform1 = glGetUniformLocation (gShaderProgramObject, "u_blue_Ld");
	u_LsUniform1 = glGetUniformLocation (gShaderProgramObject, "u_blue_Ls");
	u_light_position_uniform = glGetUniformLocation (gShaderProgramObject, "u_RedLight_Position");
	u_light_position_uniform1 = glGetUniformLocation (gShaderProgramObject, "u_BlueLight_Position");
	gMaterialShininessUniform= glGetUniformLocation (gShaderProgramObject, "u_material_shininess");

	/*-----------Vertices, Color, Shader Attribs, VAO, VBO Initialization--------------*/
	
//VAO1:Cube
        const float pyramidVertices[] = 
	{
	 0.0f, 1.0f, 0.0f,	 -1.0f, -1.0f, 1.0f,	 1.0f, -1.0f, 1.0f,	
	 0.0f, 1.0f, 0.0f,	 1.0f, -1.0f, 1.0f,	 1.0f, -1.0f, -1.0f,
	 0.0f, 1.0f, 0.0f,	 1.0f, -1.0f, -1.0f,	 -1.0f, -1.0f, -1.0f,	
	 0.0f, 1.0f, 0.0f,	 -1.0f, -1.0f, -1.0f,	 -1.0f, -1.0f, 1.0f,	
	};

	//per vertex color
        const float pyramidNormal[] = 
	{
	 0.0f, 0.447214f, 0.894427f,  0.0f, 0.447214f, 0.894427f,  0.0f, 0.447214f, 0.894427f,  
	 0.894427f, 0.447214f, 0.0f,  0.894427f, 0.447214f, 0.0f,  0.894427f, 0.447214f, 0.0f,  
	 0.0f, 0.447214f, -0.894427f,  0.0f, 0.447214f, -0.894427f,  0.0f, 0.447214f, -0.894427f,  
	 -0.894427f, 0.447214f, 0.0f,  -0.894427f, 0.447214f, 0.0f,  -0.894427f, 0.447214f, 0.0f, 
	};	

	glGenVertexArrays (1, &gVao_pyramid);
	glBindVertexArray (gVao_pyramid);
	glGenBuffers (1, &gVbo_position);
	glBindBuffer (GL_ARRAY_BUFFER, gVbo_position);
	glBufferData (GL_ARRAY_BUFFER, sizeof(pyramidVertices), pyramidVertices, GL_STATIC_DRAW);
	glVertexAttribPointer (dpk_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray (dpk_ATTRIBUTE_POSITION);
	glBindBuffer (GL_ARRAY_BUFFER, 0);

	glGenBuffers (1, &gVbo_normal);
	glBindBuffer (GL_ARRAY_BUFFER, gVbo_normal);
	glBufferData (GL_ARRAY_BUFFER, sizeof(pyramidNormal), pyramidNormal, GL_STATIC_DRAW);
	glVertexAttribPointer (dpk_ATTRIBUTE_NORMAL, 3, GL_FLOAT, GL_FALSE, 0, 0);
	glEnableVertexAttribArray (dpk_ATTRIBUTE_NORMAL);
	glBindBuffer (GL_ARRAY_BUFFER, 0);

	glBindVertexArray (0); 

	//code
	glShadeModel 	(GL_SMOOTH);
	glClearDepth 	(1.0f);		// setup depth buffer
	glEnable 	(GL_DEPTH_TEST);	//enable depth test
	glDepthFunc 	(GL_LEQUAL);	// depth test to do
	
	glClearColor 	(0.0f, 0.0f, 0.0f, 0.0f);	// set background clearing color 
gPerspectiveProjectionMatrix = mat4::identity ();

	resize 		(WIN_WIDTH, WIN_HEIGHT);	// resize window
}

void resize (int width, int height)
{
	if (height == 0)
		height = 1;
	if (width == 0)
		width = 1;

	glViewport (0, 0, (GLsizei)width, (GLsizei)height);

	if (width <= height)
		gPerspectiveProjectionMatrix = perspective (45.0f, ((float)height/(float)width), 0.1f, 1000.0f);
	else
		gPerspectiveProjectionMatrix = perspective (45.0f, ((float)width/(float)height), 0.1f, 1000.0f);
}

void spin (void)
{
	gAngleCube = gAngleCube + 0.3f;
	if (gAngleCube > 360.0f)
		gAngleCube = 0.0f;
}

void display (void)
{
	// code 
	glClear (GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	glUseProgram (gShaderProgramObject);
	if (gbLight == true)
	{
		glUniform1i (gLKeyPressedUniform, 1);		//signal shader that user wants to enable light
		glUniform3fv (u_LaUniform, 1, lightAmbient);
		glUniform3fv (u_LdUniform, 1, lightDiffuse);
		glUniform3fv (u_LsUniform, 1, lightSpecular);

		glUniform3fv (u_LaUniform1, 1, lightAmbient1);
		glUniform3fv (u_LdUniform1, 1, lightDiffuse1);
		glUniform3fv (u_LsUniform1, 1, lightSpecular1);

		glUniform3fv (gKaUniform, 1, materialAmbient);
		glUniform3fv (gKdUniform, 1, materialDiffuse);
		glUniform3fv (gKsUniform, 1, materialSpecular);
		glUniform1f (gMaterialShininessUniform, materialShininess);

		glUniform4fv (u_light_position_uniform, 1, (GLfloat*)lightPosition);
		glUniform4fv (u_light_position_uniform1, 1, (GLfloat*)lightPosition1);
	}
	else
	{
		glUniform1i (gLKeyPressedUniform, 0);
	}

	mat4 modelMatrix = mat4::identity ();	
        mat4 viewMatrix = mat4::identity();        
        mat4 projectionMatrix = mat4::identity();
        mat4 rotationMatrix = mat4::identity();

        modelMatrix = translate (0.0f, 0.0f, -6.0f);

        rotationMatrix = rotate (gAngleCube, 0.0f, 1.0f, 0.0f);
        modelMatrix = modelMatrix * rotationMatrix;	//Order is imp

	modelMatrix = modelMatrix * scale (0.7f, 0.7f, 0.7f);
	glUniformMatrix4fv (modelUniform, 1, GL_FALSE, modelMatrix);
	glUniformMatrix4fv (viewUniform, 1, GL_FALSE, viewMatrix);

	glUniformMatrix4fv (projectionUniform, 1, GL_FALSE, gPerspectiveProjectionMatrix);
           
        glBindVertexArray (gVao_pyramid);

	glDrawArrays(GL_TRIANGLE_FAN, 0, 12);	// 4*3 = 4 triangles drawn 
       
        glBindVertexArray (0);
	
	glUseProgram (0);
	glXSwapBuffers (gpDisplay, gWindow);
}

void uninitialize (void)
{
	// code

	//Releasiing Shader objects
        if (gShaderProgramObject)
        {
                if (gVertexShaderObject)
                        glDetachShader (gShaderProgramObject, gVertexShaderObject);

                if (gFragmentShaderObject)
                        glDetachShader (gShaderProgramObject, gFragmentShaderObject);
        }

        if (gVertexShaderObject)
        {
                glDeleteShader (gVertexShaderObject);
                gVertexShaderObject = 0;
        }
        if(gFragmentShaderObject)
        {
                glDeleteShader (gFragmentShaderObject);
                gFragmentShaderObject = 0;
        }
        if (gShaderProgramObject)
        {
                glDeleteProgram (gShaderProgramObject);
                gShaderProgramObject = 0;
        }

        if(gVao_pyramid)
        {
                glDeleteVertexArrays (1, &gVao_pyramid);
                gVao_pyramid = 0;
        }
        if(gVbo_position)
        {
                glDeleteBuffers (1, &gVbo_position);
                gVbo_position = 0;
        }

        //Stop using program
        glUseProgram (0);

	// Releasing OpenGL and XWindow related objects
	GLXContext currentContext = glXGetCurrentContext ();
	
	if (currentContext != NULL && currentContext == gGLXContext)
		glXMakeCurrent (gpDisplay, 0, 0);

	if (gGLXContext)
		glXDestroyContext (gpDisplay, gGLXContext);

	if (gWindow)
		XDestroyWindow (gpDisplay, gWindow);
	
	if (gColormap)
		XFreeColormap (gpDisplay, gColormap);

	if (gpXVisualInfo)
	{
		free (gpXVisualInfo);
		gpXVisualInfo = NULL;
	}

	if (gpDisplay)
	{
		XCloseDisplay (gpDisplay);
		gpDisplay = NULL;
	}

	if (gpFile)
	{
		fprintf (gpFile, "Log file is successfuly closed\n");
		fclose (gpFile);
		gpFile = NULL;
	}
}


