//headers
#import <Foundation/Foundation.h>
#import <Cocoa/Cocoa.h>

//Core Video
#import <QuartzCore/CVDisplayLink.h>

//OpenGL
#import <OpenGL/gl3.h>
#import <OpenGL/gl3ext.h>

//vector maths --> matrix/vector operations
#import "vmath.h"

#import "sphere.h"
#import "material.h"



enum
{
    VDG_ATTRIBUTE_POSITION = 0,
    VDG_ATTRIBUTE_COLOR,
    VDG_ATTRIBUTE_NORMAL,
    VDG_ATTRIBUTE_TEXTURE0,
};

GLfloat gAngle = 0.0f;

GLfloat lightAmbient[] = { 0.0f, 0.0f, 0.0f, 1.0f };   
GLfloat lightDiffuse[] = { 1.0f, 1.0f, 1.0f, 1.0f };   
GLfloat lightSpecular[] = { 1.0f, 1.0f, 1.0f, 1.0f };  
GLfloat lightPosition[] = { 0.0f, 0.0f, 0.0f, 0.0f };  

GLfloat materialAmbient[] = { 0.0f, 0.0f,0.0f,1.0f };
GLfloat materialDiffuse[] = { 1.0f,1.0f,1.0f,1.0f };
GLfloat materialSpecular[] = { 1.0f, 1.0f,1.0f,1.0f };
GLfloat materialShininess = 50.0f;


//Sphere declarations
GLuint gNumElements;
GLuint gNumVertices;
GLfloat sphere_vertices[1146];
GLfloat sphere_normals[1146];
GLfloat sphere_textures[764];
GLshort sphere_elements[2280];

//log file pointer
FILE *gpFile = NULL;

// 'C' style function declaration
CVReturn MyDisplayLinkCallback(CVDisplayLinkRef, const CVTimeStamp*, const CVTimeStamp*, CVOptionFlags, CVOptionFlags*, void*);

//interface declarations
@interface AppDelegate: NSObject <NSApplicationDelegate, NSWindowDelegate>
@end

@interface GLView: NSOpenGLView
@end

//entry point function
int main(int argc, const char *argv[])
{
    NSAutoreleasePool *pPool = [[NSAutoreleasePool alloc] init];
    
    NSApp = [NSApplication sharedApplication];
    
    [NSApp setDelegate:[[AppDelegate alloc] init]];
    
    [NSApp run];    //run loop
    
    [pPool release];
    
    return (0);
}

//implementations
@implementation AppDelegate
{
@private
    NSWindow *window;
    GLView *glView;
}

-(void)applicationDidFinishLaunching:(NSNotification*)aNotification
{
    //code
    //create log file
    NSBundle *bundle = [NSBundle mainBundle];
    NSString *appDirPath = [bundle bundlePath];
    NSString *appDirParentPath = [appDirPath stringByDeletingLastPathComponent];
    NSString *logFilePath = [NSString stringWithFormat:@"%@/Log.txt",appDirParentPath];
    //convert NSSTring to char*
    const char* szLogFilePath = [logFilePath cStringUsingEncoding:NSASCIIStringEncoding];
    
    gpFile = fopen(szLogFilePath, "w");
    if(gpFile == NULL)
    {
        printf("Cannot Open Log.txt. Exiting...\n");
        [self release];
        [NSApp terminate:self];
    }
    else
    {
        fprintf(gpFile, "Applicaton Is Started Successfully\n");
    }
    
    //create window
    NSRect win_rect = NSMakeRect(0.0, 0.0, 800.0, 600.0);
    window = [[NSWindow alloc] initWithContentRect:win_rect
                                         styleMask:NSWindowStyleMaskTitled | NSWindowStyleMaskClosable | NSWindowStyleMaskMiniaturizable | NSWindowStyleMaskResizable
                                           backing:NSBackingStoreBuffered
                                             defer:NO];
    
    //set window properties
    [window setTitle:@"24"];    
    [window center];
    
    //create OpenGL view
    glView = [[GLView alloc] initWithFrame:[window frame]];
    
    if(glView == nil)
    {
        fprintf(gpFile, "Unable to Create GLView. Exiting...\n");
        [self release];
        [NSApp terminate:self];
    }
    
    //attach glview to window
    [window setContentView:glView];
    [window setDelegate:self];
    [window makeKeyAndOrderFront:self];
}

-(void)applicationWillTerminate:(NSNotification*)aNotification
{
    fprintf(gpFile, "Application Is Close Successfully\n");
    if(gpFile)
    {
        fclose(gpFile);
        gpFile = NULL;
    }
}

-(void)windowWillClose:(NSNotification*)aNotification
{
    //code
    [NSApp terminate:self];
}

-(void)dealloc
{
    //code
    [glView release];
    [window release];
    
    [super dealloc];
}
@end

@implementation GLView
{
@private
    CVDisplayLinkRef displayLink;
    
    Sphere *sphereObject;
    
    GLuint perFragVertexShaderObject;
    GLuint perFragFragmentShaderObject;
    GLuint perFragShaderProgramObject;
    
    
    GLuint vao[NR_SHAPES];
    GLuint vbo[NR_SHAPES][NR_SHAPE_ATTRS];
    GLuint uniform[NR_UNIFORMS];
    
    BOOL gbLightEnable;
    BOOL gbX;
    BOOL gbY;
    BOOL gbZ;
    
	enum eShapes   
    {
        SPHERE = 0,
        NR_SHAPES,
    };
    enum eShape_Attr
    {
        POSITION = 0,
        NORMAL,
        ELEMENTS,
        NR_SHAPE_ATTRS,
    };
    enum eUniforms
    {
        MV = 0,
        PROJECTION,
        LA,
        LD,
        LS,
        KA,
        KD,
        KS,
        SHININESS,
        LIGHT_POS,
        LIGHT_EN,
        NR_UNIFORMS,
    };
    
    vmath::mat4 projectonMatrix;
}

-(id)initWithFrame:(NSRect)frame
{
    self = [super initWithFrame:frame];
    
    if(self)
    {
        [[self window] setContentView:self];
        
        //Pixel Format Attribute list
        NSOpenGLPixelFormatAttribute attrs [] =
        {
            //key,value(optional)
            NSOpenGLPFAOpenGLProfile,NSOpenGLProfileVersion4_1Core,
            NSOpenGLPFAScreenMask,CGDisplayIDToOpenGLDisplayMask(kCGDirectMainDisplay),
            NSOpenGLPFANoRecovery,   
            NSOpenGLPFAAccelerated,
            NSOpenGLPFAColorSize,24,
            NSOpenGLPFADepthSize,24,
            NSOpenGLPFAAlphaSize,8,
            NSOpenGLPFADoubleBuffer,
            0   //last must be 0 indicating end of list
        };
        
        //Ask NSOpenGL for pixel format by providing above attributes
        NSOpenGLPixelFormat *pixelFormat = [[[NSOpenGLPixelFormat alloc] initWithAttributes:attrs] autorelease];
        if(pixelFormat == nil)
        {
            fprintf(gpFile, "No Valid Pixel Format Available. Exiting..\n");
            [self release];
            [NSApp terminate:self];
        }
        
        //Ask NSOpenGL for context by providing pixelFormat
        NSOpenGLContext *glContext = [[[NSOpenGLContext alloc] initWithFormat:pixelFormat shareContext:nil] autorelease];
        
        //Save context and pixel format in GLView
        [self setPixelFormat:pixelFormat];
        [self setOpenGLContext:glContext];
    }
    return (self);
}

-(CVReturn)getFrameForTime:(const CVTimeStamp*)pOutTime
{
    NSAutoreleasePool *pPool = [[NSAutoreleasePool alloc] init];
    
    [self drawView];
    
    [pPool release];
    return (kCVReturnSuccess);
}

-(void)initializePFShaderProgram
{
    /*****VERTEX SHADER********/
    perFragVertexShaderObject = glCreateShader(GL_VERTEX_SHADER);
    const GLchar* vsSource =
    "#version 410 core" \
    "\n" \
    "in vec4 vPosition;" \
    "in vec3 vNormal;" \
    "uniform mat4 u_model_view_matrix;" \
    "uniform mat4 u_projection_matrix;" \
    "uniform int u_light_enabled;" \
    "uniform vec4 u_light_position;" \
    "out vec3 transformed_normals;" \
    "out vec3 light_direction;" \
    "out vec3 viewer_vector;" \
    "void main(void)" \
    "{" \
    "if(u_light_enabled==1)" \
    "{" \
    "vec4 eyeCoordinates = u_model_view_matrix * vPosition;" \
    "transformed_normals = mat3(u_model_view_matrix) * vNormal;" \
    "light_direction = vec3(u_light_position) - eyeCoordinates.xyz;" \
    "viewer_vector = -eyeCoordinates.xyz;" \
    "}"\
    "gl_Position = u_projection_matrix * u_model_view_matrix * vPosition;" \
    "}";
    
    
    glShaderSource(perFragVertexShaderObject, 1, (const GLchar**)&vsSource, NULL);
    glCompileShader(perFragVertexShaderObject);
    GLint iShaderCompileStatus = 0;
    glGetShaderiv(perFragVertexShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);
    if(GL_FALSE == iShaderCompileStatus)
    {
        GLsizei iLogLen = 0;
        glGetShaderiv(perFragVertexShaderObject, GL_INFO_LOG_LENGTH, &iLogLen);
        if(iLogLen)
        {
            GLchar* szLog = (GLchar*)malloc(iLogLen);
            if(szLog)
            {
                GLsizei written;
                glGetShaderInfoLog(perFragVertexShaderObject, iLogLen, &written, szLog);
                fprintf(gpFile, "Vertex Shader Compile Info: %s\n", szLog);
                free(szLog);
                [self release];
                [NSApp terminate:self];
            }
        }
    }//================
    
    /*****FRAGMENT SHADER********/
    perFragFragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);
    const GLchar* fsSource =
    "#version 410 core" \
    "\n" \
    "in vec3 transformed_normals;" \
    "in vec3 light_direction;" \
    "in vec3 viewer_vector;" \
    "uniform vec3 u_La;" \
    "uniform vec3 u_Ld;" \
    "uniform vec3 u_Ls;" \
    "uniform int u_light_enabled;" \
    "uniform vec3 u_Ka;" \
    "uniform vec3 u_Kd;" \
    "uniform vec3 u_Ks;" \
    "uniform float u_material_shininess;" \
    "out vec4 FragColor;" \
    "void main(void)" \
    "{" \
    "vec3 phong_ads_color; " \
    "if(u_light_enabled == 1)" \
    "{" \
    "vec3 normalized_transformed_normals = normalize(transformed_normals);" \
    "vec3 normalized_light_direction = normalize(light_direction);" \
    "vec3 normalized_viewer_vector = normalize(viewer_vector);" \
    "float tn_dot_ld = max(dot(normalized_transformed_normals, normalized_light_direction), 0.0);" \
    "vec3 ambient = u_La * u_Ka;" \
    "vec3 diffuse = u_Ld * u_Kd * tn_dot_ld;" \
    "vec3 reflection_vector = reflect(-normalized_light_direction, normalized_transformed_normals);" \
    "float r_dot_v = max(dot(reflection_vector, normalized_viewer_vector), 0.0);" \
    "vec3 specular = u_Ls * u_Ks * pow(r_dot_v, u_material_shininess);" \
    "phong_ads_color = ambient + diffuse + specular;" \
    "}" \
    "else" \
    "{" \
    "phong_ads_color = vec3(1.0, 1.0, 1.0);" \
    "}" \
    "FragColor = vec4(phong_ads_color, 1.0);" \
    "}";
    
    glShaderSource(perFragFragmentShaderObject, 1, (const GLchar**)&fsSource, NULL);
    glCompileShader(perFragFragmentShaderObject);
    iShaderCompileStatus = 0;
    glGetShaderiv(perFragFragmentShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);
    if(GL_FALSE == iShaderCompileStatus)
    {
        GLsizei iLogLen = 0;
        glGetShaderiv(perFragFragmentShaderObject, GL_INFO_LOG_LENGTH, &iLogLen);
        if(iLogLen)
        {
            GLchar* szLog = (GLchar*)malloc(iLogLen);
            if(szLog)
            {
                GLsizei written;
                glGetShaderInfoLog(perFragFragmentShaderObject, iLogLen, &written, szLog);
                fprintf(gpFile, "Fragment Shader Compile Info: %s\n", szLog);
                free(szLog);
                [self release];
                [NSApp terminate:self];
            }
        }
    }
    


    /*****SHADER PROGRAM********/
    perFragShaderProgramObject = glCreateProgram();

    glAttachShader(perFragShaderProgramObject, perFragVertexShaderObject);
    glAttachShader(perFragShaderProgramObject, perFragFragmentShaderObject);
    

    glBindAttribLocation(perFragShaderProgramObject, VDG_ATTRIBUTE_POSITION, "vPosition");     // <--- Modify/add as per requirement
    glBindAttribLocation(perFragShaderProgramObject, VDG_ATTRIBUTE_NORMAL, "vNormal");
    
    glLinkProgram(perFragShaderProgramObject);
    
    //uniforms
    uniform[MV] = glGetUniformLocation(perFragShaderProgramObject, "u_model_view_matrix");           // <--- Modify/add as per requirement
    uniform[PROJECTION] = glGetUniformLocation(perFragShaderProgramObject, "u_projection_matrix");
    uniform[LA] = glGetUniformLocation(perFragShaderProgramObject, "u_La");
    uniform[LD] = glGetUniformLocation(perFragShaderProgramObject, "u_Ld");
    uniform[LS] = glGetUniformLocation(perFragShaderProgramObject, "u_Ls");
    uniform[KA] = glGetUniformLocation(perFragShaderProgramObject, "u_Ka");
    uniform[KD] = glGetUniformLocation(perFragShaderProgramObject, "u_Kd");
    uniform[KS] = glGetUniformLocation(perFragShaderProgramObject, "u_Ks");
    uniform[SHININESS] = glGetUniformLocation(perFragShaderProgramObject, "u_material_shininess");
    uniform[LIGHT_POS] = glGetUniformLocation(perFragShaderProgramObject, "u_light_position");
    uniform[LIGHT_EN] = glGetUniformLocation(perFragShaderProgramObject, "u_light_enabled");
}


-(void)prepareOpenGL
{
    //code
    fprintf(gpFile, "OpenGL Version :%s\n", glGetString(GL_VERSION));
    fprintf(gpFile, "GLSL Version   :%s\n", glGetString(GL_SHADING_LANGUAGE_VERSION));
    
    [[self openGLContext] makeCurrentContext];
    

    GLint swapInt = 1;
    [[self openGLContext] setValues:&swapInt forParameter:NSOpenGLCPSwapInterval];
    

    
    [self initializePFShaderProgram];
    
    /*******LOAD TEXTURES****************/

    /*************VERTICES, COLOR, NORMALS, VAO, VBO INITIALIZATIONS******************/
    
    //Sphere
    sphereObject = [[Sphere alloc] init];
    [sphereObject getSphereVertexData:sphere_vertices :sphere_normals :sphere_textures :sphere_elements];
    gNumVertices = [sphereObject getNumberOfSphereVertices];
    gNumElements = [sphereObject getNumberOfSphereElements];
    
   
    //VAO:Sphere
    glGenVertexArrays(1, &vao[SPHERE]);
    glBindVertexArray(vao[SPHERE]);

        glGenBuffers(1, &vbo[SPHERE][POSITION]);
        glBindBuffer(GL_ARRAY_BUFFER, vbo[SPHERE][POSITION]);
        glBufferData(GL_ARRAY_BUFFER, sizeof(sphere_vertices), sphere_vertices, GL_STATIC_DRAW);
        glVertexAttribPointer(VDG_ATTRIBUTE_POSITION,
                              3, 
                              GL_FLOAT,
                              GL_FALSE, 
                              0, NULL); 
        glEnableVertexAttribArray(VDG_ATTRIBUTE_POSITION);
        glBindBuffer(GL_ARRAY_BUFFER, 0);

        glGenBuffers(1, &vbo[SPHERE][NORMAL]);
        glBindBuffer(GL_ARRAY_BUFFER, vbo[SPHERE][NORMAL]);
        glBufferData(GL_ARRAY_BUFFER, sizeof(sphere_normals), sphere_normals, GL_STATIC_DRAW);
        glVertexAttribPointer(VDG_ATTRIBUTE_NORMAL,
                              3, 
                              GL_FLOAT,
                              GL_FALSE,
                              0, NULL);
        glEnableVertexAttribArray(VDG_ATTRIBUTE_NORMAL);
        glBindBuffer(GL_ARRAY_BUFFER, 0);

        glGenBuffers(1, &vbo[SPHERE][ELEMENTS]);
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo[SPHERE][ELEMENTS]);
        
        glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(sphere_elements), sphere_elements, GL_STATIC_DRAW);
        
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);   //unbind

    glBindVertexArray(0);   //unbind

    gbLightEnable = YES; 

    gbX = YES; 
	    gbY = NO;
			gbZ = NO;
    
    //Other OpenGL Settings       
    glClearDepth(1.0f);         
    glEnable(GL_DEPTH_TEST);    
    glDepthFunc(GL_LEQUAL);     
    //glEnable(GL_CULL_FACE);   

    

    glClearColor(0.0f, 0.0f, 0.0f, 1.0f);

    projectonMatrix = vmath::mat4::identity();
    
    

    /*************CVDisplayLink INIT***************************/
    
	CVDisplayLinkCreateWithActiveCGDisplays(&displayLink);
    
	CVDisplayLinkSetOutputCallback(displayLink, &MyDisplayLinkCallback, self);
    
	CGLPixelFormatObj cglPixelFormat = (CGLPixelFormatObj) [[self pixelFormat] CGLPixelFormatObj];
    CGLContextObj cglContext = (CGLContextObj) [[self openGLContext] CGLContextObj];
    
	CVDisplayLinkSetCurrentCGDisplayFromOpenGLContext(displayLink, cglContext, cglPixelFormat);
    
	CVDisplayLinkStart(displayLink);
}

-(void)reshape
{
    //code
    CGLLockContext((CGLContextObj) [[self openGLContext] CGLContextObj]);
    
    NSRect rect = [self bounds];
    GLfloat width = rect.size.width;
    GLfloat height = rect.size.height;
    
    
	glViewport(0, 0, (GLsizei)width, (GLsizei)height);
    
    
	projectonMatrix = vmath::perspective(45.0f, width/height, 0.1f, 100.0f);      
    
    CGLUnlockContext((CGLContextObj) [[self openGLContext] CGLContextObj]);
}

-(void)drawRect:(NSRect)dirtyRect
{
    //code
    [self drawView];
}

//our method
-(void)drawView
{
    //code
    static GLfloat abc = 0.0f;
    static GLint spheree = 5000;    
    
    CGLLockContext((CGLContextObj) [[self openGLContext] CGLContextObj]);
    [[self openGLContext] makeCurrentContext];
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    
    glUseProgram(perFragShaderProgramObject);
    
    //SEND LIGHTING VALUES TO SHADER
    if(gbLightEnable == YES)
    {
        glUniform1i(uniform[LIGHT_EN], 1);
        

        glUniform3fv(uniform[LA], 1, lightAmbient);
        glUniform3fv(uniform[LD], 1, lightDiffuse);
        glUniform3fv(uniform[LS], 1, lightSpecular);
        
        if (gbX == YES)
        {

            lightPosition[0] = 0.0f;
            lightPosition[1] = 200.0f*cos(gAngle);
            lightPosition[2] = 200.0f*sin(gAngle);
            glUniform4fv(uniform[LIGHT_POS], 1, lightPosition);
        }
        else if(gbY == YES)
        {

            lightPosition[0] = 200.0f*cos(gAngle);
            lightPosition[1] = 0.0f;
            lightPosition[2] = 200.0f*sin(gAngle);
            glUniform4fv(uniform[LIGHT_POS], 1, lightPosition);
        }
        else
        {

            lightPosition[0] = 200.0f*cos(gAngle);
            lightPosition[1] = 200.0f*sin(gAngle);
            lightPosition[2] = 0.0f;
            glUniform4fv(uniform[LIGHT_POS], 1, lightPosition);
        }
        
        glUniform3fv(uniform[KA], 1, materialAmbient);
        glUniform3fv(uniform[KD], 1, materialDiffuse);
        glUniform3fv(uniform[KS], 1, materialSpecular);
        glUniform1f(uniform[SHININESS], materialShininess);
    }
    else
    {
        glUniform1i(uniform[LIGHT_EN], 0);
    }
    //==========
    
    for (int c = 0; c < 4; c++)
    {
        for (int r = 0; r < 6; r++)
        {
            //01-Set ModelVeiw and ModelViewProjection matrices to identity
            vmath::mat4 modelViewMatrix = vmath::mat4::identity();
            
            //Keeping each sphere in 24 separate viewports
            NSRect rect = [self bounds];
            GLfloat width = rect.size.width;
            GLfloat height = rect.size.height;
            
            GLfloat viewPortWidth = (width/4);
            GLfloat viewPortHeight = (height/6);
            
            GLfloat x_pos_offset = c * viewPortWidth;
            GLfloat y_pos_offset = r * viewPortHeight;
            glViewport(x_pos_offset, y_pos_offset, viewPortWidth, viewPortHeight);
            projectonMatrix = vmath::perspective(45.0f, viewPortWidth/viewPortHeight, 0.1f, 100.0f);
            
            //02_a: Translate M matrix to deep inside along z axix as we are in Perspective projection
            modelViewMatrix = vmath::translate(0.0f, 0.0f, -2.0f);
            
            //03-Pass above MV and P matrix separately to vertex shader in 'modelViewMatrix',and
            //'u_projection_matrix' respective shader variable whose
            // position value is already calculated in initWithFrame() by using glGetUniformLocation();
            glUniformMatrix4fv(uniform[MV], 1, GL_FALSE, modelViewMatrix);
            glUniformMatrix4fv(uniform[PROJECTION], 1, GL_FALSE, projectonMatrix);
            
            glUniform3fv(uniform[KA], 1, material_ambient[c][r]);
            glUniform3fv(uniform[KD], 1, material_diffuse[c][r]);
            glUniform3fv(uniform[KS], 1, material_specular[c][r]);
            glUniform1f(uniform[SHININESS], material_shininess[c][r]);
            
            //04- Bind Vao
            glBindVertexArray(vao[SPHERE]);
            
            //05- Draw either by glDrawTriagnles(), glDrawArrays(), glDrawElements()
            glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo[SPHERE][ELEMENTS]);
            glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);
            glBindVertexArray(0);   //unbind
        }
    }
    
    glUseProgram(0);    //un-use program
    
    CGLFlushDrawable((CGLContextObj) [[self openGLContext] CGLContextObj]); //flushes data into other framebuffer which is not being displayed
    CGLUnlockContext((CGLContextObj) [[self openGLContext] CGLContextObj]);
    
    //light angle
    abc = abc + 9.0f;
    if (abc >= spheree)
        abc = 0;
    gAngle = (2 * 3.14 * abc) / spheree;
}

//Hardware Event Handling
-(BOOL)acceptsFirstResponder
{
    //code
    [[self window] makeFirstResponder:self];     
    
    return (YES);
}

-(void)keyDown:(NSEvent*)theEvent
{
    //code
    int key = (int) [[theEvent characters] characterAtabc:0];
    switch(key)     
    {
        case 27:    
            [self release];
            [NSApp terminate:self];
            break;
        case 'f':   
        case 'F':
            [[self window] toggleFullScreen:self];
            break;
        case 'L':
        case 'l':
            if(gbLightEnable == NO)
                gbLightEnable = YES;
            else
                gbLightEnable = NO;
            break;
            
        case 'x':
        case 'X':
            gbX = YES;
            gbY = NO;
            gbZ = NO;
            break;
        case 'y':
        case 'Y':
            gbX = NO;
            gbY = YES;
            gbZ = NO;
            break;
        case 'z':
        case 'Z':
            gbX = NO;
            gbY = NO;
            gbZ = YES;
            break;
            
        default:
            break;
    }
}

-(void)mouseDown:(NSEvent*)theEvent
{
    //code
}

-(void)mouseDragged:(NSEvent*)theEvent
{
    //code
}

-(void)rightMouseDown:(NSEvent*)theEvent
{
    //code
}

-(void)dealloc
{
    //stop and release Core Video link
    CVDisplayLinkStop(displayLink);
    CVDisplayLinkRelease(displayLink);
    
    //Release vao
    for(int i=NR_SHAPES-1; i>=0; i--)
    {
        if(vao[i])
        {
            glDeleteVertexArrays(1, &vao[i]);
            vao[i] = 0;
        }
    }
    glDeleteVertexArrays(NR_SHAPES, vao);
    
    //Release vbo, As I have array of vao objects, i would have released this in 'glDeleteVertexArrays(NR_SHAPES, vao)' way.
    //But at this point of time, i am not sure, all declared vao objects have been initialized or not.
    //So its good to check and release vao objects one by one, though it looks funny :-)
    for(int i=NR_SHAPES-1; i>=0; i--)
    {
        for(int j=NR_SHAPE_ATTRS-1; i>=0; i--)
        {
            if(vbo[i][j])
            {
                glDeleteBuffers(1, &vbo[i][j]);
                vbo[i][j] = 0;
            }
        }
    }
    
    //Release Shader objects
    if(perFragShaderProgramObject)
    {
        if(perFragFragmentShaderObject)
        {
            glDetachShader(perFragShaderProgramObject, perFragFragmentShaderObject);
            glDeleteShader(perFragFragmentShaderObject);
            perFragFragmentShaderObject = 0;
        }
        if(perFragVertexShaderObject)
        {
            glDetachShader(perFragShaderProgramObject, perFragVertexShaderObject);
            glDeleteShader(perFragVertexShaderObject);
            perFragVertexShaderObject = 0;
        }
        
        glDeleteProgram(perFragShaderProgramObject);
        perFragShaderProgramObject = 0;
    }
    
    //Un-use shader program
    glUseProgram(0);
    
    [sphereObject release];
    [super dealloc];
}

@end

//This callback is called on display refresh rate
CVReturn MyDisplayLinkCallback(CVDisplayLinkRef displayLink, const CVTimeStamp* pNow, const CVTimeStamp *pOutTime, CVOptionFlags FlagsIn, CVOptionFlags *FlagsOut, void* pCVDisplayContext)
{
    CVReturn ret = kCVReturnSuccess;
    
    GLView *glView = (GLView*) pCVDisplayContext;
    
    ret = (CVReturn) [glView getFrameForTime:pOutTime];
    
    return (ret);
}
