#ifndef _MATERIAL_PROPERTIES_H_
#define _MATERIAL_PROPERTIES_H_

//#include <gl/GL.h>
// ***** 1st sphere on 1st column, emerald *****
// ambient material
GLfloat emerald_mat_ambient[] = {
    0.0215f, // r
    0.1745f, // g
    0.0215f, // b
    1.0f   // a
};

// difuse material
GLfloat emerald_mat_diffuse[] = {
    0.07568f, // r
    0.61424f, // g
    0.07568f, // b
    1.0f    // a
};

// specular material
GLfloat emerald_mat_specular[] = {
    0.633f,    // r
    0.727811f, // g
    0.633f,    // b
    1.0f     // a
};

// shininess
GLfloat emerald_mat_shininess = 0.6f * 128;

// ***** 2nd sphere on 1st column, jade *****
// ambient material
GLfloat jade_mat_ambient[] = {
    0.135f,        // r
    0.2225f,    // g
    0.1575f,    // b
    1.0f // a
};

// difuse material
GLfloat jade_mat_diffuse[] = {
    0.54f,    // r
    0.89f,    // g
    0.63f,    // b
    1.0f    // a
};

// specular material
GLfloat jade_mat_specular[] = {
    0.316228f, // r
    0.316228f, // g
    0.316228f, // b
    1.0f     // a
};

// shininess
GLfloat jade_mat_shininess = 0.1f * 128;

// ***** 3rd sphere on 1st column, obsidian *****
// ambient material
GLfloat obsidian_mat_ambient[] = {
    0.05375f, // r
    0.05f,    // g
    0.06625f, // b
    1.0f    // a
};

// difuse material
GLfloat obsidian_mat_diffuse[] = {
    0.18275f, // r
    0.17f,    // g
    0.22525f, // b
    1.0f    // a
};
// specular material
GLfloat obsidian_mat_specular[] = {
    0.332741f, // r
    0.328634f, // g
    0.346435f, // b
    1.0f     // a
};
// shininess
GLfloat obsidian_mat_shininess = 0.3f * 128;

// ***** 4th sphere on 1st column, pearl *****
// ambient material
GLfloat pearl_mat_ambient[] = {
    0.25f,    // r
    0.20725f, // g
    0.20725f, // b
    1.0f    // a
};

// difuse material
GLfloat pearl_mat_diffuse[] = {
    1.0f,   // r
    0.829f, // g
    0.829f, // b
    1.0f  // a
};

// specular material
GLfloat pearl_mat_specular[] = {
    0.296648f, // r
    0.296648f, // g
    0.296648f, // b
    1.0f     // a
};
// shininess
GLfloat pearl_mat_shininess = 0.088f * 128;

// ***** 5th sphere on 1st column, ruby *****
// ambient material
GLfloat ruby_mat_ambient[] = {
    0.1745f,  // r
    0.01175f, // g
    0.01175f, // b
    1.0f    // a
};

// difuse material
GLfloat ruby_mat_diffuse[] = {
    0.61424f, // r
    0.04136f, // g
    0.04136f, // b
    1.0f    // a
};

// specular material
GLfloat ruby_mat_specular[] = {
    0.727811f, // r
    0.626959f, // g
    0.626959f, // b
    1.0f     // a
};
// shininess
GLfloat ruby_mat_shininess = 0.6f * 128;

// ***** 6th sphere on 1st column, turquoise *****
// ambient material
GLfloat turquoise_mat_ambient[] = {
    0.1f,     // r
    0.18725f, // g
    0.1745f,  // b
    1.0f    // a
};

// difuse material
GLfloat turquoise_mat_diffuse[] = {
    0.396f,   // r
    0.74151f, // g
    0.69102f, // b
    1.0f    // a
};

// specular material
GLfloat turquoise_mat_specular[] = {
    0.297254f, // r
    0.30829f,  // g
    0.306678f, // b
    1.0f     // a
};
// shininess
GLfloat turquoise_mat_shininess = 0.1f * 128;

// ***** 1st sphere on 2nd column, brass *****
// ambient material
GLfloat brass_mat_ambient[] = {
    0.329412f, // r
    0.223529f, // g
    0.027451f, // b
    1.0f     // a
};

// difuse material
GLfloat brass_mat_diffuse[] = {
    0.780392f, // r
    0.568627f, // g
    0.113725f, // b
    1.0f     // a
};

// specular material
GLfloat brass_mat_specular[] = {
    0.992157f, // r
    0.941176f, // g
    0.807843f, // b
    1.0f     // a
};
// shininess
GLfloat brass_mat_shininess = 0.21794872f * 128;

// ***** 2nd sphere on 2nd column, bronze *****
// ambient material
GLfloat bronze_mat_ambient[] = {
    0.2125f, // r
    0.1275f, // g
    0.054f,  // b
    1.0f   // a
};

// difuse material
GLfloat bronze_mat_diffuse[] = {
    0.714f,   // r
    0.4284f,  // g
    0.18144f, // b
    1.0f    // a
};

// specular material
GLfloat bronze_mat_specular[] = {
    0.393548f, // r
    0.271906f, // g
    0.166721f, // b
    1.0f     // a
};
// shininess
GLfloat bronze_mat_shininess = 0.2f * 128;

// ***** 3rd sphere on 2nd column, chrome *****
// ambient material
GLfloat chrome_mat_ambient[] = {
    0.25f, // r
    0.25f, // g
    0.25f, // b
    1.0f // a
};

// difuse material
GLfloat chrome_mat_diffuse[] = {
    0.4f,  // r
    0.4f,  // g
    0.4f,  // b
    1.0f // a
};

// specular material
GLfloat chrome_mat_specular[] = {
    0.774597f, // r
    0.774597f, // g
    0.774597f, // b
    1.0f     // a
};
// shininess
GLfloat chrome_mat_shininess = 0.6f * 128;

// ***** 4th sphere on 2nd column, copper *****
// ambient material
GLfloat copper_mat_ambient[] = {
    0.19125f, // r
    0.0735f,  // g
    0.0225f,  // b
    1.0f    // a
};

// difuse material
GLfloat copper_mat_diffuse[] = {
    0.7038f,  // r
    0.27048f, // g
    0.0828f,  // b
    1.0f    // a
};

// specular material
GLfloat copper_mat_specular[] = {
    0.256777f, // r
    0.137622f, // g
    0.086014f, // b
    1.0f     // a
};
// shininess
GLfloat copper_mat_shininess = 0.1f * 128;

// ***** 5th sphere on 2nd column, gold *****
// ambient material
GLfloat gold_mat_ambient[] = {
    0.24725f, // r
    0.1995f,  // g
    0.0745f,  // b
    1.0f    // a
};

// difuse material
GLfloat gold_mat_diffuse[] = {
    0.75164f, // r
    0.60648f, // g
    0.22648f, // b
    1.0f    // a
};

// specular material
GLfloat gold_mat_specular[] = {
    0.628281f, // r
    0.555802f, // g
    0.366065f, // b
    1.0f     // a
};
// shininess
GLfloat gold_mat_shininess = 0.4f * 128;


// ***** 6th sphere on 2nd column, silver *****
// ambient material
GLfloat silver_mat_ambient[] = {
    0.19225f, // r
    0.19225f, // g
    0.19225f, // b
    1.0f    // a
};

// difuse material
GLfloat silver_mat_diffuse[] = {
    0.50754f, // r
    0.50754f, // g
    0.50754f, // b
    1.0f    // a
};

// specular material
GLfloat silver_mat_specular[] = {
    0.508273f, // r
    0.508273f, // g
    0.508273f, // b
    1.0f     // a
};
// shininess
GLfloat silver_mat_shininess = 0.4f * 128;


// ***** 1st sphere on 3rd column, black *****
// ambient material
GLfloat black1_mat_ambient[] = {
    0.0f,  // r
    0.0f,  // g
    0.0f,  // b
    1.0f // a
};

// difuse material
GLfloat black1_mat_diffuse[] = {
    0.01f, // r
    0.01f, // g
    0.01f, // b
    1.0f // a
};

// specular material
GLfloat black1_mat_specular[] = {
    0.50f, // r
    0.50f, // g
    0.50f, // b
    1.0f // a
};
// shininess
GLfloat black1_mat_shininess = 0.25f * 128;


// ***** 2nd sphere on 3rd column, cyan *****
// ambient material
GLfloat cyan1_mat_ambient[] = {
    0.0f,  // r
    0.1f,  // g
    0.06f, // b
    1.0f // a
};

// difuse material
GLfloat cyan1_mat_diffuse[] = {
    0.0f,        // r
    0.50980392f, // g
    0.50980392f, // b
    1.0f       // a
};

// specular material
GLfloat cyan1_mat_specular[] = {
    0.50196078f, // r
    0.50196078f, // g
    0.50196078f, // b
    1.0f       // a
};
// shininess
GLfloat cyan1_mat_shininess = 0.25f * 128;

// ***** 3rd sphere on 2nd column, green *****
// ambient material
GLfloat green1_mat_ambient[] = {
    0.0f,  // r
    0.0f,  // g
    0.0f,  // b
    1.0f // a
};

// difuse material
GLfloat green1_mat_diffuse[] = {
    0.1f,  // r
    0.35f, // g
    0.1f,  // b
    1.0f // a
};

// specular material
GLfloat green1_mat_specular[] = {
    0.45f, // r
    0.55f, // g
    0.45f, // b
    1.0f // a
};
// shininess
GLfloat green1_mat_shininess = 0.25f * 128;

// ***** 4th sphere on 3rd column, red *****
// ambient material
GLfloat red1_mat_ambient[] = {
    0.0f,  // r
    0.0f,  // g
    0.0f,  // b
    1.0f // a
};

// difuse material
GLfloat red1_mat_diffuse[] = {
    0.5f,  // r
    0.0f,  // g
    0.0f,  // b
    1.0f // a
};

// specular material
GLfloat red1_mat_specular[] = {
    0.7f,  // r
    0.6f,  // g
    0.6f,  // b
    1.0f // a
};

// shininess
GLfloat red1_mat_shininess = 0.25f * 128;

// ***** 5th sphere on 3rd column, white *****
// ambient material
GLfloat white1_mat_ambient[] = {
    0.0f,  // r
    0.0f,  // g
    0.0f,  // b
    1.0f // a
};

// difuse material
GLfloat white1_mat_diffuse[] = {
    0.55f, // r
    0.55f, // g
    0.55f, // b
    1.0f // a
};

// specular material
GLfloat white1_mat_specular[] = {
    0.70f, // r
    0.70f, // g
    0.70f, // b
    1.0f // a
};
// shininess
GLfloat white1_mat_shininess = 0.25f * 128;

// ***** 6th sphere on 3rd column, yellow plastic *****
// ambient material
GLfloat yellow_plastic_mat_ambient[] = {
    0.0f,  // r
    0.0f,  // g
    0.0f,  // b
    1.0f // a
};

// difuse material
GLfloat yellow_plastic_mat_diffuse[] = {
    0.5f,  // r
    0.5f,  // g
    0.0f,  // b
    1.0f // a
};

// specular material
GLfloat yellow_plastic_mat_specular[] = {
    0.60f, // r
    0.60f, // g
    0.50f, // b
    1.0f // a
};
// shininess
GLfloat yellow_plastic_mat_shininess = 0.25f * 128;

// ***** 1st sphere on 4th column, black *****
// ambient material
GLfloat black2_mat_ambient[] = {
    0.02f, // r
    0.02f, // g
    0.02f, // b
    1.0f // a
};

// difuse material
GLfloat black2_mat_diffuse[] = {
    0.01f, // r
    0.01f, // g
    0.01f, // b
    1.0f // a
};

// specular material
GLfloat black2_mat_specular[] = {
    0.4f,  // r
    0.4f,  // g
    0.4f,  // b
    1.0f // a
};
// shininess
GLfloat black2_mat_shininess = 0.078125f * 128;

// ***** 2nd sphere on 4th column, cyan *****
// ambient material
GLfloat cyan2_mat_ambient[] = {
    0.0f,  // r
    0.05f, // g
    0.05f, // b
    1.0f // a
};

// difuse material
GLfloat cyan2_mat_diffuse[] = {
    0.4f,  // r
    0.5f,  // g
    0.5f,  // b
    1.0f // a
};

// specular material
GLfloat cyan2_mat_specular[] = {
    0.04f, // r
    0.7f,  // g
    0.7f,  // b
    1.0f // a
};
// shininess
GLfloat cyan2_mat_shininess = 0.078125f * 128;


// ***** 3rd sphere on 4th column, green *****
// ambient material
GLfloat green2_mat_ambient[] = {
    0.0f,  // r
    0.05f, // g
    0.0f,  // b
    1.0f // a
};

// difuse material
GLfloat green2_mat_diffuse[] = {
    0.4f,  // r
    0.5f,  // g
    0.4f,  // b
    1.0f // a
};

// specular material
GLfloat green2_mat_specular[] = {
    0.04f, // r
    0.7f,  // g
    0.04f, // b
    1.0f // a
};
// shininess
GLfloat green2_mat_shininess = 0.078125f * 128;

// ***** 4th sphere on 4th column, red *****
// ambient material
GLfloat red2_mat_ambient[] = {
    0.05f, // r
    0.0f,  // g
    0.0f,  // b
    1.0f // a
};

// difuse material
GLfloat red2_mat_diffuse[] = {
    0.5f,  // r
    0.4f,  // g
    0.4f,  // b
    1.0f // a
};

// specular material
GLfloat red2_mat_specular[] = {
    0.7f,  // r
    0.04f, // g
    0.04f, // b
    1.0f // a
};
// shininess
GLfloat red2_mat_shininess = 0.078125f * 128;


// ***** 5th sphere on 4th column, white *****
// ambient material
GLfloat white2_mat_ambient[] = {
    0.05f, // r
    0.05f, // g
    0.05f, // b
    1.0f // a
};

// difuse material
GLfloat white2_mat_diffuse[] = {
    0.5f,  // r
    0.5f,  // g
    0.5f,  // b
    1.0f // a
};

// specular material
GLfloat white2_mat_specular[] = {
    0.7f,  // r
    0.7f,  // g
    0.7f,  // b
    1.0f // a
};
// shininess
GLfloat white2_mat_shininess = 0.078125f * 128;

// ***** 6th sphere on 4th column, yellow rubber *****
// ambient material
GLfloat yellow_rubber_mat_ambient[] = {
    0.05f, // r
    0.05f, // g
    0.0f,  // b
    1.0f // a
};

// difuse material
GLfloat yellow_rubber_mat_diffuse[] = {
    0.5f,  // r
    0.5f,  // g
    0.4f,  // b
    1.0f // a
};

// specular material
GLfloat yellow_rubber_mat_specular[] = {
    0.7f,  // r
    0.7f,  // g
    0.04f, // b
    1.0f // a
};
// shininess
GLfloat yellow_rubber_mat_shininess = 0.078125f * 128;

//c  r
GLfloat* material_ambient[4][6] = {
    {emerald_mat_ambient, jade_mat_ambient,     obsidian_mat_ambient, pearl_mat_ambient, ruby_mat_ambient, turquoise_mat_ambient},
    {brass_mat_ambient, bronze_mat_ambient,     chrome_mat_ambient, copper_mat_ambient, gold_mat_ambient, silver_mat_ambient },
    {black1_mat_ambient, cyan1_mat_ambient, green1_mat_ambient, red1_mat_ambient, white1_mat_ambient, yellow_plastic_mat_ambient},
    {black2_mat_ambient, cyan2_mat_ambient, green2_mat_ambient, red2_mat_ambient, white2_mat_ambient, yellow_rubber_mat_ambient}
};

//c  r
GLfloat* material_diffuse[4][6] = {
    { emerald_mat_diffuse, jade_mat_diffuse,     obsidian_mat_diffuse, pearl_mat_diffuse, ruby_mat_diffuse, turquoise_mat_diffuse },
    { brass_mat_diffuse, bronze_mat_diffuse,     chrome_mat_diffuse, copper_mat_diffuse, gold_mat_diffuse, silver_mat_diffuse },
    { black1_mat_diffuse, cyan1_mat_diffuse, green1_mat_diffuse, red1_mat_diffuse, white1_mat_diffuse, yellow_plastic_mat_diffuse },
    { black2_mat_diffuse, cyan2_mat_diffuse, green2_mat_diffuse, red2_mat_diffuse, white2_mat_diffuse, yellow_rubber_mat_diffuse }
};

//c  r
GLfloat* material_specular[4][6] = {
    { emerald_mat_specular, jade_mat_specular,     obsidian_mat_specular, pearl_mat_specular, ruby_mat_specular, turquoise_mat_specular },
    { brass_mat_specular, bronze_mat_specular,     chrome_mat_specular, copper_mat_specular, gold_mat_specular, silver_mat_specular },
    { black1_mat_specular, cyan1_mat_specular, green1_mat_specular, red1_mat_specular, white1_mat_specular, yellow_plastic_mat_specular },
    { black2_mat_specular, cyan2_mat_specular, green2_mat_specular, red2_mat_specular, white2_mat_specular, yellow_rubber_mat_specular }
};

//c  r
GLfloat material_shininess[4][6] = {
    { emerald_mat_shininess, jade_mat_shininess,     obsidian_mat_shininess, pearl_mat_shininess, ruby_mat_shininess, turquoise_mat_shininess },
    { brass_mat_shininess, bronze_mat_shininess,     chrome_mat_shininess, copper_mat_shininess, gold_mat_shininess, silver_mat_shininess },
    { black1_mat_shininess, cyan1_mat_shininess, green1_mat_shininess, red1_mat_shininess, white1_mat_shininess, yellow_plastic_mat_shininess },
    { black2_mat_shininess, cyan2_mat_shininess, green2_mat_shininess, red2_mat_shininess, white2_mat_shininess, yellow_rubber_mat_shininess }
};
#endif // !_MATERIAL_PROPERTIES_H_
