#import <Foundation/Foundation.h>
#import <Cocoa/Cocoa.h>

//Core Video
#import <QuartzCore/CVDisplayLink.h>

//OpenGL
#import <OpenGL/gl3.h>
#import <OpenGL/gl3ext.h>

#import "vmath.h"

#import "sphere.h"

enum
{
    PRP_ATTRIBUTE_POSITION = 0,
    PRP_ATTRIBUTE_COLOR,
    PRP_ATTRIBUTE_NORMAL,
    PRP_ATTRIBUTE_TEXTURE0,
};

GLfloat lightAmbient[] = { 0.0f,0.0f,0.0f,1.0f };
GLfloat lightDiffuse[] = { 1.0f,1.0f,1.0f,1.0f };
GLfloat lightSpecular[] = { 1.0f,1.0f,1.0f,1.0f };
GLfloat lightPosition[] = { 100.0f,100.0f,100.0f,1.0f };

GLfloat materialAmbient[] = { 0.0f, 0.0f,0.0f,1.0f };
GLfloat materialDiffuse[] = { 1.0f,1.0f,1.0f,1.0f };
GLfloat materialSpecular[] = { 1.0f, 1.0f,1.0f,1.0f };
GLfloat materialShininess = 50.0f;


//Sphere declarations
GLuint gNumElements;
GLuint gNumVertices;
GLfloat sphere_vertices[1146];
GLfloat sphere_normals[1146];
GLfloat sphere_textures[764];
GLshort sphere_elements[2280];

//log file pointer
FILE *gpFile = NULL;

// 'C' style function declaration
CVReturn MyDisplayLinkCallback(CVDisplayLinkRef, const CVTimeStamp*, const CVTimeStamp*, CVOptionFlags, CVOptionFlags*, void*);

//interface declarations
@interface AppDelegate: NSObject <NSApplicationDelegate, NSWindowDelegate>
@end

@interface GLView: NSOpenGLView
@end

//entry point function
int main(int argc, const char *argv[])
{
    NSAutoreleasePool *pPool = [[NSAutoreleasePool alloc] init];
    
    NSApp = [NSApplication sharedApplication];
    
    [NSApp setDelegate:[[AppDelegate alloc] init]];
    
    [NSApp run];    //run loop
    
    [pPool release];
    
    return (0);
}

//implementations
@implementation AppDelegate
{
@private
    NSWindow *window;
    GLView *glView;
}

-(void)applicationDidFinishLaunching:(NSNotification*)aNotification
{
    //code
    //create log file
    NSBundle *bundle = [NSBundle mainBundle];
    NSString *appDirPath = [bundle bundlePath];
    NSString *appDirParentPath = [appDirPath stringByDeletingLastPathComponent];
    NSString *logFilePath = [NSString stringWithFormat:@"%@/Log.txt",appDirParentPath];
    //convert NSSTring to char*
    const char* szLogFilePath = [logFilePath cStringUsingEncoding:NSASCIIStringEncoding];
    
    gpFile = fopen(szLogFilePath, "w");
    if(gpFile == NULL)
    {
        printf("Cannot Open Log.txt. Exiting...\n");
        [self release];
        [NSApp terminate:self];
    }
    else
    {
        fprintf(gpFile, "Applicaton Is Started Successfully\n");
    }
    
    //create window
    NSRect win_rect = NSMakeRect(0.0, 0.0, 800.0, 600.0);
    window = [[NSWindow alloc] initWithContentRect:win_rect
                                         styleMask:NSWindowStyleMaskTitled | NSWindowStyleMaskClosable | NSWindowStyleMaskMiniaturizable | NSWindowStyleMaskResizable
                                           backing:NSBackingStoreBuffered
                                             defer:NO];
    
    //set window properties
    [window setTitle:@"macOS OpenGL --> Per Vertex(V) Per Fragment(F) Phong Shading Lighting Model"];     // <--- Modify as per requirement
    [window center];
    
    //create OpenGL view
    glView = [[GLView alloc] initWithFrame:[window frame]];
    
    if(glView == nil)
    {
        fprintf(gpFile, "Unable to Create GLView. Exiting...\n");
        [self release];
        [NSApp terminate:self];
    }
    
    //attach glview to window
    [window setContentView:glView];
    [window setDelegate:self];
    [window makeKeyAndOrderFront:self];
}

-(void)applicationWillTerminate:(NSNotification*)aNotification
{
    fprintf(gpFile, "Application Is Close Successfully\n");
    if(gpFile)
    {
        fclose(gpFile);
        gpFile = NULL;
    }
}

-(void)windowWillClose:(NSNotification*)aNotification
{
    //code
    [NSApp terminate:self];
}

-(void)dealloc
{
    //code
    [glView release];
    [window release];
    
    [super dealloc];
}
@end

@implementation GLView
{
@private
    CVDisplayLinkRef displayLink;
    
    Sphere *sphr;
    
    //your shader variables
    GLuint vertexShaderObject_PV;
    GLuint fragmentShaderObject_PV;
    GLuint shaderProgramObject_PV;
    
    GLuint vertexShaderObject_PF;
    GLuint fragmentShaderObject_PF;
    GLuint shaderProgramObject_PF;
    
    /*************
     Dont Keep Un-used enums in this code,
     else vao, vbo, uniform arrays will be populated unnecessarly
     ***************/
    enum eShapes            // <--- Modify as per requirement
    {
        SPHERE = 0,
        NR_SHAPES,
    };
    enum eShape_Attr
    {
        POSITION = 0,
        NORMAL,
        ELEMENTS,
        NR_SHAPE_ATTRS,
    };
    enum eUniforms
    {
        MV = 0,
        PROJECTION,
        LA,
        KA,
        LD,
        KD,
        LS,
        KS,
        SHININESS,
        LIGHT_POS,
        LIGHT_EN,
        NR_UNIFORMS,
    };
    
    GLuint vao[NR_SHAPES];
    GLuint vbo[NR_SHAPES][NR_SHAPE_ATTRS];
    GLuint uniform[NR_UNIFORMS];
    
    BOOL bLighting_enable;
    BOOL bPerVertex;
    BOOL bPerFragment;
    
    vmath::mat4 projectonMatrix;
}

-(id)initWithFrame:(NSRect)frame
{
    self = [super initWithFrame:frame];
    
    if(self)
    {
        [[self window] setContentView:self];
        
        //Pixel Format Attribute list
        NSOpenGLPixelFormatAttribute attrs [] =
        {
            //key,value(optional)
            NSOpenGLPFAOpenGLProfile,NSOpenGLProfileVersion4_1Core,
            NSOpenGLPFAScreenMask,CGDisplayIDToOpenGLDisplayMask(kCGDirectMainDisplay),
            NSOpenGLPFANoRecovery,   //dont fallback to sw renderer if hw doesnot found
            NSOpenGLPFAAccelerated,
            NSOpenGLPFAColorSize,24,
            NSOpenGLPFADepthSize,24,
            NSOpenGLPFAAlphaSize,8,
            NSOpenGLPFADoubleBuffer,
            0   //last must be 0 indicating end of list
        };
        
        //Ask NSOpenGL for pixel format by providing above attributes
        NSOpenGLPixelFormat *pixelFormat = [[[NSOpenGLPixelFormat alloc] initWithAttributes:attrs] autorelease];
        if(pixelFormat == nil)
        {
            fprintf(gpFile, "No Valid Pixel Format Available. Exiting..\n");
            [self release];
            [NSApp terminate:self];
        }
        
        //Ask NSOpenGL for context by providing pixelFormat
        NSOpenGLContext *glContext = [[[NSOpenGLContext alloc] initWithFormat:pixelFormat shareContext:nil] autorelease];
        
        //Save context and pixel format in GLView
        [self setPixelFormat:pixelFormat];
        [self setOpenGLContext:glContext];
    }
    return (self);
}

-(CVReturn)getFrameForTime:(const CVTimeStamp*)pOutTime
{
    NSAutoreleasePool *pPool = [[NSAutoreleasePool alloc] init];
    
    [self drawView];
    
    [pPool release];
    return (kCVReturnSuccess);
}

-(void)initializePFShaderProgram
{
    /*****VERTEX SHADER********/
    vertexShaderObject_PF = glCreateShader(GL_VERTEX_SHADER);
    const GLchar* vsSource =
    "#version 410 core" \
    "\n" \
    "in vec4 vPosition;" \
    "in vec3 vNormal;" \
    "uniform mat4 u_model_view_matrix;" \
    "uniform mat4 u_projection_matrix;" \
    "uniform int u_light_enabled;" \
    "uniform vec4 u_light_position;" \
    "out vec3 transformed_normals;" \
    "out vec3 light_direction;" \
    "out vec3 viewer_vector;" \
    "void main(void)" \
    "{" \
    "if(u_light_enabled==1)" \
    "{" \
    "vec4 eyeCoordinates = u_model_view_matrix * vPosition;" \
    "transformed_normals = mat3(u_model_view_matrix) * vNormal;" \
    "light_direction = vec3(u_light_position) - eyeCoordinates.xyz;" \
    "viewer_vector = -eyeCoordinates.xyz;" \
    "}"\
    "gl_Position = u_projection_matrix * u_model_view_matrix * vPosition;" \
    "}";
    
    
    glShaderSource(vertexShaderObject_PF, 1, (const GLchar**)&vsSource, NULL);
    glCompileShader(vertexShaderObject_PF);
    GLint iShaderCompileStatus = 0;
    glGetShaderiv(vertexShaderObject_PF, GL_COMPILE_STATUS, &iShaderCompileStatus);
    if(GL_FALSE == iShaderCompileStatus)
    {
        GLsizei iLogLen = 0;
        glGetShaderiv(vertexShaderObject_PF, GL_INFO_LOG_LENGTH, &iLogLen);
        if(iLogLen)
        {
            GLchar* szLog = (GLchar*)malloc(iLogLen);
            if(szLog)
            {
                GLsizei written;
                glGetShaderInfoLog(vertexShaderObject_PF, iLogLen, &written, szLog);
                fprintf(gpFile, "Vertex Shader Compile Info: %s\n", szLog);
                free(szLog);
                [self release];
                [NSApp terminate:self];
            }
        }
    }//================
    
    /*****FRAGMENT SHADER********/
    fragmentShaderObject_PF = glCreateShader(GL_FRAGMENT_SHADER);
    const GLchar* fsSource =
    "#version 400 core" \
    "\n" \
    "in vec3 transformed_normals;" \
    "in vec3 light_direction;" \
    "in vec3 viewer_vector;" \
    "uniform vec3 u_La;" \
    "uniform vec3 u_Ld;" \
    "uniform vec3 u_Ls;" \
    "uniform int u_light_enabled;" \
    "uniform vec3 u_Ka;" \
    "uniform vec3 u_Kd;" \
    "uniform vec3 u_Ks;" \
    "uniform float u_material_shininess;" \
    "out vec4 FragColor;" \
    "void main(void)" \
    "{" \
    "vec3 phong_ads_color; " \
    "if(u_light_enabled == 1)" \
    "{" \
    "vec3 normalized_transformed_normals = normalize(transformed_normals);" \
    "vec3 normalized_light_direction = normalize(light_direction);" \
    "vec3 normalized_viewer_vector = normalize(viewer_vector);" \
    "float tn_dot_ld = max(dot(normalized_transformed_normals, normalized_light_direction), 0.0);" \
    "vec3 ambient = u_La * u_Ka;" \
    "vec3 diffuse = u_Ld * u_Kd * tn_dot_ld;" \
    "vec3 reflection_vector = reflect(-normalized_light_direction, normalized_transformed_normals);" \
    "float r_dot_v = max(dot(reflection_vector, normalized_viewer_vector), 0.0);" \
    "vec3 specular = u_Ls * u_Ks * pow(r_dot_v, u_material_shininess);" \
    "phong_ads_color = ambient + diffuse + specular;" \
    "}" \
    "else" \
    "{" \
    "phong_ads_color = vec3(1.0, 1.0, 1.0);" \
    "}" \
    "FragColor = vec4(phong_ads_color, 1.0);" \
    "}";
    
    glShaderSource(fragmentShaderObject_PF, 1, (const GLchar**)&fsSource, NULL);
    glCompileShader(fragmentShaderObject_PF);
    iShaderCompileStatus = 0;
    glGetShaderiv(fragmentShaderObject_PF, GL_COMPILE_STATUS, &iShaderCompileStatus);
    if(GL_FALSE == iShaderCompileStatus)
    {
        GLsizei iLogLen = 0;
        glGetShaderiv(fragmentShaderObject_PF, GL_INFO_LOG_LENGTH, &iLogLen);
        if(iLogLen)
        {
            GLchar* szLog = (GLchar*)malloc(iLogLen);
            if(szLog)
            {
                GLsizei written;
                glGetShaderInfoLog(fragmentShaderObject_PF, iLogLen, &written, szLog);
                fprintf(gpFile, "Fragment Shader Compile Info: %s\n", szLog);
                free(szLog);
                [self release];
                [NSApp terminate:self];
            }
        }
    }//===================
    
    /*****SHADER PROGRAM********/
    shaderProgramObject_PF = glCreateProgram();
    //attach vs and fs to shader program
    glAttachShader(shaderProgramObject_PF, vertexShaderObject_PF);
    glAttachShader(shaderProgramObject_PF, fragmentShaderObject_PF);
    
    //pre-link binding of vertex attributes
    glBindAttribLocation(shaderProgramObject_PF, PRP_ATTRIBUTE_POSITION, "vPosition");     // <--- Modify/add as per requirement
    glBindAttribLocation(shaderProgramObject_PF, PRP_ATTRIBUTE_NORMAL, "vNormal");
    
    //link program
    glLinkProgram(shaderProgramObject_PF);
    
    //post-link, get uniform locations from shaders
    uniform[MV] = glGetUniformLocation(shaderProgramObject_PF, "u_model_view_matrix");           // <--- Modify/add as per requirement
    uniform[PROJECTION] = glGetUniformLocation(shaderProgramObject_PF, "u_projection_matrix");
    uniform[LA] = glGetUniformLocation(shaderProgramObject_PF, "u_La");
    uniform[KA] = glGetUniformLocation(shaderProgramObject_PF, "u_Ka");
    uniform[LD] = glGetUniformLocation(shaderProgramObject_PF, "u_Ld");
    uniform[KD] = glGetUniformLocation(shaderProgramObject_PF, "u_Kd");
    uniform[LS] = glGetUniformLocation(shaderProgramObject_PF, "u_Ls");
    uniform[KS] = glGetUniformLocation(shaderProgramObject_PF, "u_Ks");
    uniform[SHININESS] = glGetUniformLocation(shaderProgramObject_PF, "u_material_shininess");
    uniform[LIGHT_POS] = glGetUniformLocation(shaderProgramObject_PF, "u_light_position");
    uniform[LIGHT_EN] = glGetUniformLocation(shaderProgramObject_PF, "u_light_enabled");
}

-(void)initializePVShaderProgram
{
    /*****VERTEX SHADER********/
    vertexShaderObject_PV = glCreateShader(GL_VERTEX_SHADER);
    const GLchar* vsSource =
    "#version 410 core" \
    "\n" \
    "in vec4 vPosition;" \
    "in vec3 vNormal;" \
    "uniform mat4 u_model_view_matrix;" \
    "uniform mat4 u_projection_matrix;" \
    "uniform int u_light_enabled;" \
    "uniform vec4 u_light_position;" \
    "uniform vec3 u_La;" \
    "uniform vec3 u_Ld;" \
    "uniform vec3 u_Ls;" \
    "uniform vec3 u_Ka;" \
    "uniform vec3 u_Kd;" \
    "uniform vec3 u_Ks;" \
    "uniform float u_material_shininess;" \
    "out vec3 phong_ads_color;" \
    "void main(void)" \
    "{" \
    "if(u_light_enabled==1)" \
    "{" \
    "vec3 ambient = u_La * u_Ka;" \
    "vec4 eyeCoordinates = u_model_view_matrix * vPosition;" \
    "vec3 transformed_normals = normalize(mat3(u_model_view_matrix) * vNormal);" \
    "vec3 light_direction = normalize(vec3(u_light_position) - eyeCoordinates.xyz);" \
    "float tn_dot_ld = max(dot(transformed_normals, light_direction), 0.0);" \
    "vec3 diffuse = u_Ld * u_Kd * tn_dot_ld;" \
    
    "vec3 reflection_vector = reflect(-light_direction, transformed_normals);" \
    "vec3 viewer_vector = normalize(-eyeCoordinates.xyz);" \
    "float r_dot_v = max(dot(reflection_vector, viewer_vector), 0.0);" \
    "vec3 specular = u_Ls * u_Ks * pow(r_dot_v, u_material_shininess);" \
    "phong_ads_color = ambient + diffuse + specular;" \
    "}"\
    "else" \
    "{" \
    "phong_ads_color = vec3(1.0, 1.0, 1.0);" \
    "}" \
    "gl_Position = u_projection_matrix * u_model_view_matrix * vPosition;" \
    "}";
    
    
    glShaderSource(vertexShaderObject_PV, 1, (const GLchar**)&vsSource, NULL);
    glCompileShader(vertexShaderObject_PV);
    GLint iShaderCompileStatus = 0;
    glGetShaderiv(vertexShaderObject_PV, GL_COMPILE_STATUS, &iShaderCompileStatus);
    if(GL_FALSE == iShaderCompileStatus)
    {
        GLsizei iLogLen = 0;
        glGetShaderiv(vertexShaderObject_PV, GL_INFO_LOG_LENGTH, &iLogLen);
        if(iLogLen)
        {
            GLchar* szLog = (GLchar*)malloc(iLogLen);
            if(szLog)
            {
                GLsizei written;
                glGetShaderInfoLog(vertexShaderObject_PV, iLogLen, &written, szLog);
                fprintf(gpFile, "PV: Vertex Shader Compile Info: %s\n", szLog);
                free(szLog);
                [self release];
                [NSApp terminate:self];
            }
        }
    }//================
    
    /*****FRAGMENT SHADER********/
    fragmentShaderObject_PV = glCreateShader(GL_FRAGMENT_SHADER);
    const GLchar* fsSource =
    "#version 410 core" \
    "\n" \
    "in vec3 phong_ads_color;" \
    "out vec4 FragColor;" \
    "void main(void)" \
    "{" \
    "FragColor = vec4(phong_ads_color, 1.0);" \
    "}";
    
    glShaderSource(fragmentShaderObject_PV, 1, (const GLchar**)&fsSource, NULL);
    glCompileShader(fragmentShaderObject_PV);
    iShaderCompileStatus = 0;
    glGetShaderiv(fragmentShaderObject_PV, GL_COMPILE_STATUS, &iShaderCompileStatus);
    if(GL_FALSE == iShaderCompileStatus)
    {
        GLsizei iLogLen = 0;
        glGetShaderiv(fragmentShaderObject_PV, GL_INFO_LOG_LENGTH, &iLogLen);
        if(iLogLen)
        {
            GLchar* szLog = (GLchar*)malloc(iLogLen);
            if(szLog)
            {
                GLsizei written;
                glGetShaderInfoLog(fragmentShaderObject_PV, iLogLen, &written, szLog);
                fprintf(gpFile, "PV: Fragment Shader Compile Info: %s\n", szLog);
                free(szLog);
                [self release];
                [NSApp terminate:self];
            }
        }
    }//===================
    
    /*****SHADER PROGRAM********/
    shaderProgramObject_PV = glCreateProgram();
    //attach vs and fs to shader program
    glAttachShader(shaderProgramObject_PV, vertexShaderObject_PV);
    glAttachShader(shaderProgramObject_PV, fragmentShaderObject_PV);
    
    //pre-link binding of vertex attributes
    glBindAttribLocation(shaderProgramObject_PV, PRP_ATTRIBUTE_POSITION, "vPosition");     // <--- Modify/add as per requirement
    glBindAttribLocation(shaderProgramObject_PV, PRP_ATTRIBUTE_NORMAL, "vNormal");
    
    //link program
    glLinkProgram(shaderProgramObject_PV);
    
    //post-link, get uniform locations from shaders
    uniform[MV] = glGetUniformLocation(shaderProgramObject_PV, "u_model_view_matrix");           // <--- Modify/add as per requirement
    uniform[PROJECTION] = glGetUniformLocation(shaderProgramObject_PV, "u_projection_matrix");
    uniform[LA] = glGetUniformLocation(shaderProgramObject_PV, "u_La");
    uniform[KA] = glGetUniformLocation(shaderProgramObject_PV, "u_Ka");
    uniform[LD] = glGetUniformLocation(shaderProgramObject_PV, "u_Ld");
    uniform[KD] = glGetUniformLocation(shaderProgramObject_PV, "u_Kd");
    uniform[LS] = glGetUniformLocation(shaderProgramObject_PV, "u_Ls");
    uniform[KS] = glGetUniformLocation(shaderProgramObject_PV, "u_Ks");
    uniform[SHININESS] = glGetUniformLocation(shaderProgramObject_PV, "u_material_shininess");
    uniform[LIGHT_POS] = glGetUniformLocation(shaderProgramObject_PV, "u_light_position");
    uniform[LIGHT_EN] = glGetUniformLocation(shaderProgramObject_PV, "u_light_enabled");
}

-(void)uiniformBinding
{
    if(bPerVertex == YES)
    {
        //get uniform locations from shaders
        uniform[MV] = glGetUniformLocation(shaderProgramObject_PV, "u_model_view_matrix");           // <--- Modify/add as per requirement
        uniform[PROJECTION] = glGetUniformLocation(shaderProgramObject_PV, "u_projection_matrix");
        uniform[LA] = glGetUniformLocation(shaderProgramObject_PV, "u_La");
        uniform[KA] = glGetUniformLocation(shaderProgramObject_PV, "u_Ka");
        uniform[LD] = glGetUniformLocation(shaderProgramObject_PV, "u_Ld");
        uniform[KD] = glGetUniformLocation(shaderProgramObject_PV, "u_Kd");
        uniform[LS] = glGetUniformLocation(shaderProgramObject_PV, "u_Ls");
        uniform[KS] = glGetUniformLocation(shaderProgramObject_PV, "u_Ks");
        uniform[SHININESS] = glGetUniformLocation(shaderProgramObject_PV, "u_material_shininess");
        uniform[LIGHT_POS] = glGetUniformLocation(shaderProgramObject_PV, "u_light_position");
        uniform[LIGHT_EN] = glGetUniformLocation(shaderProgramObject_PV, "u_light_enabled");
    }
    else
    {//per fragment
        //get uniform locations from shaders
        uniform[MV] = glGetUniformLocation(shaderProgramObject_PF, "u_model_view_matrix");           // <--- Modify/add as per requirement
        uniform[PROJECTION] = glGetUniformLocation(shaderProgramObject_PF, "u_projection_matrix");
        uniform[LA] = glGetUniformLocation(shaderProgramObject_PF, "u_La");
        uniform[KA] = glGetUniformLocation(shaderProgramObject_PF, "u_Ka");
        uniform[LD] = glGetUniformLocation(shaderProgramObject_PF, "u_Ld");
        uniform[KD] = glGetUniformLocation(shaderProgramObject_PF, "u_Kd");
        uniform[LS] = glGetUniformLocation(shaderProgramObject_PF, "u_Ls");
        uniform[KS] = glGetUniformLocation(shaderProgramObject_PF, "u_Ks");
        uniform[SHININESS] = glGetUniformLocation(shaderProgramObject_PF, "u_material_shininess");
        uniform[LIGHT_POS] = glGetUniformLocation(shaderProgramObject_PF, "u_light_position");
        uniform[LIGHT_EN] = glGetUniformLocation(shaderProgramObject_PF, "u_light_enabled");
    }
}

-(void)prepareOpenGL
{
    //code
    fprintf(gpFile, "OpenGL Version :%s\n", glGetString(GL_VERSION));
    fprintf(gpFile, "GLSL Version   :%s\n", glGetString(GL_SHADING_LANGUAGE_VERSION));
    
    [[self openGLContext] makeCurrentContext];
    
    //set swap interval to 1, to render your scene on display refresh rate
    GLint swapInt = 1;
    [[self openGLContext] setValues:&swapInt forParameter:NSOpenGLCPSwapInterval];
    
    /*************OPENGL and SHADER INIT************************/       // <--- Modify as per requirement
    
    [self initializePFShaderProgram];
    [self initializePVShaderProgram];
    
    
    /*******LOAD TEXTURES****************/
    //=============
    
    /*************VERTICES, COLOR, NORMALS, VAO, VBO INITIALIZATIONS******************/
    
    //Sphere
    sphr = [[Sphere alloc] init];
    [sphr getSphereVertexData:sphere_vertices :sphere_normals :sphere_textures :sphere_elements];
    gNumVertices = [sphr getNumberOfSphereVertices];
    gNumElements = [sphr getNumberOfSphereElements];
    
    fprintf(gpFile, "nr vertices:%d\n", gNumVertices);
    fprintf(gpFile, "nr elements:%d\n", gNumElements);
    //============
    //VAO:Sphere
    glGenVertexArrays(1, &vao[SPHERE]);
    glBindVertexArray(vao[SPHERE]);
    //VBO1: Position
    {
        glGenBuffers(1, &vbo[SPHERE][POSITION]);
        glBindBuffer(GL_ARRAY_BUFFER, vbo[SPHERE][POSITION]);
        glBufferData(GL_ARRAY_BUFFER, sizeof(sphere_vertices), sphere_vertices, GL_STATIC_DRAW);
        glVertexAttribPointer(PRP_ATTRIBUTE_POSITION,
                              3, //xyz
                              GL_FLOAT,
                              GL_FALSE, //non-normalize
                              0, NULL); //0=stride
        glEnableVertexAttribArray(PRP_ATTRIBUTE_POSITION);
        glBindBuffer(GL_ARRAY_BUFFER, 0);   //unbind
    }
    //VBO2: Normals
    {
        glGenBuffers(1, &vbo[SPHERE][NORMAL]);
        glBindBuffer(GL_ARRAY_BUFFER, vbo[SPHERE][NORMAL]);
        glBufferData(GL_ARRAY_BUFFER, sizeof(sphere_normals), sphere_normals, GL_STATIC_DRAW);
        glVertexAttribPointer(PRP_ATTRIBUTE_NORMAL,
                              3, //nx, ny, nz
                              GL_FLOAT,
                              GL_FALSE, //non-normalize
                              0, NULL); //0=stride
        glEnableVertexAttribArray(PRP_ATTRIBUTE_NORMAL);
        glBindBuffer(GL_ARRAY_BUFFER, 0);   //unbind
    }
    //VBO3: Elements
    {
        glGenBuffers(1, &vbo[SPHERE][ELEMENTS]);
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo[SPHERE][ELEMENTS]);
        
        glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(sphere_elements), sphere_elements, GL_STATIC_DRAW);
        
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);   //unbind
    }
    glBindVertexArray(0);   //unbind
    //==================
    
    //Other OpenGL Settings         // <--- Modify as per requirement
    glClearDepth(1.0f);         // 3D change 1b.1(M): Full depth(far plane), added for 3D drawing
    glEnable(GL_DEPTH_TEST);    // 3D change 1b.2(M): Enable depth function
    glDepthFunc(GL_LEQUAL);     // 3D change 1b.3(M): draw objects from far plane, less than or equal distance from far to near
    //glEnable(GL_CULL_FACE);    // We will always cull back faces for better performance
    //==================
    
    //set backgroud clearing colof
    glClearColor(0.0f, 0.0f, 0.0f, 1.0f);   //black                  // <--- Modify as per requirement
    
    //fill I matrix in perspective projection matrix
    projectonMatrix = vmath::mat4::identity();
    
    bLighting_enable = YES; //by default lighting enabled
    bPerVertex = YES;   //by default per vertex lighting
    bPerFragment = NO;
    //=======================
    
    /*************CVDisplayLink INIT***************************/
    //01-Create Core Video Link
    CVDisplayLinkCreateWithActiveCGDisplays(&displayLink);
    //02-Set Core Video Callback
    CVDisplayLinkSetOutputCallback(displayLink, &MyDisplayLinkCallback, self);
    //03-Convert NSOpenGL context and pixelformat to CGL
    CGLPixelFormatObj cglPixelFormat = (CGLPixelFormatObj) [[self pixelFormat] CGLPixelFormatObj];
    CGLContextObj cglContext = (CGLContextObj) [[self openGLContext] CGLContextObj];
    //04-Set Core Video Current display
    CVDisplayLinkSetCurrentCGDisplayFromOpenGLContext(displayLink, cglContext, cglPixelFormat);
    //05-Start Core Video
    CVDisplayLinkStart(displayLink);
}

-(void)reshape
{
    //code
    CGLLockContext((CGLContextObj) [[self openGLContext] CGLContextObj]);
    
    NSRect rect = [self bounds];
    GLfloat width = rect.size.width;
    GLfloat height = rect.size.height;
    
    //Set opengl viewport
    glViewport(0, 0, (GLsizei)width, (GLsizei)height);
    
    //set perspective projection--> (fovy, AR, near, far)
    projectonMatrix = vmath::perspective(45.0f, width/height, 0.1f, 100.0f);        // <--- Modify as per requirement
    
    CGLUnlockContext((CGLContextObj) [[self openGLContext] CGLContextObj]);
}

-(void)drawRect:(NSRect)dirtyRect
{
    //code
    [self drawView];
}

//our method
-(void)drawView
{
    //code
    static GLfloat angleCube = 0.0f;
    
    CGLLockContext((CGLContextObj) [[self openGLContext] CGLContextObj]);
    [[self openGLContext] makeCurrentContext];
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    
    [self uiniformBinding]; //change uniforms as per shader selection, PV or PF
    if(bPerVertex == YES)
        glUseProgram(shaderProgramObject_PV);
    else
        glUseProgram(shaderProgramObject_PF);
    
    
    
    //SEND LIGHTING VALUES TO SHADER
    if(bLighting_enable == YES)
    {
        glUniform1i(uniform[LIGHT_EN], 1);
        
        glUniform3fv(uniform[LA], 1, lightAmbient);
        glUniform3fv(uniform[KA], 1, materialAmbient);
        glUniform3fv(uniform[LD], 1, lightDiffuse);
        glUniform3fv(uniform[KD], 1, materialDiffuse);
        glUniform3fv(uniform[LS], 1, lightSpecular);
        glUniform3fv(uniform[KS], 1, materialSpecular);
        glUniform1f(uniform[SHININESS], materialShininess);
        
        glUniform4fv(uniform[LIGHT_POS], 1, lightPosition);
    }
    else
    {
        glUniform1i(uniform[LIGHT_EN], 0);
    }
    //==========
    
    //CUBE
    vmath::mat4 modelViewMatrix = vmath::mat4::identity();
    vmath::mat4 translateMatrix = vmath::mat4::identity();
    vmath::mat4 modelViewProjectionMatrix = vmath::mat4::identity();
    
    translateMatrix = vmath::translate(0.0f, 0.0f, -3.0f);
    //MV = T * MV
    modelViewMatrix = translateMatrix * modelViewMatrix;
    
    //send MV to shader
    glUniformMatrix4fv(uniform[MV], 1, GL_FALSE, modelViewMatrix);
    
    //send Projection matrix to shader
    glUniformMatrix4fv(uniform[PROJECTION], 1, GL_FALSE, projectonMatrix);
    
    //bind sphere vao
    glBindVertexArray(vao[SPHERE]);
    //draw by elements
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo[SPHERE][ELEMENTS]);
    glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);
    glBindVertexArray(0);   //unbind
    
    glUseProgram(0);    //un-use program
    
    CGLFlushDrawable((CGLContextObj) [[self openGLContext] CGLContextObj]); //flushes data into other framebuffer which is not being displayed
    CGLUnlockContext((CGLContextObj) [[self openGLContext] CGLContextObj]);
    
    angleCube = angleCube + 2.0f;
}

//Hardware Event Handling
-(BOOL)acceptsFirstResponder
{
    //code
    [[self window] makeFirstResponder:self];        //tell your window that GLView(self) will handle key, mouse etc. events
    
    return (YES);
}

-(void)keyDown:(NSEvent*)theEvent
{
    //code
    int key = (int) [[theEvent characters] characterAtIndex:0];
    switch(key)     // <--- Modify here as per requirement
    {
        case 27:    //escape key: Full screen
            [[self window] toggleFullScreen:self];  //auto repainting is done
            break;
        case 'f':   //switch to per fragment shading
        case 'F':
            bPerFragment = YES;
            bPerVertex = NO;
            break;
        case 'L':
        case 'l':
            if(bLighting_enable == NO)
                bLighting_enable = YES;
            else
                bLighting_enable = NO;
            break;
        case 'V':   //switch to per vertex shading
        case 'v':
            bPerFragment = NO;
            bPerVertex = YES;
            break;
        case 'Q':   //quit application
        case 'q':
            [self release];
            [NSApp terminate:self];
            break;
        default:
            break;
    }
}

-(void)mouseDown:(NSEvent*)theEvent
{
    //code
}

-(void)mouseDragged:(NSEvent*)theEvent
{
    //code
}

-(void)rightMouseDown:(NSEvent*)theEvent
{
    //code
}

-(void)dealloc
{
    //stop and release Core Video link
    CVDisplayLinkStop(displayLink);
    CVDisplayLinkRelease(displayLink);
    
    //Release vao
    for(int i=NR_SHAPES-1; i>=0; i--)
    {
        if(vao[i])
        {
            glDeleteVertexArrays(1, &vao[i]);
            vao[i] = 0;
        }
    }
    glDeleteVertexArrays(NR_SHAPES, vao);
    
    //Release vbo, As I have array of vao objects, i would have released this in 'glDeleteVertexArrays(NR_SHAPES, vao)' way.
    //But at this point of time, i am not sure, all declared vao objects have been initialized or not.
    //So its good to check and release vao objects one by one, though it looks funny :-)
    for(int i=NR_SHAPES-1; i>=0; i--)
    {
        for(int j=NR_SHAPE_ATTRS-1; i>=0; i--)
        {
            if(vbo[i][j])
            {
                glDeleteBuffers(1, &vbo[i][j]);
                vbo[i][j] = 0;
            }
        }
    }
    
    //Release Shader objects
    if(shaderProgramObject_PF)
    {
        if(fragmentShaderObject_PF)
        {
            glDetachShader(shaderProgramObject_PF, fragmentShaderObject_PF);
            glDeleteShader(fragmentShaderObject_PF);
            fragmentShaderObject_PF = 0;
        }
        if(vertexShaderObject_PF)
        {
            glDetachShader(shaderProgramObject_PF, vertexShaderObject_PF);
            glDeleteShader(vertexShaderObject_PF);
            vertexShaderObject_PF = 0;
        }
        
        glDeleteProgram(shaderProgramObject_PF);
        shaderProgramObject_PF = 0;
    }
    
    if(shaderProgramObject_PV)
    {
        if(fragmentShaderObject_PV)
        {
            glDetachShader(shaderProgramObject_PV, fragmentShaderObject_PV);
            glDeleteShader(fragmentShaderObject_PV);
            fragmentShaderObject_PV = 0;
        }
        if(vertexShaderObject_PV)
        {
            glDetachShader(shaderProgramObject_PV, vertexShaderObject_PV);
            glDeleteShader(vertexShaderObject_PV);
            vertexShaderObject_PV = 0;
        }
        
        glDeleteProgram(shaderProgramObject_PV);
        shaderProgramObject_PV = 0;
    }
    
    //Un-use shader program
    glUseProgram(0);
    
    [sphr release];
    [super dealloc];
}

@end

//This callback is called on display refresh rate
CVReturn MyDisplayLinkCallback(CVDisplayLinkRef displayLink, const CVTimeStamp* pNow, const CVTimeStamp *pOutTime, CVOptionFlags FlagsIn, CVOptionFlags *FlagsOut, void* pCVDisplayContext)
{
    CVReturn ret = kCVReturnSuccess;
    
    GLView *glView = (GLView*) pCVDisplayContext;
    
    ret = (CVReturn) [glView getFrameForTime:pOutTime];
    
    return (ret);
}
