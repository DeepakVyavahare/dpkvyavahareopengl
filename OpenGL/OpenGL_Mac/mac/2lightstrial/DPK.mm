//2 lights

#import <Foundation/Foundation.h>
#import <Cocoa/Cocoa.h>

#import <QuartzCore/CVDisplayLink.h>

#import <OpenGL/gl3.h>
#import <OpenGL/gl3ext.h>

#import "vmath.h"

enum
{
    VDG_ATTRIBUTE_VERTEX,
    VDG_ATTRIBUTE_COLOR,
    VDG_ATTRIBUTE_NORMAL,
    VDG_ATTRIBUTE_TEXTURE0,
};

GLfloat lightAmbient[] = { 1.0f,0.0f,0.0f,1.0f };
GLfloat lightDiffuse[] = { 1.0f,0.0f,0.0f,1.0f };
GLfloat lightSpecular[] = { 1.0f,1.0f,1.0f,1.0f };
GLfloat lightPosition[] = { 50.0f,0.0f,0.0f,1.0f };


GLfloat lightAmbient2[] = { 0.0f,0.0f,1.0f,1.0f };
GLfloat lightDiffuse2[] = { 0.0f,0.0f,1.0f,1.0f };
GLfloat lightSpecular2[] = { 1.0f,1.0f,1.0f,1.0f };
GLfloat lightPosition2[] = { -50.0f,0.0f,0.0f,1.0f };



GLfloat material_ambient[] = { 0.0f,0.0f,0.0f,1.0f };
GLfloat material_diffuse[] = { 1.0f,1.0f,1.0f,1.0f };
GLfloat material_specular[] = { 1.0f,1.0f,1.0f,1.0f };
GLfloat material_shininess = 50.0f;

GLfloat angle = 0.0f;

FILE *gpFile = NULL;

CVReturn MyDisplayCallback(CVDisplayLinkRef, const CVTimeStamp*, const CVTimeStamp*, CVOptionFlags, CVOptionFlags*, void*);

//interface declarations
@interface AppDelegate: NSObject <NSApplicationDelegate, NSWindowDelegate>
@end

@interface GLView: NSOpenGLView
@end

//entry point function
int main(int argc, const char* argv[])
{
    NSAutoreleasePool *pPool = [[NSAutoreleasePool alloc] init];
    
    NSApp = [NSApplication sharedApplication];
    
    [NSApp setDelegate: [[AppDelegate alloc] init]];
    
    [NSApp run];
    
    [pPool release];
    
    return (0);
}

//implementations
@implementation AppDelegate
{
@private
    NSWindow *window;
    GLView *glView;
}

-(void)applicationDidFinishLaunching:(NSNotification*)aNotification
{
    //code
    //create log file
    NSBundle *bundle = [NSBundle mainBundle];
    NSString *appDirPath = [bundle bundlePath];
    NSString *appDirParentPath = [appDirPath stringByDeletingLastPathComponent];
    NSString *logFilePath = [NSString stringWithFormat:@"%@/Log.txt", appDirParentPath];
    //covert it to const char*
    const char* szLogFilePath = [logFilePath cStringUsingEncoding:NSASCIIStringEncoding];
    
    gpFile = fopen(szLogFilePath, "w");
    if(gpFile == NULL)
    {
        printf("Can Not Open Log File. Exiting...\n");
        [self release];
        [NSApp terminate:self];
    }
    else
    {
        fprintf(gpFile, "Application Is Started Successfully\n");
    }
    
    //create window
    NSRect win_rect  = NSMakeRect(0.0, 0.0, 800.0, 600.0);
    window = [[NSWindow alloc] initWithContentRect:win_rect
                                         styleMask:NSWindowStyleMaskTitled | NSWindowStyleMaskClosable | NSWindowStyleMaskMiniaturizable | NSWindowStyleMaskResizable
                                           backing:NSBackingStoreBuffered
                                             defer:NO];
    [window setTitle:@"macOS:OpenGL --> 3D-Rotation : Pyramid-Cube"];
    [window center];
    
    //get view
    glView = [[GLView alloc] initWithFrame: [window frame]];
    
    //attach view to window
    [window setContentView:glView];
    [window setDelegate:self];
    [window makeKeyAndOrderFront:self];
}

-(void)applicationWillTerminate:(NSNotification*)aNotification
{
    if(gpFile)
    {
        fprintf(gpFile, "Application Is Terminated Successfully\n");
        fclose(gpFile);
        gpFile = NULL;
    }
}

-(void)windowWillClose:(NSNotification*)aNotification
{
    //code
    [NSApp terminate:self];
}

-(void)dealloc
{
    [glView release];
    [window release];
    
    [super dealloc];
}
@end

@implementation GLView
{
@private
    CVDisplayLinkRef displayLink;
    
    GLuint vertexShaderObject;
    GLuint fragmentShaderObject;
    GLuint shaderProgramObject;
    
    GLuint vao_pyramid;
    GLuint vbo_pyramid_position;
    GLuint vbo_pyramid_color;
    
    GLuint vao_cube;
    GLuint vbo_cube_position;
    GLuint vbo_cube_normal;
    GLuint vbo_cube_color;
    
    GLuint uniform_mvp;
    GLuint    view_matrix_uniform;
    GLuint model_matrix_uniform;
    GLuint projection_matrix_uniform;
    
    GLfloat anglePyramid;
    GLfloat angleCube;
    
    bool gbAnimate;
    bool gbLight;
    
    GLuint L_KeyPressed_uniform;
    
    GLuint Light_ambient_uniform;
    GLuint Light_diffuse_uniform;
    GLuint Light_specular_uniform;
    GLuint light_position_uniform;
   
    GLuint Light_ambient_uniform2;
    GLuint Light_diffuse_uniform2;
    GLuint Light_specular_uniform2;
    GLuint light_position_uniform2;
    
    
    GLuint Material_ambient_uniform;
    GLuint Material_diffuse_uniform;
    GLuint Material_specular_uniform;
    GLuint material_shininess_uniform;
 
    //light0
   

    
  vmath::mat4 perspectiveProjectionMatrix;
}

-(id)initWithFrame:(NSRect)frame
{
    self = [super initWithFrame:frame];
    
    if(self)
    {
        [[self window] setContentView:self];
        
        //set pixel format attributes
        NSOpenGLPixelFormatAttribute attrs[] =
        {
            NSOpenGLPFAOpenGLProfile, NSOpenGLProfileVersion4_1Core,
            NSOpenGLPFAScreenMask, CGDisplayIDToOpenGLDisplayMask(kCGDirectMainDisplay),
            NSOpenGLPFANoRecovery,
            NSOpenGLPFAAccelerated,
            NSOpenGLPFAColorSize, 24,
            NSOpenGLPFADepthSize, 24,
            NSOpenGLPFAAlphaSize, 8,
            NSOpenGLPFADoubleBuffer,
            0 //last must be 0
        };
        
        //get pixel format from PFA attributes
        NSOpenGLPixelFormat *pixelFormat = [[[NSOpenGLPixelFormat alloc] initWithAttributes:attrs] autorelease];
        //get opengl context
        NSOpenGLContext *glContext = [[[NSOpenGLContext alloc] initWithFormat:pixelFormat shareContext:nil] autorelease];
        
        //set those
        [self setPixelFormat:pixelFormat];
        [self setOpenGLContext:glContext];
    }
    return (self);
}

//our method: called from CVDisplay thread
-(CVReturn)getFrameForTime:(const CVTimeStamp *)pOutTime
{
    NSAutoreleasePool *pPool = [[NSAutoreleasePool alloc] init];
    
    [self drawView];
    
    [pPool release];
    return (kCVReturnSuccess);
}

-(void)prepareOpenGL
{
    //code
    fprintf(gpFile, "OpenGL Version :%s\n", glGetString(GL_VERSION));
    fprintf(gpFile, "GLSL Version   :%s\n", glGetString(GL_SHADING_LANGUAGE_VERSION));
    
    gbAnimate = false;
    gbLight = false;

    //set swap interval to 1, to render our scene at display refresh rate provided by CVDisplayLink, so as to avoid frame taring
    int swapInt = 1;
    [[self openGLContext] setValues:&swapInt forParameter:NSOpenGLCPSwapInterval];
    
    //OpenGL Shader Configuration/Initialization
    // create shader
    vertexShaderObject = glCreateShader(GL_VERTEX_SHADER);
    
    // shader source code
    const GLchar *vertexShaderSourceCode =
    "#version 410" \
    "\n" \
    "in vec4 vPosition;" \
    "in vec3 vNormal;" \
    "uniform mat4 u_model_matrix;" \
    "uniform mat4 u_view_matrix;" \
    "uniform mat4 u_projection_matrix;" \
    "uniform int u_lighting_enabled;" \
    "uniform vec3 u_La;" \
    "uniform vec3 u_Ld;" \
    "uniform vec3 u_Ls;" \
    "uniform vec4 u_light_position;" \
    "uniform vec3 u_La2;" \
    "uniform vec3 u_Ld2;" \
    "uniform vec3 u_Ls2;" \
    "uniform vec4 u_light_position2;" \
    "uniform vec3 u_Ka;" \
    "uniform vec3 u_Kd;" \
    "uniform vec3 u_Ks;" \
    "uniform float u_material_shininess;" \
    "out vec3 phong_ads_color;" \
    "out vec3 phong_ads_color2;" \
    "void main(void)" \
    "{" \
    "if(u_lighting_enabled==1)" \
    "{" \
    "vec4 eye_coordinates=u_view_matrix * u_model_matrix * vPosition;" \
    "vec3 transformed_normals=normalize(mat3(u_view_matrix * u_model_matrix) * vNormal);" \
    "vec3 light_direction = normalize(vec3(u_light_position) - eye_coordinates.xyz);" \
    "float tn_dot_ld = max(dot(transformed_normals, light_direction),0.0);" \
    "vec3 ambient = u_La * u_Ka;" \
    "vec3 diffuse = u_Ld * u_Kd * tn_dot_ld;" \
    "vec3 reflection_vector = reflect(-light_direction, transformed_normals);" \
    "vec3 viewer_vector = normalize(-eye_coordinates.xyz);" \
    "vec3 specular = u_Ls * u_Ks * pow(max(dot(reflection_vector, viewer_vector), 0.0), u_material_shininess);" \
    "phong_ads_color=ambient + diffuse + specular;" \
    "vec4 eye_coordinates2=u_view_matrix * u_model_matrix * vPosition;" \
    "vec3 transformed_normals2=normalize(mat3(u_view_matrix * u_model_matrix) * vNormal);" \
    "vec3 light_direction2 = normalize(vec3(u_light_position2) - eye_coordinates2.xyz);" \
    "float tn_dot_ld2 = max(dot(transformed_normals2, light_direction2),0.0);" \
    "vec3 ambient2 = u_La2 * u_Ka;" \
    "vec3 diffuse2 = u_Ld2 * u_Kd * tn_dot_ld2;" \
    "vec3 reflection_vector2 = reflect(-light_direction2, transformed_normals2);" \
    "vec3 viewer_vector2 = normalize(-eye_coordinates2.xyz);" \
    "vec3 specular2 = u_Ls2 * u_Ks * pow(max(dot(reflection_vector2, viewer_vector2), 0.0), u_material_shininess);" \
    "phong_ads_color2=ambient2 + diffuse2 + specular2;" \
"phong_ads_color=phong_ads_color+phong_ads_color2;" \
    "}" \
    "gl_Position=u_projection_matrix * u_view_matrix * u_model_matrix * vPosition;" \
    "}";
    
    glShaderSource(vertexShaderObject, 1, (const GLchar **)&vertexShaderSourceCode, NULL);
    
    // compile shader
    glCompileShader(vertexShaderObject);
    GLint iInfoLogLength = 0;
    GLint iShaderCompiledStatus = 0;
    char *szInfoLog = NULL;
    glGetShaderiv(vertexShaderObject, GL_COMPILE_STATUS, &iShaderCompiledStatus);
    if (iShaderCompiledStatus == GL_FALSE)
    {
        glGetShaderiv(vertexShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
        if (iInfoLogLength > 0)
        {
            szInfoLog = (char *)malloc(iInfoLogLength);
            if (szInfoLog != NULL)
            {
                GLsizei written;
                glGetShaderInfoLog(vertexShaderObject, iInfoLogLength, &written, szInfoLog);
                fprintf(gpFile, "Vertex Shader Compilation Log : %s\n", szInfoLog);
                free(szInfoLog);
               
                exit(0);
            }
        }
    }
    
    // *** FRAGMENT SHADER ***
    // create shader
    fragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);
    
    // provide source code to shader
    const GLchar *fragmentShaderSourceCode =
    "#version 410" \
    "\n" \
    "in vec3 phong_ads_color;" \
    "in vec3 phong_ads_color2;" \
    "out vec4 FragColor;" \
    "void main(void)" \
    "{" \
    "FragColor = vec4(phong_ads_color, 1.0);" \
    "}";
    
    glShaderSource(fragmentShaderObject, 1, (const GLchar **)&fragmentShaderSourceCode, NULL);
    
    // compile shader
    glCompileShader(fragmentShaderObject);
    glGetShaderiv(fragmentShaderObject, GL_COMPILE_STATUS, &iShaderCompiledStatus);
    if (iShaderCompiledStatus == GL_FALSE)
    {
        glGetShaderiv(fragmentShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
        if (iInfoLogLength > 0)
        {
            szInfoLog = (char *)malloc(iInfoLogLength);
            if (szInfoLog != NULL)
            {
                GLsizei written;
                glGetShaderInfoLog(fragmentShaderObject, iInfoLogLength, &written, szInfoLog);
                fprintf(gpFile, "Fragment Shader Compilation Log : %s\n", szInfoLog);
                free(szInfoLog);
               
                exit(0);
            }
        }
    }
    
    // *** SHADER PROGRAM ***
    // create
    shaderProgramObject = glCreateProgram();
    
    // attach vertex shader to shader program
    glAttachShader(shaderProgramObject, vertexShaderObject);
    
    // attach fragment shader to shader program
    glAttachShader(shaderProgramObject, fragmentShaderObject);
    
    // pre-link binding of shader program object with vertex shader position attribute
    glBindAttribLocation(shaderProgramObject, VDG_ATTRIBUTE_VERTEX, "vPosition");
    glBindAttribLocation(shaderProgramObject, VDG_ATTRIBUTE_COLOR, "vColor");
    glBindAttribLocation(shaderProgramObject, VDG_ATTRIBUTE_NORMAL, "vNormal");
    
    // link shader
    glLinkProgram(shaderProgramObject);
    GLint iShaderProgramLinkStatus = 0;
    glGetProgramiv(shaderProgramObject, GL_LINK_STATUS, &iShaderProgramLinkStatus);
    if (iShaderProgramLinkStatus == GL_FALSE)
    {
        glGetProgramiv(shaderProgramObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
        if (iInfoLogLength>0)
        {
            szInfoLog = (char *)malloc(iInfoLogLength);
            if (szInfoLog != NULL)
            {
                GLsizei written;
                glGetProgramInfoLog(shaderProgramObject, iInfoLogLength, &written, szInfoLog);
                fprintf(gpFile, "Shader Program Link Log : %s\n", szInfoLog);
                free(szInfoLog);
               
                exit(0);
            }
        }
    }
    
    // get uniform locations
 
    model_matrix_uniform = glGetUniformLocation(shaderProgramObject, "u_model_matrix");
    view_matrix_uniform = glGetUniformLocation(shaderProgramObject, "u_view_matrix");
    projection_matrix_uniform = glGetUniformLocation(shaderProgramObject, "u_projection_matrix");
    
    L_KeyPressed_uniform = glGetUniformLocation(shaderProgramObject, "u_lighting_enabled");
    
    Light_ambient_uniform = glGetUniformLocation(shaderProgramObject, "u_La");
    Light_ambient_uniform2 = glGetUniformLocation(shaderProgramObject, "u_La2");
    //    Light_ambient_uniform2 = glGetUniformLocation(shaderProgramObject, "u_La3");
    
    Light_diffuse_uniform = glGetUniformLocation(shaderProgramObject, "u_Ld");
    Light_diffuse_uniform2 = glGetUniformLocation(shaderProgramObject, "u_Ld2");
    //Light_diffuse_uniform2 = glGetUniformLocation(shaderProgramObject, "u_Ld3");
    
    Light_specular_uniform = glGetUniformLocation(shaderProgramObject, "u_Ls");
    Light_specular_uniform2 = glGetUniformLocation(shaderProgramObject, "u_Ls2");
    //    Light_specular_uniform2 = glGetUniformLocation(shaderProgramObject, "u_Ls3");
    
    
    light_position_uniform = glGetUniformLocation(shaderProgramObject, "u_light_position");;
    light_position_uniform2 = glGetUniformLocation(shaderProgramObject, "u_light_position2");;
    //    light_position_uniform2 = glGetUniformLocation(shaderProgramObject, "u_light_position3");;
    // ambient reflective color intensity of material

    Material_ambient_uniform = glGetUniformLocation(shaderProgramObject, "u_Ka");
    //    Material_ambient_uniform1 = glGetUniformLocation(shaderProgramObject, "u_Ka2");
    //    Material_ambient_uniform2 = glGetUniformLocation(shaderProgramObject, "u_Ka3");
    
    Material_diffuse_uniform = glGetUniformLocation(shaderProgramObject, "u_Kd");
    //    Material_diffuse_uniform1 = glGetUniformLocation(shaderProgramObject, "u_Kd2");
    //    Material_diffuse_uniform2 = glGetUniformLocation(shaderProgramObject, "u_Kd3");
    
    Material_specular_uniform = glGetUniformLocation(shaderProgramObject, "u_Ks");
    //    Material_specular_uniform1 = glGetUniformLocation(shaderProgramObject, "u_Ks2");
    //    Material_specular_uniform2 = glGetUniformLocation(shaderProgramObject, "u_Ks2");
    
    material_shininess_uniform = glGetUniformLocation(shaderProgramObject, "u_material_shininess");;
    //    material_shininess_uniform1 = glGetUniformLocation(shaderProgramObject, "u_material_shininess2");;
    //    material_shininess_uniform2 = glGetUniformLocation(shaderProgramObject, "u_material_shininess3");;
    
    // *** vertices, colors, shader attribs, vbo, vao initializations ***
    GLfloat cubeVertices[] =
    {
        1.0f, 1.0f, -1.0f,
        -1.0f, 1.0f, -1.0f,
        -1.0f, 1.0f, 1.0f,
        1.0f, 1.0f, 1.0f,
        
        1.0f, -1.0f, 1.0f,
        -1.0f, -1.0f, 1.0f,
        -1.0f, -1.0f, -1.0f,
        1.0f, -1.0f, -1.0f,
        
        1.0f, 1.0f, 1.0f,
        -1.0f, 1.0f, 1.0f,
        -1.0f, -1.0f, 1.0f,
        1.0f, -1.0f, 1.0f,
        
        1.0f, -1.0f, -1.0f,
        -1.0f, -1.0f, -1.0f,
        -1.0f, 1.0f, -1.0f,
        1.0f, 1.0f, -1.0f,
        
        -1.0f, 1.0f, 1.0f,
        -1.0f, 1.0f, -1.0f,
        -1.0f, -1.0f, -1.0f,
        -1.0f, -1.0f, 1.0f,
        
        1.0f, 1.0f, -1.0f,
        1.0f, 1.0f, 1.0f,
        1.0f, -1.0f, 1.0f,
        1.0f, -1.0f, -1.0f,
    };
    
    for (int i = 0; i<72; i++)
    {
        if (cubeVertices[i]<0.0f)
            cubeVertices[i] = cubeVertices[i] + 0.25f;
        else if (cubeVertices[i]>0.0f)
            cubeVertices[i] = cubeVertices[i] - 0.25f;
        else
            cubeVertices[i] = cubeVertices[i];
    }
    
    const GLfloat cubeNormals[] =
    {
        0.0f, 1.0f, 0.0f,
        0.0f, 1.0f, 0.0f,
        0.0f, 1.0f, 0.0f,
        0.0f, 1.0f, 0.0f,
        
        0.0f, -1.0f, 0.0f,
        0.0f, -1.0f, 0.0f,
        0.0f, -1.0f, 0.0f,
        0.0f, -1.0f, 0.0f,
        
        0.0f, 0.0f, 1.0f,
        0.0f, 0.0f, 1.0f,
        0.0f, 0.0f, 1.0f,
        0.0f, 0.0f, 1.0f,
        
        0.0f, 0.0f, -1.0f,
        0.0f, 0.0f, -1.0f,
        0.0f, 0.0f, -1.0f,
        0.0f, 0.0f, -1.0f,
        
        -1.0f, 0.0f, 0.0f,
        -1.0f, 0.0f, 0.0f,
        -1.0f, 0.0f, 0.0f,
        -1.0f, 0.0f, 0.0f,
        
        1.0f, 0.0f, 0.0f,
        1.0f, 0.0f, 0.0f,
        1.0f, 0.0f, 0.0f,
        1.0f, 0.0f, 0.0f
    };

    
    const GLfloat cubeColor[] =
    {
        0.0f, 1.0f, 1.0f,
        0.0f, 1.0f, 1.0f,
        0.0f, 1.0f, 1.0f,
        0.0f, 1.0f, 1.0f,
        
        0.0f, -1.0f, 1.0f,
        0.0f, -1.0f, 1.0f,
        0.0f, -1.0f, 1.0f,
        0.0f, -1.0f, 1.0f,
        
        0.0f, 0.0f, 1.0f,
        0.0f, 0.0f, 1.0f,
        0.0f, 0.0f, 1.0f,
        0.0f, 0.0f, 1.0f,
        
        0.0f, 0.0f, -1.0f,
        0.0f, 0.0f, -1.0f,
        0.0f, 0.0f, -1.0f,
        0.0f, 0.0f, -1.0f,
        
        -1.0f, 0.0f, 1.0f,
        -1.0f, 0.0f, 1.0f,
        -1.0f, 0.0f, 1.0f,
        -1.0f, 0.0f, 1.0f,
        
        1.0f, 0.0f, 1.0f,
        1.0f, 0.0f, 1.0f,
        1.0f, 0.0f, 1.0f,
        1.0f, 0.0f, 1.0f
    };

    // CUBE CODE
    // vao
    glGenVertexArrays(1, &vao_cube);
    glBindVertexArray(vao_cube);
    
    // position vbo
    glGenBuffers(1, &vbo_cube_position);
    glBindBuffer(GL_ARRAY_BUFFER, vbo_cube_position);
    glBufferData(GL_ARRAY_BUFFER, sizeof(cubeVertices), cubeVertices, GL_STATIC_DRAW);
    
    glVertexAttribPointer(VDG_ATTRIBUTE_VERTEX, 3, GL_FLOAT, GL_FALSE, 0, NULL);
    
    glEnableVertexAttribArray(VDG_ATTRIBUTE_VERTEX);
    
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    
    // normal vbo
    glGenBuffers(1, &vbo_cube_normal);
    glBindBuffer(GL_ARRAY_BUFFER, vbo_cube_normal);
    glBufferData(GL_ARRAY_BUFFER, sizeof(cubeNormals), cubeNormals, GL_STATIC_DRAW);
    
    glVertexAttribPointer(VDG_ATTRIBUTE_NORMAL, 3, GL_FLOAT, GL_FALSE, 0, NULL);
    
    glEnableVertexAttribArray(VDG_ATTRIBUTE_NORMAL);
    
    glBindBuffer(GL_ARRAY_BUFFER, 0);

    
    
    // normal vbo
    glGenBuffers(1, &vbo_cube_color);
    glBindBuffer(GL_ARRAY_BUFFER, vbo_cube_color);
    glBufferData(GL_ARRAY_BUFFER, sizeof(cubeColor), cubeColor, GL_STATIC_DRAW);
    
    glVertexAttribPointer(VDG_ATTRIBUTE_COLOR, 3, GL_FLOAT, GL_FALSE, 0, NULL);
    
    glEnableVertexAttribArray(VDG_ATTRIBUTE_COLOR);
    
    glBindBuffer(GL_ARRAY_BUFFER, 0);

    
    glBindVertexArray(0);
    
  //  glShadeModel(GL_SMOOTH);
    // set-up depth buffer
    glClearDepth(1.0f);
    // enable depth testing
    glEnable(GL_DEPTH_TEST);
    // depth test to do
    glDepthFunc(GL_LEQUAL);
    // set really nice percpective calculations ?
    //glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);
    // We will always cull back faces for better performance
    glEnable(GL_CULL_FACE);
    
    // set background color
    glClearColor(0.0f, 0.0f, 0.0f, 0.0f); 
    
    // set perspective matrix to identitu matrix
    perspectiveProjectionMatrix = vmath::mat4::identity();
    
    
    //set perspective projection matrix to identity matrix
    perspectiveProjectionMatrix = vmath::mat4::identity();
   // anglePyramid = 0.0f;
    angleCube = 0.0f;
    
    //CVDisplayLink settings
    CVDisplayLinkCreateWithActiveCGDisplays(&displayLink);      //create CVDisplayLinkRef
    CVDisplayLinkSetOutputCallback(displayLink, &MyDisplayCallback, self);  //register callback on display refresh rate
    //convert NSOpenGL context and pixel format to CGL context and pixel format
    CGLPixelFormatObj cglPixelFormat = (CGLPixelFormatObj) [[self pixelFormat] CGLPixelFormatObj];
    CGLContextObj cglContext = (CGLContextObj) [[self openGLContext] CGLContextObj];
    
    CVDisplayLinkSetCurrentCGDisplayFromOpenGLContext(displayLink, cglContext, cglPixelFormat);
    CVDisplayLinkStart(displayLink);
}

-(void)reshape
{
    CGLLockContext((CGLContextObj) [[self openGLContext] CGLContextObj]);
    
    NSRect rect = [self bounds];
    GLfloat width = rect.size.width;
    GLfloat height = rect.size.height;
    
    glViewport(0, 0, (GLsizei)width, (GLsizei)height);
    
    perspectiveProjectionMatrix = vmath::perspective(45.0f, width/height, 0.1f, 1000.0f);
    
    CGLUnlockContext((CGLContextObj) [[self openGLContext] CGLContextObj]);
}

-(void)drawRect:(NSRect)dirtyRect
{
    //code
    [self drawView];    //this called in context of main thread
}

-(void)drawView
{
    CGLLockContext((CGLContextObj) [[self openGLContext] CGLContextObj]);
    
    [[self openGLContext] makeCurrentContext];
    
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glUseProgram(shaderProgramObject);
    
        glUniform1i(L_KeyPressed_uniform, 1);  //1i == one integer
        
        glUniform3fv(Light_ambient_uniform, 1, lightAmbient);
        glUniform3fv(Light_diffuse_uniform, 1, lightDiffuse);
        glUniform3fv(Light_specular_uniform, 1, lightSpecular);
        glUniform4fv(light_position_uniform, 1, lightPosition);
    
    
    glUniform3fv(Light_ambient_uniform2, 1, lightAmbient2);
    glUniform3fv(Light_diffuse_uniform2, 1, lightDiffuse2);
    glUniform3fv(Light_specular_uniform2, 1, lightSpecular2);
    glUniform4fv(light_position_uniform2, 1, lightPosition2);
        
        glUniform3fv(Material_ambient_uniform, 1, material_ambient);
        glUniform3fv(Material_diffuse_uniform, 1, material_diffuse);
        glUniform3fv(Material_specular_uniform, 1, material_specular);
        glUniform1f(material_shininess_uniform, material_shininess);


    
    vmath::mat4 modelMatrix = vmath::mat4::identity();
    vmath::mat4 viewMatrix = vmath::mat4::identity();
    vmath::mat4 rotationMatrix = vmath::mat4::identity();
    
    modelMatrix = vmath::translate(0.0f, 0.0f, -5.0f);
    
    rotationMatrix = vmath::rotate(angleCube, angleCube, angleCube);
    modelMatrix = modelMatrix*rotationMatrix;
    glUniformMatrix4fv(model_matrix_uniform, 1, GL_FALSE, modelMatrix);
    glUniformMatrix4fv(view_matrix_uniform, 1, GL_FALSE, viewMatrix);
    glUniformMatrix4fv(projection_matrix_uniform, 1, GL_FALSE, perspectiveProjectionMatrix);
    
    // vao
    glBindVertexArray(vao_cube);
    
    //  draw /display
    glDrawArrays(GL_TRIANGLE_FAN, 0, 4);
    glDrawArrays(GL_TRIANGLE_FAN, 4, 4);
    glDrawArrays(GL_TRIANGLE_FAN, 8, 4);
    glDrawArrays(GL_TRIANGLE_FAN, 12, 4);
    glDrawArrays(GL_TRIANGLE_FAN, 16, 4);
    glDrawArrays(GL_TRIANGLE_FAN, 20, 4);
    
    glBindVertexArray(0);
    
    glUseProgram(0);
    [self spin];
    
    CGLFlushDrawable((CGLContextObj) [[self openGLContext] CGLContextObj]);
    CGLUnlockContext((CGLContextObj) [[self openGLContext] CGLContextObj]);
}

//our method
-(void)spin
{
    
    angleCube = angleCube + 0.08f;
    if(angleCube > 360.0f)
        angleCube -= 360.0f;
}

-(BOOL)acceptsFirstResponder
{
    //code
    [[self window] makeFirstResponder:self];
    return (YES);
}

-(void)keyDown:(NSEvent*)theEvent
{
    int key = (int)[[theEvent characters] characterAtIndex:0];
    switch(key)
    {
        case 27:
            [self release];
            [NSApp terminate:self];
            break;
        case 'F':
        case'f':
            [[self window] toggleFullScreen:self];  //auto repaint
            break;
        default:
            break;
    }
}

-(void)dealloc
{
    CVDisplayLinkStop(displayLink);
    CVDisplayLinkRelease(displayLink);
    
    if(vao_cube)
    {
        glDeleteVertexArrays(1, &vao_cube);
        vao_cube = 0;
    }
    if(vbo_cube_position)
    {
        glDeleteBuffers(1, &vbo_cube_position);
        vbo_cube_position = 0;
    }
    if(vao_pyramid)
    {
        glDeleteVertexArrays(1, &vao_pyramid);
        vao_pyramid = 0;
    }
    if(vbo_pyramid_position)
    {
        glDeleteBuffers(1, &vbo_pyramid_position);
        vbo_pyramid_position = 0;
    }
    
    if(shaderProgramObject)
    {
        if(fragmentShaderObject)
        {
            glDetachShader(shaderProgramObject, fragmentShaderObject);
            glDeleteShader(fragmentShaderObject);
            fragmentShaderObject = 0;
        }
        if(vertexShaderObject)
        {
            glDetachShader(shaderProgramObject, vertexShaderObject);
            glDeleteShader(vertexShaderObject);
            vertexShaderObject = 0;
        }
        glDeleteProgram(shaderProgramObject);
        shaderProgramObject = 0;
    }
    
    [super dealloc];
}
@end

CVReturn MyDisplayCallback(CVDisplayLinkRef displayLink, const CVTimeStamp *pNow, const CVTimeStamp *pOutTime, CVOptionFlags FlagsIn, CVOptionFlags *pFlagsOut, void* pDipslayLinkContext)
{
    CVReturn ret = kCVReturnSuccess;
    
    GLView *glView = (GLView*) pDipslayLinkContext;
    
    ret  = [glView getFrameForTime:pOutTime];
    
    return (ret);
}





