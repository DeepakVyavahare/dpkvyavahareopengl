//color triangle cube-2 obj
#import <Foundation/Foundation.h>
#import <Cocoa/Cocoa.h>

#import <QuartzCore/CVDisplayLink.h>

#import <OpenGL/gl3.h>
#import <OpenGL/gl3ext.h>

#import "vmath.h"

enum
{
    VDG_ATTRIBUTE_VERTEX,
    VDG_ATTRIBUTE_COLOR,
    VDG_ATTRIBUTE_NORMAL,
    VDG_ATTRIBUTE_TEXTURE0
};

FILE *gpFile = NULL;

//'C' style function declarations
CVReturn MyDisplayCallback(CVDisplayLinkRef, const CVTimeStamp*, const CVTimeStamp*, CVOptionFlags, CVOptionFlags*, void*);

//interface declarations
@interface AppDelegate: NSObject <NSApplicationDelegate, NSWindowDelegate>
@end

@interface GLView: NSOpenGLView
@end

//entry point function
int main(int argc, const char* argv[])
{
    NSAutoreleasePool *pPool = [[NSAutoreleasePool alloc] init];
    
    NSApp = [NSApplication sharedApplication];
    
    [NSApp setDelegate: [[AppDelegate alloc] init]];
    
    [NSApp run];
    
    [pPool release];
    
    return (0);
}

//implementations
@implementation AppDelegate
{
@private
    NSWindow *window;
    GLView *glView;
}

-(void)applicationDidFinishLaunching:(NSNotification*)aNotification
{
    //code
    //create log file
    NSBundle *bundle = [NSBundle mainBundle];
    NSString *appDirPath = [bundle bundlePath];
    NSString *appDirParentPath = [appDirPath stringByDeletingLastPathComponent];
    NSString *logFilePath = [NSString stringWithFormat:@"%@/Log.txt", appDirParentPath];
    //covert it to const char*
    const char* szLogFilePath = [logFilePath cStringUsingEncoding:NSASCIIStringEncoding];
    
    gpFile = fopen(szLogFilePath, "w");
    if(gpFile == NULL)
    {
        printf("Can Not Open Log File. Exiting...\n");
        [self release];
        [NSApp terminate:self];
    }
    else
    {
        fprintf(gpFile, "Application Is Started Successfully\n");
    }
    
    //create window
    NSRect win_rect  = NSMakeRect(0.0, 0.0, 800.0, 600.0);
    window = [[NSWindow alloc] initWithContentRect:win_rect
                                         styleMask:NSWindowStyleMaskTitled | NSWindowStyleMaskClosable | NSWindowStyleMaskMiniaturizable | NSWindowStyleMaskResizable
                                           backing:NSBackingStoreBuffered
                                             defer:NO];
    [window setTitle:@"macOS:OpenGL --> 3D-Rotation : Pyramid-Cube"];
    [window center];
    
    //get view
    glView = [[GLView alloc] initWithFrame: [window frame]];
    
    //attach view to window
    [window setContentView:glView];
    [window setDelegate:self];
    [window makeKeyAndOrderFront:self];
}

-(void)applicationWillTerminate:(NSNotification*)aNotification
{
    if(gpFile)
    {
        fprintf(gpFile, "Application Is Terminated Successfully\n");
        fclose(gpFile);
        gpFile = NULL;
    }
}

-(void)windowWillClose:(NSNotification*)aNotification
{
    //code
    [NSApp terminate:self];
}

-(void)dealloc
{
    [glView release];
    [window release];
    
    [super dealloc];
}
@end

@implementation GLView
{
@private
    CVDisplayLinkRef displayLink;
    
    GLuint vertexShaderObject;
    GLuint fragmentShaderObject;
    GLuint shaderProgramObject;
    
    GLuint vao_pyramid;
    GLuint vbo_pyramid_position;
    GLuint vbo_pyramid_color;
    
    GLuint vao_cube;
    GLuint vbo_cube_position;
    GLuint vbo_cube_color;
    
    GLuint uniform_mvp;
    
    GLfloat anglePyramid;
    GLfloat angleCube;
    
    vmath::mat4 perspectiveProjectionMatrix;
}

-(id)initWithFrame:(NSRect)frame
{
    self = [super initWithFrame:frame];
    
    if(self)
    {
        [[self window] setContentView:self];
        
        //set pixel format attributes
        NSOpenGLPixelFormatAttribute attrs[] =
        {
            NSOpenGLPFAOpenGLProfile, NSOpenGLProfileVersion4_1Core,
            NSOpenGLPFAScreenMask, CGDisplayIDToOpenGLDisplayMask(kCGDirectMainDisplay),
            NSOpenGLPFANoRecovery,
            NSOpenGLPFAAccelerated,
            NSOpenGLPFAColorSize, 24,
            NSOpenGLPFADepthSize, 24,
            NSOpenGLPFAAlphaSize, 8,
            NSOpenGLPFADoubleBuffer,
            0 //last must be 0
        };
        
        //get pixel format from PFA attributes
        NSOpenGLPixelFormat *pixelFormat = [[[NSOpenGLPixelFormat alloc] initWithAttributes:attrs] autorelease];
        //get opengl context
        NSOpenGLContext *glContext = [[[NSOpenGLContext alloc] initWithFormat:pixelFormat shareContext:nil] autorelease];
        
        //set those
        [self setPixelFormat:pixelFormat];
        [self setOpenGLContext:glContext];
    }
    return (self);
}

//our method: called from CVDisplay thread
-(CVReturn)getFrameForTime:(const CVTimeStamp *)pOutTime
{
    NSAutoreleasePool *pPool = [[NSAutoreleasePool alloc] init];
    
    [self drawView];
    
    [pPool release];
    return (kCVReturnSuccess);
}

-(void)prepareOpenGL
{
    //code
    fprintf(gpFile, "OpenGL Version :%s\n", glGetString(GL_VERSION));
    fprintf(gpFile, "GLSL Version   :%s\n", glGetString(GL_SHADING_LANGUAGE_VERSION));
    
    //Set interval ..RR
    int swapInt = 1;
    [[self openGLContext] setValues:&swapInt forParameter:NSOpenGLCPSwapInterval];
    
    //OpenGL Shader Configuration/Initialization
    /**************VERTEX SHADER**********************/
    vertexShaderObject = glCreateShader(GL_VERTEX_SHADER);
    const GLchar *vsSource =
    "#version 410" \
    "\n" \
    "in vec4 vPosition;" \
    "in vec4 vColor;" \
    "uniform mat4 u_mvp_matrix;" \
    "out vec4 outColor;" \
    "void main(void)" \
    "{" \
    "gl_Position = u_mvp_matrix * vPosition;" \
    "outColor = vColor;" \
    "}";
    glShaderSource(vertexShaderObject, 1, &vsSource, NULL);
    glCompileShader(vertexShaderObject);
    GLint iShaderCompileStatus = 0;
    glGetShaderiv(vertexShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);
    if(GL_FALSE == iShaderCompileStatus)
    {
        GLint iShaderInfoLogLen = 0;
        glGetShaderiv(vertexShaderObject, GL_INFO_LOG_LENGTH, &iShaderInfoLogLen);
        if(iShaderInfoLogLen)
        {
            char *szLog = (char*)malloc(iShaderInfoLogLen);
            if(szLog)
            {
                GLint written;
                glGetShaderInfoLog(vertexShaderObject, iShaderInfoLogLen, &written, szLog);
                fprintf(gpFile, "Vertex Shader Info: %s\n", szLog);
                free(szLog);
                [self release];
                [NSApp terminate:self];
            }
        }
    }
    
    /**************FRAGMENT SHADER**********************/
    fragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);
    const GLchar *fsSource =
    "#version 410" \
    "\n" \
    "in vec4 outColor;" \
    "out vec4 FragColor;" \
    "void main(void)" \
    "{" \
    "FragColor = outColor;" \
    "}";
    glShaderSource(fragmentShaderObject, 1, &fsSource, NULL);
    glCompileShader(fragmentShaderObject);
    iShaderCompileStatus = 0;
    glGetShaderiv(fragmentShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);
    if(GL_FALSE == iShaderCompileStatus)
    {
        GLint iShaderInfoLogLen = 0;
        glGetShaderiv(fragmentShaderObject, GL_INFO_LOG_LENGTH, &iShaderInfoLogLen);
        if(iShaderInfoLogLen)
        {
            char* szLog = (char*)malloc(iShaderInfoLogLen);
            if(szLog)
            {
                GLint written;
                glGetShaderInfoLog(fragmentShaderObject, iShaderInfoLogLen, &written, szLog);
                fprintf(gpFile, "Fragment Shader Compile Info: %s\n", szLog);
                free(szLog);
                [self release];
                [NSApp terminate:self];
            }
        }
    }
    
    /**************PROGRAM SHADER**********************/
    shaderProgramObject = glCreateProgram();
    //attach vs and fs to program
    glAttachShader(shaderProgramObject, vertexShaderObject);
    glAttachShader(shaderProgramObject, fragmentShaderObject);
    
    //pre-link binding of shader attributes
    glBindAttribLocation(shaderProgramObject, VDG_ATTRIBUTE_VERTEX, "vPosition");
    glBindAttribLocation(shaderProgramObject, VDG_ATTRIBUTE_COLOR, "vColor");
    
    //link program
    glLinkProgram(shaderProgramObject);
    GLint iProgramLinkStatus = 0;
    glGetProgramiv(shaderProgramObject, GL_LINK_STATUS, &iProgramLinkStatus);
    if(GL_FALSE == iProgramLinkStatus)
    {
        GLint iProgramInfoLen = 0;
        glGetProgramiv(shaderProgramObject, GL_INFO_LOG_LENGTH, &iProgramInfoLen);
        if(iProgramInfoLen)
        {
            char *szLog = (char*) malloc(iProgramInfoLen);
            if(szLog)
            {
                GLint written;
                glGetProgramInfoLog(shaderProgramObject, iProgramInfoLen, &written, szLog);
                fprintf(gpFile, "Shader Link Status Info: %s\n", szLog);
                free(szLog);
                [self release];
                [NSApp terminate:self];
            }
        }
    }
    //post link get uniform locations
    uniform_mvp = glGetUniformLocation(shaderProgramObject, "u_mvp_matrix");
    
    /**************Vertices, Color, VAO, VBO****************/
    /*VAO: Pyramid */
    GLfloat pyramidVertices [] =
    {
         0.0f, 1.0f, 0.0f,               //apex
        -1.0f, -1.0f, 1.0f,             //left-bottom
        1.0f, -1.0f, 1.0f,              //right-bottom
        
         0.0f, 1.0f, 0.0f,               //apex
        1.0f, -1.0f, 1.0f,              //left-bottom
        1.0f, -1.0f, -1.0f,             //right-bottom
      
	    0.0f, 1.0f, 0.0f,               //apex
        1.0f, -1.0f, -1.0f,             //left-bottom
        -1.0f, -1.0f, -1.0f,    //right-bottom
        
        0.0f, 1.0f, 0.0f,               //apex
        -1.0f, -1.0f, -1.0f,            //left-bottom
        -1.0f, -1.0f, 1.0f,     //right-bottom
    };
    
    GLfloat pyramidColor [] =
    {
        1.0f,   0.0f,   0.0f,   //red
        0.0f,   1.0f,   0.0f,   //green
        0.0f,   0.0f,   1.0f,   //blue
        
        1.0f,   0.0f,   0.0f,   //red
        0.0f,   0.0f,   1.0f,    //blue
        0.0f,   1.0f,   0.0f,   //green
        
        1.0f,   0.0f,   0.0f,   //red
        0.0f,   1.0f,   0.0f,   //green
        0.0f,   0.0f,   1.0f,    //blue
        
        1.0f,   0.0f,   0.0f,   //red
        0.0f,   0.0f,   1.0f,    //blue
        0.0f,   1.0f,   0.0f   //green
    };
    
    //vao_pyramid
    glGenVertexArrays(1, &vao_pyramid);
    glBindVertexArray(vao_pyramid);
    
        glGenBuffers(1, &vbo_pyramid_position);
        glBindBuffer(GL_ARRAY_BUFFER, vbo_pyramid_position);
        glBufferData(GL_ARRAY_BUFFER, sizeof(pyramidVertices), pyramidVertices, GL_STATIC_DRAW);
        
        glVertexAttribPointer(VDG_ATTRIBUTE_VERTEX,
                              3, //x,y,z
                              GL_FLOAT,
                              GL_FALSE,
                              0, NULL);
        glEnableVertexAttribArray(VDG_ATTRIBUTE_VERTEX);
        
        glBindBuffer(GL_ARRAY_BUFFER, 0);   //unbind
        glGenBuffers(1, &vbo_pyramid_color);
        glBindBuffer(GL_ARRAY_BUFFER, vbo_pyramid_color);
        glBufferData(GL_ARRAY_BUFFER, sizeof(pyramidColor), pyramidColor, GL_STATIC_DRAW);
        
        glVertexAttribPointer(VDG_ATTRIBUTE_COLOR,
                              3,    //r,g,b
                              GL_FLOAT,
                              GL_FALSE,
                              0, NULL);
        glEnableVertexAttribArray(VDG_ATTRIBUTE_COLOR);
        
        glBindBuffer(GL_ARRAY_BUFFER, 0);   //unbind
    
    glBindVertexArray(0);   //unbind
    
    /*CUBE
    GLfloat cubeVertices [] =
    {
        1.0f, 1.0f, 1.0f,
        -1.0f, 1.0f, 1.0f,
        -1.0f, -1.0f, 1.0f,
        1.0f, -1.0f, 1.0f,
        
        1.0f, 1.0f, -1.0f,
        1.0f, 1.0f, 1.0f,
        1.0f, -1.0f, 1.0f,
        1.0f, -1.0f, -1.0f,
        
        -1.0f, 1.0f, -1.0f,
        1.0f, 1.0f, -1.0f,
        1.0f, -1.0f, -1.0f,
        -1.0f, -1.0f, -1.0f,
        
        /*left*/
        -1.0f, 1.0f, 1.0f,
        -1.0f, 1.0f, -1.0f,
        -1.0f, -1.0f, -1.0f,
        -1.0f, -1.0f, 1.0f,
        
        -1.0f, 1.0f, 1.0f,
        1.0f, 1.0f, 1.0f,
        1.0f, 1.0f, -1.0f,
        -1.0f, 1.0f, -1.0f,
        
        1.0f, -1.0f, -1.0f,
        -1.0f, -1.0f, -1.0f,
        -1.0f, -1.0f, 1.0f,
        1.0f, -1.0f, 1.0f,
    };
    
    //per vertex
    GLfloat cubeColor[] = {
        1.0f, 0.0f, 0.0f,  1.0f, 0.0f, 0.0f,  1.0f, 0.0f, 0.0f,  1.0f, 0.0f, 0.0f,
        0.0f, 1.0f, 0.0f,  0.0f, 1.0f, 0.0f,  0.0f, 1.0f, 0.0f,  0.0f, 1.0f, 0.0f,
        0.0f, 0.0f, 1.0f,   0.0f, 0.0f, 1.0f,  0.0f, 0.0f, 1.0f,  0.0f, 0.0f, 1.0f,
         1.0f, 1.0f, 0.0f,  1.0f, 1.0f, 0.0f,  1.0f, 1.0f, 0.0f,  1.0f, 1.0f, 0.0f,
          0.0f, 1.0f, 1.0f,  0.0f, 1.0f, 1.0f,  0.0f, 1.0f, 1.0f,  0.0f, 1.0f, 1.0f,
        1.0f, 0.0f, 1.0f,   1.0f, 0.0f, 1.0f,  1.0f, 0.0f, 1.0f,  1.0f, 0.0f, 1.0f,
    };
    
    //vao_cube
    glGenVertexArrays(1, &vao_cube);
    glBindVertexArray(vao_cube);
        glGenBuffers(1, &vbo_cube_position);
        glBindBuffer(GL_ARRAY_BUFFER, vbo_cube_position);
        glBufferData(GL_ARRAY_BUFFER, sizeof(cubeVertices), cubeVertices, GL_STATIC_DRAW);
        
        glVertexAttribPointer(VDG_ATTRIBUTE_VERTEX,
                              3,
                              GL_FLOAT,
                              GL_FALSE,
                              0, NULL);
        glEnableVertexAttribArray(VDG_ATTRIBUTE_VERTEX);
        glBindBuffer(GL_ARRAY_BUFFER, 0);   //unbind
   
   //color cube
        glGenBuffers(1, &vbo_cube_color);
        glBindBuffer(GL_ARRAY_BUFFER, vbo_cube_color);
        glBufferData(GL_ARRAY_BUFFER, sizeof(cubeColor), cubeColor, GL_STATIC_DRAW);
        
        glVertexAttribPointer(VDG_ATTRIBUTE_COLOR,
                              3,
                              GL_FLOAT,
                              GL_FALSE,
                              0, NULL);
        glEnableVertexAttribArray(VDG_ATTRIBUTE_COLOR);
        
        glBindBuffer(GL_ARRAY_BUFFER, 0);   //unbind
    
    
    glBindVertexArray(0);   //unbind
    
    //other opengl settings
    glClearDepth(1.0f);
    glEnable(GL_DEPTH_TEST);    //ENABLE depth test
    glDepthFunc(GL_LEQUAL);     // DEPTH test to do
    //glEnable(GL_CULL_FACE);     //we enable culling of back face for better performance
    
    //set background clearing color
    glClearColor(0.0f, 0.0f, 0.0f, 1.0f);   //black
    
    //set perspective projection matrix to identity matrix
    perspectiveProjectionMatrix = vmath::mat4::identity();
    anglePyramid = 0.0f;
    angleCube = 0.0f;
    
    //CVDisplayLink settings
    CVDisplayLinkCreateWithActiveCGDisplays(&displayLink);      //create CVDisplayLinkRef
    CVDisplayLinkSetOutputCallback(displayLink, &MyDisplayCallback, self);  //register callback on display refresh rate
    //convert NSOpenGL context and pixel format to CGL context and pixel format
    CGLPixelFormatObj cglPixelFormat = (CGLPixelFormatObj) [[self pixelFormat] CGLPixelFormatObj];
    CGLContextObj cglContext = (CGLContextObj) [[self openGLContext] CGLContextObj];
    
    CVDisplayLinkSetCurrentCGDisplayFromOpenGLContext(displayLink, cglContext, cglPixelFormat);
    CVDisplayLinkStart(displayLink);
}

-(void)reshape
{
    CGLLockContext((CGLContextObj) [[self openGLContext] CGLContextObj]);
    
    NSRect rect = [self bounds];
    GLfloat width = rect.size.width;
    GLfloat height = rect.size.height;
    
    glViewport(0, 0, (GLsizei)width, (GLsizei)height);
    
    //perspective projection --> (fovy, AR, near, far)
    perspectiveProjectionMatrix = vmath::perspective(45.0f, width/height, 0.1f, 1000.0f);
    
    CGLUnlockContext((CGLContextObj) [[self openGLContext] CGLContextObj]);
}

-(void)drawRect:(NSRect)dirtyRect
{
    //code
    [self drawView];    //this called in context of main thread
}

-(void)drawView
{
    CGLLockContext((CGLContextObj) [[self openGLContext] CGLContextObj]);
    
    [[self openGLContext] makeCurrentContext];
    
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glUseProgram(shaderProgramObject);
    
    //PYRAMID
    vmath::mat4 modelViewMatrix = vmath::mat4::identity();
    vmath::mat4 translateMatrix = vmath::mat4::identity();
    vmath::mat4 rotationMatrix = vmath::mat4::identity();
    vmath::mat4 modelViewProjectionMatrix = vmath::mat4::identity();
    
    
    rotationMatrix = vmath::rotate(anglePyramid, 0.0f, 1.0f, 0.0f);
    //MV = R * MV
    modelViewMatrix = rotationMatrix * modelViewMatrix;
    
    
    translateMatrix = vmath::translate(-1.5f, 0.0f, -6.0f);
    //MV = T * MV
    modelViewMatrix = translateMatrix * modelViewMatrix;
    
    
    //MVP = P * MV
    modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;
    //send MVP to shader
    glUniformMatrix4fv(uniform_mvp, 1, GL_FALSE, modelViewProjectionMatrix);
    
    //bind vao_pyramid
    glBindVertexArray(vao_pyramid);
    glDrawArrays(GL_TRIANGLES, 0, 3);
    glDrawArrays(GL_TRIANGLES, 3, 3);
    glDrawArrays(GL_TRIANGLES, 6, 3);
    glDrawArrays(GL_TRIANGLES, 9, 3);
    glBindVertexArray(0);   //unbind
    
    //CUBE
    modelViewMatrix = vmath::mat4::identity();
    translateMatrix = vmath::mat4::identity();
    rotationMatrix = vmath::mat4::identity();
    modelViewProjectionMatrix = vmath::mat4::identity();
    vmath::mat4 scaleMatrix = vmath::mat4::identity();
    
    scaleMatrix = vmath::scale(0.75f, 0.75f, 0.75f);
    //MV = S * MV
    modelViewMatrix = scaleMatrix * modelViewMatrix;
    
    rotationMatrix = vmath::rotate(angleCube, 1.0f, 0.0f, 0.0f);
    rotationMatrix *= vmath::rotate(angleCube, 0.0f, 1.0f, 0.0f);
    rotationMatrix *= vmath::rotate(angleCube, 0.0f, 0.0f, 1.0f);
    
    //MV = R * MV
    modelViewMatrix = rotationMatrix * modelViewMatrix;
    
    translateMatrix = vmath::translate(1.5f, 0.0f, -6.0f);
    //MV = T * MV
    modelViewMatrix = translateMatrix * modelViewMatrix;
    
    //MVP = P * MV
    modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;
    //send MVP to shader
    glUniformMatrix4fv(uniform_mvp, 1, GL_FALSE, modelViewProjectionMatrix);
    
    //bind vao_quad
    glBindVertexArray(vao_cube);
    glDrawArrays(GL_TRIANGLE_FAN, 0, 4);    //T1(0,1,2) AND T2(0,2,3)
    glDrawArrays(GL_TRIANGLE_FAN, 4, 4);    //T1(0,1,2) AND T2(0,2,3)
    glDrawArrays(GL_TRIANGLE_FAN, 8, 4);    //T1(0,1,2) AND T2(0,2,3)
    glDrawArrays(GL_TRIANGLE_FAN, 12, 4);    //T1(0,1,2) AND T2(0,2,3)
    glDrawArrays(GL_TRIANGLE_FAN, 16, 4);    //T1(0,1,2) AND T2(0,2,3)
    glDrawArrays(GL_TRIANGLE_FAN, 20, 4);    //T1(0,1,2) AND T2(0,2,3)
    glBindVertexArray(0); //unbind
    
    glUseProgram(0);    //un-use program
    
    [self spin];
    
    CGLFlushDrawable((CGLContextObj) [[self openGLContext] CGLContextObj]);
    CGLUnlockContext((CGLContextObj) [[self openGLContext] CGLContextObj]);
}

//our method
-(void)spin
{
    anglePyramid = anglePyramid + 2.0f;
    if(anglePyramid > 360.0f)
    anglePyramid -= 360.0f;
    
    angleCube = angleCube + 2.0f;
    if(angleCube > 360.0f)
    angleCube -= 360.0f;
}

//hardware event handling
-(BOOL)acceptsFirstResponder
{
    //code
    [[self window] makeFirstResponder:self];
    return (YES);
}

-(void)keyDown:(NSEvent*)theEvent
{
    int key = (int)[[theEvent characters] characterAtIndex:0];
    switch(key)
    {
            case 27:
            [self release];
            [NSApp terminate:self];
            break;
            case 'F':
            case'f':
            [[self window] toggleFullScreen:self];  //auto repaint
            break;
        default:
            break;
    }
}

-(void)dealloc
{
    CVDisplayLinkStop(displayLink);
    CVDisplayLinkRelease(displayLink);
    
    if(vao_cube)
    {
        glDeleteVertexArrays(1, &vao_cube);
        vao_cube = 0;
    }
    if(vbo_cube_position)
    {
        glDeleteBuffers(1, &vbo_cube_position);
        vbo_cube_position = 0;
    }
    if(vao_pyramid)
    {
        glDeleteVertexArrays(1, &vao_pyramid);
        vao_pyramid = 0;
    }
    if(vbo_pyramid_position)
    {
        glDeleteBuffers(1, &vbo_pyramid_position);
        vbo_pyramid_position = 0;
    }
    
    if(shaderProgramObject)
    {
        if(fragmentShaderObject)
        {
            glDetachShader(shaderProgramObject, fragmentShaderObject);
            glDeleteShader(fragmentShaderObject);
            fragmentShaderObject = 0;
        }
        if(vertexShaderObject)
        {
            glDetachShader(shaderProgramObject, vertexShaderObject);
            glDeleteShader(vertexShaderObject);
            vertexShaderObject = 0;
        }
        glDeleteProgram(shaderProgramObject);
        shaderProgramObject = 0;
    }
    
    [super dealloc];
}
@end

CVReturn MyDisplayCallback(CVDisplayLinkRef displayLink, const CVTimeStamp *pNow, const CVTimeStamp *pOutTime, CVOptionFlags FlagsIn, CVOptionFlags *pFlagsOut, void* pDipslayLinkContext)
{
    CVReturn ret = kCVReturnSuccess;
    
    GLView *glView = (GLView*) pDipslayLinkContext;
    
    ret  = [glView getFrameForTime:pOutTime];
    
    return (ret);
}





