//LIGHT per vertex....
// global variable after CVDisplayLink
//AppDelegate No change
//PrepareOpengl==Initialize..hence all initialize use here..its override function also..called at base class
    //use after Swap.. && CV cha varti
//for all 'using namespace' avoid...hence use :: operator
//resize..all windows code bewen lock & unlock...al code is same
//display code/...all windows code bewen lock & unlock
//dealloc: uninitialize..all windows code bewen lock & unlock

#import <Foundation/Foundation.h>
#import <Cocoa/Cocoa.h>

//Core Video
#import <QuartzCore/CVDisplayLink.h>

//OpenGL
#import <OpenGL/gl3.h>
#import <OpenGL/gl3ext.h>

#import "vmath.h"
#import "sphere.h"   //shpere

enum
{
    VDG_ATTRIBUTE_POSITION = 0,
    VDG_ATTRIBUTE_COLOR,
    VDG_ATTRIBUTE_NORMAL,
    VDG_ATTRIBUTE_TEXTURE0,
};

GLfloat lightAmbient[] = { 0.0f,0.0f,0.0f,1.0f };
GLfloat lightDiffuse[] = { 1.0f,1.0f,1.0f,1.0f };
GLfloat lightSpecular[] = { 1.0f,1.0f,1.0f,1.0f };
GLfloat lightPosition[] = { 100.0f,100.0f,100.0f,1.0f };

GLfloat materialAmbient[] = { 0.0f, 0.0f,0.0f,1.0f };
GLfloat materialDiffuse[] = { 1.0f,1.0f,1.0f,1.0f };
GLfloat materialSpecular[] = { 1.0f, 1.0f,1.0f,1.0f };
GLfloat materialShininess = 50.0f;


//Sphere declarations
GLuint gNumElements;
GLuint gNumVertices;
GLfloat sphere_vertices[1146];
GLfloat sphere_normals[1146];
GLfloat sphere_textures[764];
GLshort sphere_elements[2280];

//log file pointer
FILE *gpFile = NULL;

// 'C' style function declaration
CVReturn MyDisplayLinkCallback(CVDisplayLinkRef, const CVTimeStamp*, const CVTimeStamp*, CVOptionFlags, CVOptionFlags*, void*);

//interface declarations
@interface AppDelegate: NSObject <NSApplicationDelegate, NSWindowDelegate>
@end

@interface GLView: NSOpenGLView
@end

//entry point function
int main(int argc, const char *argv[])
{
    NSAutoreleasePool *pPool = [[NSAutoreleasePool alloc] init];
    
    NSApp = [NSApplication sharedApplication];
    
    [NSApp setDelegate:[[AppDelegate alloc] init]];
    
    [NSApp run];    //run loop
    
    [pPool release];
    
    return (0);
}

//implementations
@implementation AppDelegate
{
@private
    NSWindow *window;
    GLView *glView;
}

-(void)applicationDidFinishLaunching:(NSNotification*)aNotification
{
    //code
    //create log file
    NSBundle *bundle = [NSBundle mainBundle];
    NSString *appDirPath = [bundle bundlePath];
    NSString *appDirParentPath = [appDirPath stringByDeletingLastPathComponent];
    NSString *logFilePath = [NSString stringWithFormat:@"%@/Log.txt",appDirParentPath];
    //convert NSSTring to char*
    const char* szLogFilePath = [logFilePath cStringUsingEncoding:NSASCIIStringEncoding];
    
    gpFile = fopen(szLogFilePath, "w");
    if(gpFile == NULL)
    {
        printf("Cannot Open Log.txt. Exiting...\n");
        [self release];
        [NSApp terminate:self];
    }
    else
    {
        fprintf(gpFile, "Applicaton Is Started Successfully\n");
    }
    
    //create window
    NSRect win_rect = NSMakeRect(0.0, 0.0, 800.0, 600.0);
    window = [[NSWindow alloc] initWithContentRect:win_rect
                                         styleMask:NSWindowStyleMaskTitled | NSWindowStyleMaskClosable | NSWindowStyleMaskMiniaturizable | NSWindowStyleMaskResizable
                                           backing:NSBackingStoreBuffered
                                             defer:NO];
    
    //set window properties
    [window setTitle:@"Light: PerVertex"];     // <--- Modify as per requirement
    [window center];
    
    //create OpenGL view
    glView = [[GLView alloc] initWithFrame:[window frame]];
    
    if(glView == nil)
    {
        fprintf(gpFile, "Unable to Create GLView. Exiting...\n");
        [self release];
        [NSApp terminate:self];
    }
    
    //attach glview to window
    [window setContentView:glView];
    [window setDelegate:self];
    [window makeKeyAndOrderFront:self];
}

-(void)applicationWillTerminate:(NSNotification*)aNotification
{
    fprintf(gpFile, "Application Is Close Successfully\n");
    if(gpFile)
    {
        fclose(gpFile);
        gpFile = NULL;
    }
}

-(void)windowWillClose:(NSNotification*)aNotification
{
    //code
    [NSApp terminate:self];
}

-(void)dealloc
{
    //code
    [glView release];
    [window release];
    
    [super dealloc];
}
@end

@implementation GLView
{
@private
    CVDisplayLinkRef displayLink;
    
    Sphere *sphereVariable;
    
    //your shader variables
    GLuint vertexShaderObjectPerVertexVariable;
    GLuint fragmentShaderObjectPerVertexVariable;
    GLuint shaderProgramObjectPerVertexVariable;
    
    enum eShapes
    {
        SPHERE = 0,
        NR_SHAPES,
    };
    enum eShape_Attr
    {
        POSITION = 0,
        NORMAL,
        ELEMENTS,
        NR_SHAPE_ATTRS,
    };
    enum eUniforms
    {
        MV = 0,
        PROJECTION,
        LA,
        KA,
        LD,
        KD,
        LS,
        KS,
        SHININESS,
        LIGHT_POS,
        LIGHT_EN,
        NR_UNIFORMS,
    };
    
    GLuint vao[NR_SHAPES];
    GLuint vbo[NR_SHAPES][NR_SHAPE_ATTRS];
    GLuint uniform[NR_UNIFORMS];
    
    BOOL bLighting_enable;
    BOOL bPerVertex;
    BOOL bPerFragment;
    
    vmath::mat4 projectonMatrix;
}

-(id)initWithFrame:(NSRect)frame
{
    self = [super initWithFrame:frame];
    
    if(self)
    {
        [[self window] setContentView:self];
        
        //Pixel Format Attribute list
        NSOpenGLPixelFormatAttribute attrs [] =
        {
            //key,value(optional)
            NSOpenGLPFAOpenGLProfile,NSOpenGLProfileVersion4_1Core,
            NSOpenGLPFAScreenMask,CGDisplayIDToOpenGLDisplayMask(kCGDirectMainDisplay),
            NSOpenGLPFANoRecovery,   //dont fallback to sw renderer if hw doesnot found
            NSOpenGLPFAAccelerated,
            NSOpenGLPFAColorSize,24,
            NSOpenGLPFADepthSize,24,
            NSOpenGLPFAAlphaSize,8,
            NSOpenGLPFADoubleBuffer,
            0   //last must be 0 indicating end of list
        };
        
        //Ask NSOpenGL for pixel format by providing above attributes
        NSOpenGLPixelFormat *pixelFormat = [[[NSOpenGLPixelFormat alloc] initWithAttributes:attrs] autorelease];
        if(pixelFormat == nil)
        {
            fprintf(gpFile, "No Valid Pixel Format Available. Exiting..\n");
            [self release];
            [NSApp terminate:self];
        }
        
        //Ask NSOpenGL for context by providing pixelFormat
        NSOpenGLContext *glContext = [[[NSOpenGLContext alloc] initWithFormat:pixelFormat shareContext:nil] autorelease];
        
        //Save context and pixel format in GLView
        [self setPixelFormat:pixelFormat];
        [self setOpenGLContext:glContext];
    }
    return (self);
}

-(CVReturn)getFrameForTime:(const CVTimeStamp*)pOutTime
{
    NSAutoreleasePool *pPool = [[NSAutoreleasePool alloc] init];
    
    [self drawView];
    
    [pPool release];
    return (kCVReturnSuccess);
}

-(void)prepareOpenGL
{
    //code
    fprintf(gpFile, "OpenGL Version :%s\n", glGetString(GL_VERSION));
    fprintf(gpFile, "GLSL Version   :%s\n", glGetString(GL_SHADING_LANGUAGE_VERSION));
    
    [[self openGLContext] makeCurrentContext];
    
    //set swap interval to 1, to render your scene on display refresh rate
    GLint swapInt = 1;
    [[self openGLContext] setValues:&swapInt forParameter:NSOpenGLCPSwapInterval];
    
    //================================vertexShader
    
    vertexShaderObjectPerVertexVariable = glCreateShader(GL_VERTEX_SHADER);
    const GLchar* vsSource =
    "#version 410 core" \
    "\n" \
    "in vec4 vPosition;" \
    "in vec3 vNormal;" \
    "uniform mat4 u_model_view_matrix;" \
    "uniform mat4 u_projection_matrix;" \
    "uniform int u_light_enabled;" \
    "uniform vec4 u_light_position;" \
    "uniform vec3 u_La;" \
    "uniform vec3 u_Ld;" \
    "uniform vec3 u_Ls;" \
    "uniform vec3 u_Ka;" \
    "uniform vec3 u_Kd;" \
    "uniform vec3 u_Ks;" \
    "uniform float u_material_shininess;" \
    "out vec3 phong_ads_color;" \
    "void main(void)" \
    "{" \
    "if(u_light_enabled==1)" \
    "{" \
    "vec3 ambient = u_La * u_Ka;" \
    "vec4 eyeCoordinates = u_model_view_matrix * vPosition;" \
    "vec3 transformed_normals = normalize(mat3(u_model_view_matrix) * vNormal);" \
    "vec3 light_direction = normalize(vec3(u_light_position) - eyeCoordinates.xyz);" \
    "float tn_dot_ld = max(dot(transformed_normals, light_direction), 0.0);" \
    "vec3 diffuse = u_Ld * u_Kd * tn_dot_ld;" \
    
    "vec3 reflection_vector = reflect(-light_direction, transformed_normals);" \
    "vec3 viewer_vector = normalize(-eyeCoordinates.xyz);" \
    "float r_dot_v = max(dot(reflection_vector, viewer_vector), 0.0);" \
    "vec3 specular = u_Ls * u_Ks * pow(r_dot_v, u_material_shininess);" \
    "phong_ads_color = ambient + diffuse + specular;" \
    "}"\
    "else" \
    "{" \
    "phong_ads_color = vec3(1.0, 1.0, 1.0);" \
    "}" \
    "gl_Position = u_projection_matrix * u_model_view_matrix * vPosition;" \
    "}";
    
    
    glShaderSource(vertexShaderObjectPerVertexVariable, 1, (const GLchar**)&vsSource, NULL);
    glCompileShader(vertexShaderObjectPerVertexVariable);
    GLint iShaderCompileStatus = 0;
    glGetShaderiv(vertexShaderObjectPerVertexVariable, GL_COMPILE_STATUS, &iShaderCompileStatus);
    if(GL_FALSE == iShaderCompileStatus)
    {
        GLsizei iLogLen = 0;
        glGetShaderiv(vertexShaderObjectPerVertexVariable, GL_INFO_LOG_LENGTH, &iLogLen);
        if(iLogLen)
        {
            GLchar* szLog = (GLchar*)malloc(iLogLen);
            if(szLog)
            {
                GLsizei written;
                glGetShaderInfoLog(vertexShaderObjectPerVertexVariable, iLogLen, &written, szLog);
                fprintf(gpFile, "PV: Vertex Shader Compile Info: %s\n", szLog);
                free(szLog);
                [self release];
                [NSApp terminate:self];
            }
        }
    }//================
    
    /*****FRAGMENT SHADER********/
    fragmentShaderObjectPerVertexVariable = glCreateShader(GL_FRAGMENT_SHADER);
    const GLchar* fsSource =
    "#version 410 core" \
    "\n" \
    "in vec3 phong_ads_color;" \
    "out vec4 FragColor;" \
    "void main(void)" \
    "{" \
    "FragColor = vec4(phong_ads_color, 1.0);" \
    "}";
    
    glShaderSource(fragmentShaderObjectPerVertexVariable, 1, (const GLchar**)&fsSource, NULL);
    glCompileShader(fragmentShaderObjectPerVertexVariable);
    iShaderCompileStatus = 0;
    glGetShaderiv(fragmentShaderObjectPerVertexVariable, GL_COMPILE_STATUS, &iShaderCompileStatus);
    if(GL_FALSE == iShaderCompileStatus)
    {
        GLsizei iLogLen = 0;
        glGetShaderiv(fragmentShaderObjectPerVertexVariable, GL_INFO_LOG_LENGTH, &iLogLen);
        if(iLogLen)
        {
            GLchar* szLog = (GLchar*)malloc(iLogLen);
            if(szLog)
            {
                GLsizei written;
                glGetShaderInfoLog(fragmentShaderObjectPerVertexVariable, iLogLen, &written, szLog);
                fprintf(gpFile, "PV: Fragment Shader Compile Info: %s\n", szLog);
                free(szLog);
                [self release];
                [NSApp terminate:self];
            }
        }
    }//===================
    
    /*****SHADER PROGRAM********/
    shaderProgramObjectPerVertexVariable = glCreateProgram();
    //attach vs and fs to shader program
    glAttachShader(shaderProgramObjectPerVertexVariable, vertexShaderObjectPerVertexVariable);
    glAttachShader(shaderProgramObjectPerVertexVariable, fragmentShaderObjectPerVertexVariable);
    
    //pre-link binding of vertex attributes
    glBindAttribLocation(shaderProgramObjectPerVertexVariable, VDG_ATTRIBUTE_POSITION, "vPosition");     // <--- Modify/add as per requirement
    glBindAttribLocation(shaderProgramObjectPerVertexVariable, VDG_ATTRIBUTE_NORMAL, "vNormal");
    
    //link program
    glLinkProgram(shaderProgramObjectPerVertexVariable);
    
    //post-link, get uniform locations from shaders
    uniform[MV] = glGetUniformLocation(shaderProgramObjectPerVertexVariable, "u_model_view_matrix");           // <--- Modify/add as per requirement
    uniform[PROJECTION] = glGetUniformLocation(shaderProgramObjectPerVertexVariable, "u_projection_matrix");
    uniform[LA] = glGetUniformLocation(shaderProgramObjectPerVertexVariable, "u_La");
    uniform[KA] = glGetUniformLocation(shaderProgramObjectPerVertexVariable, "u_Ka");
    uniform[LD] = glGetUniformLocation(shaderProgramObjectPerVertexVariable, "u_Ld");
    uniform[KD] = glGetUniformLocation(shaderProgramObjectPerVertexVariable, "u_Kd");
    uniform[LS] = glGetUniformLocation(shaderProgramObjectPerVertexVariable, "u_Ls");
    uniform[KS] = glGetUniformLocation(shaderProgramObjectPerVertexVariable, "u_Ks");
    uniform[SHININESS] = glGetUniformLocation(shaderProgramObjectPerVertexVariable, "u_material_shininess");
    uniform[LIGHT_POS] = glGetUniformLocation(shaderProgramObjectPerVertexVariable, "u_light_position");
    uniform[LIGHT_EN] = glGetUniformLocation(shaderProgramObjectPerVertexVariable, "u_light_enabled");
    //=========================================
    /*******LOAD TEXTURES****************/
    //=============
    
    /*************VERTICES, COLOR, NORMALS, VAO, VBO INITIALIZATIONS******************/
    
    //Sphere
    sphereVariable = [[Sphere alloc] init];
    [sphereVariable getSphereVertexData:sphere_vertices :sphere_normals :sphere_textures :sphere_elements];
    gNumVertices = [sphereVariable getNumberOfSphereVertices];
    gNumElements = [sphereVariable getNumberOfSphereElements];
    
    fprintf(gpFile, "nr vertices:%d\n", gNumVertices);
    fprintf(gpFile, "nr elements:%d\n", gNumElements);
    //============
    //VAO:Sphere
    glGenVertexArrays(1, &vao[SPHERE]);
    glBindVertexArray(vao[SPHERE]);
    //VBO1: Position
    {
        glGenBuffers(1, &vbo[SPHERE][POSITION]);
        glBindBuffer(GL_ARRAY_BUFFER, vbo[SPHERE][POSITION]);
        glBufferData(GL_ARRAY_BUFFER, sizeof(sphere_vertices), sphere_vertices, GL_STATIC_DRAW);
        glVertexAttribPointer(VDG_ATTRIBUTE_POSITION,
                              3, //xyz
                              GL_FLOAT,
                              GL_FALSE, //non-normalize
                              0, NULL); //0=stride
        glEnableVertexAttribArray(VDG_ATTRIBUTE_POSITION);
        glBindBuffer(GL_ARRAY_BUFFER, 0);   //unbind
    }
    //VBO2: Normals
    {
        glGenBuffers(1, &vbo[SPHERE][NORMAL]);
        glBindBuffer(GL_ARRAY_BUFFER, vbo[SPHERE][NORMAL]);
        glBufferData(GL_ARRAY_BUFFER, sizeof(sphere_normals), sphere_normals, GL_STATIC_DRAW);
        glVertexAttribPointer(VDG_ATTRIBUTE_NORMAL,
                              3, //nx, ny, nz
                              GL_FLOAT,
                              GL_FALSE, //non-normalize
                              0, NULL); //0=stride
        glEnableVertexAttribArray(VDG_ATTRIBUTE_NORMAL);
        glBindBuffer(GL_ARRAY_BUFFER, 0);   //unbind
    }
    //VBO3: Elements
    {
        glGenBuffers(1, &vbo[SPHERE][ELEMENTS]);
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo[SPHERE][ELEMENTS]);
        
        glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(sphere_elements), sphere_elements, GL_STATIC_DRAW);
        
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);   //unbind
    }
    glBindVertexArray(0);   //unbind
    //==================
    
    glClearDepth(1.0f);
    glEnable(GL_DEPTH_TEST);
    glDepthFunc(GL_LEQUAL);
    //glEnable(GL_CULL_FACE);

    

    glClearColor(0.0f, 0.0f, 0.0f, 1.0f);   //black
    
    // matrix in perspective projection matrix
    projectonMatrix = vmath::mat4::identity();
    
    bLighting_enable = YES;
    
    /*************CVDisplayLink INIT***************************/
    //Create Core Video Link
    CVDisplayLinkCreateWithActiveCGDisplays(&displayLink);
    //Set Core Video Callback
    CVDisplayLinkSetOutputCallback(displayLink, &MyDisplayLinkCallback, self);
    //Convert NSOpenGL context and pixelformat to CGL
    CGLPixelFormatObj cglPixelFormat = (CGLPixelFormatObj) [[self pixelFormat] CGLPixelFormatObj];
    CGLContextObj cglContext = (CGLContextObj) [[self openGLContext] CGLContextObj];
    //Set Core Video Current display
    CVDisplayLinkSetCurrentCGDisplayFromOpenGLContext(displayLink, cglContext, cglPixelFormat);
    //Start Core Video
    CVDisplayLinkStart(displayLink);
}

-(void)reshape
{
    //code
    CGLLockContext((CGLContextObj) [[self openGLContext] CGLContextObj]);
    
    NSRect rect = [self bounds];
    GLfloat width = rect.size.width;
    GLfloat height = rect.size.height;
    
    //Set opengl viewport
    glViewport(0, 0, (GLsizei)width, (GLsizei)height);

    projectonMatrix = vmath::perspective(45.0f, width/height, 0.1f, 100.0f);
    
    CGLUnlockContext((CGLContextObj) [[self openGLContext] CGLContextObj]);
}

-(void)drawRect:(NSRect)dirtyRect
{
    //code
    [self drawView];
}

//our method
-(void)drawView
{
    //code
    static GLfloat angleCube = 0.0f;
    
    CGLLockContext((CGLContextObj) [[self openGLContext] CGLContextObj]);
    [[self openGLContext] makeCurrentContext];
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    
    // take uniform locations from shaders
    uniform[MV] = glGetUniformLocation(shaderProgramObjectPerVertexVariable, "u_model_view_matrix");
    uniform[PROJECTION] = glGetUniformLocation(shaderProgramObjectPerVertexVariable, "u_projection_matrix");
    uniform[LA] = glGetUniformLocation(shaderProgramObjectPerVertexVariable, "u_La");
    uniform[KA] = glGetUniformLocation(shaderProgramObjectPerVertexVariable, "u_Ka");
    uniform[LD] = glGetUniformLocation(shaderProgramObjectPerVertexVariable, "u_Ld");
    uniform[KD] = glGetUniformLocation(shaderProgramObjectPerVertexVariable, "u_Kd");
    uniform[LS] = glGetUniformLocation(shaderProgramObjectPerVertexVariable, "u_Ls");
    uniform[KS] = glGetUniformLocation(shaderProgramObjectPerVertexVariable, "u_Ks");
    uniform[SHININESS] = glGetUniformLocation(shaderProgramObjectPerVertexVariable, "u_material_shininess");
    uniform[LIGHT_POS] = glGetUniformLocation(shaderProgramObjectPerVertexVariable, "u_light_position");
    uniform[LIGHT_EN] = glGetUniformLocation(shaderProgramObjectPerVertexVariable, "u_light_enabled");
    
        glUseProgram(shaderProgramObjectPerVertexVariable);
  
    if(bLighting_enable == YES)
    {
        glUniform1i(uniform[LIGHT_EN], 1);
        
        glUniform3fv(uniform[LA], 1, lightAmbient);
        glUniform3fv(uniform[KA], 1, materialAmbient);
        glUniform3fv(uniform[LD], 1, lightDiffuse);
        glUniform3fv(uniform[KD], 1, materialDiffuse);
        glUniform3fv(uniform[LS], 1, lightSpecular);
        glUniform3fv(uniform[KS], 1, materialSpecular);
        glUniform1f(uniform[SHININESS], materialShininess);
        
        glUniform4fv(uniform[LIGHT_POS], 1, lightPosition);
    }
    else
    {
        glUniform1i(uniform[LIGHT_EN], 0);
    }
    //==========
    
    //CUBE
    vmath::mat4 modelViewMatrix = vmath::mat4::identity();
    vmath::mat4 translateMatrix = vmath::mat4::identity();
    vmath::mat4 modelViewProjectionMatrix = vmath::mat4::identity();
    
    translateMatrix = vmath::translate(0.0f, 0.0f, -4.0f);
    modelViewMatrix = translateMatrix * modelViewMatrix;
    
    glUniformMatrix4fv(uniform[MV], 1, GL_FALSE, modelViewMatrix);
    
    glUniformMatrix4fv(uniform[PROJECTION], 1, GL_FALSE, projectonMatrix);
    
    glBindVertexArray(vao[SPHERE]);
    
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo[SPHERE][ELEMENTS]);
    glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);
    glBindVertexArray(0);
    
    glUseProgram(0);
    CGLFlushDrawable((CGLContextObj) [[self openGLContext] CGLContextObj]);     CGLUnlockContext((CGLContextObj) [[self openGLContext] CGLContextObj]);
    
    angleCube = angleCube + 2.0f;
}

-(BOOL)acceptsFirstResponder
{
    //code
    [[self window] makeFirstResponder:self];
    
    return (YES);
}

-(void)keyDown:(NSEvent*)theEvent
{
    //code
    int key = (int) [[theEvent characters] characterAtIndex:0];
    switch(key)
    {
        case 27:
            [[self window] toggleFullScreen:self];
            break;
        case 'f':
        case 'F':
            bPerFragment = YES;
            bPerVertex = NO;
            break;
        case 'L':
        case 'l':
            if(bLighting_enable == NO)
                bLighting_enable = YES;
            else
                bLighting_enable = NO;
            break;
        case 'V':
        case 'v':
            bPerFragment = NO;
            bPerVertex = YES;
            break;
        case 'Q':
        case 'q':
            [self release];
            [NSApp terminate:self];
            break;
        default:
            break;
    }
}

-(void)mouseDown:(NSEvent*)theEvent
{
    //code
}

-(void)mouseDragged:(NSEvent*)theEvent
{
    //code
}

-(void)rightMouseDown:(NSEvent*)theEvent
{
    //code
}

-(void)dealloc
{
    //stop and release Core Video link
    CVDisplayLinkStop(displayLink);
    CVDisplayLinkRelease(displayLink);
    

    if(shaderProgramObjectPerVertexVariable)
    {
        if(fragmentShaderObjectPerVertexVariable)
        {
            glDetachShader(shaderProgramObjectPerVertexVariable, fragmentShaderObjectPerVertexVariable);
            glDeleteShader(fragmentShaderObjectPerVertexVariable);
            fragmentShaderObjectPerVertexVariable = 0;
        }
        if(vertexShaderObjectPerVertexVariable)
        {
            glDetachShader(shaderProgramObjectPerVertexVariable, vertexShaderObjectPerVertexVariable);
            glDeleteShader(vertexShaderObjectPerVertexVariable);
            vertexShaderObjectPerVertexVariable = 0;
        }
        
        glDeleteProgram(shaderProgramObjectPerVertexVariable);
        shaderProgramObjectPerVertexVariable = 0;
    }
    
    //Un-use shader program
    glUseProgram(0);
    
    [sphereVariable release];
    [super dealloc];
}

@end

CVReturn MyDisplayLinkCallback(CVDisplayLinkRef displayLink, const CVTimeStamp* pNow, const CVTimeStamp *pOutTime, CVOptionFlags FlagsIn, CVOptionFlags *FlagsOut, void* pCVDisplayContext)
{
    CVReturn ret = kCVReturnSuccess;
    
    GLView *glView = (GLView*) pCVDisplayContext;
    
    ret = (CVReturn) [glView getFrameForTime:pOutTime];
    
    return (ret);
}
