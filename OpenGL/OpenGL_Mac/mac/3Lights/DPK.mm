//3 lights

#import <Foundation/Foundation.h>
#import <Cocoa/Cocoa.h>

//Core Video
#import <QuartzCore/CVDisplayLink.h>

//OpenGL
#import <OpenGL/gl3.h>
#import <OpenGL/gl3ext.h>

//vector maths --> matrix/vector operations
#import "vmath.h"

#import "sphere.h"

#define PI            (3.14151415f)

enum
{
    VDG_ATTRIBUTE_POSITION = 0,
    VDG_ATTRIBUTE_COLOR,
    VDG_ATTRIBUTE_NORMAL,
    VDG_ATTRIBUTE_TEXTURE0,
};

GLfloat gAngle = 0.0f;

GLfloat lightAmbient[] = { 0.0f, 0.0f, 0.0f, 1.0f };    // ambient decides general light
GLfloat lightDiffuse[] = { 1.0f, 0.0f, 0.0f, 1.0f };    // diffuse decides light color
GLfloat lightSpecular[] = { 1.0f, 0.0f, 0.0f, 1.0f };    // specular decides highlight color of light
GLfloat light0RedPosition[] = { 0.0f, 0.0f, 0.0f, 0.0f };    // manupulate direction in display

GLfloat lightAmbient1[] = { 0.0f, 0.0f, 0.0f, 1.0f };    // ambient decides general light
GLfloat lightDiffuse1[] = { 0.0f, 1.0f, 0.0f, 1.0f };    // diffuse decides light color
GLfloat lightSpecular1[] = { 0.0f, 1.0f, 0.0f, 1.0f };    // specular decides highlight color of light
GLfloat light1GreenPosition[] = { 0.0f, 0.0f, 0.0f, 0.0f };    // manupulate direction in display

GLfloat lightAmbient2[] = { 0.0f, 0.0f, 0.0f, 1.0f };    // ambient decides general light
GLfloat lightDiffuse2[] = { 0.0f, 0.0f, 1.0f, 1.0f };    // diffuse decides light color
GLfloat lightSpecular2[] = { 0.0f, 0.0f, 1.0f, 1.0f };    // specular decides highlight color of light
GLfloat light2BluePosition[] = { 0.0f, 0.0f, 0.0f, 0.0f };    // manupulate direction in display

GLfloat materialAmbient[] = { 0.0f, 0.0f,0.0f,1.0f };
GLfloat materialDiffuse[] = { 1.0f,1.0f,1.0f,1.0f };
GLfloat materialSpecular[] = { 1.0f, 1.0f,1.0f,1.0f };
GLfloat materialShininess = 50.0f;


//Sphere declarations
GLuint gNumElements;
GLuint gNumVertices;
GLfloat sphere_vertices[1146];
GLfloat sphere_normals[1146];
GLfloat sphere_textures[764];
GLshort sphere_elements[2280];

//log file pointer
FILE *gpFile = NULL;

// 'C' style function declaration
CVReturn MyDisplayLinkCallback(CVDisplayLinkRef, const CVTimeStamp*, const CVTimeStamp*, CVOptionFlags, CVOptionFlags*, void*);

//interface declarations
@interface AppDelegate: NSObject <NSApplicationDelegate, NSWindowDelegate>
@end

@interface GLView: NSOpenGLView
@end

//entry point function
int main(int argc, const char *argv[])
{
    NSAutoreleasePool *pPool = [[NSAutoreleasePool alloc] init];
    
    NSApp = [NSApplication sharedApplication];
    
    [NSApp setDelegate:[[AppDelegate alloc] init]];
    
    [NSApp run];    //run loop
    
    [pPool release];
    
    return (0);
}

//implementations
@implementation AppDelegate
{
@private
    NSWindow *window;
    GLView *glView;
}

-(void)applicationDidFinishLaunching:(NSNotification*)aNotification
{
    //code
    //create log file
    NSBundle *bundle = [NSBundle mainBundle];
    NSString *appDirPath = [bundle bundlePath];
    NSString *appDirParentPath = [appDirPath stringByDeletingLastPathComponent];
    NSString *logFilePath = [NSString stringWithFormat:@"%@/Log.txt",appDirParentPath];
    //convert NSSTring to char*
    const char* szLogFilePath = [logFilePath cStringUsingEncoding:NSASCIIStringEncoding];
    
    gpFile = fopen(szLogFilePath, "w");
    if(gpFile == NULL)
    {
        printf("Cannot Open Log.txt. Exiting...\n");
        [self release];
        [NSApp terminate:self];
    }
    else
    {
        fprintf(gpFile, "Applicaton Is Started Successfully\n");
    }
    
    //create window
    NSRect win_rect = NSMakeRect(0.0, 0.0, 800.0, 600.0);
    window = [[NSWindow alloc] initWithContentRect:win_rect
                                         styleMask:NSWindowStyleMaskTitled | NSWindowStyleMaskClosable | NSWindowStyleMaskMiniaturizable | NSWindowStyleMaskResizable
                                           backing:NSBackingStoreBuffered
                                             defer:NO];
    
    //set window properties
    [window setTitle:@"macOS OpenGL --> Three Moving Lights On Sphere"];     // <--- Modify as per requirement
    [window center];
    
    //create OpenGL view
    glView = [[GLView alloc] initWithFrame:[window frame]];
    
    if(glView == nil)
    {
        fprintf(gpFile, "Unable to Create GLView. Exiting...\n");
        [self release];
        [NSApp terminate:self];
    }
    
    //attach glview to window
    [window setContentView:glView];
    [window setDelegate:self];
    [window makeKeyAndOrderFront:self];
}

-(void)applicationWillTerminate:(NSNotification*)aNotification
{
    fprintf(gpFile, "Application Is Close Successfully\n");
    if(gpFile)
    {
        fclose(gpFile);
        gpFile = NULL;
    }
}

-(void)windowWillClose:(NSNotification*)aNotification
{
    //code
    [NSApp terminate:self];
}

-(void)dealloc
{
    //code
    [glView release];
    [window release];
    
    [super dealloc];
}
@end

@implementation GLView
{
@private
    CVDisplayLinkRef displayLink;
    
    Sphere *sphr;
    
    //your shader variables
    GLuint vertexShaderObject_PV;
    GLuint fragmentShaderObject_PV;
    GLuint shaderProgramObject_PV;
    
    GLuint vertexShaderObject_PF;
    GLuint fragmentShaderObject_PF;
    GLuint shaderProgramObject_PF;
    
    enum eShapes
    {
        SPHERE = 0,
        NR_SHAPES,
    };
    enum eShape_Attr
    {
        POSITION = 0,
        NORMAL,
        ELEMENTS,
        NR_SHAPE_ATTRS,
    };
    enum eUniforms
    {
        MV = 0,
        PROJECTION,
        RED_LA,
        RED_LD,
        RED_LS,
        GREEN_LA,
        GREEN_LD,
        GREEN_LS,
        BLUE_LA,
        BLUE_LD,
        BLUE_LS,
        KA,
        KD,
        KS,
        SHININESS,
        RED_LIGHT_POS,
        GREEN_LIGHT_POS,
        BLUE_LIGHT_POS,
        LIGHT_EN,
        NR_UNIFORMS,
    };
    
    GLuint vao[NR_SHAPES];
    GLuint vbo[NR_SHAPES][NR_SHAPE_ATTRS];
    GLuint uniform[NR_UNIFORMS];
    
    BOOL bLighting_enable;
    BOOL bPerVertex;
    BOOL bPerFragment;
    
    vmath::mat4 projectonMatrix;
}

-(id)initWithFrame:(NSRect)frame
{
    self = [super initWithFrame:frame];
    
    if(self)
    {
        [[self window] setContentView:self];
        
        //Pixel Format Attribute list
        NSOpenGLPixelFormatAttribute attrs [] =
        {
            //key,value(optional)
            NSOpenGLPFAOpenGLProfile,NSOpenGLProfileVersion4_1Core,
            NSOpenGLPFAScreenMask,CGDisplayIDToOpenGLDisplayMask(kCGDirectMainDisplay),
            NSOpenGLPFANoRecovery,   //dont fallback to sw renderer if hw doesnot found
            NSOpenGLPFAAccelerated,
            NSOpenGLPFAColorSize,24,
            NSOpenGLPFADepthSize,24,
            NSOpenGLPFAAlphaSize,8,
            NSOpenGLPFADoubleBuffer,
            0   //last must be 0 indicating end of list
        };
        
        //Ask NSOpenGL for pixel format by providing above attributes
        NSOpenGLPixelFormat *pixelFormat = [[[NSOpenGLPixelFormat alloc] initWithAttributes:attrs] autorelease];
        if(pixelFormat == nil)
        {
            fprintf(gpFile, "No Valid Pixel Format Available. Exiting..\n");
            [self release];
            [NSApp terminate:self];
        }
        
        //Ask NSOpenGL for context by providing pixelFormat
        NSOpenGLContext *glContext = [[[NSOpenGLContext alloc] initWithFormat:pixelFormat shareContext:nil] autorelease];
        
        //Save context and pixel format in GLView
        [self setPixelFormat:pixelFormat];
        [self setOpenGLContext:glContext];
    }
    return (self);
}

-(CVReturn)getFrameForTime:(const CVTimeStamp*)pOutTime
{
    NSAutoreleasePool *pPool = [[NSAutoreleasePool alloc] init];
    
    [self drawView];
    
    [pPool release];
    return (kCVReturnSuccess);
}

-(void)perFragmentShaderProgram
{
    /*****VERTEX SHADER********/
    vertexShaderObject_PF = glCreateShader(GL_VERTEX_SHADER);
    const GLchar* vsSource =
    "#version 410 core" \
    "\n" \
    "in vec4 vPosition;" \
    "in vec3 vNormal;" \
    "uniform mat4 u_model_view_matrix;" \
    "uniform mat4 u_projection_matrix;" \
    "uniform int u_light_enabled;" \
    "uniform vec4 u_red_light_position;" \
    "uniform vec4 u_green_light_position;" \
    "uniform vec4 u_blue_light_position;" \
    "out vec3 transformed_normals;" \
    "out vec3 red_light_direction;" \
    "out vec3 green_light_direction;" \
    "out vec3 blue_light_direction;" \
    "out vec3 viewer_vector;" \
    "void main(void)" \
    "{" \
    "if(u_light_enabled==1)" \
    "{" \
    "vec4 eyeCoordinates = u_model_view_matrix * vPosition;" \
    "transformed_normals = mat3(u_model_view_matrix) * vNormal;" \
    "red_light_direction = vec3(u_red_light_position) - eyeCoordinates.xyz;" \
    "green_light_direction = vec3(u_green_light_position) - eyeCoordinates.xyz;" \
    "blue_light_direction = vec3(u_blue_light_position) - eyeCoordinates.xyz;" \
    "viewer_vector = -eyeCoordinates.xyz;" \
    "}"\
    "gl_Position = u_projection_matrix * u_model_view_matrix * vPosition;" \
    "}";
    
    
    glShaderSource(vertexShaderObject_PF, 1, (const GLchar**)&vsSource, NULL);
    glCompileShader(vertexShaderObject_PF);
    GLint iShaderCompileStatus = 0;
    glGetShaderiv(vertexShaderObject_PF, GL_COMPILE_STATUS, &iShaderCompileStatus);
    if(GL_FALSE == iShaderCompileStatus)
    {
        GLsizei iLogLen = 0;
        glGetShaderiv(vertexShaderObject_PF, GL_INFO_LOG_LENGTH, &iLogLen);
        if(iLogLen)
        {
            GLchar* szLog = (GLchar*)malloc(iLogLen);
            if(szLog)
            {
                GLsizei written;
                glGetShaderInfoLog(vertexShaderObject_PF, iLogLen, &written, szLog);
                fprintf(gpFile, "Vertex Shader Compile Info: %s\n", szLog);
                free(szLog);
                [self release];
                [NSApp terminate:self];
            }
        }
    }//================
    
    /*****FRAGMENT SHADER********/
    fragmentShaderObject_PF = glCreateShader(GL_FRAGMENT_SHADER);
    const GLchar* fsSource =
    "#version 400 core" \
    "\n" \
    "in vec3 transformed_normals;" \
    "in vec3 red_light_direction;" \
    "in vec3 green_light_direction;" \
    "in vec3 blue_light_direction;" \
    "in vec3 viewer_vector;" \
    "uniform vec3 u_red_La;" \
    "uniform vec3 u_red_Ld;" \
    "uniform vec3 u_red_Ls;" \
    "uniform vec3 u_green_La;" \
    "uniform vec3 u_green_Ld;" \
    "uniform vec3 u_green_Ls;" \
    "uniform vec3 u_blue_La;" \
    "uniform vec3 u_blue_Ld;" \
    "uniform vec3 u_blue_Ls;" \
    "uniform int u_light_enabled;" \
    "uniform vec3 u_Ka;" \
    "uniform vec3 u_Kd;" \
    "uniform vec3 u_Ks;" \
    "uniform float u_material_shininess;" \
    "out vec4 FragColor;" \
    "void main(void)" \
    "{" \
    "vec3 phong_ads_color; " \
    "if(u_light_enabled == 1)" \
    "{" \
    "vec3 normalized_transformed_normals = normalize(transformed_normals);" \
    "vec3 normalized_light_direction = normalize(red_light_direction);" \
    "vec3 normalized_viewer_vector = normalize(viewer_vector);" \
    "float tn_dot_ld = max(dot(normalized_transformed_normals, normalized_light_direction), 0.0);" \
    "vec3 ambient = u_red_La * u_Ka;" \
    "vec3 diffuse = u_red_Ld * u_Kd * tn_dot_ld;" \
    "vec3 reflection_vector = reflect(-normalized_light_direction, normalized_transformed_normals);" \
    "float r_dot_v = max(dot(reflection_vector, normalized_viewer_vector), 0.0);" \
    "vec3 specular = u_red_Ls * u_Ks * pow(r_dot_v, u_material_shininess);" \
    "phong_ads_color = ambient + diffuse + specular;" \
    
    "normalized_light_direction = normalize(green_light_direction);" \
    "tn_dot_ld = max(dot(normalized_transformed_normals, normalized_light_direction), 0.0);" \
    "ambient = u_green_La * u_Ka;" \
    "diffuse = u_green_Ld * u_Kd * tn_dot_ld;" \
    "reflection_vector = reflect(-normalized_light_direction, normalized_transformed_normals);" \
    "r_dot_v = max(dot(reflection_vector, normalized_viewer_vector), 0.0);" \
    "specular = u_green_Ls * u_Ks * pow(r_dot_v, u_material_shininess);" \
    "phong_ads_color = phong_ads_color + ambient + diffuse + specular;" \
    
    "normalized_light_direction = normalize(blue_light_direction);" \
    "tn_dot_ld = max(dot(normalized_transformed_normals, normalized_light_direction), 0.0);" \
    "ambient = u_blue_La * u_Ka;" \
    "diffuse = u_blue_Ld * u_Kd * tn_dot_ld;" \
    "reflection_vector = reflect(-normalized_light_direction, normalized_transformed_normals);" \
    "r_dot_v = max(dot(reflection_vector, normalized_viewer_vector), 0.0);" \
    "specular = u_blue_Ls * u_Ks * pow(r_dot_v, u_material_shininess);" \
    "phong_ads_color = phong_ads_color + ambient + diffuse + specular;" \
    "}" \
    "else" \
    "{" \
    "phong_ads_color = vec3(1.0, 1.0, 1.0);" \
    "}" \
    "FragColor = vec4(phong_ads_color, 1.0);" \
    "}";
    
    glShaderSource(fragmentShaderObject_PF, 1, (const GLchar**)&fsSource, NULL);
    glCompileShader(fragmentShaderObject_PF);
    iShaderCompileStatus = 0;
    glGetShaderiv(fragmentShaderObject_PF, GL_COMPILE_STATUS, &iShaderCompileStatus);
    if(GL_FALSE == iShaderCompileStatus)
    {
        GLsizei iLogLen = 0;
        glGetShaderiv(fragmentShaderObject_PF, GL_INFO_LOG_LENGTH, &iLogLen);
        if(iLogLen)
        {
            GLchar* szLog = (GLchar*)malloc(iLogLen);
            if(szLog)
            {
                GLsizei written;
                glGetShaderInfoLog(fragmentShaderObject_PF, iLogLen, &written, szLog);
                fprintf(gpFile, "Fragment Shader Compile Info: %s\n", szLog);
                free(szLog);
                [self release];
                [NSApp terminate:self];
            }
        }
    }//===================
    
    /*****SHADER PROGRAM********/
    shaderProgramObject_PF = glCreateProgram();
    //attach vs and fs to shader program
    glAttachShader(shaderProgramObject_PF, vertexShaderObject_PF);
    glAttachShader(shaderProgramObject_PF, fragmentShaderObject_PF);
    
    //pre-link binding of vertex attributes
    glBindAttribLocation(shaderProgramObject_PF, VDG_ATTRIBUTE_POSITION, "vPosition");     // <--- Modify/add as per requirement
    glBindAttribLocation(shaderProgramObject_PF, VDG_ATTRIBUTE_NORMAL, "vNormal");
    
    //link program
    glLinkProgram(shaderProgramObject_PF);
    
    //post-link, get uniform locations from shaders
    uniform[MV] = glGetUniformLocation(shaderProgramObject_PF, "u_model_view_matrix");           // <--- Modify/add as per requirement
    uniform[PROJECTION] = glGetUniformLocation(shaderProgramObject_PF, "u_projection_matrix");
    uniform[RED_LA] = glGetUniformLocation(shaderProgramObject_PF, "u_La");
    uniform[RED_LD] = glGetUniformLocation(shaderProgramObject_PF, "u_Ld");
    uniform[RED_LS] = glGetUniformLocation(shaderProgramObject_PF, "u_Ls");
    uniform[GREEN_LA] = glGetUniformLocation(shaderProgramObject_PF, "u_La1");
    uniform[GREEN_LD] = glGetUniformLocation(shaderProgramObject_PF, "u_Ld1");
    uniform[GREEN_LS] = glGetUniformLocation(shaderProgramObject_PF, "u_Ls1");
    uniform[BLUE_LA] = glGetUniformLocation(shaderProgramObject_PF, "u_La2");
    uniform[BLUE_LD] = glGetUniformLocation(shaderProgramObject_PF, "u_Ld2");
    uniform[BLUE_LS] = glGetUniformLocation(shaderProgramObject_PF, "u_Ls2");
    uniform[KA] = glGetUniformLocation(shaderProgramObject_PF, "u_Ka");
    uniform[KD] = glGetUniformLocation(shaderProgramObject_PF, "u_Kd");
    uniform[KS] = glGetUniformLocation(shaderProgramObject_PF, "u_Ks");
    uniform[SHININESS] = glGetUniformLocation(shaderProgramObject_PF, "u_material_shininess");
    uniform[RED_LIGHT_POS] = glGetUniformLocation(shaderProgramObject_PF, "u_light_position");
    uniform[GREEN_LIGHT_POS] = glGetUniformLocation(shaderProgramObject_PF, "u_light_position1");
    uniform[BLUE_LIGHT_POS] = glGetUniformLocation(shaderProgramObject_PF, "u_light_position2");
    uniform[LIGHT_EN] = glGetUniformLocation(shaderProgramObject_PF, "u_light_enabled");
}

-(void)perVertexShaderProgram
{
    /*****VERTEX SHADER********/
    vertexShaderObject_PV = glCreateShader(GL_VERTEX_SHADER);
    const GLchar* vsSource =
    "#version 410 core" \
    "\n" \
    "in vec4 vPosition;" \
    "in vec3 vNormal;" \
    "uniform mat4 u_model_view_matrix;" \
    "uniform mat4 u_projection_matrix;" \
    "uniform int u_light_enabled;" \
    "uniform vec3 u_red_La;" \
    "uniform vec3 u_red_Ld;" \
    "uniform vec3 u_red_Ls;" \
    "uniform vec4 u_red_light_position;" \
    
    "uniform vec3 u_green_La;" \
    "uniform vec3 u_green_Ld;" \
    "uniform vec3 u_green_Ls;" \
    "uniform vec4 u_green_light_position;" \
    
    "uniform vec3 u_blue_La;" \
    "uniform vec3 u_blue_Ld;" \
    "uniform vec3 u_blue_Ls;" \
    "uniform vec4 u_blue_light_position;" \
    "uniform vec3 u_Ka;" \
    "uniform vec3 u_Kd;" \
    "uniform vec3 u_Ks;" \
    "uniform float u_material_shininess;" \
    "out vec3 phong_ads_color;" \
    "void main(void)" \
    "{" \
    "if(u_light_enabled==1)" \
    "{" \
    "vec3 ambient = u_red_La * u_Ka;" \
    "vec4 eyeCoordinates = u_model_view_matrix * vPosition;" \
    "vec3 transformed_normals = normalize(mat3(u_model_view_matrix) * vNormal);" \
    "vec3 light_direction = normalize(vec3(u_red_light_position) - eyeCoordinates.xyz);" \
    "float tn_dot_ld = max(dot(transformed_normals, light_direction), 0.0);" \
    "vec3 diffuse = u_red_Ld * u_Kd * tn_dot_ld;" \
    "vec3 reflection_vector = reflect(-light_direction, transformed_normals);" \
    "vec3 viewer_vector = normalize(-eyeCoordinates.xyz);" \
    "float r_dot_v = max(dot(reflection_vector, viewer_vector), 0.0);" \
    "vec3 specular = u_red_Ls * u_Ks * pow(r_dot_v, u_material_shininess);" \
    "phong_ads_color = ambient + diffuse + specular;" \
    
    "light_direction = normalize(vec3(u_green_light_position) - eyeCoordinates.xyz);" \
    "tn_dot_ld = max(dot(transformed_normals, light_direction), 0.0);" \
    "ambient = u_green_La * u_Ka;" \
    "diffuse = u_green_Ld * u_Kd * tn_dot_ld;" \
    "reflection_vector = reflect(-light_direction, transformed_normals);" \
    "r_dot_v = max(dot(reflection_vector, viewer_vector), 0.0);" \
    "specular = u_green_Ls * u_Ks * pow(r_dot_v, u_material_shininess);" \
    "phong_ads_color = phong_ads_color + ambient + diffuse + specular;" \
    
    "light_direction = normalize(vec3(u_blue_light_position) - eyeCoordinates.xyz);" \
    "tn_dot_ld = max(dot(transformed_normals, light_direction), 0.0);" \
    "ambient = u_blue_La * u_Ka;" \
    "diffuse = u_blue_Ld * u_Kd * tn_dot_ld;" \
    "reflection_vector = reflect(-light_direction, transformed_normals);" \
    "r_dot_v = max(dot(reflection_vector, viewer_vector), 0.0);" \
    "specular = u_blue_Ls * u_Ks * pow(r_dot_v, u_material_shininess);" \
    "phong_ads_color = phong_ads_color + ambient + diffuse + specular;" \
    
    "}"\
    "else" \
    "{" \
    "phong_ads_color = vec3(1.0, 1.0, 1.0);" \
    "}" \
    "gl_Position = u_projection_matrix * u_model_view_matrix * vPosition;" \
    "}";
    
    
    glShaderSource(vertexShaderObject_PV, 1, (const GLchar**)&vsSource, NULL);
    glCompileShader(vertexShaderObject_PV);
    GLint iShaderCompileStatus = 0;
    glGetShaderiv(vertexShaderObject_PV, GL_COMPILE_STATUS, &iShaderCompileStatus);
    if(GL_FALSE == iShaderCompileStatus)
    {
        GLsizei iLogLen = 0;
        glGetShaderiv(vertexShaderObject_PV, GL_INFO_LOG_LENGTH, &iLogLen);
        if(iLogLen)
        {
            GLchar* szLog = (GLchar*)malloc(iLogLen);
            if(szLog)
            {
                GLsizei written;
                glGetShaderInfoLog(vertexShaderObject_PV, iLogLen, &written, szLog);
                fprintf(gpFile, "PV: Vertex Shader Compile Info: %s\n", szLog);
                free(szLog);
                [self release];
                [NSApp terminate:self];
            }
        }
    }//================
    
    /*****FRAGMENT SHADER********/
    fragmentShaderObject_PV = glCreateShader(GL_FRAGMENT_SHADER);
    const GLchar* fsSource =
    "#version 410 core" \
    "\n" \
    "in vec3 phong_ads_color;" \
    "out vec4 FragColor;" \
    "void main(void)" \
    "{" \
    "FragColor = vec4(phong_ads_color, 1.0);" \
    "}";
    
    glShaderSource(fragmentShaderObject_PV, 1, (const GLchar**)&fsSource, NULL);
    glCompileShader(fragmentShaderObject_PV);
    iShaderCompileStatus = 0;
    glGetShaderiv(fragmentShaderObject_PV, GL_COMPILE_STATUS, &iShaderCompileStatus);
    if(GL_FALSE == iShaderCompileStatus)
    {
        GLsizei iLogLen = 0;
        glGetShaderiv(fragmentShaderObject_PV, GL_INFO_LOG_LENGTH, &iLogLen);
        if(iLogLen)
        {
            GLchar* szLog = (GLchar*)malloc(iLogLen);
            if(szLog)
            {
                GLsizei written;
                glGetShaderInfoLog(fragmentShaderObject_PV, iLogLen, &written, szLog);
                fprintf(gpFile, "PV: Fragment Shader Compile Info: %s\n", szLog);
                free(szLog);
                [self release];
                [NSApp terminate:self];
            }
        }
    }//===================
    
    /*****SHADER PROGRAM********/
    shaderProgramObject_PV = glCreateProgram();
    //attach vs and fs to shader program
    glAttachShader(shaderProgramObject_PV, vertexShaderObject_PV);
    glAttachShader(shaderProgramObject_PV, fragmentShaderObject_PV);
    
    //pre-link binding of vertex attributes
    glBindAttribLocation(shaderProgramObject_PV, VDG_ATTRIBUTE_POSITION, "vPosition");     // <--- Modify/add as per requirement
    glBindAttribLocation(shaderProgramObject_PV, VDG_ATTRIBUTE_NORMAL, "vNormal");
    
    //link program
    glLinkProgram(shaderProgramObject_PV);
    
    //post-link, get uniform locations from shaders
    uniform[MV] = glGetUniformLocation(shaderProgramObject_PV, "u_model_view_matrix");           // <--- Modify/add as per requirement
    uniform[PROJECTION] = glGetUniformLocation(shaderProgramObject_PV, "u_projection_matrix");
    uniform[RED_LA] = glGetUniformLocation(shaderProgramObject_PV, "u_red_La");
    uniform[RED_LD] = glGetUniformLocation(shaderProgramObject_PV, "u_red_Ld");
    uniform[RED_LS] = glGetUniformLocation(shaderProgramObject_PV, "u_red_Ls");
    uniform[GREEN_LA] = glGetUniformLocation(shaderProgramObject_PV, "u_green_La");
    uniform[GREEN_LD] = glGetUniformLocation(shaderProgramObject_PV, "u_green_Ld");
    uniform[GREEN_LS] = glGetUniformLocation(shaderProgramObject_PV, "u_green_Ls");
    uniform[BLUE_LA] = glGetUniformLocation(shaderProgramObject_PV, "u_blue_La");
    uniform[BLUE_LD] = glGetUniformLocation(shaderProgramObject_PV, "u_blue_Ld");
    uniform[BLUE_LS] = glGetUniformLocation(shaderProgramObject_PV, "u_blue_Ls");
    uniform[KA] = glGetUniformLocation(shaderProgramObject_PV, "u_Ka");
    uniform[KD] = glGetUniformLocation(shaderProgramObject_PV, "u_Kd");
    uniform[KS] = glGetUniformLocation(shaderProgramObject_PV, "u_Ks");
    uniform[SHININESS] = glGetUniformLocation(shaderProgramObject_PV, "u_material_shininess");
    uniform[RED_LIGHT_POS] = glGetUniformLocation(shaderProgramObject_PV, "u_red_light_position");
    uniform[GREEN_LIGHT_POS] = glGetUniformLocation(shaderProgramObject_PV, "u_green_light_position");
    uniform[BLUE_LIGHT_POS] = glGetUniformLocation(shaderProgramObject_PV, "u_blue_light_position");
    uniform[LIGHT_EN] = glGetUniformLocation(shaderProgramObject_PV, "u_light_enabled");
}

-(void)uiniformBinding
{
    if(bPerVertex == YES)
    {
        //get uniform locations from shaders
        uniform[MV] = glGetUniformLocation(shaderProgramObject_PV, "u_model_view_matrix");           // <--- Modify/add as per requirement
        uniform[PROJECTION] = glGetUniformLocation(shaderProgramObject_PV, "u_projection_matrix");
        uniform[RED_LA] = glGetUniformLocation(shaderProgramObject_PV, "u_red_La");
        uniform[RED_LD] = glGetUniformLocation(shaderProgramObject_PV, "u_red_Ld");
        uniform[RED_LS] = glGetUniformLocation(shaderProgramObject_PV, "u_red_Ls");
        uniform[GREEN_LA] = glGetUniformLocation(shaderProgramObject_PV, "u_green_La");
        uniform[GREEN_LD] = glGetUniformLocation(shaderProgramObject_PV, "u_green_Ld");
        uniform[GREEN_LS] = glGetUniformLocation(shaderProgramObject_PV, "u_green_Ls");
        uniform[BLUE_LA] = glGetUniformLocation(shaderProgramObject_PV, "u_blue_La");
        uniform[BLUE_LD] = glGetUniformLocation(shaderProgramObject_PV, "u_blue_Ld");
        uniform[BLUE_LS] = glGetUniformLocation(shaderProgramObject_PV, "u_blue_Ls");
        uniform[KA] = glGetUniformLocation(shaderProgramObject_PV, "u_Ka");
        uniform[KD] = glGetUniformLocation(shaderProgramObject_PV, "u_Kd");
        uniform[KS] = glGetUniformLocation(shaderProgramObject_PV, "u_Ks");
        uniform[SHININESS] = glGetUniformLocation(shaderProgramObject_PV, "u_material_shininess");
        uniform[RED_LIGHT_POS] = glGetUniformLocation(shaderProgramObject_PV, "u_red_light_position");
        uniform[GREEN_LIGHT_POS] = glGetUniformLocation(shaderProgramObject_PV, "u_green_light_position");
        uniform[BLUE_LIGHT_POS] = glGetUniformLocation(shaderProgramObject_PV, "u_blue_light_position");
        uniform[LIGHT_EN] = glGetUniformLocation(shaderProgramObject_PV, "u_light_enabled");
    }
    else
    {//per fragment
        //get uniform locations from shaders
        uniform[MV] = glGetUniformLocation(shaderProgramObject_PF, "u_model_view_matrix");           // <--- Modify/add as per requirement
        uniform[PROJECTION] = glGetUniformLocation(shaderProgramObject_PF, "u_projection_matrix");
        uniform[RED_LA] = glGetUniformLocation(shaderProgramObject_PF, "u_red_La");
        uniform[RED_LD] = glGetUniformLocation(shaderProgramObject_PF, "u_red_Ld");
        uniform[RED_LS] = glGetUniformLocation(shaderProgramObject_PF, "u_red_Ls");
        uniform[GREEN_LA] = glGetUniformLocation(shaderProgramObject_PF, "u_green_La");
        uniform[GREEN_LD] = glGetUniformLocation(shaderProgramObject_PF, "u_green_Ld");
        uniform[GREEN_LS] = glGetUniformLocation(shaderProgramObject_PF, "u_green_Ls");
        uniform[BLUE_LA] = glGetUniformLocation(shaderProgramObject_PF, "u_blue_La");
        uniform[BLUE_LD] = glGetUniformLocation(shaderProgramObject_PF, "u_blue_Ld");
        uniform[BLUE_LS] = glGetUniformLocation(shaderProgramObject_PF, "u_blue_Ls");
        uniform[KA] = glGetUniformLocation(shaderProgramObject_PF, "u_Ka");
        uniform[KD] = glGetUniformLocation(shaderProgramObject_PF, "u_Kd");
        uniform[KS] = glGetUniformLocation(shaderProgramObject_PF, "u_Ks");
        uniform[SHININESS] = glGetUniformLocation(shaderProgramObject_PF, "u_material_shininess");
        uniform[RED_LIGHT_POS] = glGetUniformLocation(shaderProgramObject_PF, "u_red_light_position");
        uniform[GREEN_LIGHT_POS] = glGetUniformLocation(shaderProgramObject_PF, "u_green_light_position");
        uniform[BLUE_LIGHT_POS] = glGetUniformLocation(shaderProgramObject_PF, "u_blue_light_position");
        uniform[LIGHT_EN] = glGetUniformLocation(shaderProgramObject_PF, "u_light_enabled");
    }
}

-(void)prepareOpenGL
{
    //code
    fprintf(gpFile, "OpenGL Version :%s\n", glGetString(GL_VERSION));
    fprintf(gpFile, "GLSL Version   :%s\n", glGetString(GL_SHADING_LANGUAGE_VERSION));
    
    [[self openGLContext] makeCurrentContext];
    
    //set swap interval to 1, to render your scene on display refresh rate
    GLint swapInt = 1;
    [[self openGLContext] setValues:&swapInt forParameter:NSOpenGLCPSwapInterval];
    
//ogl shader initialization
    
    [self perFragmentShaderProgram];
    [self perVertexShaderProgram];
    
    
    /*************VERTICES, COLOR, NORMALS, VAO, VBO INITIALIZATIONS******************/
    
    //Sphere
    sphr = [[Sphere alloc] init];
    [sphr getSphereVertexData:sphere_vertices :sphere_normals :sphere_textures :sphere_elements];
    gNumVertices = [sphr getNumberOfSphereVertices];
    gNumElements = [sphr getNumberOfSphereElements];
    
    fprintf(gpFile, "nr vertices:%d\n", gNumVertices);
    fprintf(gpFile, "nr elements:%d\n", gNumElements);
    //============
    //VAO:Sphere
    glGenVertexArrays(1, &vao[SPHERE]);
    glBindVertexArray(vao[SPHERE]);
    //VBO1: Position
    {
        glGenBuffers(1, &vbo[SPHERE][POSITION]);
        glBindBuffer(GL_ARRAY_BUFFER, vbo[SPHERE][POSITION]);
        glBufferData(GL_ARRAY_BUFFER, sizeof(sphere_vertices), sphere_vertices, GL_STATIC_DRAW);
        glVertexAttribPointer(VDG_ATTRIBUTE_POSITION,
                              3, //xyz
                              GL_FLOAT,
                              GL_FALSE, //non-normalize
                              0, NULL); //0=stride
        glEnableVertexAttribArray(VDG_ATTRIBUTE_POSITION);
        glBindBuffer(GL_ARRAY_BUFFER, 0);   //unbind
    }
    //VBO2: Normals
    {
        glGenBuffers(1, &vbo[SPHERE][NORMAL]);
        glBindBuffer(GL_ARRAY_BUFFER, vbo[SPHERE][NORMAL]);
        glBufferData(GL_ARRAY_BUFFER, sizeof(sphere_normals), sphere_normals, GL_STATIC_DRAW);
        glVertexAttribPointer(VDG_ATTRIBUTE_NORMAL,
                              3, //nx, ny, nz
                              GL_FLOAT,
                              GL_FALSE, //non-normalize
                              0, NULL); //0=stride
        glEnableVertexAttribArray(VDG_ATTRIBUTE_NORMAL);
        glBindBuffer(GL_ARRAY_BUFFER, 0);   //unbind
    }
    //VBO3: Elements
    {
        glGenBuffers(1, &vbo[SPHERE][ELEMENTS]);
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo[SPHERE][ELEMENTS]);
        
        glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(sphere_elements), sphere_elements, GL_STATIC_DRAW);
        
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);   //unbind
    }
    glBindVertexArray(0);   //unbind
    //==================
    
    //Other OpenGL Settings         // <--- Modify as per requirement
    glClearDepth(1.0f);         // 3D change 1b.1(M): Full depth(far plane), added for 3D drawing
    glEnable(GL_DEPTH_TEST);    // 3D change 1b.2(M): Enable depth function
    glDepthFunc(GL_LEQUAL);     // 3D change 1b.3(M): draw objects from far plane, less than or equal distance from far to near
    //glEnable(GL_CULL_FACE);    // We will always cull back faces for better performance
    //==================
    
    //set backgroud clearing colof
    glClearColor(0.0f, 0.0f, 0.0f, 1.0f);   //black                  // <--- Modify as per requirement
    
    //fill I matrix in perspective projection matrix
    projectonMatrix = vmath::mat4::identity();
    
    bLighting_enable = YES; //by default lighting enabled
    bPerVertex = YES;   //by default per vertex lighting
    bPerFragment = NO;
    //=======================
    
    /*************CVDisplayLink INIT***************************/
    //01-Create Core Video Link
    CVDisplayLinkCreateWithActiveCGDisplays(&displayLink);
    //02-Set Core Video Callback
    CVDisplayLinkSetOutputCallback(displayLink, &MyDisplayLinkCallback, self);
    //03-Convert NSOpenGL context and pixelformat to CGL
    CGLPixelFormatObj cglPixelFormat = (CGLPixelFormatObj) [[self pixelFormat] CGLPixelFormatObj];
    CGLContextObj cglContext = (CGLContextObj) [[self openGLContext] CGLContextObj];
    //04-Set Core Video Current display
    CVDisplayLinkSetCurrentCGDisplayFromOpenGLContext(displayLink, cglContext, cglPixelFormat);
    //05-Start Core Video
    CVDisplayLinkStart(displayLink);
}

-(void)reshape
{
    //code
    CGLLockContext((CGLContextObj) [[self openGLContext] CGLContextObj]);
    
    NSRect rect = [self bounds];
    GLfloat width = rect.size.width;
    GLfloat height = rect.size.height;
    
    //Set opengl viewport
    glViewport(0, 0, (GLsizei)width, (GLsizei)height);
    
    //set perspective projection--> (fovy, AR, near, far)
    projectonMatrix = vmath::perspective(45.0f, width/height, 0.1f, 100.0f);        // <--- Modify as per requirement
    
    CGLUnlockContext((CGLContextObj) [[self openGLContext] CGLContextObj]);
}

-(void)drawRect:(NSRect)dirtyRect
{
    //code
    [self drawView];
}

//our method
-(void)drawView
{
    //code
    static GLfloat index = 0.0f;
    static GLint cicle_points = 10000;    //360
    
    CGLLockContext((CGLContextObj) [[self openGLContext] CGLContextObj]);
    [[self openGLContext] makeCurrentContext];
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    
    [self uiniformBinding]; //change uniforms as per shader selection, PV or PF
    if(bPerVertex == YES)
        glUseProgram(shaderProgramObject_PV);
    else
        glUseProgram(shaderProgramObject_PF);
    
    
    
    //SEND LIGHTING VALUES TO SHADER
    if(bLighting_enable == YES)
    {
        glUniform1i(uniform[LIGHT_EN], 1);
        
        //Red Light
        glUniform3fv(uniform[RED_LA], 1, lightAmbient);
        glUniform3fv(uniform[RED_LD], 1, lightDiffuse);
        glUniform3fv(uniform[RED_LS], 1, lightSpecular);
        //Red light move around x-axis
        light0RedPosition[0] = 0.0f;
        light0RedPosition[1] = 10.0f*cos(gAngle);
        light0RedPosition[2] = 10.0f*sin(gAngle);
        glUniform4fv(uniform[RED_LIGHT_POS], 1, light0RedPosition);
        
        //Green Light
        glUniform3fv(uniform[GREEN_LA], 1, lightAmbient1);
        glUniform3fv(uniform[GREEN_LD], 1, lightDiffuse1);
        glUniform3fv(uniform[GREEN_LS], 1, lightSpecular1);
        //Green Light move around y-axis
        light1GreenPosition[0] = 10.0f*cos(gAngle);
        light1GreenPosition[1] = 0.0f;
        light1GreenPosition[2] = 10.0f*sin(gAngle);
        glUniform4fv(uniform[GREEN_LIGHT_POS], 1, light1GreenPosition);
        
        //Blue Light
        glUniform3fv(uniform[BLUE_LA], 1, lightAmbient2);
        glUniform3fv(uniform[BLUE_LD], 1, lightDiffuse2);
        glUniform3fv(uniform[BLUE_LS], 1, lightSpecular2);
        //Green Light move around z-axis
        light2BluePosition[0] = 10.0f*cos(gAngle);
        light2BluePosition[1] = 10.0f*sin(gAngle);
        light2BluePosition[2] = 0.0f;
        glUniform4fv(uniform[BLUE_LIGHT_POS], 1, light2BluePosition);
        
        glUniform3fv(uniform[KA], 1, materialAmbient);
        glUniform3fv(uniform[KD], 1, materialDiffuse);
        glUniform3fv(uniform[KS], 1, materialSpecular);
        glUniform1f(uniform[SHININESS], materialShininess);
    }
    else
    {
        glUniform1i(uniform[LIGHT_EN], 0);
    }
    //==========
    
    //CUBE
    vmath::mat4 modelViewMatrix = vmath::mat4::identity();
    vmath::mat4 translateMatrix = vmath::mat4::identity();
    vmath::mat4 modelViewProjectionMatrix = vmath::mat4::identity();
    
    translateMatrix = vmath::translate(0.0f, 0.0f, -2.0f);
    //MV = T * MV
    modelViewMatrix = translateMatrix * modelViewMatrix;
    
    //send MV to shader
    glUniformMatrix4fv(uniform[MV], 1, GL_FALSE, modelViewMatrix);
    
    //send Projection matrix to shader
    glUniformMatrix4fv(uniform[PROJECTION], 1, GL_FALSE, projectonMatrix);
    
    //bind sphere vao
    glBindVertexArray(vao[SPHERE]);
    //draw by elements
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo[SPHERE][ELEMENTS]);
    glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);
    glBindVertexArray(0);   //unbind
    
    glUseProgram(0);    //un-use program
    
    CGLFlushDrawable((CGLContextObj) [[self openGLContext] CGLContextObj]); //flushes data into other framebuffer which is not being displayed
    CGLUnlockContext((CGLContextObj) [[self openGLContext] CGLContextObj]);
    
    //light angle
    index = index + 20.0f;
    if (index >= cicle_points)
        index = 0;
    gAngle = (2 * PI*index) / cicle_points;
}

//Hardware Event Handling
-(BOOL)acceptsFirstResponder
{
    //code
    [[self window] makeFirstResponder:self];        //tell your window that GLView(self) will handle key, mouse etc. events
    
    return (YES);
}

-(void)keyDown:(NSEvent*)theEvent
{
    //code
    int key = (int) [[theEvent characters] characterAtIndex:0];
    switch(key)     // <--- Modify here as per requirement
    {
        case 27:    //escape key: Full screen
            [[self window] toggleFullScreen:self];  //auto repainting is done
            break;
        case 'f':   //switch to per fragment shading
        case 'F':
            bPerFragment = YES;
            bPerVertex = NO;
            break;
        case 'L':
        case 'l':
            if(bLighting_enable == NO)
                bLighting_enable = YES;
            else
                bLighting_enable = NO;
            break;
        case 'V':   //switch to per vertex shading
        case 'v':
            bPerFragment = NO;
            bPerVertex = YES;
            break;
        case 'Q':   //quit application
        case 'q':
            [self release];
            [NSApp terminate:self];
            break;
        default:
            break;
    }
}

-(void)mouseDown:(NSEvent*)theEvent
{
    //code
}

-(void)mouseDragged:(NSEvent*)theEvent
{
    //code
}

-(void)rightMouseDown:(NSEvent*)theEvent
{
    //code
}

-(void)dealloc
{
    //stop and release Core Video link
    CVDisplayLinkStop(displayLink);
    CVDisplayLinkRelease(displayLink);
    
    //Release vao
    for(int i=NR_SHAPES-1; i>=0; i--)
    {
        if(vao[i])
        {
            glDeleteVertexArrays(1, &vao[i]);
            vao[i] = 0;
        }
    }
    glDeleteVertexArrays(NR_SHAPES, vao);
    
    //Release vbo, As I have array of vao objects, i would have released this in 'glDeleteVertexArrays(NR_SHAPES, vao)' way.
    //But at this point of time, i am not sure, all declared vao objects have been initialized or not.
    //So its good to check and release vao objects one by one, though it looks funny :-)
    for(int i=NR_SHAPES-1; i>=0; i--)
    {
        for(int j=NR_SHAPE_ATTRS-1; i>=0; i--)
        {
            if(vbo[i][j])
            {
                glDeleteBuffers(1, &vbo[i][j]);
                vbo[i][j] = 0;
            }
        }
    }
    
    //Release Shader objects
    if(shaderProgramObject_PF)
    {
        if(fragmentShaderObject_PF)
        {
            glDetachShader(shaderProgramObject_PF, fragmentShaderObject_PF);
            glDeleteShader(fragmentShaderObject_PF);
            fragmentShaderObject_PF = 0;
        }
        if(vertexShaderObject_PF)
        {
            glDetachShader(shaderProgramObject_PF, vertexShaderObject_PF);
            glDeleteShader(vertexShaderObject_PF);
            vertexShaderObject_PF = 0;
        }
        
        glDeleteProgram(shaderProgramObject_PF);
        shaderProgramObject_PF = 0;
    }
    
    if(shaderProgramObject_PV)
    {
        if(fragmentShaderObject_PV)
        {
            glDetachShader(shaderProgramObject_PV, fragmentShaderObject_PV);
            glDeleteShader(fragmentShaderObject_PV);
            fragmentShaderObject_PV = 0;
        }
        if(vertexShaderObject_PV)
        {
            glDetachShader(shaderProgramObject_PV, vertexShaderObject_PV);
            glDeleteShader(vertexShaderObject_PV);
            vertexShaderObject_PV = 0;
        }
        
        glDeleteProgram(shaderProgramObject_PV);
        shaderProgramObject_PV = 0;
    }
    
    //Un-use shader program
    glUseProgram(0);
    
    [sphr release];
    [super dealloc];
}

@end

//This callback is called on display refresh rate
CVReturn MyDisplayLinkCallback(CVDisplayLinkRef displayLink, const CVTimeStamp* pNow, const CVTimeStamp *pOutTime, CVOptionFlags FlagsIn, CVOptionFlags *FlagsOut, void* pCVDisplayContext)
{
    CVReturn ret = kCVReturnSuccess;
    
    GLView *glView = (GLView*) pCVDisplayContext;
    
    ret = (CVReturn) [glView getFrameForTime:pOutTime];
    
    return (ret);
}
