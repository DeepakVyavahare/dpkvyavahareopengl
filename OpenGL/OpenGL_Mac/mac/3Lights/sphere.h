@interface Sphere : NSObject
-(void)getSphereVertexData : (GLfloat*)spherePositionCoords : (GLfloat*)sphereNormalCoords : (GLfloat*)sphereTexCoords : (GLshort*)sphereElements;
-(GLuint)getNumberOfSphereVertices;
-(GLuint)getNumberOfSphereElements;
@end

