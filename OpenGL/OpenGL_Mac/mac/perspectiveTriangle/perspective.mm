//perspective triangle

#import <Foundation/Foundation.h>
#import <Cocoa/Cocoa.h>
#import <QuartzCore/CVDisplayLink.h>
#import <OpenGL/gl3.h>
#import <OpenGL/gl3ext.h>

#import "vmath.h"

enum
{
    VDG_ATTRIBUTE_VERTEX,
    VDG_ATTRIBUTE_COLOR,
    VDG_ATTRIBUTE_NORMAL,
    VDG_ATTRIBUTE_TEXTURE0
};

//log file handle
FILE *gpFile = NULL;

//C style function declarations
CVReturn MyDisplayLinkCallback(CVDisplayLinkRef, const CVTimeStamp*, const CVTimeStamp*, CVOptionFlags, CVOptionFlags*, void*);

//interface declarations
@interface AppDelegate: NSObject <NSApplicationDelegate, NSWindowDelegate>
@end

@interface GLView: NSOpenGLView
@end

//Entry point function
int main(int argc, const char* argv[])
{
    NSAutoreleasePool *pPool = [[NSAutoreleasePool alloc] init];
    
    NSApp = [NSApplication sharedApplication];
    
    [NSApp setDelegate: [[AppDelegate alloc] init]];
    
    [NSApp run];
    
    [pPool release];
    return (0);
}

//implementation
@implementation AppDelegate
{
@private
    NSWindow *window;
    GLView *glView;
}

-(void)applicationDidFinishLaunching: (NSNotification*)aNotification
{
    //code
    //Create log file
    NSBundle *pBundle = [NSBundle mainBundle];
    NSString *appDirPath = [pBundle bundlePath];
    NSString *appDirParentDirPath = [appDirPath stringByDeletingLastPathComponent];
    NSString *nsStrLogFilePath = [NSString stringWithFormat:@"%@/Log.txt", appDirParentDirPath];
    //convert to ascii string format
    const char* szLogFilePath = [nsStrLogFilePath cStringUsingEncoding:NSASCIIStringEncoding];
    
    gpFile = fopen(szLogFilePath, "w");
    if(gpFile == NULL)
    {
        printf("Can Not Create Log File. Exiting...\n");
        [self release];
        [NSApp terminate:self];
    }
    else
    {
        fprintf(gpFile, "Application Started Successfully\n");
    }
    
    //create window
    NSRect win_rect = NSMakeRect(0.0, 0.0, 800.0, 600.0);   //carbon lib method
    
    window = [[NSWindow alloc] initWithContentRect:win_rect
                                         styleMask:NSWindowStyleMaskTitled | NSWindowStyleMaskClosable | NSWindowStyleMaskMiniaturizable | NSWindowStyleMaskResizable
                                           backing:NSBackingStoreBuffered
                                             defer:NO];
    if(window == nil)
    {
        fprintf(gpFile, "Window Creation Failed. Exiting...\n");
        [self release];
        [NSApp terminate:self];
    }
    
    [window setTitle:@"macOS Perspective Triangle"];
    [window center];
    
    //creat view
    glView = [[GLView alloc] initWithFrame: [window frame]];
    if(glView == nil)
    {
        fprintf(gpFile, "GLView Initialization Failed. Exiting...\n");
        [self release];
        [NSApp terminate:self];
    }
    
    //attach view to window
    [window setContentView:glView];
    
    [window setDelegate:self];
    [window makeKeyAndOrderFront:self];
}

-(void)applicationWillTerminate: (NSNotification*)aNotification
{
    //code
    fprintf(gpFile, "Application Is Closed Successfully\n");
    if(gpFile)
    {
        fclose(gpFile);
        gpFile = NULL;
    }
}

-(void)windowWillClose: (NSNotification*)aNotification
{
    //code
    if(gpFile)
    {
        fprintf (gpFile, "In 'windowWillClose' method\n");
    }
    [NSApp terminate:self];
}

//deleting objects
-(void)dealloc
{
    [glView release];
    [window release];
    
    [super dealloc];
}

@end

//GLView implementation
@implementation GLView
{
    CVDisplayLinkRef displayLink;
    
    //Shader vars
    GLuint vertexShaderObject;
    GLuint fragmentShaderObject;
    GLuint shaderProgramObject;
    
    GLuint vao;
    GLuint vbo;
    
    GLuint uniform_mvp;
    
    vmath::mat4 perspectiveProjectionMatrix;
}

-(id)initWithFrame:(NSRect)frame
{
    self = [super initWithFrame:frame];
    
    if(self)
    {
        [[self window]setContentView:self];
        
        NSOpenGLPixelFormatAttribute attrs[] =
        {
            //key, value
            NSOpenGLPFAOpenGLProfile, NSOpenGLProfileVersion4_1Core,
            NSOpenGLPFAScreenMask, CGDisplayIDToOpenGLDisplayMask(kCGDirectMainDisplay),
            NSOpenGLPFANoRecovery,  //dont fallback to sw rendorer in hw doesnt find
            NSOpenGLPFAAccelerated,
            NSOpenGLPFAColorSize, 24,
            NSOpenGLPFADepthSize, 24,
            NSOpenGLPFAAlphaSize, 8,
            NSOpenGLPFADoubleBuffer,
            0   //last element must be 0
        };
        
        //get pixel format and opengl context from above PFA
        NSOpenGLPixelFormat *pixelFormat = [[[NSOpenGLPixelFormat alloc] initWithAttributes:attrs] autorelease];
        NSOpenGLContext *glContext = [[[NSOpenGLContext alloc] initWithFormat:pixelFormat shareContext:nil] autorelease];
        
        //set these pixelFormat and glContext
        [self setPixelFormat:pixelFormat];
        [self setOpenGLContext:glContext];
    }
    return self;
}

//overrided method
-(void)prepareOpenGL
{
    //code
    fprintf(gpFile, "OpenGL Version : %s\n", glGetString(GL_VERSION));
    fprintf(gpFile, "GLSL Version    : %s\n", glGetString(GL_SHADING_LANGUAGE_VERSION));
    
    //set swap interval to 1, so that rendering will be done at display refresh rate
    int swapInt = 1;
    [[self openGLContext] setValues:&swapInt forParameter:NSOpenGLCPSwapInterval];
    
    /********VERTEX SHADER**********/
    vertexShaderObject = glCreateShader(GL_VERTEX_SHADER);
    const GLchar* vertexShaderSourceCode =
    "#version 410" \
    "\n" \
    "in vec4 vPosition;" \
    "uniform mat4 u_mvp_matrix;" \
    "void main(void)" \
    "{" \
    "gl_Position = u_mvp_matrix * vPosition;" \
    "}";
    
    glShaderSource(vertexShaderObject, 1, (GLchar**)&vertexShaderSourceCode, NULL);
    glCompileShader(vertexShaderObject);
    GLint iShaderCompileStatus = 0;
    glGetShaderiv(vertexShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);
    if(iShaderCompileStatus == GL_FALSE)
    {
        GLint iInfoLogLen = 0;
        glGetShaderiv(vertexShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLen);
        if(iInfoLogLen)
        {
            char *szInfoLog = (char*)malloc(iInfoLogLen);
            if(szInfoLog)
            {
                GLint written;
                glGetShaderInfoLog(vertexShaderObject, iInfoLogLen, &written, szInfoLog);
                fprintf(gpFile, "Vertex Shader Compile Info: %s\n", szInfoLog);
                free(szInfoLog);
                [self release];
                [NSApp terminate:self];
            }
        }
    }
    
    /********FRAGMENT SHADER***************/
    fragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);
    const GLchar* fragmentShaderSourceCode =
    "#version 410" \
    "\n" \
    "out vec4 FragColor;" \
    "void main(void)" \
    "{" \
    "FragColor = vec4(1.0, 1.0, 1.0, 1.0);" \
    "}";
    glShaderSource(fragmentShaderObject, 1, (GLchar**)&fragmentShaderSourceCode, NULL);
    glCompileShader(fragmentShaderObject);
    iShaderCompileStatus = 0;
    glGetShaderiv(fragmentShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);
    if(iShaderCompileStatus == GL_FALSE)
    {
        GLint iInfoLogLen = 0;
        glGetShaderiv(fragmentShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLen);
        if(iInfoLogLen)
        {
            char* szInfoLog = (char*)malloc(iInfoLogLen);
            if(szInfoLog)
            {
                GLint written;
                glGetShaderInfoLog(fragmentShaderObject, iInfoLogLen, &written, szInfoLog);
                fprintf(gpFile, "Fragment Shader Compile Info:%s\n", szInfoLog);
                free(szInfoLog);
                [self release];
                [NSApp terminate:self];
            }
        }
    }
    
    /*********SHADER PROGRAM*************/
    shaderProgramObject = glCreateProgram();
    
    //attach VS and FS to Program
    glAttachShader(shaderProgramObject, vertexShaderObject);
    glAttachShader(shaderProgramObject, fragmentShaderObject);
    
    //pre-link shader attributes binding
    glBindAttribLocation(shaderProgramObject, VDG_ATTRIBUTE_VERTEX, "vPosition");
    
    //link Shader program
    glLinkProgram(shaderProgramObject);
    GLint iProgramLinkStatus = 0;
    glGetProgramiv(shaderProgramObject, GL_LINK_STATUS, &iProgramLinkStatus);
    if(iProgramLinkStatus == GL_FALSE)
    {
        GLint iProgramInfoLen = 0;
        glGetProgramiv(shaderProgramObject, GL_INFO_LOG_LENGTH, &iProgramInfoLen);
        if(iProgramInfoLen)
        {
            char* szInfo = (char*) malloc(iProgramInfoLen);
            if(szInfo)
            {
                GLint written;
                glGetProgramInfoLog(shaderProgramObject, iProgramInfoLen, &written, szInfo);
                fprintf(gpFile, "Shader Link Info: %s\n", szInfo);
                free(szInfo);
                [self release];
                [NSApp terminate:self];
            }
        }
    }
    //post link get uniform locations
    uniform_mvp = glGetUniformLocation(shaderProgramObject, "u_mvp_matrix");
    
    
    /********VERTICES, COLOR, NORMAL, VAO, VBO Initialization****************/
    
    //Perspective vertices
    GLfloat triangleVertices [] =
    {
        1.0f,   -1.0f,  0.0f,
        0.0f,   1.0f,   0.0f,
        -1.0f,  -1.0f,  0.0f
    };
    
    //vao
    glGenVertexArrays(1, &vao);
    glBindVertexArray(vao);
    //vbo
    {
        glGenBuffers(1, &vbo);
        glBindBuffer(GL_ARRAY_BUFFER, vbo);
        glBufferData(GL_ARRAY_BUFFER, sizeof(triangleVertices), triangleVertices, GL_STATIC_DRAW);
        
        glVertexAttribPointer(VDG_ATTRIBUTE_VERTEX,
                              3,    //X, Y, Z
                              GL_FLOAT, //DATA TYPE
                              GL_FALSE,
                              0, NULL); //stride and offset address none
        glEnableVertexAttribArray(VDG_ATTRIBUTE_VERTEX);
        
        glBindBuffer(GL_ARRAY_BUFFER, 0);    //unbind vbo
    }
    glBindVertexArray(0);  //unbind vao
    
    //other OpenGL settings
    glClearDepth(1.0f);
    glEnable(GL_DEPTH_TEST);    //Enable depth test
    glDepthFunc(GL_LEQUAL);     //Depth test to do
    glEnable(GL_CULL_FACE);     //We culling back faces for better performance(when front becomes back side, it will be rendered at all)
    
    //set background clearing color
    glClearColor(0.0f, 0.0f, 1.0f, 1.0f);   //blue
    
    perspectiveProjectionMatrix = vmath::mat4::identity();
    
    //Initialize CVDisplayLink
    CVDisplayLinkCreateWithActiveCGDisplays(&displayLink);
    CVDisplayLinkSetOutputCallback(displayLink, &MyDisplayLinkCallback, self);  //self: last argument passed to callback function whenever called
    
    //convert NSOpenGL context and pixel format to CGL context and pixel format
    CGLContextObj cglContext = (CGLContextObj) [[self openGLContext] CGLContextObj];
    CGLPixelFormatObj cglPixelFormat = (CGLPixelFormatObj) [[self pixelFormat] CGLPixelFormatObj];
    
    //set current cgl display and start the display
    CVDisplayLinkSetCurrentCGDisplayFromOpenGLContext(displayLink, cglContext, cglPixelFormat);
    CVDisplayLinkStart(displayLink);
}

//overrided method
-(void)reshape
{
    CGLLockContext((CGLContextObj) [[self openGLContext] CGLContextObj]);
    
    NSRect rect = [self bounds];
    GLfloat width = rect.size.width;
    GLfloat height = rect.size.height;
    
    if(height == 0.0f)
    height = 1.0f;
    
    glViewport(0, 0, (GLsizei)width, (GLsizei)height);

//change    
    perspectiveProjectionMatrix = vmath::perspective(45.0f, width/height, 0.1f, 100.0f);
    
    CGLUnlockContext((CGLContextObj) [[self openGLContext] CGLContextObj] );
}

//our method
-(CVReturn)getFrameForTime:(const CVTimeStamp*)pOutTime
{
    //code
    NSAutoreleasePool *pPool = [[NSAutoreleasePool alloc] init];
    
    [self drawView];
    
    [pPool release];
    return (kCVReturnSuccess);
}


//overrided method
-(void)drawRect:(NSRect)dirtyRect   //called from main thread
{
    //code
    [self drawView];
}

-(void)drawView     //render method
{
    CGLLockContext((CGLContextObj) [[self openGLContext] CGLContextObj]);
    
    [[self openGLContext] makeCurrentContext];
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    
    glUseProgram(shaderProgramObject);
    
    vmath::mat4 modelViewProjectionMatrix = vmath::mat4::identity();
    vmath::mat4 modelViewMatrix = vmath::mat4::identity();
	//change due to perspective projection..added translation matrix
    vmath::mat4 translateMatrix = vmath::mat4::identity();  
    
    translateMatrix = vmath::translate(0.0f, 0.0f, -6.0f);
    modelViewMatrix = translateMatrix * modelViewMatrix;
    
    modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;
    
    glUniformMatrix4fv (uniform_mvp, 1, GL_FALSE, modelViewProjectionMatrix);
    
    glBindVertexArray(vao);  
    glDrawArrays(GL_TRIANGLES, 0, 3);  
    glBindVertexArray(0);  
    
    glUseProgram(0);    
    
    CGLFlushDrawable((CGLContextObj) [[self openGLContext] CGLContextObj]);
    CGLUnlockContext((CGLContextObj) [[self openGLContext] CGLContextObj]);
}

//event handling
-(BOOL)acceptsFirstResponder
{
    [[self window] makeFirstResponder:self];
    return (YES);
}

-(void)keyDown:(NSEvent*)theEvent
{
    int key = (int)[[theEvent characters] characterAtIndex:0];
    switch(key)
    {
            case 27:    //escape key
            [self release];
            [NSApp terminate:self];
            break;
            case 'F':
            case 'f':
            [[self window] toggleFullScreen:self];  //auto repaint done
            break;
        default:
            break;
    }
}

-(void) dealloc
{
    //CVDisplayLinkStop(displayLink);
    //CVDisplayLinkRelease(displayLink);
    
    if(vao)
    {
        glDeleteVertexArrays(1, &vao);
        vao = 0;
    }
    
    if(vbo)
    {
        glDeleteBuffers(1, &vbo);
        vbo = 0;
    }
    
    if(shaderProgramObject)
    {
        if(fragmentShaderObject)
        {
            glDetachShader(shaderProgramObject, fragmentShaderObject);
            glDeleteShader(fragmentShaderObject);
            fragmentShaderObject = 0;
        }
        
        if(vertexShaderObject)
        {
            glDetachShader(shaderProgramObject, vertexShaderObject);
            glDeleteShader(vertexShaderObject);
            vertexShaderObject = 0;
        }
        
        glDeleteProgram(shaderProgramObject);
        shaderProgramObject = 0;
    }
    
    glUseProgram(0);
    
    [super dealloc];
}
@end

CVReturn MyDisplayLinkCallback(CVDisplayLinkRef displayLink, const CVTimeStamp *pNow, const CVTimeStamp *pOutTime, CVOptionFlags FlagsIn, CVOptionFlags* pFlagsOut, void* pDisplayLinkContext)
{
    CVReturn ret = kCVReturnSuccess;
    
    GLView *pglView = (GLView*)pDisplayLinkContext;
    
    ret = [pglView getFrameForTime:pOutTime];
    
    return (ret);
}
