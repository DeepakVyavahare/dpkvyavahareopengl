//smiley texture
//headers
#import <Foundation/Foundation.h>
#import <Cocoa/Cocoa.h>

//Core Video
#import <QuartzCore/CVDisplayLink.h>

//OpenGL
#import <OpenGL/gl3.h>
#import <OpenGL/gl3ext.h>

//vector maths --> matrix/vector operations
#import "vmath.h"

enum
{
    VDG_ATTRIBUTE_VERTEX= 0,
    VDG_ATTRIBUTE_COLOR,
    VDG_ATTRIBUTE_NORMAL,
    VDG_ATTRIBUTE_TEXTURE0,
};

//log file pointer
FILE *gpFile = NULL;

// 'C' style function declaration
CVReturn MyDisplayLinkCallback(CVDisplayLinkRef, const CVTimeStamp*, const CVTimeStamp*, CVOptionFlags, CVOptionFlags*, void*);

//interface declarations
@interface AppDelegate: NSObject <NSApplicationDelegate, NSWindowDelegate>
@end

@interface GLView: NSOpenGLView
@end

//entry point function
int main(int argc, const char *argv[])
{
    NSAutoreleasePool *pPool = [[NSAutoreleasePool alloc] init];
    
    NSApp = [NSApplication sharedApplication];
    
    [NSApp setDelegate:[[AppDelegate alloc] init]];
    
    [NSApp run];    //run loop
    
    [pPool release];
    
    return (0);
}

//implementations
@implementation AppDelegate
{
@private
    NSWindow *window;
    GLView *glView;
}

-(void)applicationDidFinishLaunching:(NSNotification*)aNotification
{
    //code
    //create log file
    NSBundle *bundle = [NSBundle mainBundle];
    NSString *appDirPath = [bundle bundlePath];
    NSString *appDirParentPath = [appDirPath stringByDeletingLastPathComponent];
    NSString *logFilePath = [NSString stringWithFormat:@"%@/Log.txt",appDirParentPath];
    //convert NSSTring to char*
    const char* szLogFilePath = [logFilePath cStringUsingEncoding:NSASCIIStringEncoding];
    
    gpFile = fopen(szLogFilePath, "w");
    if(gpFile == NULL)
    {
        printf("Cannot Open Log.txt. Exiting...\n");
        [self release];
        [NSApp terminate:self];
    }
    else
    {
        fprintf(gpFile, "Applicaton Is Started Successfully\n");
    }
    
    //create window
    NSRect win_rect = NSMakeRect(0.0, 0.0, 800.0, 600.0);
    window = [[NSWindow alloc] initWithContentRect:win_rect
                                         styleMask:NSWindowStyleMaskTitled | NSWindowStyleMaskClosable | NSWindowStyleMaskMiniaturizable | NSWindowStyleMaskResizable
                                           backing:NSBackingStoreBuffered
                                             defer:NO];
    
    //set window properties
    [window setTitle:@"macOS OpenGL --> Tweak Smiley Texture"];     // <--- Modify as per requirement
    [window center];
    
    //create OpenGL view
    glView = [[GLView alloc] initWithFrame:[window frame]];
    
    if(glView == nil)
    {
        fprintf(gpFile, "Unable to Create GLView. Exiting...\n");
        [self release];
        [NSApp terminate:self];
    }
    
    //attach glview to window
    [window setContentView:glView];
    [window setDelegate:self];
    [window makeKeyAndOrderFront:self];
}

-(void)applicationWillTerminate:(NSNotification*)aNotification
{
    fprintf(gpFile, "Application Is Close Successfully\n");
    if(gpFile)
    {
        fclose(gpFile);
        gpFile = NULL;
    }
}

-(void)windowWillClose:(NSNotification*)aNotification
{
    //code
    [NSApp terminate:self];
}

-(void)dealloc
{
    //code
    [glView release];
    [window release];
    
    [super dealloc];
}
@end

@implementation GLView
{
@private
    CVDisplayLinkRef displayLink;
    
    GLuint vertexShaderObject;
    GLuint fragmentShaderObject;
    GLuint shaderProgramObject;
    
    
    GLuint vao[NR_SHAPES];
    GLuint vbo[NR_SHAPES][NR_SHAPE_ATTRS];
    GLuint uniform[NR_UNIFORMS];
    
    GLuint texture_smiley;
    GLuint procedureTextureObject;
    GLuint keyPress;
    
    vmath::mat4 projectonMatrix;
    
	enum eShapes           
    {
        QUAD=0,
        NR_SHAPES,
    };
    enum eShape_Attr
    {
        POSITION = 0,
        TEXCOORDS,
        NR_SHAPE_ATTRS,
    };
    enum eUniforms
    {
        MVP = 0,
        TEXTURE0_SAMPLER_2D,
        NR_UNIFORMS,
    };
    
    GLubyte proceduralImageArray[64][64][4];

}

-(id)initWithFrame:(NSRect)frame
{
    self = [super initWithFrame:frame];
    
    if(self)
    {
        [[self window] setContentView:self];
        
        //Pixel Format Attribute list
        NSOpenGLPixelFormatAttribute attrs [] =
        {
            //key,value(optional)
            NSOpenGLPFAOpenGLProfile,NSOpenGLProfileVersion4_1Core,
            NSOpenGLPFAScreenMask,CGDisplayIDToOpenGLDisplayMask(kCGDirectMainDisplay),
            NSOpenGLPFANoRecovery,   //dont fallback to sw renderer if hw doesnot found
            NSOpenGLPFAAccelerated,
            NSOpenGLPFAColorSize,24,
            NSOpenGLPFADepthSize,24,
            NSOpenGLPFAAlphaSize,8,
            NSOpenGLPFADoubleBuffer,
            0   //last must be 0 indicating end of list
        };
        
        //Ask NSOpenGL for pixel format by providing above attributes
        NSOpenGLPixelFormat *pixelFormat = [[[NSOpenGLPixelFormat alloc] initWithAttributes:attrs] autorelease];
        if(pixelFormat == nil)
        {
            fprintf(gpFile, "No Valid Pixel Format Available. Exiting..\n");
            [self release];
            [NSApp terminate:self];
        }
        
        //Ask NSOpenGL for context by providing pixelFormat
        NSOpenGLContext *glContext = [[[NSOpenGLContext alloc] initWithFormat:pixelFormat shareContext:nil] autorelease];
        
        //Save context and pixel format in GLView
        [self setPixelFormat:pixelFormat];
        [self setOpenGLContext:glContext];
    }
    return (self);
}

-(CVReturn)getFrameForTime:(const CVTimeStamp*)pOutTime
{
    NSAutoreleasePool *pPool = [[NSAutoreleasePool alloc] init];
    
    [self drawView];
    
    [pPool release];
    return (kCVReturnSuccess);
}

-(void)prepareOpenGL
{
    //code
    fprintf(gpFile, "OpenGL Version :%s\n", glGetString(GL_VERSION));
    fprintf(gpFile, "GLSL Version   :%s\n", glGetString(GL_SHADING_LANGUAGE_VERSION));
    
    [[self openGLContext] makeCurrentContext];
    
    //set swap interval to 1, to render your scene on display refresh rate
    GLint swapInt = 1;
    [[self openGLContext] setValues:&swapInt forParameter:NSOpenGLCPSwapInterval];
    
    /*************OPENGL and SHADER INIT************************/       // <--- Modify as per requirement
    
    /*****VERTEX SHADER********/
    vertexShaderObject = glCreateShader(GL_VERTEX_SHADER);
    const GLchar* vsSource =
    "#version 410" \
    "\n" \
    "in vec4 vPosition;" \
    "in vec2 vTexture0_Coord;" \
    "uniform mat4 u_mvp_matrix;" \
    "out vec2 oTexture0_Coord;" \
    "void main(void)" \
    "{" \
    "gl_Position = u_mvp_matrix * vPosition;" \
    "oTexture0_Coord = vTexture0_Coord;" \
    "}";
    
    glShaderSource(vertexShaderObject, 1, (const GLchar**)&vsSource, NULL);
    glCompileShader(vertexShaderObject);
    GLint iShaderCompileStatus = 0;
    glGetShaderiv(vertexShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);
    if(GL_FALSE == iShaderCompileStatus)
    {
        GLsizei iLogLen = 0;
        glGetShaderiv(vertexShaderObject, GL_INFO_LOG_LENGTH, &iLogLen);
        if(iLogLen)
        {
            GLchar* szLog = (GLchar*)malloc(iLogLen);
            if(szLog)
            {
                GLsizei written;
                glGetShaderInfoLog(vertexShaderObject, iLogLen, &written, szLog);
                fprintf(gpFile, "Vertex Shader Compile Info: %s\n", szLog);
                free(szLog);
                [self release];
                [NSApp terminate:self];
            }
        }
    }//================
    
    /*****FRAGMENT SHADER********/
    fragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);
    const GLchar* fsSource =
    "#version 410" \
    "\n" \
    "in vec2 oTexture0_Coord;" \
    "uniform sampler2D u_texture0_sampler;" \
    "out vec4 FragColor;" \
    "void main(void)" \
    "{" \
    "vec3 tex = vec3(texture(u_texture0_sampler, oTexture0_Coord));" \
    "FragColor = vec4(tex, 1.0);" \
    "}";
    
    glShaderSource(fragmentShaderObject, 1, (const GLchar**)&fsSource, NULL);
    glCompileShader(fragmentShaderObject);
    iShaderCompileStatus = 0;
    glGetShaderiv(fragmentShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);
    if(GL_FALSE == iShaderCompileStatus)
    {
        GLsizei iLogLen = 0;
        glGetShaderiv(fragmentShaderObject, GL_INFO_LOG_LENGTH, &iLogLen);
        if(iLogLen)
        {
            GLchar* szLog = (GLchar*)malloc(iLogLen);
            if(szLog)
            {
                GLsizei written;
                glGetShaderInfoLog(fragmentShaderObject, iLogLen, &written, szLog);
                fprintf(gpFile, "Fragment Shader Compile Info: %s\n", szLog);
                free(szLog);
                [self release];
                [NSApp terminate:self];
            }
        }
    }//===================
    
    /*****SHADER PROGRAM********/
    shaderProgramObject = glCreateProgram();
    //attach vs and fs to shader program
    glAttachShader(shaderProgramObject, vertexShaderObject);
    glAttachShader(shaderProgramObject, fragmentShaderObject);
    
    //pre-link binding of vertex attributes
    glBindAttribLocation(shaderProgramObject, VDG_ATTRIBUTE_POSITION, "vPosition");     // <--- Modify/add as per requirement
    glBindAttribLocation(shaderProgramObject, VDG_ATTRIBUTE_TEXTURE0, "vTexture0_Coord");
    
    //link program
    glLinkProgram(shaderProgramObject);
    
    //post-link, get uniform locations from shaders
    uniform[MVP] = glGetUniformLocation(shaderProgramObject, "u_mvp_matrix");           // <--- Modify/add as per requirement
    uniform[TEXTURE0_SAMPLER_2D] = glGetUniformLocation(shaderProgramObject, "u_texture0_sampler");
    
    /*******LOAD TEXTURES****************/
    texture_smiley = [self loadTextureFromBMPFile:"Smiley.bmp"];
    procedureTextureObject = [self loadproceduralImageArray];
    
	/*************VERTICES, COLOR, NORMALS, VAO, VBO INITIALIZATIONS******************/
    GLfloat quadVertices [] =
    {
       
	     1.0f,   1.0f,   0.0f,   
		   -1.0f,  1.0f,   0.0f,  
		   -1.0f,  -1.0f,  0.0f,   
		    1.0f,   -1.0f,  0.0f
    };
    
	
	GLfloat quadTexCoords [] =
    {
    
	  1.0f,   0.0f,
	   0.0f,  0.0f,
	     0.0f,  1.0f, 
		  1.0f,   1.0f,
    };
    
    glGenVertexArrays(1, &vao[QUAD]);
    glBindVertexArray(vao[QUAD]);
    
        glGenBuffers(1, &vbo[QUAD][POSITION]);
        glBindBuffer(GL_ARRAY_BUFFER, vbo[QUAD][POSITION]);
        glBufferData(GL_ARRAY_BUFFER, sizeof(quadVertices), quadVertices, GL_STATIC_DRAW);
        
        glVertexAttribPointer(VDG_ATTRIBUTE_POSITION,
                              3,    //x,y,z
                              GL_FLOAT,
                              GL_FALSE,
                              0, NULL);
        glEnableVertexAttribArray(VDG_ATTRIBUTE_POSITION);
        
        glBindBuffer(GL_ARRAY_BUFFER, 0);   //unbind
    
	
	    glGenBuffers(1, &vbo[QUAD][TEXCOORDS]);
        glBindBuffer(GL_ARRAY_BUFFER, vbo[QUAD][TEXCOORDS]);
        glBufferData(GL_ARRAY_BUFFER, sizeof(quadTexCoords), quadTexCoords, GL_DYNAMIC_DRAW);   //going to change in drawView
        
        glVertexAttribPointer(VDG_ATTRIBUTE_TEXTURE0,
                              2,    
                              GL_FLOAT,
                              GL_FALSE,
                              0, NULL); 
        glEnableVertexAttribArray(VDG_ATTRIBUTE_TEXTURE0);
        
        glBindBuffer(GL_ARRAY_BUFFER, 0);   //unbind
    
    glBindVertexArray(0);   
    //==================
    
    //set backgroud clearing colof
    glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
    
	projectonMatrix = vmath::mat4::identity();
    
    /*************CVDisplayLink INIT***************************/
    CVDisplayLinkCreateWithActiveCGDisplays(&displayLink);
    
	CVDisplayLinkSetOutputCallback(displayLink, &MyDisplayLinkCallback, self);
    
	CGLPixelFormatObj cglPixelFormat = (CGLPixelFormatObj) [[self pixelFormat] CGLPixelFormatObj];
    CGLContextObj cglContext = (CGLContextObj) [[self openGLContext] CGLContextObj];
    
	CVDisplayLinkSetCurrentCGDisplayFromOpenGLContext(displayLink, cglContext, cglPixelFormat);
    
	CVDisplayLinkStart(displayLink);
}

-(GLuint)loadTextureFromBMPFile:(const char*)texFile
{
    NSBundle *bundle = [NSBundle mainBundle];
    NSString *appDirPath = [bundle bundlePath];
    NSString *appDirParentPath = [appDirPath stringByDeletingLastPathComponent];
    NSString *textureFilePath = [NSString stringWithFormat:@"%@/%s",appDirParentPath, texFile];
    
    NSImage *bmpImage = [[NSImage alloc] initWithContentsOfFile:textureFilePath];
    if(!bmpImage)
    {
        NSLog(@"Can't File %@", textureFilePath);
        return (0);
    }
    
    //Convert NSImage to CGImageRef
    CGImageRef cgImage = [bmpImage CGImageForProposedRect:nil context:nil hints:nil];
    
    int w = (int)CGImageGetWidth(cgImage);
    int h = (int)CGImageGetHeight(cgImage);
    
    CFDataRef imageData = CGDataProviderCopyData(CGImageGetDataProvider(cgImage));
    
    void *pixels = (void*)CFDataGetBytePtr(imageData);
    //=====
    
    GLuint bmpTexture;
    glGenTextures(1, &bmpTexture);
    glPixelStorei(GL_UNPACK_ALIGNMENT, 1);  //1 byte alignment for better performanc, default=4
    glBindTexture(GL_TEXTURE_2D, bmpTexture);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    
    glTexImage2D(GL_TEXTURE_2D,
                 0, 
                 GL_RGBA,  
                 w,
                 h,
                 0, 
                 GL_RGBA,
                 GL_UNSIGNED_BYTE,
                 pixels);
    
    glBindTexture(GL_TEXTURE_2D, 0);  
    CFRelease(imageData);
    
    return (bmpTexture);
}

-(void) functionTexture
{
    int i, j, c;
    
    for (i = 0; i<64; i++)
    {
        for (j = 0; j<64; j++)
        {
            proceduralImageArray[i][j][0] = 0xFF;  
            proceduralImageArray[i][j][1] = 0xFF;  
            proceduralImageArray[i][j][2] = 0xFF;  
            proceduralImageArray[i][j][3] = 0xFF;  
        }
    }
}

-(GLuint)loadproceduralImageArray
{
    [self functionTexture];
    
    GLuint whiteTexture;
    
    glGenTextures(1, &whiteTexture);
    glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
    glBindTexture(GL_TEXTURE_2D, whiteTexture);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexImage2D(GL_TEXTURE_2D,        
                 0,                    
                 GL_RGBA,            
                 64, 64,
                 0,                  
                 GL_RGBA,            
                 GL_UNSIGNED_BYTE,   
                 proceduralImageArray);
    return(whiteTexture);
}

-(void)reshape
{
    //code
    CGLLockContext((CGLContextObj) [[self openGLContext] CGLContextObj]);
    
    NSRect rect = [self bounds];
    GLfloat width = rect.size.width;
    GLfloat height = rect.size.height;
    
    glViewport(0, 0, (GLsizei)width, (GLsizei)height);
    
    projectonMatrix = vmath::perspective(45.0f, width/height, 0.1f, 100.0f);        
    
    CGLUnlockContext((CGLContextObj) [[self openGLContext] CGLContextObj]);
}

-(void)drawRect:(NSRect)dirtyRect
{
    //code
    [self drawView];
}

//our method
-(void)drawView
{
    //code
    CGLLockContext((CGLContextObj) [[self openGLContext] CGLContextObj]);
    [[self openGLContext] makeCurrentContext];
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    
    glUseProgram(shaderProgramObject);
    
    vmath::mat4 modelViewMatrix = vmath::mat4::identity();
    vmath::mat4 translateMatrix = vmath::mat4::identity();
    vmath::mat4 modelViewProjectionMatrix = vmath::mat4::identity();
    
    translateMatrix = vmath::translate(0.0f, 0.0f, -6.0f);
    
    modelViewMatrix = translateMatrix * modelViewMatrix;
    
    modelViewProjectionMatrix = projectonMatrix * modelViewMatrix;
    
    glUniformMatrix4fv(uniform[MVP], 1, GL_FALSE, modelViewProjectionMatrix);
    
    //*** bind texture ***
    GLuint texture = texture_smiley;
    GLfloat texCoords [8];  //4 vertex, 2 cordinates(s,t)
    switch (keyPress)
    {
	        
            case 0:
			   default:
					texture = procedureTextureObject;
     
	       case 1:
            texCoords[0] = 0.5f;    texCoords[1] = 0.5f;
            texCoords[2] = 0.0f;    texCoords[3] = 0.5f;
            texCoords[4] = 0.0f;    texCoords[5] = 1.0f;
            texCoords[6] = 0.5f;    texCoords[7] = 1.0f;
            break;
            
            case 2:
            texCoords[0] = 1.0f;    texCoords[1] = 0.0f;
            texCoords[2] = 0.0f;    texCoords[3] = 0.0f;
            texCoords[4] = 0.0f;    texCoords[5] = 1.0f;
            texCoords[6] = 1.0f;    texCoords[7] = 1.0f;
            break;
     
	        case 3://4 smiley
            texCoords[0] = 2.0f;    texCoords[1] = 0.0f;
            texCoords[2] = 0.0f;    texCoords[3] = 0.0f;
            texCoords[4] = 0.0f;    texCoords[5] = 2.0f;
            texCoords[6] = 2.0f;    texCoords[7] = 2.0f;
            break;
            
            case 4: //center 
            texCoords[0] = 0.5f;    texCoords[1] = 0.5f;
            texCoords[2] = 0.5f;    texCoords[3] = 0.5f;
            texCoords[4] = 0.5f;    texCoords[5] = 0.5f;
            texCoords[6] = 0.5f;    texCoords[7] = 0.5f;
            break;
    }
    glBindTexture(GL_TEXTURE_2D, texture);
    
    //*** bind vao ***
    glBindVertexArray(vao[QUAD]);
    {
        glBindBuffer(GL_ARRAY_BUFFER, vbo[QUAD][TEXCOORDS]);
        glBufferData(GL_ARRAY_BUFFER, sizeof(texCoords), texCoords, GL_DYNAMIC_DRAW);
        glBindBuffer(GL_ARRAY_BUFFER, 0);
        
        //Draw/render using glDrawArrays
        glDrawArrays(GL_TRIANGLE_FAN, 0, 4);    //T1(0,1,2) and  T2(0,2,3)
    }
    glBindVertexArray(0);   //unbind
    
    glUseProgram(0);
    
    CGLFlushDrawable((CGLContextObj) [[self openGLContext] CGLContextObj]); //flushes data into other framebuffer which is not being displayed
    CGLUnlockContext((CGLContextObj) [[self openGLContext] CGLContextObj]);
}

//Hardware Event Handling
-(BOOL)acceptsFirstResponder
{
    //code
    [[self window] makeFirstResponder:self];        //tell your window that GLView(self) will handle key, mouse etc. events
    
    return (YES);
}

-(void)keyDown:(NSEvent*)theEvent
{
    //code
    int key = (int) [[theEvent characters] characterAtIndex:0];
    switch(key)    
    {
            case 27:    //escape key
            [self release];
            [NSApp terminate:self];
            break;

            case 'f':
            case 'F':
            [[self window] toggleFullScreen:self];  
            break;

            case '0':
            keyPress = 0;
            break;
            case '1':
            keyPress = 1;
            break;
            case '2':
            keyPress = 2;
            break;
            case '3':
            keyPress = 3;
            break;
            case '4':
            keyPress = 4;
            break;

        default:
            keyPress = 0;
            break;
    }
}

-(void)mouseDown:(NSEvent*)theEvent
{
    //code
}

-(void)mouseDragged:(NSEvent*)theEvent
{
    //code
}

-(void)rightMouseDown:(NSEvent*)theEvent
{
    //code
}

-(void)dealloc
{
    //stop and release Core Video link
    CVDisplayLinkStop(displayLink);
    CVDisplayLinkRelease(displayLink);
    
    //Release Shader objects
    if(shaderProgramObject)
    {
        if(fragmentShaderObject)
        {
            glDetachShader(shaderProgramObject, fragmentShaderObject);
            glDeleteShader(fragmentShaderObject);
            fragmentShaderObject = 0;
        }
        if(vertexShaderObject)
        {
            glDetachShader(shaderProgramObject, vertexShaderObject);
            glDeleteShader(vertexShaderObject);
            vertexShaderObject = 0;
        }
        
        glDeleteProgram(shaderProgramObject);
        shaderProgramObject = 0;
    }
    
    //Un-use shader program
    glUseProgram(0);
    
    [super dealloc];
}

@end

CVReturn MyDisplayLinkCallback(CVDisplayLinkRef displayLink, const CVTimeStamp* pNow, const CVTimeStamp *pOutTime, CVOptionFlags FlagsIn, CVOptionFlags *FlagsOut, void* pCVDisplayContext)
{
    CVReturn ret = kCVReturnSuccess;
    
    GLView *glView = (GLView*) pCVDisplayContext;
    
    ret = (CVReturn) [glView getFrameForTime:pOutTime];
    
    return (ret);
}
