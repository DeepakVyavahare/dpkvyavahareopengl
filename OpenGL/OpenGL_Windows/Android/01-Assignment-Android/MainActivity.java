package com.astromedicomp.hello_landscape;

//view & activity are in same program..no seperate view class here
// default supplied packages by android SDK
import android.app.Activity;
import android.os.Bundle;

// later added packages
import android.view.Window; // for "Window" class
import android.view.WindowManager; // for "WindowManager" class
import android.content.pm.ActivityInfo; // for "ActivityInfo" class //pm: packg manager
import android.graphics.Color; // for "Color" class
import android.widget.TextView; // for "TextView" class

import android.view.Gravity;//orientation landscape/portrait handled by device


public class MainActivity extends Activity
{
//    private MyView myView;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
		super.onCreate(savedInstanceState);

        // this is done to get rid of ActionBar
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
	        //we can ignore this.  and use only requestWindowFeature
		   //window.FEATURE_NO_TITLE.. this constant/enum

        // this is done to make Fullscreen
        //object chaining  ().abc
	   //layout windowMngr handle krto... flag && mask same ahe ithe
		this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        
	//setContentView(R.layout.activity_main);
        
        // force activity window orientation to Landscape
        //bedefault portrait mode
		// window landscape/portrait hot nai..hw krte MainActivity.this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
		//decoration view..each window has its own   color.BLACK pn //chalel

		getWindow().getDecorView().setBackgroundColor(Color.rgb(0, 0, 0));
 		//tWindow().getDecorView.setBackgroundColor(Color.rgb(0,0,0));
		//textview class for view
		TextView mytextview=new TextView(this);
	    mytextview.setText("!..Hello DPK..!");
		mytextview.setTextSize(60);
		mytextview.setTextColor(Color.GREEN);
     
		mytextview.setGravity(Gravity.CENTER);
	
	
		setContentView(mytextview);

	}
    
    @Override
    protected void onPause()
    {
        super.onPause();
    }
    
    @Override
    protected void onResume()
    {
        super.onResume();
    }
}
