package com.astromedicomp.hello2;

import android.app.Activity;
import android.os.Bundle;

import android.view.Window; //getWindow()
import android.view.WindowManager; //layout
import android.content.pm.ActivityInfo;
//import android.content.Context; // not required here
import android.graphics.Color; // for "Color" class
import android.widget.TextView; // for "TextView" class
import android.view.Gravity; //for handling orientation: landscape-portrait as its handled by device



public class MainActivity extends Activity
{
	
private MyView myview;
    
	@Override
    protected void onCreate(Bundle savedInstanceState)
	{
	
        super.onCreate(savedInstanceState);
		
		
		
		requestWindowFeature(Window.FEATURE_NO_TITLE);

		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

		MainActivity.this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE); //

		getWindow().getDecorView().setBackgroundColor(Color.BLACK); //. nntr Big letters madhe asel like "BLACK" means either constant/enum/nested class
       
	    myview = new MyView(this);  //did lot of errors here
		
		
	    setContentView(myview);
    }
}
