package com.astromedicomp.hello2;

import android.content.Context; // for drawing context related
import android.graphics.Color; // for "Color" class
import android.widget.TextView; // for "TextView" class
import android.view.Gravity;

public class MyView extends TextView
{
    
    MyView(Context context)
    {
        super(context);
        
        setTextColor(Color.rgb(255,128,0));
        setText("---Hello DPK--");
        setTextSize(60);
		setGravity(Gravity.CENTER);
		
     }
      
}
