package com.astromedicomp.Assignment3;

import android.app.Activity;
import android.os.Bundle;

import android.view.Window; 
import android.view.WindowManager;
import android.content.pm.ActivityInfo;

public class MainActivity extends Activity
{
    private dpkView glesView;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        // avoid ActionBar
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        
        //for Fullscreen
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_main);
        
        MainActivity.this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);

        glesView=new dpkView(this);
        
        setContentView(glesView);
    }
    
    @Override
    protected void onPause()
    {
        super.onPause();
    }
    
    @Override
    protected void onResume()
    {
        super.onResume();
    }
}
