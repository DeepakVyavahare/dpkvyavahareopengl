package com.astromedicomp.gestures;

import android.content.Context; // for drawing context related
import android.graphics.Color; // for "Color" class
import android.widget.TextView; // for "TextView" class
import android.view.Gravity;
import android.view.MotionEvent; // for "MotionEvent"
import android.view.GestureDetector; // for GestureDetector
import android.view.GestureDetector.OnGestureListener; // for OnGestureListener
import android.view.GestureDetector.OnDoubleTapListener; // OnDoubleTapListener


public class MyView extends TextView implements OnGestureListener, OnDoubleTapListener
{
  
      private GestureDetector gestureDetector;
    MyView(Context context)
    {
        super(context);
        
        setTextColor(Color.rgb(255,128,0));
        setText("---Hello DPK--");
        gestureDetector = new GestureDetector(context, this, null, false); // this means 'handler' i.e. who is going to handle
        gestureDetector.setOnDoubleTapListener(this); // this means 'handler' i.e. who is going to handle

        setTextSize(60);
		setGravity(Gravity.CENTER);
		
     }
  
  //vv imp as tuch is responcible for all
    @Override
    public boolean onTouchEvent(MotionEvent event)
    {
        int eventaction = event.getAction();
        if(!gestureDetector.onTouchEvent(event))
            super.onTouchEvent(event);
        return(true);
    }
    
    // abstract method from OnDoubleTapListener so must be implemented
    @Override
    public boolean onDoubleTap(MotionEvent e)
    {
        setText("Double Tap");
		        setTextSize(60);
		setGravity(Gravity.CENTER);

        return(true);
    }
    
    // abstract method from OnDoubleTapListener so must be implemented
    @Override
    public boolean onDoubleTapEvent(MotionEvent e)
    {
        // NO code req here bcoz its already in 'onDoubleTap'
        return(true);
    }
    
    // abstract method from OnDoubleTapListener so must be implemented
    @Override
    public boolean onSingleTapConfirmed(MotionEvent e)
    {
        setText("Single Tap");
        return(true);
    }
    
    // abstract method from OnGestureListener so must be implemented
    @Override
    public boolean onDown(MotionEvent e)
    {
        // NO code req here bcoz its already in 'onSingleTapConfirmed'
        return(true);
    }
    
    // abstract method from OnGestureListener so must
    @Override
    public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY)
    {
        return(true);
    }
    
    // abstract method from OnGestureListener so must
    @Override
    public void onLongPress(MotionEvent e)
    {
        setText("Long Press");
    }
    
    // abstract method from OnGestureListener so 
    @Override
    public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY)
    {
        setText("Scroll");
        return(true);
    }
    
    // abstract method from OnGestureListener so must
    @Override
    public void onShowPress(MotionEvent e)
    {
    }
    
	//must
    @Override
    public boolean onSingleTapUp(MotionEvent e)
    {
        return(true);
    }    
}
