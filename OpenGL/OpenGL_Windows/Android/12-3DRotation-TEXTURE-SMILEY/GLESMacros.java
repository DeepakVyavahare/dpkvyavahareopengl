package com.astromedicomp.myapplication;

public class GLESMacros
{
    // attribute index
    public static final int dpk_ATTRIBUTE_VERTEX=0;
    public static final int dpk_ATTRIBUTE_COLOR=1;
    public static final int dpk_ATTRIBUTE_NORMAL=2;
    public static final int dpk_ATTRIBUTE_TEXTURE0=3;
}
