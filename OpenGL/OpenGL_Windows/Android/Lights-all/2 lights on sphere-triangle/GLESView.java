// 2 lights on pyramid/cube/sphere using per fragment shader
package com.astromedicomp.myapplicationCubeTwoLights;

import android.content.Context; // for drawing context related
import android.opengl.GLSurfaceView; // for OpenGL Surface View and all related
import javax.microedition.khronos.opengles.GL10; // for OpenGLES 1.0 needed as param type GL10
import javax.microedition.khronos.egl.EGLConfig; // for EGLConfig needed as param type EGLConfig
import android.opengl.GLES32; // for OpenGLES 3.2
import android.view.MotionEvent; // for "MotionEvent"
import android.view.GestureDetector; // for GestureDetector
import android.view.GestureDetector.OnGestureListener; // OnGestureListener
import android.view.GestureDetector.OnDoubleTapListener; // for OnDoubleTapListener

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;
import java.nio.ShortBuffer;

import android.opengl.Matrix; // for Matrix math

public class GLESView extends GLSurfaceView implements GLSurfaceView.Renderer, OnGestureListener, OnDoubleTapListener
{
	private final Context context;
private GestureDetector gestureDetector;

private int doubleTap;		//to enable or disable light

private int vertexShaderObject;
private int fragmentShaderObject;
private int shaderProgramObject;

private int model_Uniform;
private int view_Uniform;
private int projection_Uniform;

private int light_enabled_Uniform;

private int La_lightAmbient_Uniform;
private int Ld_lightDiffuse_Uniform;
private int Ls_lightSpecular_Uniform;
private int light_position_Uniform;

private int light_position_Uniform1;
private int La_lightAmbient_Uniform1;
private int Ld_lightDiffuse_Uniform1;
private int Ls_lightSpecular_Uniform1;

private int Ka_Uniform;
private int Kd_Uniform;
private int Ks_Uniform;
private int material_shininess_Uniform;
private float[] perspectiveProjectionMatrix = new float[16];	//4x4 matrix

private int[] vao_pyramid = new int[1];
private int[] vbo_pyramid_position = new int[1];
private int[] vbo_pyramid_normal = new int[1];
private float anglePyramid = 0.0f;

private float light_ambient[] = new float [] {0.0f, 0.0f, 0.0f, 1.0f};
private float light_diffuse[] = new float [] {1.0f, 0.0f, 0.0f, 1.0f};
private float light_specular[] = new float [] {1.0f, 0.0f, 0.0f, 1.0f};
private float light_position[] = new float [] {-10.0f, 0.0f, 0.0f, 1.0f};//left side light

private float light_ambient1[] = new float [] {0.0f, 0.0f, 0.0f, 1.0f};
private float light_diffuse1[] = new float [] {0.0f, 0.0f, 1.0f, 1.0f};
private float light_specular1[] = new float [] {0.0f, 0.0f, 1.0f, 1.0f};
private float light_position1[] = new float [] {10.0f, 0.0f, 0.0f, 1.0f};//riht side ligth

private float material_ambient[] = new float [] {0.0f, 0.0f, 0.0f, 1.0f};
private float material_diffuse[] = new float [] {0.5f, 0.5f, 0.5f, 1.0f};
private float material_specular[] = new float [] {1.0f, 1.0f, 1.0f, 1.0f};
private float material_shininess = 50.0f;


//GLESView constructor
public GLESView(Context drawingContext)
{
	super(drawingContext);
	context = drawingContext;

	setEGLContextClientVersion(3);
	setRenderer(this);
	
	gestureDetector = new GestureDetector(context,this,null,false); //this means 'handler' i.e. who is going to handle
	gestureDetector.setOnDoubleTapListener(this); //this means 'handler' i.e. who is going to handle
}

//GLSurfaceView.Renderer abstract methods
@Override
public void onSurfaceCreated(GL10 gl, EGLConfig config)
{
	// OpenGL-ES version check
	String glesVersion = gl.glGetString(GL10.GL_VERSION);
	System.out.println("dpk: OpenGL-ES Version = " + glesVersion);

	// Get GLSL version
	String glslVersion = gl.glGetString(GLES32.GL_SHADING_LANGUAGE_VERSION);
	System.out.println("dpk: GLSL Version = " + glslVersion);
	this.initialize(gl);
}

@Override
public void onSurfaceChanged(GL10 unused, int width, int height)
{
	this.resize(width,height);
}

@Override
public void onDrawFrame(GL10 unused)
{
	this.display();
}


//Handling 'onTouchEvent' is MOST important
//because it triggers all gesture and tap events
@Override
public boolean onTouchEvent(MotionEvent e)
{
	int eventaction = e.getAction();
	if (!gestureDetector.onTouchEvent(e))
	{
		super.onTouchEvent(e);
	}
	return (true);
}

// OnDoubleTapListener: 3 abstract methods, so MUST be implemented
@Override
public boolean onDoubleTap(MotionEvent e)
{
	doubleTap = doubleTap + 1;
	if (doubleTap > 1)
		doubleTap = 0;

	return (true);
}

@Override
public boolean onDoubleTapEvent(MotionEvent e)
{
	// do not write any code here because already written 'onDoubleTap'
	return (true);
}

@Override
public boolean onSingleTapConfirmed(MotionEvent e)
{
	return (true);
}

// OnGestureListener : abstract methods, so MUST be implemented
@Override
public boolean onDown(MotionEvent e)
{
	// do not write any code here because already written 'onSingleTapConfirmed'
	return (true);
}

@Override
public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY)
{
	return (true);
}

@Override
public void onLongPress(MotionEvent e)
{
}

@Override
public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY)
{
	uninitialize();
	System.out.println("dpk: Application Exit");
	System.exit(0);	// exit the application
	return (true);
}

@Override
public void onShowPress(MotionEvent e)
{//keep empty : why?
}

@Override
public boolean onSingleTapUp(MotionEvent e)
{
	//handled onSingleTapConfirmed, so no need to handle this
	return (true);
}

private void initialize(GL10 gl)	
{
	/*********VERTEX SHADER***********/
	vertexShaderObject = GLES32.glCreateShader(GLES32.GL_VERTEX_SHADER);

	final String vertexShaderSourceCode = String.format
	(
		"#version 320 es" +
		"\n" +
		"in vec4 vPosition;" +
		"in vec3 vNormal;" +
		"uniform mat4 u_model_matrix;" +
		"uniform mat4 u_view_matrix;" +
		"uniform mat4 u_projection_matrix;" +
		"uniform mediump int u_light_enabled;" +
		"uniform vec4 u_light_position;" +
		"uniform vec4 u_light_position1;" +
		"out vec3 Transformed_normals;" +
		"out vec3 light_direction0;" +
		"out vec3 light_direction1;" +
		"out vec3 Viewer_vector;" +
		"void main(void)" +
		"{" +
		"if(u_light_enabled == 1)" +
		"{" +
		"vec4 eyeCoordinates = u_view_matrix * u_model_matrix * vPosition;" +
		"Transformed_normals = mat3(u_view_matrix * u_model_matrix) * vNormal;" +
		"light_direction0 = vec3(u_light_position) - eyeCoordinates.xyz;" +
		"light_direction1 = vec3(u_light_position1) - eyeCoordinates.xyz;" +
		"Viewer_vector = vec3(-eyeCoordinates.xyz);" +
		"}" +
		"gl_Position = u_projection_matrix * u_view_matrix * u_model_matrix * vPosition;" +
		"}"
	);

	GLES32.glShaderSource(vertexShaderObject, vertexShaderSourceCode);

	GLES32.glCompileShader(vertexShaderObject);

	int[] iShaderCompileStatus = new int[1];	
	GLES32.glGetShaderiv(vertexShaderObject, GLES32.GL_COMPILE_STATUS, iShaderCompileStatus, 0);	
	if (iShaderCompileStatus[0] == GLES32.GL_FALSE)
	{
		int iInfoLogLength[] = new int[1];		
		GLES32.glGetShaderiv(vertexShaderObject, GLES32.GL_INFO_LOG_LENGTH, iInfoLogLength, 0);
		if (iInfoLogLength[0] > 0)
		{
			String szInfoLog = GLES32.glGetShaderInfoLog(vertexShaderObject);
			System.out.println("dpk: Vertex Shader Compilation Log = " + szInfoLog);
			uninitialize();
			System.exit(0);
		}
	}

	/*********FRAGMENT SHADER***********/
	fragmentShaderObject = GLES32.glCreateShader(GLES32.GL_FRAGMENT_SHADER);

	final String fragmentShaderSourceCode = String.format
	(
		"#version 320 es" +
		"\n" +
		"precision highp float;" +
		"in vec3 Transformed_normals;" +
		"in vec3 light_direction0;" +
		"in vec3 light_direction1;" +
		"in vec3 Viewer_vector;" +
		"uniform int u_light_enabled;" +
		"uniform vec3 u_La;" +
		"uniform vec3 u_Ld;" +
		"uniform vec3 u_Ls;" +
	
		"uniform vec3 u_La_1;" +
		"uniform vec3 u_Ld_1;" +
		"uniform vec3 u_Ls_1;" +
	
		"uniform vec3 u_Ka;" +
		"uniform vec3 u_Kd;" +
		"uniform vec3 u_Ks;" +
		"uniform float u_material_shininess;" +
	
		"out vec4 FragColor;" +
	
		"void main(void)" +
		"{" +
		"FragColor = vec4(1.0, 1.0, 1.0, 1.0);" +
		"if(u_light_enabled == 1)" +
		"{" +
		
			"vec3 ambient  = u_La * u_Ka;" +
			"vec3 normalized_transformed_normals = normalize(Transformed_normals);" +
			"vec3 normalized_light_direction = normalize(light_direction0);" +
			"vec3 normalized_viewer_vector = normalize(Viewer_vector);" +
			"float tn_dot_ld = max(dot(normalized_transformed_normals, normalized_light_direction), 0.0);" +
			"vec3 diffuse = u_Ld * u_Kd * tn_dot_ld;" +
			"vec3 reflection_vector = reflect(-normalized_light_direction, normalized_transformed_normals);" +
			"float r_dot_v = max(dot(reflection_vector, normalized_viewer_vector), 0.0);" +
			"vec3 specular = u_Ls * u_Ks * pow(r_dot_v, u_material_shininess);" +
			"vec3 phong_ads_color = ambient + diffuse + specular;" +

			"ambient  = u_La_1 * u_Ka;" +
			"normalized_light_direction = normalize(light_direction1);" +
			"tn_dot_ld = max(dot(normalized_transformed_normals, normalized_light_direction), 0.0);" +
			"diffuse = u_Ld_1 * u_Kd * tn_dot_ld;" +
			"reflection_vector = reflect(-normalized_light_direction, normalized_transformed_normals);" +
			"r_dot_v = max(dot(reflection_vector, normalized_viewer_vector), 0.0);" +
			"specular = u_Ls_1 * u_Ks * pow(r_dot_v, u_material_shininess);" +
			"vec3 phong_ads_color2 =  ambient + diffuse + specular;" +
		
			"phong_ads_color=phong_ads_color+phong_ads_color2;"+
			"FragColor = vec4(phong_ads_color, 1.0);" +
		"}" +
		"}"
	);
//silly mistake...use same variable in fragcolor..phong_ads_color...otherwise NO OUTPUT..
//shader output in other variable n using diff variable..casing blank output...
//change in second light compnent only
	GLES32.glShaderSource(fragmentShaderObject, fragmentShaderSourceCode);

	GLES32.glCompileShader(fragmentShaderObject);

	iShaderCompileStatus[0] = 0;

	GLES32.glGetShaderiv(fragmentShaderObject, GLES32.GL_COMPILE_STATUS, iShaderCompileStatus, 0);
	if (iShaderCompileStatus[0] == GLES32.GL_FALSE)
	{
		int[] iInfoLogLength = new int[1];
		GLES32.glGetShaderiv(fragmentShaderObject, GLES32.GL_INFO_LOG_LENGTH, iInfoLogLength, 0);
		if (iInfoLogLength[0] > 0)
		{	String szInfoLog = GLES32.glGetShaderInfoLog(fragmentShaderObject);
			System.out.println("dpk: Fragment Shader Compile Log = " + szInfoLog);
			uninitialize();
			System.exit(0);
		}
	}

	/*********SHADER PROGRAM***********/
	shaderProgramObject = GLES32.glCreateProgram();

	GLES32.glAttachShader(shaderProgramObject, vertexShaderObject);

	GLES32.glAttachShader(shaderProgramObject, fragmentShaderObject);

	GLES32.glBindAttribLocation(shaderProgramObject, GLESMacros.dpk_ATTRIBUTE_VERTEX, "vPosition");
	GLES32.glBindAttribLocation(shaderProgramObject, GLESMacros.dpk_ATTRIBUTE_NORMAL, "vNormal");

	GLES32.glLinkProgram(shaderProgramObject);

	int[] iShaderLinkStatus = new int[1];
	GLES32.glGetProgramiv(shaderProgramObject, GLES32.GL_LINK_STATUS, iShaderLinkStatus, 0);
	if (iShaderLinkStatus[0] == GLES32.GL_FALSE)
	{
		int[] iInfoLogLength = new int[1];
		GLES32.glGetProgramiv(shaderProgramObject, GLES32.GL_INFO_LOG_LENGTH, iInfoLogLength, 0);
		if (iInfoLogLength[0] > 0)
		{	String szInfoLog = GLES32.glGetProgramInfoLog(shaderProgramObject);
			System.out.println("dpk: Shader Program Link Log = " + szInfoLog);
			uninitialize();
			System.exit(0);
		}
	}

//uniform location
	model_Uniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_model_matrix");
	view_Uniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_view_matrix");
	projection_Uniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_projection_matrix");
	
	light_enabled_Uniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_light_enabled");
	//light 0
	La_lightAmbient_Uniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_La");
	Ld_lightDiffuse_Uniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_Ld");
	Ls_lightSpecular_Uniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_Ls");
	light_position_Uniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_light_position");
	
	//light 1
	La_lightAmbient_Uniform1 = GLES32.glGetUniformLocation(shaderProgramObject, "u_La_1");
	Ld_lightDiffuse_Uniform1 = GLES32.glGetUniformLocation(shaderProgramObject, "u_Ld_1");
	Ls_lightSpecular_Uniform1 = GLES32.glGetUniformLocation(shaderProgramObject, "u_Ls_1");
light_position_Uniform1 = GLES32.glGetUniformLocation(shaderProgramObject, "u_light_position1");
	
	
	Ka_Uniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_Ka");
	Kd_Uniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_Kd");
	Ks_Uniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_Ks");
	material_shininess_Uniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_material_shininess");

//pyramid details
	final float pyramidVertices[] = new float[]
	{
0.0f, 1.0f, 0.0f,	-1.0f, -1.0f, 1.0f,	1.0f, -1.0f, 1.0f,	
0.0f, 1.0f, 0.0f,	1.0f, -1.0f, 1.0f,	1.0f, -1.0f, -1.0f,
0.0f, 1.0f, 0.0f,	1.0f, -1.0f, -1.0f,	-1.0f, -1.0f, -1.0f,
0.0f, 1.0f, 0.0f,	-1.0f, -1.0f, -1.0f,	-1.0f, -1.0f, 1.0f,
	};

	final float pyramidNormal[] = new float[]
	{
		0.0f, 0.447214f, 0.894427f,	0.0f, 0.447214f, 0.894427f,0.0f, 0.447214f, 0.894427f,	
		0.894427f, 0.447214f, 0.0f,	0.894427f, 0.447214f, 0.0f,0.894427f, 0.447214f, 0.0f,	
		0.0f, 0.447214f, -0.894427f,0.0f, 0.447214f, -0.894427f,0.0f, 0.447214f, -0.894427f,
		-0.894427f, 0.447214f, 0.0f,-0.894427f, 0.447214f, 0.0f,-0.894427f, 0.447214f, 0.0f,
	};

	//Generate vao: static void glGenVertexArrays(int n, int[] arrays, int offset);
	GLES32.glGenVertexArrays(1, vao_pyramid, 0);
	//Bind vao : static void BindVertexArray(int array);
	GLES32.glBindVertexArray(vao_pyramid[0]);

	//VBO..pyramid
	{
		GLES32.glGenBuffers(1, vbo_pyramid_position, 0);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vbo_pyramid_position[0]);

		ByteBuffer byteBuffer = ByteBuffer.allocateDirect(pyramidVertices.length * 4);

		byteBuffer.order(ByteOrder.nativeOrder());

		FloatBuffer verticesBuffer = byteBuffer.asFloatBuffer();

		verticesBuffer.put(pyramidVertices);
		verticesBuffer.position(0);

		GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER,
			pyramidVertices.length * 4,
			verticesBuffer,
			GLES32.GL_STATIC_DRAW);

		
		GLES32.glVertexAttribPointer(GLESMacros.dpk_ATTRIBUTE_VERTEX,3,	GLES32.GL_FLOAT,false,0, 0);

		GLES32.glEnableVertexAttribArray(GLESMacros.dpk_ATTRIBUTE_VERTEX);

		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0);
	}

	//VBO
	{
		GLES32.glGenBuffers(1, vbo_pyramid_normal, 0);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vbo_pyramid_normal[0]);

		//To provide data use ByteBuffer
		ByteBuffer byteBuffer = ByteBuffer.allocateDirect(pyramidNormal.length * 4);
		byteBuffer.order(ByteOrder.nativeOrder());
		FloatBuffer colorBuffer = byteBuffer.asFloatBuffer();
		colorBuffer.put(pyramidNormal);
		colorBuffer.position(0);

		GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER, (pyramidNormal.length * 4), colorBuffer, GLES32.GL_STATIC_DRAW);
		GLES32.glVertexAttribPointer(GLESMacros.dpk_ATTRIBUTE_NORMAL,
			3, 	//x,y,z
			GLES32.GL_FLOAT,
			false,	
			0, 0);	
		GLES32.glEnableVertexAttribArray(GLESMacros.dpk_ATTRIBUTE_NORMAL);
		//Unbind vbo_pyramid_normal
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0);
	}

	//Unbind vao: static void glBindVertexArray(int array);
	GLES32.glBindVertexArray(0);

	//unbind vao_quad
	GLES32.glBindVertexArray(0);

	//Enable depth test: static void glEnable(int cap);	cap=capability
	GLES32.glEnable(GLES32.GL_DEPTH_TEST);
	//Select depth test: static void glDepthFunc(int func);
	GLES32.glDepthFunc(GLES32.GL_LEQUAL);
	GLES32.glClearColor(0.0f,0.0f,0.0f,1.0f);	//black

	doubleTap = 0;

	Matrix.setIdentityM(perspectiveProjectionMatrix, 0);
	System.out.println("dpk: initialize() exit");
}

private void resize(int width, int height)
{
	GLES32.glViewport(0,0,width,height);

	Matrix.perspectiveM(perspectiveProjectionMatrix, 0,	45.0f,((float)width / (float)height),0.1f, 1000.0f);					  
}

private void spin()
{
	anglePyramid = anglePyramid + 1.0f;
	if (anglePyramid>360.0f)
		anglePyramid = 0.0f;
}

public void display()
{
	//code
	GLES32.glClear(GLES32.GL_COLOR_BUFFER_BIT | GLES32.GL_DEPTH_BUFFER_BIT);

	GLES32.glUseProgram(shaderProgramObject);

	if (doubleTap == 1)
	{
		GLES32.glUniform1i(light_enabled_Uniform, 1);
		GLES32.glUniform4fv(light_position_Uniform, 1, light_position, 0);
		GLES32.glUniform3fv(La_lightAmbient_Uniform, 1, light_ambient, 0);
		GLES32.glUniform3fv(Ld_lightDiffuse_Uniform, 1, light_diffuse, 0);
		GLES32.glUniform3fv(Ls_lightSpecular_Uniform, 1, light_specular, 0);

		GLES32.glUniform4fv(light_position_Uniform1, 1, light_position1, 0);
		GLES32.glUniform3fv(La_lightAmbient_Uniform1, 1, light_ambient1, 0);
		GLES32.glUniform3fv(Ld_lightDiffuse_Uniform1, 1, light_diffuse1, 0);
		GLES32.glUniform3fv(Ls_lightSpecular_Uniform1, 1, light_specular1, 0);

		GLES32.glUniform3fv(Ka_Uniform, 1, material_ambient, 0);
		GLES32.glUniform3fv(Kd_Uniform, 1, material_diffuse, 0);
		GLES32.glUniform3fv(Ks_Uniform, 1, material_specular, 0);
		GLES32.glUniform1f(material_shininess_Uniform, material_shininess);
	}
	else
	{
		GLES32.glUniform1i(light_enabled_Uniform, 0);
	}
	//OpenGL-ES drawing
	float modelMatrix[] = new float[16];	
	float viewMatrix[] = new float[16];	
	float rotationMatrix[] = new float[16];

	Matrix.setIdentityM(modelMatrix, 0);
	Matrix.setIdentityM(viewMatrix, 0);

	Matrix.translateM(modelMatrix, 0, 0.0f, 0.0f, -6.0f);

	Matrix.rotateM(modelMatrix,0, anglePyramid, 0.0f, 1.0f, 0.0f);

	GLES32.glUniformMatrix4fv(model_Uniform, 1, false, modelMatrix, 0);
	GLES32.glUniformMatrix4fv(view_Uniform, 1, false, viewMatrix, 0);
	GLES32.glUniformMatrix4fv(projection_Uniform, 1, false, perspectiveProjectionMatrix, 0);

	//Bind vao
	GLES32.glBindVertexArray(vao_pyramid[0]);

	// draw, either by glDrawTriangles() or glDrawArrays() or glDrawElements()
	GLES32.glDrawArrays(GLES32.GL_TRIANGLES, 0, 12); // 4*3 (each with its x,y,z ) vertices in triangleVertices array

													 //Unbind vao
	GLES32.glBindVertexArray(0);

	GLES32.glUseProgram(0);

	//spin call
	this.spin();

	requestRender();
}

private void uninitialize()
{
	if (vao_pyramid[0] != 0)
	{
		GLES32.glDeleteVertexArrays(1, vao_pyramid, 0);
		vao_pyramid[0] = 0;
	}
	if (vbo_pyramid_position[0] != 0)
	{
		GLES32.glDeleteBuffers(1, vbo_pyramid_position, 0);
		vbo_pyramid_position[0] = 0;
	}
	if (vbo_pyramid_normal[0] != 0)
	{
		GLES32.glDeleteBuffers(1, vbo_pyramid_normal, 0);
		vbo_pyramid_normal[0] = 0;
	}

	if (shaderProgramObject != 0)
	{
		if (vertexShaderObject != 0)
		{
			GLES32.glDetachShader(shaderProgramObject, vertexShaderObject);
			GLES32.glDeleteShader(vertexShaderObject);
			vertexShaderObject = 0;
		}

		if (fragmentShaderObject != 0)
		{
			GLES32.glDetachShader(shaderProgramObject, fragmentShaderObject);
			GLES32.glDeleteShader(fragmentShaderObject);
			fragmentShaderObject = 0;
		}

		GLES32.glDeleteProgram(shaderProgramObject);
		shaderProgramObject = 0;
	}
}
}



