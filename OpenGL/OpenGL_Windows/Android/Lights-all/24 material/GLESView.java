// 24 Material-3 lights sphere
//new learnings: floatBuffer
//hurdles:2D Array in Java/android..beacuse of this took much time again to understand how it works..
//import java.util.Arrays; //for material array..but didnt used 2d array like windows..need to check hw to use 2d array in Java
//hence tried with FloatBuffer..worked..
 
package com.astromedicomp.myapplication24Material;

import android.content.Context;	//for drawing context related
import android.opengl.GLSurfaceView; //for OpenGL Surface View and related
import javax.microedition.khronos.opengles.GL10;	//for OpenGLES 1.0 needed as param type GL10
import javax.microedition.khronos.egl.EGLConfig; 	//for EGLConfig needed as param type EGLConfig
import android.opengl.GLES32; 	//for OpenGL 3.0, 3.1=GLES31, 3.2=GLES32
import android.view.MotionEvent; // for MotionEvent class
import android.view.GestureDetector; //for GestureDetector class
import android.view.GestureDetector.OnGestureListener; //for OnGestureListener class
import android.view.GestureDetector.OnDoubleTapListener; //for OnDoubleTapListener calls

// for vbo
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer; // new one....similar to bytebuffer...
							//it supports 4 type of oprations..get-put for float data..bulk get n put..its either directional/non dirctctionl
import java.nio.ShortBuffer; //another option available

//import java.util.Arrays; //for material array..but didnt used 2d array like windows..need to check hw to use 2d array in Java


import android.opengl.Matrix;	
import java.lang.Math;			// for sin cos - math

public class GLESView extends GLSurfaceView implements GLSurfaceView.Renderer,OnGestureListener, OnDoubleTapListener
{
	private final Context context;
	private GestureDetector gestureDetector;
	
	private int vertexShaderObject;
	private int fragmentShaderObject;
	private int shaderProgramObject;
	
	private int modelUniform;		
	private int viewUniform;		
	private int projectionUniform;	
	
	private int LKeyPressedUniform;		
	
	private int lightPositionUniform;	
	private int Light_ambient_Uniform;			
	private int Light_diffuse_Uniform;			
		private int Light_specular_Uniform;		
	
	//material uniform
	private int KaUniform;			
		private int KdUniform;			
	private int KsUniform;			
	private int materialShininessUniform;  
	
	private float[] perspectiveProjectionMatrix = new float[16];	//4x4 matrix
	
	private int[] vao_sphere = new int[1];
    private int[] vbo_sphere_position = new int[1];
    private int[] vbo_sphere_normal = new int[1];
    private int[] vbo_sphere_element = new int[1];
	private int numVertices = 0;
	private int numElements = 0;
	
	private float lightPosition[] = new float[] { 0.0f,0.0f,0.0f,1.0f };
	private float lightAmbient[] = new float[]  { 0.0f,0.0f,0.0f,1.0f };
	private float lightDiffuse[] = new float[]  { 1.0f,1.0f,1.0f,1.0f };
	private float lightSpecular[] = new float[]	{ 1.0f,1.0f,1.0f,1.0f };
	
	private float materialAmbient[] = new float[]{ 0.0f, 0.0f,0.0f,1.0f };
	private float materialDiffuse[] = new float[]{ 1.0f,1.0f,1.0f,1.0f };
	private float materialSpecular[] = new float[]{ 1.0f, 1.0f,1.0f,1.0f };
	private float materialShininess = 50.0f;
	
	FloatBuffer floatBuffer_for_material_ambient;
	FloatBuffer floatBuffer_for_material_diffuse;
	FloatBuffer floatBuffer_for_material_specular;
	FloatBuffer floatBuffer_for_material_shine;
	
	
	private float gAngleForSphere = 0.0f;	
	private float pi = 3.141414f;			
	
	
	private int singleTap=0;		
	private int doubleTap=0;		


//material=====================
		public static final float mat_ambient[] = 
		{
		//lot of confusion here...zzz..2D array in Java..so made it single array wise
	 mbient material
	0.0215f,  
	0.1745f,  
	0.0215f,  
	1.0f,    

	0.135f,		 
	0.2225f,	 
	0.1575f,	 
	1.0f, 

	0.05375f,  
	0.05f,     
	0.06625f,  
	1.0f ,    

	0.25f,     
	0.20725f,  
	0.20725f,  
	1.0f ,    

	0.1745f,   
	0.01175f,  
	0.01175f,  
	1.0f ,    
	//};

	0.1f,      
	0.18725f,  
	0.1745f,   
	1.0f ,    

	0.329412f,  
	0.223529f,  
	0.027451f,  
	1.0f  ,    

	0.2125f,  
	0.1275f,  
	0.054f,   
	1.0f,    

	0.25f,  
	0.25f,  
	0.25f,  
	1.0f,  

	0.19125f,  
	0.0735f,   
	0.0225f,   
	1.0f ,    

	0.24725f,  
	0.1995f,   
	0.0745f,   
	1.0f ,    

	0.19225f,  
	0.19225f,  
	0.19225f,  
	1.0f ,    

	0.0f,   
	0.0f,   
	0.0f,   
	1.0f,  

	0.0f,   
	0.1f,   
	0.06f,  
	1.0f,  

	0.0f,   
	0.0f,   
	0.0f,   
	1.0f,  

	0.0f,   
	0.0f,   
	0.0f,   
	1.0f,  

	0.0f,   
	0.0f,   
	0.0f,   
	1.0f,  

	0.0f,   
	0.0f,   
	0.0f,   
	1.0f,  

	0.02f,  
	0.02f,  
	0.02f,  
	1.0f,  
	//};

	0.0f,   
	0.05f,  
	0.05f,  
	1.0f,  

	0.0f,   
	0.05f,  
	0.0f,   
	1.0f,  

	0.05f,  
	0.0f,   
	0.0f,   
	1.0f,  


	0.05f,  
	0.05f,  
	0.05f,  
	1.0f,  
	
	0.05f,  
	0.05f,  
	0.0f,   
	1.0f,  
	
};

public static final float mat_diffuse[] = {
	// difuse material
	0.07568f,  
	0.61424f,  
	0.07568f,  
	1.0f ,    
	
	0.54f,	 
	0.89f,	 
	0.63f,	 
	1.0f,	 
	
	0.18275f,  
	0.17f,     
	0.22525f,  
	1.0f ,    
	
	1.0f,    
	0.829f,  
	0.829f,  
	1.0f,   
	
	0.61424f,  
	0.04136f,  
 	0.04136f,  
 	1.0f ,    
 	

 	0.396f,    
 	0.74151f,  
 	0.69102f,  
 	1.0f ,    
 	
 	0.780392f,  
 	0.568627f,  
 	0.113725f,  
 	1.0f ,    
 	//};
 
 	0.714f,    
 	0.4284f,   
 	0.18144f,  
 	1.0f ,    
 	
	0.4f,   
 	0.4f,   
 	0.4f,   
 	1.0f,  
 	
	0.7038f,   
 	0.27048f,  
 	0.0828f,   
 	1.0f ,    
 	
	0.75164f,  
 	0.60648f,  
 	0.22648f,  
 	1.0f ,    
 	
	0.50754f,  
 	0.50754f,  
 	0.50754f,  
 	1.0f ,    
 	
	0.01f,  
 	0.01f,  
 	0.01f,  
 	1.0f,  
 	
	0.0f,         
 	0.50980392f,  
 	0.50980392f,  
 	1.0f    ,    
 	
	0.1f,   
 	0.35f,  
 	0.1f,   
 	1.0f,  
 	
	0.5f,   
 	0.0f,   
 	0.0f,   
 	1.0f,  
 	
	0.55f,  
 	0.55f,  
 	0.55f,  
 	1.0f,  
 	
	0.5f,   
 	0.5f,   
 	0.0f,   
 	1.0f,  
 	
	0.01f,  
 	0.01f,  
 	0.01f,  
 	1.0f,  
 	
	0.4f,   
 	0.5f,   
 	0.5f,   
 	1.0f,  
 	
	0.4f,   
 	0.5f,   
 	0.4f,   
 	1.0f,  
 	
	0.5f,   
 	0.4f,   
 	0.4f,   
 	1.0f,  
 	
	0.5f,   
 	0.5f,   
 	0.5f,   
 	1.0f,  
 	
	0.5f,   
 	0.5f,   
 	0.4f,   
 	1.0f,  
 	
 };
 
 public static final float mat_specular[] = {
 	0.633f,     
 	0.727811f,  
 	0.633f,     
 	1.0f  ,    
 	
	0.316228f,  
 	0.316228f,  
 	0.316228f,  
 	1.0f  ,    
 	//};
 
 	0.332741f,  
 	0.328634f,  
 	0.346435f,  
 	1.0f  ,    
 	
	0.296648f,  
 	0.296648f,  
 	0.296648f,  
 	1.0f  ,    
 	
	0.727811f,  
 	0.626959f,  
 	0.626959f,  
 	1.0f  ,    
 	
	0.297254f,  
 	0.30829f,   
 	0.306678f,  
 	1.0f  ,    
 	
	0.992157f,  
 	0.941176f,  
 	0.807843f,  
 	1.0f  ,    
 	
	0.393548f,  
 	0.271906f,  
 	0.166721f,  
 	1.0f  ,    
 	
	0.774597f,  
 	0.774597f,  
 	0.774597f,  
 	1.0f  ,    
 	
	0.256777f,  
 	0.137622f,  
 	0.086014f,  
 	1.0f  ,    
 	
	0.628281f,  
 	0.555802f,  
 	0.366065f,  
 	1.0f  ,    
 	//};
 	
 	0.508273f,  
 	0.508273f,  
 	0.508273f,  
 	1.0f  ,    
 	
	0.50f,  
 	0.50f,  
 	0.50f,  
 	1.0f,  
 	
	0.50196078f,  
 	0.50196078f,  
 	0.50196078f,  
 	1.0f    ,    
 	
	0.45f,  
 	0.55f,  
 	0.45f,  
 	1.0f,  
 	
	0.7f,   
 	0.6f,   
 	0.6f,   
 	1.0f,  
 	
	0.70f,  
 	0.70f,  
 	0.70f,  
 	1.0f,  
 	
	0.60f,  
 	0.60f,  
 	0.50f,  
 	1.0f,  
 	
	0.4f,   
 	0.4f,   
 	0.4f,   
 	1.0f,  
 	//};
 	
 	0.04f,  
 	0.7f,   
 	0.7f,   
 	1.0f,  
 	//};
 	0.04f,  
 	0.7f,   
 	0.04f,  
 	1.0f,  
 	
	0.7f,   
 	0.04f,  
 	0.04f,  
 	1.0f,  
 	
	0.7f,   
 	0.7f,   
 	0.7f,   
 	1.0f,  
 	//};
 	
 	0.7f,   
 	0.7f,   
 	0.04f,  
 	1.0f,  
 	
 };
 
 public static final float mat_shininess [] = 
 {
  0.6f * 128,
  0.1f * 128,
  0.3f * 128,
  0.088f * 128,
  0.6f * 128,
  0.1f * 128,
  0.21794872f * 128,
  0.2f * 128,
  0.6f * 128,
  0.1f * 128,
  0.4f * 128,
  0.4f * 128,
  0.25f * 128,

  0.25f * 128,
  0.25f * 128,
  0.25f * 128,
  0.25f * 128,
  0.25f * 128,
  0.078125f * 128,
  0.078125f * 128,
  0.078125f * 128,
  0.078125f * 128,
  0.078125f * 128,
  0.078125f * 128,

};

//==========================
		
//GLESView constructor
	public GLESView (Context drawingContext)
	{
		super(drawingContext);
		context = drawingContext;
		
		setEGLContextClientVersion(3); 
		
		setRenderer(this);	
		
		gestureDetector = new GestureDetector(context,this,null,false); 
		gestureDetector.setOnDoubleTapListener(this); 
	}
	
//GLSurfaceView.Renderer abstract methods
	@Override 
	public void onSurfaceCreated(GL10 gl, EGLConfig config)
	{
		// OpenGL-ES version check
		String glesVersion = gl.glGetString(GL10.GL_VERSION);
		System.out.println("dpk: OpenGL-ES Version = "+glesVersion);
		
		 et GLSL version
		String glslVersion = gl.glGetString(GLES32.GL_SHADING_LANGUAGE_VERSION);
		System.out.println("dpk: GLSL Version = "+glslVersion);
		this.initialize(gl);
	}
	
	@Override
	public void onSurfaceChanged(GL10 unused, int width, int height)
	{
		this.resize(width,height);
	}
	
	@Override
	public void onDrawFrame(GL10 unused)
	{
					display();
					update();

	}


//Handling 'onTouchEvent' is MOST important
//because it triggers all gesture and tap events
	@Override
	public boolean onTouchEvent (MotionEvent e)
	{
		int eventaction = e.getAction();
		if(!gestureDetector.onTouchEvent(e))
		{
			super.onTouchEvent(e);
		}
		return (true);
	}
	
// OnDoubleTapListener: 3 abstract methods, so MUST be implemented
	@Override
	public boolean onDoubleTap(MotionEvent e)
	{
		doubleTap = 1;
		return (true);
	}
	
	@Override 
	public boolean onDoubleTapEvent(MotionEvent e)
	{
		// do not write any code here because already written 'onDoubleTap'
		return (true);
	}
	
	@Override
	public boolean onSingleTapConfirmed(MotionEvent e)
	{
		singleTap++;
		if(singleTap > 3)
			singleTap = 0;
		return (true);
	}
	
// OnGestureListener : abstract methods, so MUST be implemented
	@Override
	public boolean onDown(MotionEvent e)
	{
		// do not write any code here because already written 'onSingleTapConfirmed'
		return (true);
	}
	
	@Override 
	public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY)
	{
		return (true);
	}
	
	@Override
	public void onLongPress(MotionEvent e)
	{
	}
	
	@Override
	public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY)
	{
		System.out.println("dpk:Application Exit");
		uninitialize();
		System.exit(0);	// exit the application
		return (true);
	}
	
	@Override
	public void onShowPress(MotionEvent e)
	{
	}
	
	@Override 
	public boolean onSingleTapUp(MotionEvent e)
	{
		//handled onSingleTapConfirmed, so no need to handle this
		return (true);
	}
	
	private void initialize(GL10 gl)	 L10 means GL1.0 class
	{
		/*********VERTEX SHADER***********/
		vertexShaderObject = GLES32.glCreateShader(GLES32.GL_VERTEX_SHADER);
	
		final String vertexShaderSourceCode = String.format
		(
		 "#version 320 es"+
		 "\n"+
		 "in vec4 vPosition;"+
		 "in vec3 vNormal;"+
		 "uniform mat4 u_model_matrix;"+
		 "uniform mat4 u_view_matrix;"+
		 "uniform mat4 u_projection_matrix;"+
		 "uniform mediump int u_LKeyPressed;"+
		 "uniform vec4 u_light_position;"+
		 "out vec3 phong_ads_color;"+
		 "out vec3 oTransformed_normals;"+
		 "out vec3 oLight_direction;"+
		 "out vec3 oViewer_vector;"+
		 "void main (void)"+
		 "{"+
			"if(u_LKeyPressed == 1)"+
			"{"+
				"vec4 eyeCoordinates = u_view_matrix * u_model_matrix * vPosition;"+
				"oTransformed_normals = mat3(u_view_matrix * u_model_matrix) * vNormal;"+
				"oLight_direction = vec3(u_light_position) - eyeCoordinates.xyz;"+
				"oViewer_vector = -eyeCoordinates.xyz;"+
			"}"+
			"else"+
			"{"+
				"phong_ads_color = vec3(1.0, 1.0, 1.0);"+
			"}"+
			"gl_Position = u_projection_matrix * u_view_matrix * u_model_matrix * vPosition;"+
		 "}"
		);
		
		GLES32.glShaderSource(vertexShaderObject, vertexShaderSourceCode);
		
		GLES32.glCompileShader(vertexShaderObject);
		
		int [] iShaderCompileStatus = new int[1];	//on way to declare array
		
		GLES32.glGetShaderiv(vertexShaderObject, GLES32.GL_COMPILE_STATUS, iShaderCompileStatus, 0);	
		if(iShaderCompileStatus[0] == GLES32.GL_FALSE)
		{// error in compiling vertex shader
			int iInfoLogLength[] = new int[1];		//other way to declare array
			GLES32.glGetShaderiv(vertexShaderObject, GLES32.GL_INFO_LOG_LENGTH, iInfoLogLength, 0);
			if(iInfoLogLength[0] > 0)
			{
				String szInfoLog = GLES32.glGetShaderInfoLog(vertexShaderObject);
				System.out.println("dpk: Vertex Shader Compilation Log = "+szInfoLog);
				uninitialize();
				System.exit(0);
			}
		}
		
		/*********FRAGMENT SHADER***********/
		fragmentShaderObject = GLES32.glCreateShader(GLES32.GL_FRAGMENT_SHADER);
		
		final String fragmentShaderSourceCode = String.format
		(
		 "#version 320 es"+
		 "\n"+
		 "precision highp float;"+
		 "in vec3 oTransformed_normals;"+
		 "in vec3 oLight_direction;"+
		 "in vec3 oViewer_vector;"+
		 "in vec3 phong_ads_color;"+
		 "uniform int u_LKeyPressed;"+
		 "uniform vec3 u_La;"+
		 "uniform vec3 u_Ld;"+
		 "uniform vec3 u_Ls;"+
		 "uniform vec3 u_Ka;"+
		 "uniform vec3 u_Kd;"+
		 "uniform vec3 u_Ks;"+
		 "uniform float u_material_shininess;"+
		 "out vec4 FragColor;"+
		 "void main(void)"+
		 "{"+
			"if(u_LKeyPressed == 1)"+
			"{"+
				"vec3 normalized_transformed_normals = normalize(oTransformed_normals);"+
				"vec3 normalized_viewer_vector = normalize(oViewer_vector);"+
				"vec3 normalized_light_direction = normalize(oLight_direction);"+
				"float tn_dot_ld = max(dot(normalized_transformed_normals, normalized_light_direction), 0.0);"+
				"vec3 ambient = u_La * u_Ka;"+
				"vec3 diffuse = u_Ld * u_Kd * tn_dot_ld;"+
				"vec3 reflection_vector = reflect(-normalized_light_direction, normalized_transformed_normals);"+
				"float r_dot_v = max(dot(reflection_vector, normalized_viewer_vector), 0.0);"+
				"vec3 specular = u_Ls * u_Ks * pow(r_dot_v, u_material_shininess);"+
				"vec3 tmp_phong_ads_color = ambient + diffuse + specular;"+
				"FragColor = vec4(tmp_phong_ads_color, 1.0);"+
			"}"+
			"else"+
			"{"+
				"FragColor = vec4(phong_ads_color, 1.0f);"+
			"}"+
		 "}"
		);
		
		GLES32.glShaderSource(fragmentShaderObject, fragmentShaderSourceCode);
				
		GLES32.glCompileShader(fragmentShaderObject);
		
		iShaderCompileStatus[0] = 0;	
		
		GLES32.glGetShaderiv(fragmentShaderObject, GLES32.GL_COMPILE_STATUS, iShaderCompileStatus, 0);
		if(iShaderCompileStatus[0] == GLES32.GL_FALSE)
		{
			int [] iInfoLogLength = new int [1];
			GLES32.glGetShaderiv(fragmentShaderObject, GLES32.GL_INFO_LOG_LENGTH, iInfoLogLength, 0);
			if(iInfoLogLength[0] > 0)
			{//if info string present
				/*static String glGetShaderInfoLog(int shader)*/
				String szInfoLog = GLES32.glGetShaderInfoLog(fragmentShaderObject);
				System.out.println("dpk: Fragment Shader Compile Log = "+szInfoLog);
				uninitialize();
				System.exit(0);
			}
		}
		
		/*********SHADER PROGRAM***********/
		shaderProgramObject = GLES32.glCreateProgram();
		GLES32.glAttachShader(shaderProgramObject, vertexShaderObject);
		GLES32.glAttachShader(shaderProgramObject, fragmentShaderObject);
		GLES32.glBindAttribLocation(shaderProgramObject, GLESMacros.dpk_ATTRIBUTE_VERTEX, "vPosition");
		GLES32.glBindAttribLocation(shaderProgramObject, GLESMacros.dpk_ATTRIBUTE_NORMAL, "vNormal");
		
		GLES32.glLinkProgram(shaderProgramObject);
		
		int[] iShaderLinkStatus = new int[1];
		GLES32.glGetProgramiv(shaderProgramObject, GLES32.GL_LINK_STATUS, iShaderLinkStatus, 0);
		if(iShaderLinkStatus[0] == GLES32.GL_FALSE)
		{
			int[] iInfoLogLength = new int[1];
			GLES32.glGetProgramiv(shaderProgramObject, GLES32.GL_INFO_LOG_LENGTH, iInfoLogLength, 0);
			if(iInfoLogLength[0] > 0)
			{//if log message
				String szInfoLog = GLES32.glGetProgramInfoLog(shaderProgramObject);
				System.out.println("dpk: Shader Program Link Log = "+szInfoLog);
				uninitialize();
				System.exit(0);
			}
		}
		
		//Get MVP uniform location: static int glGetUniformLocation(int program, String name);
		modelUniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_model_matrix");
		viewUniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_view_matrix");
		projectionUniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_projection_matrix");
		
		lightPositionUniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_light_position");
		Light_ambient_Uniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_La");
		Light_diffuse_Uniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_Ld");
		Light_specular_Uniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_Ls");
				
		KaUniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_Ka");
		KdUniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_Kd");
		KsUniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_Ks");
		materialShininessUniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_material_shininess");
		
		LKeyPressedUniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_LKeyPressed");
		
		/***********Vertices, Color, shader attribs, vao, vbo initialization ****************/	
		//Sphere 
		Sphere sphere = new Sphere();
		float sphere_vertices[]=new float[1146];
        float sphere_normals[]=new float[1146];
        float sphere_textures[]=new float[764];
        short sphere_elements[]=new short[2280];
        sphere.getSphereVertexData(sphere_vertices, sphere_normals, sphere_textures, sphere_elements);
        numVertices = sphere.getNumberOfSphereVertices();
        numElements = sphere.getNumberOfSphereElements();
		

		//Vao_qaud
		GLES32.glGenVertexArrays(1, vao_sphere, 0);
		GLES32.glBindVertexArray(vao_sphere[0]);
		
		//vbo_sphere_position
		{
			GLES32.glGenBuffers(1, vbo_sphere_position, 0);
			GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vbo_sphere_position[0]);
			
			//To provide data, do some different arrangement 
			//01-Create byte buffer
			ByteBuffer byteBuffer = ByteBuffer.allocateDirect(sphere_vertices.length*4);
			//02-Tell java which byte order you want
			byteBuffer.order(ByteOrder.nativeOrder());
			//03-Convert byte buffer to float buffer since our data is of float type
			FloatBuffer verticesBuffer = byteBuffer.asFloatBuffer();
			verticesBuffer.put(sphere_vertices);
			verticesBuffer.position(0);
			
			//Provide data to vbo
			GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER, (sphere_vertices.length*4), verticesBuffer, GLES32.GL_STATIC_DRAW);
			GLES32.glVertexAttribPointer(GLESMacros.dpk_ATTRIBUTE_VERTEX, 
										 3, // x,y,z
										 GLES32.GL_FLOAT,
										 false,	//non-normalize
										 0, 0);	//no stride
										 
			GLES32.glEnableVertexAttribArray(GLESMacros.dpk_ATTRIBUTE_VERTEX);
			
			//unbind vbo
			GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0);	
		}
		
		//VBO_SPHERE: FOR NORMAL
		{
			GLES32.glGenBuffers(1, vbo_sphere_normal, 0);
			GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vbo_sphere_normal[0]);
			//prepare data for vbo_sphere_normal
			ByteBuffer byteBuffer = ByteBuffer.allocateDirect(sphere_normals.length*4);
			byteBuffer.order(ByteOrder.nativeOrder());
			FloatBuffer normalBuffer = byteBuffer.asFloatBuffer();
			normalBuffer.put(sphere_normals);
			normalBuffer.position(0);
			
			GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER, (sphere_normals.length*4), normalBuffer, GLES32.GL_STATIC_DRAW);
			GLES32.glVertexAttribPointer(GLESMacros.dpk_ATTRIBUTE_NORMAL, 
										 3, 	//x,y,z
										 GLES32.GL_FLOAT,	//above data is of float type
										 false, //non-normalize
										 0,0);	//no stride
			GLES32.glEnableVertexAttribArray(GLESMacros.dpk_ATTRIBUTE_NORMAL);
			
			//Unbind vbo for normal
			GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0);
		}
		
		//Vbo : element
		{
			GLES32.glGenBuffers(1, vbo_sphere_element, 0);
			GLES32.glBindBuffer(GLES32.GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_element[0]);
			
			ByteBuffer byteBuffer = ByteBuffer.allocateDirect(sphere_elements.length*2);
			byteBuffer.order(ByteOrder.nativeOrder());
			ShortBuffer elementBuffer = byteBuffer.asShortBuffer();
			elementBuffer.put(sphere_elements);
			elementBuffer.position(0);
			GLES32.glBufferData(GLES32.GL_ELEMENT_ARRAY_BUFFER, 
								sphere_elements.length*2,
								elementBuffer,
								GLES32.GL_STATIC_DRAW);
			
			GLES32.glBindBuffer(GLES32.GL_ELEMENT_ARRAY_BUFFER, 0);
		}
		//unbind vao_quad
		GLES32.glBindVertexArray(0);
	
		//Enable depth test: static void glEnable(int cap);	cap=capability
		GLES32.glEnable(GLES32.GL_DEPTH_TEST);
		//Select depth test: static void glDepthFunc(int func);
		GLES32.glDepthFunc(GLES32.GL_LEQUAL);
		//We will always cull the back faces for better performance
		//GLES32.glEnable(GLES32.GL_CULL_FACE); // comment since want to render back faces
	
		//set the background frame color
		GLES32.glClearColor(0.0f,0.0f,0.0f,1.0f);	//black
		
		singleTap = 0;
		doubleTap = 1;
		
		Matrix.setIdentityM(perspectiveProjectionMatrix, 0);
		
//read in byte-> order set->Store in FloatBuffer->put function->change position till 24 material
		
		ByteBuffer bb_ambient = ByteBuffer.allocateDirect(mat_ambient.length*4);	//24*sizeof(float) 
		bb_ambient.order(ByteOrder.nativeOrder());
		floatBuffer_for_material_ambient = bb_ambient.asFloatBuffer();
		floatBuffer_for_material_ambient.put(mat_ambient);
		floatBuffer_for_material_ambient.position(0);

		
		ByteBuffer bb_diffuse = ByteBuffer.allocateDirect(mat_diffuse.length*4);	//24*sizeof(float) 
		bb_diffuse.order(ByteOrder.nativeOrder());
		floatBuffer_for_material_diffuse = bb_diffuse.asFloatBuffer();
		floatBuffer_for_material_diffuse.put(mat_diffuse);
		floatBuffer_for_material_diffuse.position(0);
		
		ByteBuffer bb_specular = ByteBuffer.allocateDirect(mat_specular.length*4);	//24*sizeof(float) 
		bb_specular.order(ByteOrder.nativeOrder());
		floatBuffer_for_material_specular = bb_specular.asFloatBuffer();
		floatBuffer_for_material_specular.put(mat_specular);
		floatBuffer_for_material_specular.position(0);
		
		ByteBuffer bb_shininess = ByteBuffer.allocateDirect(mat_shininess.length*4);	//24*sizeof(float) 
		bb_shininess.order(ByteOrder.nativeOrder());
		floatBuffer_for_material_shine = bb_shininess.asFloatBuffer();
		floatBuffer_for_material_shine.put(mat_shininess);
		floatBuffer_for_material_shine.position(0);
		
		
	}
	
	private void resize(int width, int height)
	{
		//Set view port
		GLES32.glViewport(0,0,width,height);
		
		Matrix.perspectiveM(perspectiveProjectionMatrix, 0,	  45.0f,  ((float)width/(float)height),	  0.1f, 1000.0f);	
	}
	
	private void update()
	{

	if (gAngleForSphere < 2 * pi)
		gAngleForSphere = gAngleForSphere + 1.0f;
	else
		gAngleForSphere = 0.0f;

	}
	
	public void display()
	{
		//code
		GLES32.glClear(GLES32.GL_COLOR_BUFFER_BIT | GLES32.GL_DEPTH_BUFFER_BIT);
		
		GLES32.glUseProgram(shaderProgramObject);
		
		if(doubleTap == 1)
		{
			GLES32.glUniform1i(LKeyPressedUniform, 1);	//Per Fragment
			gAngleForSphere = gAngleForSphere + 1.0f;

			//change position only....
			if(singleTap == 1 )
			{//x
				lightPosition[1] = 50.0f * (float)Math.sin(gAngleForSphere);
				lightPosition[2] = 50.0f * (float)Math.cos(gAngleForSphere);
				GLES32.glUniform4fv(lightPositionUniform, 1, lightPosition, 0);
			}
			else if(singleTap == 2)
			{
				//y
				lightPosition[0] = 50.0f * (float)Math.sin(gAngleForSphere);
				lightPosition[2] = 50.0f * (float)Math.cos(gAngleForSphere);
				GLES32.glUniform4fv(lightPositionUniform, 1, lightPosition, 0);
			}
			else if(singleTap == 3)
			{
//z				lightPosition[0] = 50.0f * (float)Math.cos(gAngleForSphere);
				lightPosition[1] = 50.0f * (float)Math.sin(gAngleForSphere);
				GLES32.glUniform4fv(lightPositionUniform, 1, lightPosition, 0);
			}
				
			GLES32.glUniform3fv(Light_ambient_Uniform, 1, lightAmbient, 0);
			GLES32.glUniform3fv(Light_diffuse_Uniform, 1, lightDiffuse, 0);
			GLES32.glUniform3fv(Light_specular_Uniform, 1, lightSpecular, 0);
			GLES32.glUniform1f(materialShininessUniform, materialShininess);
		
		}
		else
		{
			GLES32.glUniform1i(LKeyPressedUniform, 0);
		}
		
		float modelMatrix [] = new float [16];	
		float viewMatrix[] = new float[16];		

		
				Matrix.setIdentityM(modelMatrix, 0);
				Matrix.setIdentityM(viewMatrix, 0);
				
				Matrix.translateM(modelMatrix, 0, -6.60f, 0.0f, -9.0f);
				GLES32.glUniformMatrix4fv(modelUniform, 1,  false,	  modelMatrix,		  0);
				GLES32.glUniformMatrix4fv(viewUniform,1,	 false,	 viewMatrix,	  0);								  
				GLES32.glUniformMatrix4fv(projectionUniform,1, false, perspectiveProjectionMatrix,0);

//change position in float buffer for 24 material
				floatBuffer_for_material_ambient.position(0);
				GLES32.glUniform3fv(KaUniform, 1, floatBuffer_for_material_ambient);
				floatBuffer_for_material_diffuse.position(0);
				GLES32.glUniform3fv(KdUniform, 1, floatBuffer_for_material_diffuse);
				floatBuffer_for_material_specular.position(0);
				GLES32.glUniform3fv(KsUniform, 1, floatBuffer_for_material_specular);
				floatBuffer_for_material_shine.position(0);
				GLES32.glUniform3fv(materialShininessUniform, 1, floatBuffer_for_material_shine);

				//Bind vao_quad
				GLES32.glBindVertexArray(vao_sphere[0]);
				
				//Draw
				GLES32.glBindBuffer(GLES32.GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_element[0]);
				GLES32.glDrawElements(GLES32.GL_TRIANGLES, numElements, GLES32.GL_UNSIGNED_SHORT, 0);
				
				//Unbind vao_quad
				GLES32.glBindVertexArray(0);			
					//2
						Matrix.setIdentityM(modelMatrix, 0);	  
						Matrix.setIdentityM(viewMatrix, 0);
				
						Matrix.translateM(modelMatrix, 0, 6.60f, 0.0f, -9.0f);
								  
						GLES32.glUniformMatrix4fv(modelUniform, 1,  false,	  modelMatrix,		  0);
										  
						GLES32.glUniformMatrix4fv(viewUniform,1,	 false,	 viewMatrix,	  0);								  
										  
						GLES32.glUniformMatrix4fv(projectionUniform,1, false, perspectiveProjectionMatrix,0);


						floatBuffer_for_material_ambient.position(1);
						GLES32.glUniform3fv(KaUniform, 1, floatBuffer_for_material_ambient);
										  
						floatBuffer_for_material_diffuse.position(1);
						GLES32.glUniform3fv(KdUniform, 1, floatBuffer_for_material_diffuse);
				
						floatBuffer_for_material_specular.position(1);
						GLES32.glUniform3fv(KsUniform, 1, floatBuffer_for_material_specular);
				
						floatBuffer_for_material_shine.position(1);
						GLES32.glUniform3fv(materialShininessUniform, 1, floatBuffer_for_material_shine);
			

						//Bind vao_quad
						GLES32.glBindVertexArray(vao_sphere[0]);
						//Draw your scene using glDrawArrays OR glDrawElements
						GLES32.glBindBuffer(GLES32.GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_element[0]);
						GLES32.glDrawElements(GLES32.GL_TRIANGLES, numElements, GLES32.GL_UNSIGNED_SHORT, 0);
						//Unbind vao_quad
						GLES32.glBindVertexArray(0);
		
		//sphere no : 3
				Matrix.setIdentityM(modelMatrix, 0);	  
				Matrix.setIdentityM(viewMatrix, 0);
				
				Matrix.translateM(modelMatrix, 0, -5.0f, 0.0f, -9.0f);
				GLES32.glUniformMatrix4fv(modelUniform, 1,  false,	  modelMatrix,		  0);
				GLES32.glUniformMatrix4fv(viewUniform,1,	 false,	 viewMatrix,	  0);								  
				GLES32.glUniformMatrix4fv(projectionUniform,1, false, perspectiveProjectionMatrix,0);


				floatBuffer_for_material_ambient.position(2);
				GLES32.glUniform3fv(KaUniform, 1, floatBuffer_for_material_ambient);
				floatBuffer_for_material_diffuse.position(2);
				GLES32.glUniform3fv(KdUniform, 1, floatBuffer_for_material_diffuse);
				floatBuffer_for_material_specular.position(2);
				GLES32.glUniform3fv(KsUniform, 1, floatBuffer_for_material_specular);
				floatBuffer_for_material_shine.position(2);
				GLES32.glUniform3fv(materialShininessUniform, 1, floatBuffer_for_material_shine);

				//Bind vao_quad
				GLES32.glBindVertexArray(vao_sphere[0]);
				
				//Draw
				GLES32.glBindBuffer(GLES32.GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_element[0]);
				GLES32.glDrawElements(GLES32.GL_TRIANGLES, numElements, GLES32.GL_UNSIGNED_SHORT, 0);
				
				//Unbind vao_quad
				GLES32.glBindVertexArray(0);			
					//sphere no : 4
						Matrix.setIdentityM(modelMatrix, 0);	  
						Matrix.setIdentityM(viewMatrix, 0);
				
						Matrix.translateM(modelMatrix, 0, 5.0f, 0.0f, -9.0f);
								  
						GLES32.glUniformMatrix4fv(modelUniform, 1,  false,	  modelMatrix,		  0);
										  
						GLES32.glUniformMatrix4fv(viewUniform,1,	 false,	 viewMatrix,	  0);								  
										  
						GLES32.glUniformMatrix4fv(projectionUniform,1, false, perspectiveProjectionMatrix,0);


						floatBuffer_for_material_ambient.position(3);
						GLES32.glUniform3fv(KaUniform, 1, floatBuffer_for_material_ambient);
										  
						floatBuffer_for_material_diffuse.position(3);
						GLES32.glUniform3fv(KdUniform, 1, floatBuffer_for_material_diffuse);
				
						floatBuffer_for_material_specular.position(3);
						GLES32.glUniform3fv(KsUniform, 1, floatBuffer_for_material_specular);
				
						floatBuffer_for_material_shine.position(3);
						GLES32.glUniform3fv(materialShininessUniform, 1, floatBuffer_for_material_shine);
			

						//Bind vao_quad
						GLES32.glBindVertexArray(vao_sphere[0]);
						//Draw your scene using glDrawArrays OR glDrawElements
						GLES32.glBindBuffer(GLES32.GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_element[0]);
						GLES32.glDrawElements(GLES32.GL_TRIANGLES, numElements, GLES32.GL_UNSIGNED_SHORT, 0);
						//Unbind vao_quad
						GLES32.glBindVertexArray(0);
		
		//sphere no :5
				Matrix.setIdentityM(modelMatrix, 0);	  
				Matrix.setIdentityM(viewMatrix, 0);
				
				Matrix.translateM(modelMatrix, 0, -3.0f, 0.0f, -9.0f);
				GLES32.glUniformMatrix4fv(modelUniform, 1,  false,	  modelMatrix,		  0);
				GLES32.glUniformMatrix4fv(viewUniform,1,	 false,	 viewMatrix,	  0);								  
				GLES32.glUniformMatrix4fv(projectionUniform,1, false, perspectiveProjectionMatrix,0);


				floatBuffer_for_material_ambient.position(4);
				GLES32.glUniform3fv(KaUniform, 1, floatBuffer_for_material_ambient);
				floatBuffer_for_material_diffuse.position(4);
				GLES32.glUniform3fv(KdUniform, 1, floatBuffer_for_material_diffuse);
				floatBuffer_for_material_specular.position(4);
				GLES32.glUniform3fv(KsUniform, 1, floatBuffer_for_material_specular);
				floatBuffer_for_material_shine.position(4);
				GLES32.glUniform3fv(materialShininessUniform, 1, floatBuffer_for_material_shine);

				//Bind vao_quad
				GLES32.glBindVertexArray(vao_sphere[0]);
				
				//Draw
				GLES32.glBindBuffer(GLES32.GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_element[0]);
				GLES32.glDrawElements(GLES32.GL_TRIANGLES, numElements, GLES32.GL_UNSIGNED_SHORT, 0);
				
				//Unbind vao_quad
				GLES32.glBindVertexArray(0);			
					//6
						Matrix.setIdentityM(modelMatrix, 0);	  
						Matrix.setIdentityM(viewMatrix, 0);
				
						Matrix.translateM(modelMatrix, 0, 3.0f, 0.0f, -9.0f);
								  
						GLES32.glUniformMatrix4fv(modelUniform, 1,  false,	  modelMatrix,		  0);
										  
						GLES32.glUniformMatrix4fv(viewUniform,1,	 false,	 viewMatrix,	  0);								  
										  
						GLES32.glUniformMatrix4fv(projectionUniform,1, false, perspectiveProjectionMatrix,0);


						floatBuffer_for_material_ambient.position(5);
						GLES32.glUniform3fv(KaUniform, 1, floatBuffer_for_material_ambient);
										  
						floatBuffer_for_material_diffuse.position(5);
						GLES32.glUniform3fv(KdUniform, 1, floatBuffer_for_material_diffuse);
				
						floatBuffer_for_material_specular.position(5);
						GLES32.glUniform3fv(KsUniform, 1, floatBuffer_for_material_specular);
				
						floatBuffer_for_material_shine.position(5);
						GLES32.glUniform3fv(materialShininessUniform, 1, floatBuffer_for_material_shine);
			

						//Bind vao_quad
						GLES32.glBindVertexArray(vao_sphere[0]);
						//Draw your scene using glDrawArrays OR glDrawElements
						GLES32.glBindBuffer(GLES32.GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_element[0]);
						GLES32.glDrawElements(GLES32.GL_TRIANGLES, numElements, GLES32.GL_UNSIGNED_SHORT, 0);
						//Unbind vao_quad
						GLES32.glBindVertexArray(0);
		
		//7
				Matrix.setIdentityM(modelMatrix, 0);	  
				Matrix.setIdentityM(viewMatrix, 0);
				
				Matrix.translateM(modelMatrix, 0, -1.0f, 0.0f, -9.0f);
				GLES32.glUniformMatrix4fv(modelUniform, 1,  false,	  modelMatrix,		  0);
				GLES32.glUniformMatrix4fv(viewUniform,1,	 false,	 viewMatrix,	  0);								  
				GLES32.glUniformMatrix4fv(projectionUniform,1, false, perspectiveProjectionMatrix,0);


				floatBuffer_for_material_ambient.position(6);
				GLES32.glUniform3fv(KaUniform, 1, floatBuffer_for_material_ambient);
				floatBuffer_for_material_diffuse.position(6);
				GLES32.glUniform3fv(KdUniform, 1, floatBuffer_for_material_diffuse);
				floatBuffer_for_material_specular.position(6);
				GLES32.glUniform3fv(KsUniform, 1, floatBuffer_for_material_specular);
				floatBuffer_for_material_shine.position(6);
				GLES32.glUniform3fv(materialShininessUniform, 1, floatBuffer_for_material_shine);

				//Bind vao_quad
				GLES32.glBindVertexArray(vao_sphere[0]);
				
				//Draw
				GLES32.glBindBuffer(GLES32.GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_element[0]);
				GLES32.glDrawElements(GLES32.GL_TRIANGLES, numElements, GLES32.GL_UNSIGNED_SHORT, 0);
				
				//Unbind vao_quad
				GLES32.glBindVertexArray(0);			
					//8
						Matrix.setIdentityM(modelMatrix, 0);	  
						Matrix.setIdentityM(viewMatrix, 0);
				
						Matrix.translateM(modelMatrix, 0, 1.0f, 0.0f, -9.0f);
								  
						GLES32.glUniformMatrix4fv(modelUniform, 1,  false,	  modelMatrix,		  0);
										  
						GLES32.glUniformMatrix4fv(viewUniform,1,	 false,	 viewMatrix,	  0);								  
										  
						GLES32.glUniformMatrix4fv(projectionUniform,1, false, perspectiveProjectionMatrix,0);


						floatBuffer_for_material_ambient.position(7);
						GLES32.glUniform3fv(KaUniform, 1, floatBuffer_for_material_ambient);
										  
						floatBuffer_for_material_diffuse.position(7);
						GLES32.glUniform3fv(KdUniform, 1, floatBuffer_for_material_diffuse);
				
						floatBuffer_for_material_specular.position(7);
						GLES32.glUniform3fv(KsUniform, 1, floatBuffer_for_material_specular);
				
						floatBuffer_for_material_shine.position(7);
						GLES32.glUniform3fv(materialShininessUniform, 1, floatBuffer_for_material_shine);
			

						//Bind vao_quad
						GLES32.glBindVertexArray(vao_sphere[0]);
						//Draw your scene using glDrawArrays OR glDrawElements
						GLES32.glBindBuffer(GLES32.GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_element[0]);
						GLES32.glDrawElements(GLES32.GL_TRIANGLES, numElements, GLES32.GL_UNSIGNED_SHORT, 0);
						//Unbind vao_quad
						GLES32.glBindVertexArray(0);
//==========================		
//row 2

				Matrix.setIdentityM(modelMatrix, 0);	  
				Matrix.setIdentityM(viewMatrix, 0);
				
				Matrix.translateM(modelMatrix, 0, -6.60f, 3.0f, -9.0f);
				GLES32.glUniformMatrix4fv(modelUniform, 1,  false,	  modelMatrix,		  0);
				GLES32.glUniformMatrix4fv(viewUniform,1,	 false,	 viewMatrix,	  0);								  
				GLES32.glUniformMatrix4fv(projectionUniform,1, false, perspectiveProjectionMatrix,0);


				floatBuffer_for_material_ambient.position(8);
				GLES32.glUniform3fv(KaUniform, 1, floatBuffer_for_material_ambient);
				floatBuffer_for_material_diffuse.position(8);
				GLES32.glUniform3fv(KdUniform, 1, floatBuffer_for_material_diffuse);
				floatBuffer_for_material_specular.position(8);
				GLES32.glUniform3fv(KsUniform, 1, floatBuffer_for_material_specular);
				floatBuffer_for_material_shine.position(8);
				GLES32.glUniform3fv(materialShininessUniform, 1, floatBuffer_for_material_shine);

				//Bind vao_quad
				GLES32.glBindVertexArray(vao_sphere[0]);
				
				//Draw
				GLES32.glBindBuffer(GLES32.GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_element[0]);
				GLES32.glDrawElements(GLES32.GL_TRIANGLES, numElements, GLES32.GL_UNSIGNED_SHORT, 0);
				
				//Unbind vao_quad
				GLES32.glBindVertexArray(0);			
					//2
						Matrix.setIdentityM(modelMatrix, 0);	  
						Matrix.setIdentityM(viewMatrix, 0);
				
						Matrix.translateM(modelMatrix, 0, 6.60f, 3.0f, -9.0f);
								  
						GLES32.glUniformMatrix4fv(modelUniform, 1,  false,	  modelMatrix,		  0);
										  
						GLES32.glUniformMatrix4fv(viewUniform,1,	 false,	 viewMatrix,	  0);								  
										  
						GLES32.glUniformMatrix4fv(projectionUniform,1, false, perspectiveProjectionMatrix,0);


						floatBuffer_for_material_ambient.position(9);
						GLES32.glUniform3fv(KaUniform, 1, floatBuffer_for_material_ambient);
										  
						floatBuffer_for_material_diffuse.position(9);
						GLES32.glUniform3fv(KdUniform, 1, floatBuffer_for_material_diffuse);
				
						floatBuffer_for_material_specular.position(9);
						GLES32.glUniform3fv(KsUniform, 1, floatBuffer_for_material_specular);
				
						floatBuffer_for_material_shine.position(9);
						GLES32.glUniform3fv(materialShininessUniform, 1, floatBuffer_for_material_shine);
			

						//Bind vao_quad
						GLES32.glBindVertexArray(vao_sphere[0]);
						//Draw your scene using glDrawArrays OR glDrawElements
						GLES32.glBindBuffer(GLES32.GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_element[0]);
						GLES32.glDrawElements(GLES32.GL_TRIANGLES, numElements, GLES32.GL_UNSIGNED_SHORT, 0);
						//Unbind vao_quad
						GLES32.glBindVertexArray(0);
		
		//sphere no : 3
				Matrix.setIdentityM(modelMatrix, 0);	  
				Matrix.setIdentityM(viewMatrix, 0);
				
				Matrix.translateM(modelMatrix, 0, -5.0f, 3.0f, -9.0f);
				GLES32.glUniformMatrix4fv(modelUniform, 1,  false,	  modelMatrix,		  0);
				GLES32.glUniformMatrix4fv(viewUniform,1,	 false,	 viewMatrix,	  0);								  
				GLES32.glUniformMatrix4fv(projectionUniform,1, false, perspectiveProjectionMatrix,0);


				floatBuffer_for_material_ambient.position(10);
				GLES32.glUniform3fv(KaUniform, 1, floatBuffer_for_material_ambient);
				floatBuffer_for_material_diffuse.position(10);
				GLES32.glUniform3fv(KdUniform, 1, floatBuffer_for_material_diffuse);
				floatBuffer_for_material_specular.position(10);
				GLES32.glUniform3fv(KsUniform, 1, floatBuffer_for_material_specular);
				floatBuffer_for_material_shine.position(10);
				GLES32.glUniform3fv(materialShininessUniform, 1, floatBuffer_for_material_shine);

				//Bind vao_quad
				GLES32.glBindVertexArray(vao_sphere[0]);
				
				//Draw
				GLES32.glBindBuffer(GLES32.GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_element[0]);
				GLES32.glDrawElements(GLES32.GL_TRIANGLES, numElements, GLES32.GL_UNSIGNED_SHORT, 0);
				
				//Unbind vao_quad
				GLES32.glBindVertexArray(0);			
					//sphere no : 4
						Matrix.setIdentityM(modelMatrix, 0);	  
						Matrix.setIdentityM(viewMatrix, 0);
				
						Matrix.translateM(modelMatrix, 0, 5.0f, 3.0f, -9.0f);
								  
						GLES32.glUniformMatrix4fv(modelUniform, 1,  false,	  modelMatrix,		  0);
										  
						GLES32.glUniformMatrix4fv(viewUniform,1,	 false,	 viewMatrix,	  0);								  
										  
						GLES32.glUniformMatrix4fv(projectionUniform,1, false, perspectiveProjectionMatrix,0);


						floatBuffer_for_material_ambient.position(11);
						GLES32.glUniform3fv(KaUniform, 1, floatBuffer_for_material_ambient);
										  
						floatBuffer_for_material_diffuse.position(11);
						GLES32.glUniform3fv(KdUniform, 1, floatBuffer_for_material_diffuse);
				
						floatBuffer_for_material_specular.position(11);
						GLES32.glUniform3fv(KsUniform, 1, floatBuffer_for_material_specular);
				
						floatBuffer_for_material_shine.position(11);
						GLES32.glUniform3fv(materialShininessUniform, 1, floatBuffer_for_material_shine);
			

						//Bind vao_quad
						GLES32.glBindVertexArray(vao_sphere[0]);
						//Draw your scene using glDrawArrays OR glDrawElements
						GLES32.glBindBuffer(GLES32.GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_element[0]);
						GLES32.glDrawElements(GLES32.GL_TRIANGLES, numElements, GLES32.GL_UNSIGNED_SHORT, 0);
						//Unbind vao_quad
						GLES32.glBindVertexArray(0);
		
		//sphere no :5
				Matrix.setIdentityM(modelMatrix, 0);	  
				Matrix.setIdentityM(viewMatrix, 0);
				
				Matrix.translateM(modelMatrix, 0, -3.0f, 3.0f, -9.0f);
				GLES32.glUniformMatrix4fv(modelUniform, 1,  false,	  modelMatrix,		  0);
				GLES32.glUniformMatrix4fv(viewUniform,1,	 false,	 viewMatrix,	  0);								  
				GLES32.glUniformMatrix4fv(projectionUniform,1, false, perspectiveProjectionMatrix,0);


				floatBuffer_for_material_ambient.position(12);
				GLES32.glUniform3fv(KaUniform, 1, floatBuffer_for_material_ambient);
				floatBuffer_for_material_diffuse.position(12);
				GLES32.glUniform3fv(KdUniform, 1, floatBuffer_for_material_diffuse);
				floatBuffer_for_material_specular.position(12);
				GLES32.glUniform3fv(KsUniform, 1, floatBuffer_for_material_specular);
				floatBuffer_for_material_shine.position(12);
				GLES32.glUniform3fv(materialShininessUniform, 1, floatBuffer_for_material_shine);

				//Bind vao_quad
				GLES32.glBindVertexArray(vao_sphere[0]);
				
				//Draw
				GLES32.glBindBuffer(GLES32.GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_element[0]);
				GLES32.glDrawElements(GLES32.GL_TRIANGLES, numElements, GLES32.GL_UNSIGNED_SHORT, 0);
				
				//Unbind vao_quad
				GLES32.glBindVertexArray(0);			
					//6
						Matrix.setIdentityM(modelMatrix, 0);	  
						Matrix.setIdentityM(viewMatrix, 0);
				
						Matrix.translateM(modelMatrix, 0, 3.0f, 3.0f, -9.0f);
								  
						GLES32.glUniformMatrix4fv(modelUniform, 1,  false,	  modelMatrix,		  0);
										  
						GLES32.glUniformMatrix4fv(viewUniform,1,	 false,	 viewMatrix,	  0);								  
										  
						GLES32.glUniformMatrix4fv(projectionUniform,1, false, perspectiveProjectionMatrix,0);


						floatBuffer_for_material_ambient.position(13);
						GLES32.glUniform3fv(KaUniform, 1, floatBuffer_for_material_ambient);
										  
						floatBuffer_for_material_diffuse.position(13);
						GLES32.glUniform3fv(KdUniform, 1, floatBuffer_for_material_diffuse);
				
						floatBuffer_for_material_specular.position(13);
						GLES32.glUniform3fv(KsUniform, 1, floatBuffer_for_material_specular);
				
						floatBuffer_for_material_shine.position(13);
						GLES32.glUniform3fv(materialShininessUniform, 1, floatBuffer_for_material_shine);
			

						//Bind vao_quad
						GLES32.glBindVertexArray(vao_sphere[0]);
						//Draw your scene using glDrawArrays OR glDrawElements
						GLES32.glBindBuffer(GLES32.GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_element[0]);
						GLES32.glDrawElements(GLES32.GL_TRIANGLES, numElements, GLES32.GL_UNSIGNED_SHORT, 0);
						//Unbind vao_quad
						GLES32.glBindVertexArray(0);
		
		//7
				Matrix.setIdentityM(modelMatrix, 0);	  
				Matrix.setIdentityM(viewMatrix, 0);
				
				Matrix.translateM(modelMatrix, 0, -1.0f, 3.0f, -9.0f);
				GLES32.glUniformMatrix4fv(modelUniform, 1,  false,	  modelMatrix,		  0);
				GLES32.glUniformMatrix4fv(viewUniform,1,	 false,	 viewMatrix,	  0);								  
				GLES32.glUniformMatrix4fv(projectionUniform,1, false, perspectiveProjectionMatrix,0);


				floatBuffer_for_material_ambient.position(14);
				GLES32.glUniform3fv(KaUniform, 1, floatBuffer_for_material_ambient);
				floatBuffer_for_material_diffuse.position(14);
				GLES32.glUniform3fv(KdUniform, 1, floatBuffer_for_material_diffuse);
				floatBuffer_for_material_specular.position(14);
				GLES32.glUniform3fv(KsUniform, 1, floatBuffer_for_material_specular);
				floatBuffer_for_material_shine.position(14);
				GLES32.glUniform3fv(materialShininessUniform, 1, floatBuffer_for_material_shine);

				//Bind vao_quad
				GLES32.glBindVertexArray(vao_sphere[0]);
				
				//Draw
				GLES32.glBindBuffer(GLES32.GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_element[0]);
				GLES32.glDrawElements(GLES32.GL_TRIANGLES, numElements, GLES32.GL_UNSIGNED_SHORT, 0);
				
				//Unbind vao_quad
				GLES32.glBindVertexArray(0);			
					//8
						Matrix.setIdentityM(modelMatrix, 0);	  
						Matrix.setIdentityM(viewMatrix, 0);
				
						Matrix.translateM(modelMatrix, 0, 1.0f, 3.0f, -9.0f);
								  
						GLES32.glUniformMatrix4fv(modelUniform, 1,  false,	  modelMatrix,		  0);
										  
						GLES32.glUniformMatrix4fv(viewUniform,1,	 false,	 viewMatrix,	  0);								  
										  
						GLES32.glUniformMatrix4fv(projectionUniform,1, false, perspectiveProjectionMatrix,0);


						floatBuffer_for_material_ambient.position(15);
						GLES32.glUniform3fv(KaUniform, 1, floatBuffer_for_material_ambient);
										  
						floatBuffer_for_material_diffuse.position(15);
						GLES32.glUniform3fv(KdUniform, 1, floatBuffer_for_material_diffuse);
				
						floatBuffer_for_material_specular.position(15);
						GLES32.glUniform3fv(KsUniform, 1, floatBuffer_for_material_specular);
				
						floatBuffer_for_material_shine.position(15);
						GLES32.glUniform3fv(materialShininessUniform, 1, floatBuffer_for_material_shine);
			

						//Bind vao_quad
						GLES32.glBindVertexArray(vao_sphere[0]);
						//Draw your scene using glDrawArrays OR glDrawElements
						GLES32.glBindBuffer(GLES32.GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_element[0]);
						GLES32.glDrawElements(GLES32.GL_TRIANGLES, numElements, GLES32.GL_UNSIGNED_SHORT, 0);
						//Unbind vao_quad
						GLES32.glBindVertexArray(0);
		




//============================
//row 3		
				Matrix.setIdentityM(modelMatrix, 0);	  
				Matrix.setIdentityM(viewMatrix, 0);
				
				Matrix.translateM(modelMatrix, 0, -6.60f, -3.0f, -9.0f);
				GLES32.glUniformMatrix4fv(modelUniform, 1,  false,	  modelMatrix,		  0);
				GLES32.glUniformMatrix4fv(viewUniform,1,	 false,	 viewMatrix,	  0);								  
				GLES32.glUniformMatrix4fv(projectionUniform,1, false, perspectiveProjectionMatrix,0);


				floatBuffer_for_material_ambient.position(16);
				GLES32.glUniform3fv(KaUniform, 1, floatBuffer_for_material_ambient);
				floatBuffer_for_material_diffuse.position(16);
				GLES32.glUniform3fv(KdUniform, 1, floatBuffer_for_material_diffuse);
				floatBuffer_for_material_specular.position(16);
				GLES32.glUniform3fv(KsUniform, 1, floatBuffer_for_material_specular);
				floatBuffer_for_material_shine.position(16);
				GLES32.glUniform3fv(materialShininessUniform, 1, floatBuffer_for_material_shine);

				//Bind vao_quad
				GLES32.glBindVertexArray(vao_sphere[0]);
				
				//Draw
				GLES32.glBindBuffer(GLES32.GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_element[0]);
				GLES32.glDrawElements(GLES32.GL_TRIANGLES, numElements, GLES32.GL_UNSIGNED_SHORT, 0);
				
				//Unbind vao_quad
				GLES32.glBindVertexArray(0);			
					//2
						Matrix.setIdentityM(modelMatrix, 0);	  
						Matrix.setIdentityM(viewMatrix, 0);
				
						Matrix.translateM(modelMatrix, 0, 6.60f, -3.0f, -9.0f);
								  
						GLES32.glUniformMatrix4fv(modelUniform, 1,  false,	  modelMatrix,		  0);
										  
						GLES32.glUniformMatrix4fv(viewUniform,1,	 false,	 viewMatrix,	  0);								  
										  
						GLES32.glUniformMatrix4fv(projectionUniform,1, false, perspectiveProjectionMatrix,0);


						floatBuffer_for_material_ambient.position(17);
						GLES32.glUniform3fv(KaUniform, 1, floatBuffer_for_material_ambient);
										  
						floatBuffer_for_material_diffuse.position(17);
						GLES32.glUniform3fv(KdUniform, 1, floatBuffer_for_material_diffuse);
				
						floatBuffer_for_material_specular.position(17);
						GLES32.glUniform3fv(KsUniform, 1, floatBuffer_for_material_specular);
				
						floatBuffer_for_material_shine.position(17);
						GLES32.glUniform3fv(materialShininessUniform, 1, floatBuffer_for_material_shine);
			

						//Bind vao_quad
						GLES32.glBindVertexArray(vao_sphere[0]);
						//Draw your scene using glDrawArrays OR glDrawElements
						GLES32.glBindBuffer(GLES32.GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_element[0]);
						GLES32.glDrawElements(GLES32.GL_TRIANGLES, numElements, GLES32.GL_UNSIGNED_SHORT, 0);
						//Unbind vao_quad
						GLES32.glBindVertexArray(0);
		
		//sphere no : 3
				Matrix.setIdentityM(modelMatrix, 0);	  
				Matrix.setIdentityM(viewMatrix, 0);
				
				Matrix.translateM(modelMatrix, 0, -5.0f, -3.0f, -9.0f);
				GLES32.glUniformMatrix4fv(modelUniform, 1,  false,	  modelMatrix,		  0);
				GLES32.glUniformMatrix4fv(viewUniform,1,	 false,	 viewMatrix,	  0);								  
				GLES32.glUniformMatrix4fv(projectionUniform,1, false, perspectiveProjectionMatrix,0);


				floatBuffer_for_material_ambient.position(18);
				GLES32.glUniform3fv(KaUniform, 1, floatBuffer_for_material_ambient);
				floatBuffer_for_material_diffuse.position(18);
				GLES32.glUniform3fv(KdUniform, 1, floatBuffer_for_material_diffuse);
				floatBuffer_for_material_specular.position(18);
				GLES32.glUniform3fv(KsUniform, 1, floatBuffer_for_material_specular);
				floatBuffer_for_material_shine.position(18);
				GLES32.glUniform3fv(materialShininessUniform, 1, floatBuffer_for_material_shine);

				//Bind vao_quad
				GLES32.glBindVertexArray(vao_sphere[0]);
				
				//Draw
				GLES32.glBindBuffer(GLES32.GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_element[0]);
				GLES32.glDrawElements(GLES32.GL_TRIANGLES, numElements, GLES32.GL_UNSIGNED_SHORT, 0);
				
				//Unbind vao_quad
				GLES32.glBindVertexArray(0);			
					//sphere no : 4
						Matrix.setIdentityM(modelMatrix, 0);	  
						Matrix.setIdentityM(viewMatrix, 0);
				
						Matrix.translateM(modelMatrix, 0, 5.0f, -3.0f, -9.0f);
								  
						GLES32.glUniformMatrix4fv(modelUniform, 1,  false,	  modelMatrix,		  0);
										  
						GLES32.glUniformMatrix4fv(viewUniform,1,	 false,	 viewMatrix,	  0);								  
										  
						GLES32.glUniformMatrix4fv(projectionUniform,1, false, perspectiveProjectionMatrix,0);


						floatBuffer_for_material_ambient.position(19);
						GLES32.glUniform3fv(KaUniform, 1, floatBuffer_for_material_ambient);
										  
						floatBuffer_for_material_diffuse.position(19);
						GLES32.glUniform3fv(KdUniform, 1, floatBuffer_for_material_diffuse);
				
						floatBuffer_for_material_specular.position(19);
						GLES32.glUniform3fv(KsUniform, 1, floatBuffer_for_material_specular);
				
						floatBuffer_for_material_shine.position(19);
						GLES32.glUniform3fv(materialShininessUniform, 1, floatBuffer_for_material_shine);
			

						//Bind vao_quad
						GLES32.glBindVertexArray(vao_sphere[0]);
						//Draw your scene using glDrawArrays OR glDrawElements
						GLES32.glBindBuffer(GLES32.GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_element[0]);
						GLES32.glDrawElements(GLES32.GL_TRIANGLES, numElements, GLES32.GL_UNSIGNED_SHORT, 0);
						//Unbind vao_quad
						GLES32.glBindVertexArray(0);
		
		//sphere no :5
				Matrix.setIdentityM(modelMatrix, 0);	  
				Matrix.setIdentityM(viewMatrix, 0);
				
				Matrix.translateM(modelMatrix, 0, -3.0f, -3.0f, -9.0f);
				GLES32.glUniformMatrix4fv(modelUniform, 1,  false,	  modelMatrix,		  0);
				GLES32.glUniformMatrix4fv(viewUniform,1,	 false,	 viewMatrix,	  0);								  
				GLES32.glUniformMatrix4fv(projectionUniform,1, false, perspectiveProjectionMatrix,0);


				floatBuffer_for_material_ambient.position(20);
				GLES32.glUniform3fv(KaUniform, 1, floatBuffer_for_material_ambient);
				floatBuffer_for_material_diffuse.position(20);
				GLES32.glUniform3fv(KdUniform, 1, floatBuffer_for_material_diffuse);
				floatBuffer_for_material_specular.position(20);
				GLES32.glUniform3fv(KsUniform, 1, floatBuffer_for_material_specular);
				floatBuffer_for_material_shine.position(20);
				GLES32.glUniform3fv(materialShininessUniform, 1, floatBuffer_for_material_shine);

				//Bind vao_quad
				GLES32.glBindVertexArray(vao_sphere[0]);
				
				//Draw
				GLES32.glBindBuffer(GLES32.GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_element[0]);
				GLES32.glDrawElements(GLES32.GL_TRIANGLES, numElements, GLES32.GL_UNSIGNED_SHORT, 0);
				
				//Unbind vao_quad
				GLES32.glBindVertexArray(0);			
					//6
						Matrix.setIdentityM(modelMatrix, 0);	  
						Matrix.setIdentityM(viewMatrix, 0);
				
						Matrix.translateM(modelMatrix, 0, 3.0f, -3.0f, -9.0f);
								  
						GLES32.glUniformMatrix4fv(modelUniform, 1,  false,	  modelMatrix,		  0);
										  
						GLES32.glUniformMatrix4fv(viewUniform,1,	 false,	 viewMatrix,	  0);								  
										  
						GLES32.glUniformMatrix4fv(projectionUniform,1, false, perspectiveProjectionMatrix,0);


						floatBuffer_for_material_ambient.position(21);
						GLES32.glUniform3fv(KaUniform, 1, floatBuffer_for_material_ambient);
										  
						floatBuffer_for_material_diffuse.position(21);
						GLES32.glUniform3fv(KdUniform, 1, floatBuffer_for_material_diffuse);
				
						floatBuffer_for_material_specular.position(21);
						GLES32.glUniform3fv(KsUniform, 1, floatBuffer_for_material_specular);
				
						floatBuffer_for_material_shine.position(21);
						GLES32.glUniform3fv(materialShininessUniform, 1, floatBuffer_for_material_shine);
			

						//Bind vao_quad
						GLES32.glBindVertexArray(vao_sphere[0]);
						//Draw your scene using glDrawArrays OR glDrawElements
						GLES32.glBindBuffer(GLES32.GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_element[0]);
						GLES32.glDrawElements(GLES32.GL_TRIANGLES, numElements, GLES32.GL_UNSIGNED_SHORT, 0);
						//Unbind vao_quad
						GLES32.glBindVertexArray(0);
		
		//7
				Matrix.setIdentityM(modelMatrix, 0);	  
				Matrix.setIdentityM(viewMatrix, 0);
				
				Matrix.translateM(modelMatrix, 0, -1.0f, -3.0f, -9.0f);
				GLES32.glUniformMatrix4fv(modelUniform, 1,  false,	  modelMatrix,		  0);
				GLES32.glUniformMatrix4fv(viewUniform,1,	 false,	 viewMatrix,	  0);								  
				GLES32.glUniformMatrix4fv(projectionUniform,1, false, perspectiveProjectionMatrix,0);


				floatBuffer_for_material_ambient.position(22);
				GLES32.glUniform3fv(KaUniform, 1, floatBuffer_for_material_ambient);
				floatBuffer_for_material_diffuse.position(22);
				GLES32.glUniform3fv(KdUniform, 1, floatBuffer_for_material_diffuse);
				floatBuffer_for_material_specular.position(22);
				GLES32.glUniform3fv(KsUniform, 1, floatBuffer_for_material_specular);
				floatBuffer_for_material_shine.position(22);
				GLES32.glUniform3fv(materialShininessUniform, 1, floatBuffer_for_material_shine);

				//Bind vao_quad
				GLES32.glBindVertexArray(vao_sphere[0]);
				
				//Draw
				GLES32.glBindBuffer(GLES32.GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_element[0]);
				GLES32.glDrawElements(GLES32.GL_TRIANGLES, numElements, GLES32.GL_UNSIGNED_SHORT, 0);
				
				//Unbind vao_quad
				GLES32.glBindVertexArray(0);			
					//8
						Matrix.setIdentityM(modelMatrix, 0);	  
						Matrix.setIdentityM(viewMatrix, 0);
				
						Matrix.translateM(modelMatrix, 0, 1.0f, -3.0f, -9.0f);
								  
						GLES32.glUniformMatrix4fv(modelUniform, 1,  false,	  modelMatrix,		  0);
										  
						GLES32.glUniformMatrix4fv(viewUniform,1,	 false,	 viewMatrix,	  0);								  
										  
						GLES32.glUniformMatrix4fv(projectionUniform,1, false, perspectiveProjectionMatrix,0);


						floatBuffer_for_material_ambient.position(23);
						GLES32.glUniform3fv(KaUniform, 1, floatBuffer_for_material_ambient);
										  
						floatBuffer_for_material_diffuse.position(23);
						GLES32.glUniform3fv(KdUniform, 1, floatBuffer_for_material_diffuse);
				
						floatBuffer_for_material_specular.position(23);
						GLES32.glUniform3fv(KsUniform, 1, floatBuffer_for_material_specular);
				
						floatBuffer_for_material_shine.position(23);
						GLES32.glUniform3fv(materialShininessUniform, 1, floatBuffer_for_material_shine);
			

						//Bind vao_quad
						GLES32.glBindVertexArray(vao_sphere[0]);
						//Draw your scene using glDrawArrays OR glDrawElements
						GLES32.glBindBuffer(GLES32.GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_element[0]);
						GLES32.glDrawElements(GLES32.GL_TRIANGLES, numElements, GLES32.GL_UNSIGNED_SHORT, 0);
						//Unbind vao_quad
						GLES32.glBindVertexArray(0);
		

//===============================		
		//Un-use shader program
		GLES32.glUseProgram(0);
		
		requestRender();
	}	
	
	private void uninitialize()
	{
		//destroy vao and vbo	
		if(vao_sphere[0] != 0)
		{
			GLES32.glDeleteVertexArrays(1, vao_sphere, 0);
			vao_sphere[0] = 0;
		}
		if(vbo_sphere_position[0] != 0)
		{
			GLES32.glDeleteBuffers(1, vbo_sphere_position,0);
			vbo_sphere_position[0] = 0;
		}
		if(vbo_sphere_normal[0] != 0)
		{
			GLES32.glDeleteBuffers(1, vbo_sphere_normal,0);
			vbo_sphere_normal[0] = 0;
		}
		
		if(vbo_sphere_element[0] != 0)
		{
			GLES32.glDeleteBuffers(1, vbo_sphere_element,0);
			vbo_sphere_element[0] = 0;
		}
		
        if(shaderProgramObject != 0)
        {
            if(vertexShaderObject != 0)
            {
                // detach vertex shader from shader program object
                GLES32.glDetachShader(shaderProgramObject, vertexShaderObject);
                // delete vertex shader object
                GLES32.glDeleteShader(vertexShaderObject);
                vertexShaderObject = 0;
            }
            
            if(fragmentShaderObject != 0)
            {
                // detach fragment  shader from shader program object
                GLES32.glDetachShader(shaderProgramObject, fragmentShaderObject);
                // delete fragment shader object
                GLES32.glDeleteShader(fragmentShaderObject);
                fragmentShaderObject = 0;
            }
		
			// delete shader program object		
			GLES32.glDeleteProgram(shaderProgramObject);
            shaderProgramObject = 0;
        }
    }		
}