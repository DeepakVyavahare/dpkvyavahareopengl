#include <GL/freeglut.h>
#include <stdlib.h>
//#include<GL/glut>

#pragma comment(lib,"opengl32.lib")
#pragma comment(lib,"glu32.lib")

//static int year = 0, day = 0;
void init(void)
{
	glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
	glShadeModel(GL_FLAT);
}


//global variable declarations
bool gbFullscreen = false;

static int shoulder = 0;
static int elbow = 0;

//main()
int main(int argc, char** argv)
{

	//function prototypes
	void display(void);
	void resize(int, int);
	void keyboard(unsigned char, int, int);
	void mouse(int, int, int, int);
	void initialize(void);
	void uninitialize(void);

	glutInit(&argc, argv);

	glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB);

	glutInitWindowSize(500, 500); //as per the source code from Red Book
	glutInitWindowPosition(100, 100); //as per the source code from Red Book
	glutCreateWindow("OpenGL : FPP : Freeglut : RoboticArm");

	initialize();

	glutDisplayFunc(display);
	glutReshapeFunc(resize);
	glutKeyboardFunc(keyboard);
	glutMouseFunc(mouse);
	glutCloseFunc(uninitialize);

	glutMainLoop();
	return 0;


}
void initialize(void)
{
	glClearColor(0.0f, 0.0f, 0.0f, 0.0f); //black
	glClearDepth(1.0f);
	glDepthFunc(GL_LEQUAL);
	glEnable(GL_DEPTH_TEST);
	glShadeModel(GL_FLAT);
	glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);
}

void display(void)
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glColor3f(0.50, 0.50, 0.50);
	glPushMatrix(); //push matrix 1
	glTranslatef(-1.0f,0.0f,0.0f);
	glRotatef((GLfloat)shoulder, 0.0, 0.0, 1.0);

	glTranslatef(1.0, 0.0, 0.0);
	glPushMatrix();
	glScalef(2.0f,0.4f,1.0f);
	//glutSolidCube(1.0f);
	glutWireCube(1.0f);
	glPopMatrix();


	glTranslatef(1.0f, 0.0f, 0.0f);
	glRotatef((GLfloat)elbow, 0.0, 0.0, 1.0);
	glTranslatef(1.0, 0.0, 0.0);
	glPushMatrix();
	glScalef(2.0f, 0.4f, 1.0f);
	glutWireCube(1.0f);
	glPopMatrix();

	glPopMatrix();

	glutSwapBuffers();
}

void keyboard(unsigned char key, int x, int y)
{
	switch (key)
	{
	case 27:
		glutLeaveMainLoop();
		break;

	case 'F':
	case 'f':
		if (gbFullscreen == false)
		{
			glutFullScreen();
			gbFullscreen = true;
		}
		else
		{
			glutLeaveFullScreen();
			gbFullscreen = false;
		}
		break;

	case 's': 
		shoulder = (shoulder + 5) % 360;
		glutPostRedisplay();
		break;

	case 'S':
		shoulder = (shoulder - 5) % 360;
		glutPostRedisplay();
		break;

	case 'e':
		elbow = (elbow + 5) % 360;
		glutPostRedisplay();
		break;

	case 'E':
		elbow = (elbow - 5) % 360;
		glutPostRedisplay();
		break;

	default:
		break;
	}
}

void mouse(int button, int state, int x, int y)
{
	switch (button)
	{
	case GLUT_LEFT_BUTTON:
		break;

	default:
		break;
	}
}

void resize(int width, int height)
{
	if (height == 0)
		height = 1;

	glViewport(0, 0, (GLsizei)width, (GLsizei)height);

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluPerspective(60, (GLsizei)width / (GLsizei)height, 1.0, 20.0);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
//	glTranslatef(0.0,0.0,0.5);
	gluLookAt(0.0, 0.0, 5.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0);

}

void uninitialize(void)
{
	//code
}