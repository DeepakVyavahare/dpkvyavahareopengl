#include <GL/freeglut.h>
#include <stdlib.h>

#pragma comment(lib,"opengl32.lib")
#pragma comment(lib,"glu32.lib")

//static int year = 0, day = 0;
void init(void)
{
	glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
	glShadeModel(GL_FLAT);
}


//global variable declarations
bool gbFullscreen = false;

static int year = 0;
static int day = 0;

//main()
int main(int argc, char** argv)
{

	//function prototypes
	void display(void);
	void resize(int, int);
	void keyboard(unsigned char, int, int);
	void mouse(int, int, int, int);
	void initialize(void);
	void uninitialize(void);

	glutInit(&argc, argv);

	glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB);

	glutInitWindowSize(500, 500); //as per the source code from Red Book
	glutInitWindowPosition(100, 100); //as per the source code from Red Book
	glutCreateWindow("OpenGL : FPP : Freeglut : Solar System");

	initialize();

	glutDisplayFunc(display);
	glutReshapeFunc(resize);
	glutKeyboardFunc(keyboard);
	glutMouseFunc(mouse);
	glutCloseFunc(uninitialize);

	glutMainLoop();
	return 0;


}
void initialize(void)
{
	glClearColor(0.0f, 0.0f, 0.0f, 0.0f); //black
	glClearDepth(1.0f);
	glDepthFunc(GL_LEQUAL);
	glEnable(GL_DEPTH_TEST);
	glShadeModel(GL_FLAT);
	glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);
}

void display(void)
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glColor3f(1.0, 1.0, 0.0);
	glPushMatrix(); //push matrix 1
	glutSolidSphere(1.0, 20, 16);
//	glutSphere(1.0, 20, 16); //draw sun
	glRotatef((GLfloat)year, 0.0, 1.0, 0.0);
	glTranslatef(2.0, 0.0, 0.0);
	glRotatef((GLfloat)day, 0.0, 1.0, 0.0);
	glColor3f(1.0f,0.0f,1.0f);
	glutWireSphere(0.2, 10, 8); //draw smaller planet
	glPopMatrix(); //pop matrix 1

	glutSwapBuffers();
}

void keyboard(unsigned char key, int x, int y)
{
	switch (key)
	{
	case 27:
		glutLeaveMainLoop();
		break;

	case 'F':
	case 'f':
		if (gbFullscreen == false)
		{
			glutFullScreen();
			gbFullscreen = true;
		}
		else
		{
			glutLeaveFullScreen();
			gbFullscreen = false;
		}
		break;

	case 'd': //'d' for day
		day = (day + 10) % 360;
		glutPostRedisplay();
		break;

	case 'D':
		day = (day - 10) % 360;
		glutPostRedisplay();
		break;

	case 'y':// 'y' for year
		year = (year + 5) % 360;
		glutPostRedisplay();
		break;

	case 'Y':
		year = (year - 5) % 360;
		glutPostRedisplay();
		break;

	default:
		break;
	}
}

void mouse(int button, int state, int x, int y)
{
	switch (button)
	{
	case GLUT_LEFT_BUTTON:
		break;

	default:
		break;
	}
}

void resize(int width, int height)
{
	if (height == 0)
		height = 1;

	glViewport(0, 0, (GLsizei)width, (GLsizei)height);

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluPerspective(60, (GLsizei)width / (GLsizei)height, 1.0, 20.0);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	gluLookAt(0.0, 0.0, 5.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0);
}

void uninitialize(void)
{
	//code
}