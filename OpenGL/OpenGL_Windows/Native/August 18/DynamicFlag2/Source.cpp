
#include <Windows.h>
#include <gl/GL.h>
#include <gl/GLU.h>
#include<math.h>
#include <mmsystem.h> //for playsound

#pragma comment(lib, "opengl32.lib")
#pragma comment(lib, "glu32.lib")

#define WIN_WIDTH 800
#define WIN_HEIGHT 600

//global function declarations
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

//global variable declaration
static float SmallCircleFlag = -1;


const float PI = 3.1415;
static float angle = 0.0f;
static float angleS = -1.0f;
static float angleWhite = -0.6f;
static float angleOrange = -0.2f;




//global variable declarations
bool gbFullscreen = false;
bool gbEscapeKeyIsPressed = false;
bool gbActiveWindow = false;
static int globalFlagForINDIA = -1; 

HWND ghwnd;
HDC ghdc;
HGLRC ghrc;

DWORD dwStyle;
WINDOWPLACEMENT wpPrev = { sizeof(WINDOWPLACEMENT) };

static float angleTranslateForI = -1.0f;
static float angleTranslateForSecondI = -1.5f;
static float angleTranslateForN = 1.5f;

static float angleTranslateForA = 1.2f;
static float angleTranslateForLinesOfA = -2.0f;
static float changeColorForD = 0.0f;

static float scaleX = 0.0f;
static float scaleY = 0.0f;
static float scaleZ = 0.0f;
static float scaleXX = 0.0f;
static float scaleYY = 0.0f;
static float scaleZZ = 0.0f;


static float translateVariableForCircle = -1.0f;


static float midline = -1.0f;
static float midlineEnd = -1.0f;


int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int nCmdShow)
{
	//function prototypes
	void initialize(void);
	void update(void);
	void display(void);
	void uninitialize(void);
//	void updateSpin(void);
	//void updateSpinSmallCircles(void);
	//void updateSpinSmallCirclesWhite(void);
	//void updateSpinSmallCirclesOrange(void);

//	void updateSpinTranslate(void);
	//void Timer(float);
//	void translateCircles(void);



	//local variable declarations
	WNDCLASSEX wndclass;
	TCHAR szAppName[] = TEXT("Opengl FFp");
	HWND hwnd;
	MSG msg;

	
	bool bDone = false;

	wndclass.cbSize = sizeof(WNDCLASSEX);
	wndclass.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
	wndclass.cbClsExtra = 0;
	wndclass.cbWndExtra = 0;
	wndclass.lpfnWndProc = WndProc;
	wndclass.hInstance = hInstance;
	wndclass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndclass.hbrBackground = (HBRUSH)GetStockObject(GRAY_BRUSH);
	wndclass.lpszClassName = szAppName;
	wndclass.lpszMenuName = NULL;
	wndclass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);

	RegisterClassEx(&wndclass);


	hwnd = CreateWindowEx(WS_EX_APPWINDOW,
		szAppName,
		TEXT("I N D I A"),
		WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_CLIPSIBLINGS | WS_VISIBLE,
		0,
		0,
		WIN_WIDTH,
		WIN_HEIGHT,
		NULL,
		NULL,
		hInstance,
		NULL);


	ghwnd = hwnd;

	initialize();

	ShowWindow(hwnd, SW_SHOW);
	SetForegroundWindow(hwnd);
	SetFocus(hwnd);

	//PlaySound(TEXT("Gayatri_Mantra_-_Divinity_7.wav"), NULL, SND_ASYNC | SND_FILENAME);
	//PlaySound(TEXT("Saare Jahan Se Accha.wav"), NULL, SND_ASYNC | SND_FILENAME);

	//game/message loop
	while (bDone == false)
	{
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			if (msg.message == WM_QUIT)
				bDone = true;
			else
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
		else
		{
			if (gbActiveWindow == true)
			{
				if (gbEscapeKeyIsPressed == true)
					bDone = true;
	
				
				update();
				
//				updateSpin();  //roatation for cisrcle
	//			updateSpinSmallCircles();
		//		updateSpinSmallCirclesWhite();
			//	updateSpinSmallCirclesOrange();
//				updateSpinTranslate();
//				updateTranslationForI();
//				updateScale();
//				updateScaleForWheel();
	
				//translateCircles();

				//  Timer(0);
				display();//call to display in game loop

			}
		}
	}

	uninitialize();
	return((int)msg.wParam);
}

LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	//function prototypes
	void ToggleFullscreen(void);
	void resize(int, int);

	switch (iMsg)
	{
	case WM_ACTIVATE:
		if (HIWORD(wParam) == 0)
		{
			gbActiveWindow = true;
		}
		else
		{
			gbActiveWindow = false;
		}
		break;

	case WM_SIZE:
		resize(LOWORD(lParam), HIWORD(lParam));
		break;

	case WM_KEYDOWN:
		switch (LOWORD(wParam))
		{
		case VK_ESCAPE:
			gbEscapeKeyIsPressed = true;
			break;

		case 0x46: //'F' or 'f' Key
			if (gbFullscreen == false)
			{
				ToggleFullscreen();
				gbFullscreen = true;
			}
			else
			{
				ToggleFullscreen();
				gbFullscreen = false;
			}
			break;

		default:
			break;
		}
		break;
	case WM_DESTROY:
		PostQuitMessage(0);
		break;
	}
	return(DefWindowProc(hwnd, iMsg, wParam, lParam));
}

void ToggleFullscreen(void)
{
	//local variable declarations
	BOOL bIsWindowPlacement;
	BOOL bIsMonitorInfo;
	HMONITOR hMonitor;
	MONITORINFO mi;

	if (gbFullscreen == false)
	{
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);
		bIsWindowPlacement = GetWindowPlacement(ghwnd, &wpPrev);

		mi.cbSize = sizeof(MONITORINFO);
		hMonitor = MonitorFromWindow(ghwnd, MONITORINFOF_PRIMARY);
		bIsMonitorInfo = GetMonitorInfo(hMonitor, &mi);

		if (bIsWindowPlacement == TRUE && bIsMonitorInfo == TRUE)
		{
			SetWindowLong(ghwnd, GWL_STYLE, dwStyle & ~WS_OVERLAPPEDWINDOW);
			SetWindowPos(ghwnd,
				HWND_TOP,
				mi.rcMonitor.left,
				mi.rcMonitor.top,
				(mi.rcMonitor.right - mi.rcMonitor.left),
				(mi.rcMonitor.bottom - mi.rcMonitor.top),
				SWP_NOZORDER | SWP_FRAMECHANGED);
			ShowCursor(FALSE);
		}
	}
	else if (gbFullscreen == true)
	{
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPED);
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd,
			HWND_TOP,
			0,
			0,
			0,
			0,
			SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED);
		ShowCursor(TRUE);
	}
}

void initialize(void)
{
	//function prototypes
	void resize(int, int);

	//local variable declarations
	PIXELFORMATDESCRIPTOR pfd;
	int iPixelFormatIndex;

	ZeroMemory(&pfd, sizeof(PIXELFORMATDESCRIPTOR));

	pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	pfd.nVersion = 1;
	pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
	pfd.iPixelType = PFD_TYPE_RGBA;
	pfd.cColorBits = 32;
	pfd.cRedBits = 8;
	pfd.cGreenBits = 8;
	pfd.cBlueBits = 8;
	pfd.cAlphaBits = 8;
	pfd.cDepthBits = 8;

	ghdc = GetDC(ghwnd);
	if (ghdc == 0)
	{
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}
	iPixelFormatIndex = ChoosePixelFormat(ghdc, &pfd);
	if (iPixelFormatIndex == 0)
	{
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	if (SetPixelFormat(ghdc, iPixelFormatIndex, &pfd) == 0)
	{
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	ghrc = wglCreateContext(ghdc);
	if (ghrc == 0)
	{
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	if (wglMakeCurrent(ghdc, ghrc) == false)
	{
		wglDeleteContext(ghrc);
		ghrc = NULL;

		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	glClearColor(0.0f, 0.0f, 0.0f, 0.0f); //black

	glClearDepth(1.0f);
	glDepthFunc(GL_LEQUAL);
	glEnable(GL_DEPTH_TEST);
	glShadeModel(GL_SMOOTH);
	glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);

	resize(WIN_WIDTH, WIN_HEIGHT);
}


void resize(int width, int height)
{
	if (height == 0)
		height = 1;

	glViewport(0, 0, (GLsizei)width, (GLsizei)height);

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluPerspective(0.0f, (GLfloat)width / (GLfloat)height, 0.1f, 100.0f);
}

/*
void translateCircles(void)
{
translateVariableForCircle = translateVariableForCircle + 1.0f;
if (translateVariableForCircle >= 20.0f)
translateVariableForCircle = -1.0f;
}*/
/*
void updateScale(void)
{

	if (scaleX <= 0.95f || scaleY <= 0.95f || scaleZ <= 0.95f)
	{

		scaleX = scaleX + 0.001f;
		scaleY = scaleY + 0.001f;
		scaleZ = scaleZ + 0.001f;
	}
	else
	{
		scaleX = 0.95f;
		scaleY = 0.95f;
		scaleZ = 0.95f;
	}
}


void updateScaleForWheel(void)
{

	if (scaleXX <= 0.8f || scaleYY <= 0.8f || scaleZZ <= 0.8f)
	{

		scaleXX = scaleXX + 0.0001f;
		scaleYY = scaleYY + 0.0001f;
		scaleZZ = scaleZZ + 0.0001f;
	}
	else
	{
		scaleXX = 0.95f;
		scaleYY = 0.95f;
		scaleZZ = 0.95f;
	}
}


void updateTranslationForI(void)
{

	if (angleTranslateForI <= -0.0009f)
	{
		angleTranslateForI = angleTranslateForI + 0.0003f;
	}
	else
	{
		angleTranslateForI = -0.0009f;

	}

}

void updateTranslationForSecondI(void)
{

	if (angleTranslateForSecondI <= -0.0009f)
	{
		angleTranslateForSecondI = angleTranslateForSecondI + 0.0009;
	}
	else
	{
		angleTranslateForSecondI = -0.0009f;

	}

}
void updateTranslationForA(void)
{

	if (angleTranslateForA <= -0.01f)
	{
		angleTranslateForA = angleTranslateForA + 0.0003f;
	}
	else
	{
		angleTranslateForA = -0.01f;
		FLAGCounter = 1.0f;
	}

}

void updateTranslationForAA(void)
{

	if (angleTranslateForAA <= -0.0f)
	{
		angleTranslateForAA = angleTranslateForAA + 0.0003f;
	}
	if (angleTranslateForAA > -0.0f && angleTranslateForAA <= -0.009f)
	{
		angleTranslateForAA = angleTranslateForAA - 0.0003f;
	}
	else
	{
		angleTranslateForAA = -0.01f;

	}

}


void updateSpin(void)
{


	angle = angle + 0.3f;
	if (angle == 360.0f)
		angle = -0.0f;


}

void updateSpinSmallCirclesWhite(void)
{

	if (angleWhite <= 1.0f)
		angleWhite = angleWhite + 0.0003f;
	else
		angleWhite = -1.0f;

}
void updateSpinSmallCirclesOrange(void)
{
	if (angleOrange <= 1.0f)
		angleOrange = angleOrange + 0.0003f;
	else
		angleOrange = -1.0f;

}
void updateSpinSmallCircles(void)
{

	if (angleS <= 1.0f)
		angleS = angleS + 0.0003f;
	else
		angleS = -1.0f;


}
*/

void display(void)
{

	//function prototypes
	void DrawI(void);
	//void DrawSecondI(void);

	void DrawN(void);
	void DrawD(void);
	
	void DrawA(void);
	void midLineForA(void);

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	//I
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(angleTranslateForI, 0.0f, -0.8f);
	glLineWidth(25.0f);
	DrawI();

	if (angleTranslateForI >= -0.7f)
	{
		globalFlagForINDIA = 1;
	}

	//N
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	if (globalFlagForINDIA == 1)
	{
		glTranslatef(-0.5f, angleTranslateForN, -0.8f);
		DrawN();
	}

	if (angleTranslateForN <= 0.0f)
	{
		globalFlagForINDIA = 2;
	}

	//D
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	if (globalFlagForINDIA == 2)
	{
		glTranslatef(-0.1f, 0.0f, -0.8f);
		DrawD();
	}

	//Second I
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	//glScalef(scaleX, scaleY, scaleZ);
	if (globalFlagForINDIA == 3)
	{
		glTranslatef(0.3f, angleTranslateForSecondI, -0.8f);
		DrawI();
	}

	if (angleTranslateForSecondI >= 0.0f)
	{
		globalFlagForINDIA = 4;
	}

	//A
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	if (globalFlagForINDIA == 4)
	{
		glTranslatef(angleTranslateForA, 0.0f, -0.8f);
		DrawA();
	}

	if (angleTranslateForA <= 0.7f)
	{
		globalFlagForINDIA = 5;
	}

	//midline for A
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	if (globalFlagForINDIA == 5)
	{
		midLineForA();
	}

	SwapBuffers(ghdc);
}

void DrawI(void)
{
	glLineWidth(15.0f);
	glBegin(GL_LINES);
	glColor3f(1.0f, 0.5f, 0.0f); 
	glVertex3f(0.0f, 0.6f, 0.0f);
	glColor3f(0.0f, 1.0f, 0.0f); 
	glVertex3f(0.0f, -0.6, 0.0f);
	glEnd();
}

void DrawN(void)
{
	glLineWidth(15.0f);

	//first
	glBegin(GL_LINES);
	glColor3f(1.0f, 0.5f, 0.0f); 
	glVertex3f(0.0f, 0.6f, 0.0f);
	glColor3f(0.0f, 1.0f, 0.0f); 
	glVertex3f(0.0f, -0.6, 0.0f);
	glEnd();

	//second
	glBegin(GL_LINES);
	glColor3f(1.0f, 0.5f, 0.0f); 
	glVertex3f(0.0f, 0.6f, 0.0f);
	glColor3f(0.0f, 1.0f, 0.0f); 
	glVertex3f(0.2f, -0.6, 0.0f);
	glEnd();

	//
	glBegin(GL_LINES);
	glColor3f(1.0f, 0.5f, 0.0f); 
	glVertex3f(0.2f, 0.6f, 0.0f);
	glColor3f(0.0f, 1.0f, 0.0f); 
	glVertex3f(0.2f, -0.6f, 0.0f);
	glEnd();
}

void DrawD(void)
{
	glLineWidth(15.0f);

	glBegin(GL_LINES);
	glColor3f(1.0f, changeColorForD, 0.0f); 
	glVertex3f(0.0f, 0.6f, 0.0f);
	glColor3f(0.0f, changeColorForD * 1.35, 0.0f); 
	glVertex3f(0.0f, -0.6, 0.0f);
	glEnd();

	
	glBegin(GL_LINES);
	glColor3f(1.0f, changeColorForD, 0.0f); 
	glVertex3f(0.0f, 0.6f, 0.0f);
	glColor3f(0.0f, changeColorForD * 1.35, 0.0f); 
	glVertex3f(0.0f, -0.6, 0.0f);
	glEnd();


	glBegin(GL_LINES);
	glColor3f(1.0f, changeColorForD, 0.0f); 
	glVertex3f(-0.025f, 0.6f, 0.0f);
	glColor3f(1.0f, changeColorForD * 1.35, 0.0f); 
	glVertex3f(0.2f, 0.6, 0.0f);
	glEnd();


	glBegin(GL_LINES);
	glColor3f(1.0f, changeColorForD, 0.0f); 
	glVertex3f(0.2f, 0.6f, 0.0f);
	glColor3f(0.0f, changeColorForD * 1.35, 0.0f); 
	glVertex3f(0.2f, -0.6, 0.0f);
	glEnd();

	
	glBegin(GL_LINES);
	glColor3f(0.0f, changeColorForD * 1.35, 0.0f); 
	glVertex3f(-0.025f, -0.6f, 0.0f);
	glColor3f(0.0f, changeColorForD * 1.35, 0.0f); 
	glVertex3f(0.2f, -0.6, 0.0f);
	glEnd();

	if (changeColorForD <= 0.5f)
		changeColorForD = changeColorForD + 0.00009f;

	if (changeColorForD >= 0.5f)
	{
		globalFlagForINDIA = 3;
	}
}

void DrawA(void)
{
	glLineWidth(15.0f);

	
	glBegin(GL_LINES);
	glColor3f(1.0f, 0.5f, 0.0f); 
	glVertex3f(0.0f, 0.6f, 0.0f);
	glColor3f(0.0f, 1.0f, 0.0f); 
	glVertex3f(-0.2f, -0.6, 0.0f);
	glEnd();

	
	glBegin(GL_LINES);
	glColor3f(1.0f, 0.5f, 0.0f); 
	glVertex3f(0.0f, 0.6f, 0.0f);
	glColor3f(0.0f, 1.0f, 0.0f); 
	glVertex3f(0.2f, -0.6, 0.0f);
	glEnd();
}

void midLineForA(void)
{
	glLineWidth(6.0f);
	glBegin(GL_LINES);
	if (midline <= 0.8f)
	{
		midline = midline + 0.0005f;
	}
	else
	{
		if (midlineEnd <= 0.6f)
			midlineEnd = midlineEnd + 0.0005f;
	}

	glColor3f(1.0f, 0.5f, 0.0f); 
	glVertex3f(midlineEnd, 0.0f, 0.0f);
	glColor3f(1.0f, 0.5f, 0.0f); 
	glVertex3f(midline, 0.0, 0.0f);

	//w
	glColor3f(1.0f, 1.0f, 1.0f); 
	glVertex3f(midlineEnd, -0.005f, 0.0f);
	glColor3f(1.0f, 1.0f, 1.0f); 
	glVertex3f(midline, -0.005f, 0.0f);
	

	glColor3f(0.0f, 1.0f, 0.0f); 
	glVertex3f(midlineEnd, -0.010f, 0.0f);
	glColor3f(0.0f, 1.0f, 0.0f); 
	glVertex3f(midline, -0.010f, 0.0f);
	glEnd();
}

void update(void)
{
	if (angleTranslateForI <= -0.7f)
		angleTranslateForI = angleTranslateForI + 0.00005f;

	if (globalFlagForINDIA == 1)
	{
		if (angleTranslateForN >= 0.0f)
			angleTranslateForN = angleTranslateForN - 0.00005f;
	}

	if (globalFlagForINDIA == 3)
	{
		if (angleTranslateForSecondI <= 0.0f)
			angleTranslateForSecondI = angleTranslateForSecondI + 0.0005f;
	}

	if (globalFlagForINDIA == 4)
	{
		if (angleTranslateForA >= 0.7f)
			angleTranslateForA = angleTranslateForA - 0.0005f;
	}
}

void uninitialize(void)
{
	if (gbFullscreen == true)
	{
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPED);
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd,
			HWND_TOP,
			0,
			0,
			0,
			0,
			SWP_NOZORDER | SWP_FRAMECHANGED);
		ShowCursor(TRUE);
	}

	wglMakeCurrent(NULL, NULL);

	wglDeleteContext(NULL);
	ghrc = NULL;

	ReleaseDC(NULL, NULL);
	ghdc = NULL;

	DestroyWindow(NULL);
	ghwnd = NULL;
}