#include<iostream>
#include<stdio.h>
#include<conio.h>
using namespace std;

class Matrix
{
        int matrixArray[3][3];
       
    public:
 int randomCount;
        void inputMatrix();
        void displayMatrix();
        void operator +(Matrix x);
        void operator -(Matrix x);
        void operator *(Matrix x);
  
};

void Matrix::inputMatrix()
{
    int i,j;
        cout<<"\n Enter Matrix Value : \n";
        for(i=0; i<3; i++)
        {
                for(j=0; j<3; j++)
                {
                        cin>>matrixArray[i][j];
                }
        }
}
void Matrix::displayMatrix()
{
        for(int i=0; i<3; i++)
        {
                for(int j=0; j<3; j++)
                {
                        cout<<"\t"<<matrixArray[i][j];
                }
                cout<<"\n";
        }
}


// + operator
void Matrix::operator +(Matrix x)
{
        int tempMatrix[3][3],i,j;
        for(i=0; i<3; i++)
        {
                for(j=0; j<3; j++)
                {
                        tempMatrix[i][j]=matrixArray[i][j]+x.matrixArray[i][j];
                }
        }
        cout<<"\n --: Matrix Addition :-- \n";
        for(int i=0; i<3; i++)
        {
                cout<<" ";
                for(int j=0; j<3; j++)
                {
                        cout<<tempMatrix[i][j]<<"\t";
                }
                cout<<"\n";
        }
}


//- operator
void Matrix::operator -(Matrix y)
{
        int tempMatrix[3][3],i,j;
        for(i=0; i<3; i++)
        {
                for(j=0; j<3; j++)
                {
                        tempMatrix[i][j]=matrixArray[i][j] - y.matrixArray[i][j];
                }
        }
        cout<<"\n --: Matrix Subtraction :-- \n";
        for(int i=0; i<3; i++)
        {
                cout<<" ";
                for(int j=0; j<3; j++)
                {
                        cout<<tempMatrix[i][j]<<"\t";
                }
                cout<<"\n";
        }
}


int main()
{
        Matrix inputA,inputB,cc;

        inputA.inputMatrix();
        inputB.inputMatrix();

        // display both the matrix which was inserted early
            cout<<"\n : First Matrix : \n";
            inputA.displayMatrix(); 
            cout<<"\n : Second Matrix : \n";
            inputB.displayMatrix(); 

        //operator overloading.... Addition Subtraction..
        inputA+inputB;
        inputA-inputB;

        getch();
        return 0;
}
