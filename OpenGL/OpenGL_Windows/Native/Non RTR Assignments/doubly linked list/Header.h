#ifndef _SINGLYLINKEDLIST_
#define _SINGLYLINKEDLIST_
#include <stdio.h>
#include <stdlib.h>
typedef int DATA;

typedef struct node
{
	DATA value;
	struct node * next;

	struct node * prev; //for doubly linked list
}NODE;


void create_list(NODE ** head);
DATA addNode(NODE **, DATA);
void deleteFirstNode(NODE **);
void deleteNode(NODE **, DATA);
void displayList(NODE *);
DATA deleteAfter(NODE *, DATA);
void firstNode(NODE **delet);
void lastNode(NODE **delet);
void deleteLastNode(NODE *delet);
void lastNodeConsideringStartFromEnd(NODE **delet);
#endif