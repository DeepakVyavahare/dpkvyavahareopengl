
#include "Header.h"


//ok 1 //create empty list
void createList(NODE *node)
{
	while (node != NULL)
	{
		node = NULL;
		printf("List is empty now \n");
	}
	node = NULL;
	printf("Empty list is created \n");
}

//ok 2 //add node to linked lists doubly
int addNode(NODE **newNode, int dt)
{
	NODE *new1,*currentNode;
	currentNode = *newNode;

	while (*newNode != NULL)
	{
		newNode = &(*newNode)->next;
	}
	new1 = malloc(sizeof(NODE));
	if (new1 == NULL)
		return 0;
	new1->value = dt;
	new1->prev = currentNode; //previous node
	new1->next = (*newNode);
	*newNode = new1;

}

//ok 3 //delete first node
void deleteFirstNode(NODE **delet)
{
	NODE *temporaryNode;

	if (*delet == NULL)
		printf("Node not found or empty list.\n\n");
	else
	{
		temporaryNode = *delet;
		*delet = (*delet)->next;
		(*delet)->prev = NULL; // making first node as head and removing earlier ;also made it to NULL as its only doubly linked list not circular
		printf("\n deleting node with value : %d\n", temporaryNode->value);
		free(temporaryNode);
	}
}

//ok 4 //delete given node
void deleteNode(NODE **delet, int val)
{
	NODE *temp;
	while (*delet != NULL && (*delet)->value != val)
		delet = &(*delet)->next;
	if (*delet == NULL)
		printf("Node not found or empty list.\n\n");
	else
	{
		temp = *delet;
		*delet = (*delet)->next;
		(*delet)->prev->prev = (*delet)->next;
		printf("\n deleting node with value : %d\n", temp->value);
		free(temp);
	}
}


// ok 5
void displayList(NODE * temp)
{
	if (temp != NULL)
	{
		printf(" node data : ");

		while (temp != NULL)
		{
			printf(" -> %d  ", temp->value);
			temp = temp->next;
		}
		printf(" \n");
	}
	else  //NULL means empty list
		printf("\n List is currently empty.\n");

}
//ok 7
//delete after given node
int deleteAfter(NODE * temp, int val)
{
	NODE * abc = NULL;
	NODE * pqr = NULL;
	do
	{

		if (temp->value == val)
		{
			abc = temp;
			abc->next = temp->next->next;
			//	pqr->next = abc->next;
			//			free(temp);
		}
		temp = temp->next;
	} while (temp != NULL);


}



//ok 6
void firstNode(NODE **delet)
{
	NODE *tt;
	if (*delet == NULL)
		printf("Node not found or empty list.\n\n");

	else
	{
		tt = *delet;
		printf("\n FIrst noded %d =", tt->value);
	}

}

void lastNode(NODE **delet)
{
	NODE *tt;
	tt = *delet;
	while (tt->next != NULL)
	{
		tt = tt->next;
	}
	printf("Last Node %d \n", tt->value);
	if (tt == NULL)
		printf("Node not found or empty list.\n\n");

}

void lastNodeConsideringStartFromEnd(NODE **delet)
{

	NODE *tt;
	tt = *delet;
	while (tt->next != NULL)
	{
		tt = tt->next;
	}
	printf("Last Node %d \n", tt->value);

	while (tt->prev != NULL)
	{
		tt = tt->prev;
	}
	printf("Last Node tracersal from end %d \n", tt->value);

	if (tt == NULL)
		printf("Node not found or empty list.\n\n");


}

void deleteLastNode(NODE *delet)
{
	NODE *temporaryNode;
	NODE *currentNode = delet;

	while (currentNode->next->next != NULL)
	{
		currentNode = currentNode->next;
	}
	currentNode->next = NULL;
	currentNode->prev = currentNode->prev->prev;
}


int deleteBefore(NODE *temp, int val)
{
	NODE  *current = temp;
	NODE  *prev = NULL;
	while ((*current).next != NULL)
	{
		prev = current;
		current = (*current).next;

		if (current->value == val)
		{
			(*prev).next = (*current).next;
			//	pqr->next = abc->next;
			//			free(temp);
		}

	}

}



int main(void)
{
	NODE * head = NULL;
	int option, i;
	int y, z;

	printf("\n 1) create new list       DONE  \n");
	printf(" 2) Add  node               DONE\n");
	printf(" 3) Delet fiirst node       DONE\n");
	printf(" 4) Delete node             DONE\n");
	printf(" 5) Print all nodes         DONE\n");
	printf(" 6) First node              DONE\n");
	printf(" 7) deleteAfter node        DONE  \n");
	printf(" 8) Delet Last nodes         DONE\n");
	printf(" 9) Last node              DONE \n");
	printf(" 10)deleteBefore node        \n");
	printf(" 11) lastNodeConsideringStartFromEnd                    \n\n");

	scanf("%d enter ur choice : ", &option);
	while ((option = getchar()) != EOF)
	{
		switch (option)
		{
		case '1':
			createList(&head);
			printf("Creating new list\n");
			break;
		case '2':
//			for (i = 0;i < 10;i++)
			printf("insert Node ");
			scanf("%d", &y);	
			addNode(&head, y);
			printf("Numbers added to the list\n");
			break;
		case '3':
			deleteFirstNode(&head);
			lastNodeConsideringStartFromEnd(&head);
			break;
		case '4':
			printf("Input integer value for node to be removed: ");
			scanf("%d", &y);
			deleteNode(&head, y);
			printf("\nNode 3 deleted \n");
			break;
		case '5':
			displayList(head);

			break;
		case '6':
			firstNode(&head);
			break;
		case '7':
			printf("Input integer value for node to be deleteAfterd in the list: ");
			scanf("%d", &y);
			if (deleteBefore(head, y))//(deleteAfter(head, y))
				printf("\n %d Node found && deleted from the list\n\n");
			//else
			//printf("Node not found in the list.\n\n");
			break;

		case '8':
			deleteLastNode(head);
			break;
		case '9':
			lastNode(&head);
			break;
		case '10':
			printf("Input integer value for node to be deleteAfterd in the list: ");
			scanf("%d", &z);
			deleteBefore(head, z);
			break;
		case '11':
			lastNodeConsideringStartFromEnd(&head);
			break;
		default:
			break;
		}
	}
	return 0;
}
