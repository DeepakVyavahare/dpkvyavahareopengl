
float material_ambient[] = { 0.0215f,0.1745f,0.0215f,1.0f };
float material_diffuse[] = { 0.07568f,0.61424f,0.07568f,1.0f };
float material_specular[] = { 0.633f,0.727811f,0.633f,1.0f };
double material_shine = { 0.6 * 128 };


//2
float material2_ambient[] = { 0.135,0.2225,0.1575,1.0f };
float material2_diffuse[] = { 0.54,0.89,0.63,1.0f };
float material2_specular[] = { 0.316228,0.316228,0.316228,1.0f
};
double material2_shine = { 0.1 * 128 };

//3
float material3_ambient[] = {
	0.05375,0.05,0.06625,1.0f };
float material3_diffuse[] = { 0.18275,0.17,0.22525,1.0f };
float material3_specular[] = { 0.332741,0.328634,0.346435,1.0f };
double material3_shine = { 0.3 * 128 };
//4
float material4_ambient[] = { 0.25,0.20725,0.20725,1.0f };
float material4_diffuse[] = { 1.0,0.829,0.829,1.0f };
float material4_specular[] = { 0.296648,0.296648,0.296648,1.0f };
double material4_shine = { 0.088 * 128 };

//5
float material5_ambient[] = { 0.1745,0.01175,0.01175,1.0f };
float material5_diffuse[] = { 0.61424,0.04136,0.04136,1.0f };
float material5_specular[] = { 0.727811,0.626959,0.626959,1.0f };
double material5_shine = { 0.6 * 128 };

//6
float material6_ambient[] = { 0.1,0.18725,0.1745,1.0f };
float material6_diffuse[] = { 0.396,0.74151,0.69102,1.0f };
float material6_specular[] = { 0.297254,0.30829,0.306678,1.0f };
double material6_shine = { 0.1 * 128 };


//   7
float material7_ambient[] = { 0.329412,0.223529,0.027451,1.0f };
float material7_diffuse[] = { 0.780392,0.568627,0.113725,1.0f };
float material7_specular[] = { 0.992157,0.941176,0.807843,1.0f };
double material7_shine = { 0.21794872 * 128 };


// 8
float material8_ambient[] = { 0.2125,0.1275,0.054,1.0f };
float material8_diffuse[] = { 0.714,0.4284,0.18144,1.0f };
float material8_specular[] = { 0.393548,0.271906,0.166721,1.0f };
double material8_shine = { 0.2 * 128 };

//9
float material9_ambient[] = { 0.25,0.25,0.25,1.0f };
float material9_diffuse[] = { 0.4,0.4,0.4,1.0f };
float material9_specular[] = { 0.774597,0.774597,0.774597,1.0f };
double material9_shine = { 0.6 * 128 };

float material10_ambient[] = { 0.19125,0.0735,0.0225,1.0f };
float material10_diffuse[] = { 0.7038,0.27048,0.0828,1.0f };
float material10_specular[] = { 0.256777,0.137622,0.086014,1.0f };
double material10_shine = { 0.1 * 128 };
//11
float material11_ambient[] = { 0.24725,0.1995,0.0745,1.0f };
float material11_diffuse[] = { 0.75164,0.60648,0.22648,1.0f };
float material11_specular[] = { 0.628281,0.555802,0.366065,1.0f };
double material11_shine = { 0.4 * 128 };


float material12_ambient[] = { 0.19225,0.19225,0.19225,1.0f };
float material12_diffuse[] = { 0.50754,0.50754,0.50754,1.0f };
float material12_specular[] = { 0.508273,0.508273,0.508273,1.0f };
double material12_shine = { 0.4 * 128 };
// first row 13
float material13_ambient[] = { 0.0,0.0,0.0,1.0f };
float material13_diffuse[] = { 0.01,0.01,0.01,1.0f };
float material13_specular[] = { 0.50,0.50,0.50,1.0f };
double material13_shine = { 0.25 * 128 };


float material14_ambient[] = { 0.0,0.1,0.06,1.0f };
float material14_diffuse[] = { 0.0,0.50980392,0.50980392,1.0f };
float material14_specular[] = { 0.50196078,0.50196078,0.50196078,1.0f };
double material14_shine = { 0.25 * 128 };

//15
float material15_ambient[] = { 0.0,0.0,0.0,1.0f };
float material15_diffuse[] = { 0.1,0.35,0.1,1.0f };
float material15_specular[] = { 0.45,0.55,0.45,1.0f };
double material15_shine = { 0.25 * 128 };

float material16_ambient[] = { 0.0,0.0,0.0,1.0f };
float material16_diffuse[] = { 0.5,0.0,0.0,1.0f };
float material16_specular[] = { 0.7,0.6,0.6,1.0f };
double material16_shine = { 0.25 * 128 };
//17
float material17_ambient[] = { 0.0,0.0,0.0,1.0f };
float material17_diffuse[] = { 0.55,0.55,0.55,1.0f };
float material17_specular[] = { 0.70,0.70,0.70,1.0f };
double material17_shine = { 0.25 * 128 };

float material18_ambient[] = { 0.0,0.0,0.0,1.0f };
float material18_diffuse[] = { 0.5,0.5,0.0,1.0f };
float material18_specular[] = { 0.60,0.60,0.50,1.0f };
double material18_shine = { 0.25 * 128 };

//first row 4th: 19

float material19_ambient[] = { 0.02,0.02,0.02,1.0f };
float material19_diffuse[] = { 0.01,0.01,0.01,1.0f };
float material19_specular[] = { 0.4,0.4,0.4,1.0f };
double material19_shine = { 0.078125 * 128 };


float material20_ambient[] = { 0.0,0.05,0.05,1.0f };
float material20_diffuse[] = { 0.4,0.5,0.5,1.0f };
float material20_specular[] = { 0.04,0.7,0.7,1.0f };
double material20_shine = { 0.078125 * 128 };

//21
float material21_ambient[] = { 0.0,0.05,0.0,1.0f };
float material21_diffuse[] = { 0.4,0.5,0.4,1.0f };
float material21_specular[] = { 0.04,0.7,0.04,1.0f };
double material21_shine = { 0.078125 * 128 };

float material22_ambient[] = { 0.05,0.0,0.0,1.0f };
float material22_diffuse[] = { 0.5,0.4,0.4,1.0f };
float material22_specular[] = { 0.7,0.04,0.04,1.0f };
double material22_shine = { 0.078125 * 128 };

// 23
float material23_ambient[] = { 0.05,0.05,0.05,1.0f };
float material23_diffuse[] = { 0.5,0.5,0.5,1.0f };
float material23_specular[] = { 0.7,0.7,0.7,1.0f };
double material23_shine = { 0.078125 * 128 };
//24
float material24_ambient[] = { 0.05,0.05,0.0,1.0f };
float material24_diffuse[] = { 0.5,0.5,0.4,1.0f };
float material24_specular[] = { 0.7,0.7,0.04,1.0f };
double material24_shine = { 0.078125 * 128 };


float* material_ambient_array[4][6] = { { material_ambient,material2_ambient ,material3_ambient ,material4_ambient,material5_ambient ,material6_ambient } ,{ material7_ambient,material8_ambient ,  material9_ambient ,material10_ambient,material11_ambient ,material12_ambient },{ material13_ambient,material14_ambient ,material15_ambient ,material16_ambient , material17_ambient ,material18_ambient } ,{ material19_ambient,material20_ambient ,material21_ambient ,  material22_ambient,material23_ambient ,material24_ambient } };

float* material_diffuse_array[4][6] = { { material_diffuse,material2_diffuse ,material3_diffuse ,material4_diffuse,material5_diffuse ,material6_diffuse },{ material7_diffuse,material8_diffuse ,material9_diffuse ,material10_diffuse,material11_diffuse ,material12_diffuse },{ material13_diffuse,material14_diffuse ,material15_diffuse ,material16_diffuse,material17_diffuse ,material18_diffuse },{ material19_diffuse,material20_diffuse ,material21_diffuse ,material22_diffuse,material23_diffuse ,material24_diffuse } };
float* material_specular_array[4][6] = { { material_specular,material2_specular ,material3_specular ,material4_specular,material5_specular ,material6_specular },{ material7_specular, material8_specular, material9_specular, material10_specular, material11_specular, material12_specular },{ material13_specular, material14_specular, material15_specular, material16_specular, material17_specular, material18_specular },{ material19_specular, material20_specular, material21_specular, material22_specular, material23_specular, material24_specular } };
double material_shine_array[4][6] = { { material_shine,material2_shine ,material3_shine ,material4_shine,material5_shine ,material6_shine },{ material7_shine,material8_shine ,material9_shine ,material10_shine,material11_shine ,material12_shine },{ material13_shine,material14_shine ,material15_shine ,material16_shine,material17_shine ,material18_shine },{ material19_shine,material20_shine ,material21_shine ,material22_shine,material23_shine ,material24_shine } };

 