//MONKEY HEAD

#include <windows.h>
#include <gl/GL.h>
#include <gl/GLU.h>
#include <stdio.h>
#include <stdlib.h>
#include <vector>

#define  TRUE 1
#define FALSE 0
#define  BUFFER_SIZE 256
#define S_EQUAL 0
#define  WIN_INIT_X 100
#define WIN_INIT_Y 100

#define WIN_WIDTH 800
#define WIN_HEIGHT 600

#define  VK_F 0x46
#define VK_f 0x60


#define NR_POINTS_COORDS 3
#define NR_TEXTURE_COORDS 2
#define NR_NORMAL_COORDS 3
#define NR_FACE_TOKENS 3
#define FOY_ANGLE 45
#define ZNEAR 0.1
#define ZFAR 100
#define VIEWPORT_BOTTOMLEFT_X 0
#define VIEWPORT_BOTTOMLEFT_Y 0
#define MONKEYHEAD_X_TRANSLATE 0.0f
#define MONKEYHEAD_Y_TRANSLATE 0.0f
#define MONKEYHEAD_Z_TRANSLATE -6.0f
#define MONKEYHEAD_X_SCALE_FACTOR 1.5f
#define MONKEYHEAD_Y_SCALE_FACTOR 1.5f
#define MONKEYHEAD_Z_SCALE_FACTOR 1.5f

#define START_ANGLE_POS 0.0f
#define END_ANGLE_POS 360.0f
#define MONKEYHEAD_ANGLE_INCREMENT 0.80f

#pragma comment(lib,"user32.LIB")
#pragma comment(lib,"gdi32.LIB")
#pragma comment(lib,"kernel32.lib")
#pragma comment(lib,"opengl32.lib")
#pragma comment(lib,"glu32.lib")

LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

HWND ghwnd = NULL;
HDC ghdc = NULL;
HGLRC ghrc = NULL;

DWORD dwStyle;
WINDOWPLACEMENT wpPrev = { sizeof(WINDOWPLACEMENT) };

bool gbActiveWindow = false;
bool gbEscapeKeyIsPressed = false;
bool gbFullscreen = false;
bool gbLight = false;

//vector 
GLfloat global_rotate;
std::vector<std::vector<float>> global_vector_vertices;
std::vector<std::vector<float>> global_vector_texture;
std::vector<std::vector<float>> global_vector_normal;
std::vector<std::vector<int>> g_face_tri, g_face_texture, g_face_normals;

//file pointer
FILE *g_fp_meshfile = NULL;
FILE *g_fp_logfile = NULL;

char line[BUFFER_SIZE];
float angleForGlobalLight = 0.0f;

//variables for light
GLfloat light_ambient[] = { 1.0f,1.0f,1.0f,1.0f };
GLfloat light_diffuse[] = { 1.0f,1.0f,1.0f,1.0f };
GLfloat light_specular[] = { 1.0f,1.0f,1.0f,0.0f };
GLfloat light_position[] = { 1.0f,0.0f,0.0f,0.0f }; // as per instruction


int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{

	void initialize(void);
	void uninitialize(void);
	void update(void);
	void display(void); //prototype for disp aas removed from wndproc

	WNDCLASSEX wndclass;
	HWND hwnd;
	MSG msg;
	TCHAR szClassName[] = TEXT("RTROGL");
	bool bDone = false;


	wndclass.cbSize = sizeof(WNDCLASSEX);
	wndclass.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
	wndclass.cbClsExtra = 0;
	wndclass.cbWndExtra = 0;
	wndclass.hInstance = hInstance;
	wndclass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndclass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndclass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);
	wndclass.lpfnWndProc = WndProc;
	wndclass.lpszClassName = szClassName;
	wndclass.lpszMenuName = NULL;

	RegisterClassEx(&wndclass);
	//Create Window
	hwnd = CreateWindowEx(WS_EX_APPWINDOW,
		szClassName,
		TEXT("OpenGL Native Window + Triangle + gluPerspective Projection"),
		WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_CLIPSIBLINGS | WS_VISIBLE,
		0,
		0,
		WIN_WIDTH,
		WIN_HEIGHT,
		NULL,
		NULL,
		hInstance,
		NULL);

	ghwnd = hwnd;

	//initialize
	initialize();

	ShowWindow(hwnd, SW_SHOW);
	SetForegroundWindow(hwnd);
	SetFocus(hwnd);

	//Message Loop
	while (bDone == false)
	{
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			if (msg.message == WM_QUIT)
				bDone = true;
			else
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
		else
		{
			if (gbActiveWindow == true)
			{
				if (gbEscapeKeyIsPressed == true)
					bDone = true;
			}
			update();
			display();//call to display in game loop
		}
	}

	uninitialize();
	return((int)msg.wParam);
}

//WndProc()
LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	void resize(int, int);
	void ToggleFullscreen(void);
	void uninitialize(void);
	switch (iMsg)
	{
	case WM_ACTIVATE:
		if (HIWORD(wParam) == 0)
			gbActiveWindow = true;
		else
			gbActiveWindow = false;
		break;

	case WM_SIZE:
		resize(LOWORD(lParam), HIWORD(lParam));
		break;
	case WM_KEYDOWN:
		switch (wParam)
		{
		case VK_ESCAPE:
			gbEscapeKeyIsPressed = true;
			break;
		case 0x46: //for 'f' or 'F'
			if (gbFullscreen == false)
			{
				ToggleFullscreen();
				gbFullscreen = true;
			}
			else
			{
				ToggleFullscreen();
				gbFullscreen = false;
			}
			break;
		case 0x4c: //for l
			if (gbLight == false)
			{
				gbLight = true;
				glEnable(GL_LIGHTING);

			}
			else
			{
				gbLight = false;
				glDisable(GL_LIGHTING);
			}
			break;
		default:
			break;
		}
		break;
	case WM_LBUTTONDOWN:
		break;
	case WM_DESTROY:
		PostQuitMessage(0);
		break;
	default:
		break;
	}
	return(DefWindowProc(hwnd, iMsg, wParam, lParam));
}

void ToggleFullscreen(void)
{

	MONITORINFO mi = { sizeof(MONITORINFO) };
	if (gbFullscreen == false)
	{
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);
		if (dwStyle & WS_OVERLAPPEDWINDOW)
		{

			if (GetWindowPlacement(ghwnd, &wpPrev) && GetMonitorInfo(MonitorFromWindow(ghwnd, MONITORINFOF_PRIMARY), &mi))
			{
				SetWindowLong(ghwnd, GWL_STYLE, dwStyle & ~WS_OVERLAPPEDWINDOW);
				SetWindowPos(ghwnd, HWND_TOP, mi.rcMonitor.left, mi.rcMonitor.top, mi.rcMonitor.right - mi.rcMonitor.left, mi.rcMonitor.bottom - mi.rcMonitor.top, SWP_NOZORDER | SWP_FRAMECHANGED);
			}
		}
		ShowCursor(FALSE);
	}

	else
	{
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED);

		ShowCursor(TRUE);
	}
}

void initialize(void)
{
	void resize(int, int);
	void uninitialize(void);
	void LoadMeshData(void); //loading object file

	g_fp_logfile = fopen("MONKEYHEADLOADER.LOG", "w");
	if (!g_fp_logfile)
		uninitialize();

	PIXELFORMATDESCRIPTOR pfd;
	int iPixelFormatIndex;


	//code
	ZeroMemory(&pfd, sizeof(PIXELFORMATDESCRIPTOR));

	//Initialization of structure 'PIXELFORMATDESCRIPTOR'
	pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	pfd.nVersion = 1;
	pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
	pfd.iPixelType = PFD_TYPE_RGBA;
	pfd.cColorBits = 32;
	pfd.cRedBits = 8;
	pfd.cGreenBits = 8;
	pfd.cBlueBits = 8;
	pfd.cAlphaBits = 8;
	pfd.cDepthBits = 24;

	ghdc = GetDC(ghwnd);

	iPixelFormatIndex = ChoosePixelFormat(ghdc, &pfd);
	if (iPixelFormatIndex == 0)
	{
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	if (SetPixelFormat(ghdc, iPixelFormatIndex, &pfd) == FALSE)
	{
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	ghrc = wglCreateContext(ghdc);
	if (ghrc == NULL)
	{
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	if (wglMakeCurrent(ghdc, ghrc) == FALSE)
	{
		wglDeleteContext(ghrc);
		ghrc = NULL;
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	glClearColor(0.25f, 0.25f, 0.25f, 0.0f);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);
	glClearDepth(1.0f);
	glShadeModel(GL_SMOOTH);
	glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);

	//call to obj lading
	LoadMeshData();


	//lightning
	glEnable(GL_LIGHTING);
	glEnable(GL_LIGHT0);
	glLightfv(GL_LIGHT0, GL_AMBIENT, light_ambient);
	glLightfv(GL_LIGHT0, GL_DIFFUSE, light_diffuse);
	glLightfv(GL_LIGHT0, GL_SPECULAR, light_specular);
	glLightfv(GL_LIGHT0, GL_POSITION, light_position);
	

	resize(WIN_WIDTH, WIN_HEIGHT);
}

void LoadMeshData(void)
{
	void uninitialize(void);

	//1 open object file
	g_fp_meshfile = fopen("MonkeyHead.obj", "r");
	if (!g_fp_meshfile)
		uninitialize();

	//2 variable declarations
	char *sep_space = " "; //to space
	char *sep_fslash = "/"; //to / slash
	char *first_token = NULL;
	char *token = NULL;
	char *face_tokens[NR_FACE_TOKENS];
	int nr_tokens;
	char *token_vertex_index = NULL;
	char *token_texture_index = NULL;
	char *token_normal_index = NULL;

	//3 
	while (fgets(line, BUFFER_SIZE, g_fp_meshfile) != NULL) //read whole line..1 at a time
	{
			//4
		first_token = strtok(line, sep_space); //from first read line,it will generate 1st token after bonding of line&& space
			//5 //search required character in file
			//created vector to add the component as per requirement
			//once found the v then generate subtoken using NULL parameter && add to vector using push_back()
			//e.g. v 111 222 333 -> entry in obj file for vector/vertices
		if (strcmp(first_token, "v") == S_EQUAL) 
		{
			std::vector<float> vec_point_coord(NR_POINTS_COORDS);
			for (int i = 0;i != NR_POINTS_COORDS;i++)
				vec_point_coord[i] = atof(strtok(NULL, sep_space));
			global_vector_vertices.push_back(vec_point_coord);
		}

				//same for texture coordinates
		else if (strcmp(first_token, "vt") == S_EQUAL)
		{
			std::vector<float> vec_texture_coord(NR_TEXTURE_COORDS);
			for (int i = 0;i != NR_TEXTURE_COORDS;i++)
				vec_texture_coord[i] = atof(strtok(NULL, sep_space));
			global_vector_vertices.push_back(vec_texture_coord);
		}

		//for normals
		else if (strcmp(first_token, "vn") == S_EQUAL)
		{
			std::vector<float> vec_normal_coord(NR_NORMAL_COORDS);
			for (int i = 0;i != NR_NORMAL_COORDS;i++)
				vec_normal_coord[i] = atof(strtok(NULL, sep_space));
			global_vector_vertices.push_back(vec_normal_coord);
		}

			//for FACE component
			// face also has sub component entity with 3 values  
			// e.g. f i/ii/iii i/ii/iii i/ii/iii 
			//
		else if (strcmp(first_token, "f") == S_EQUAL)
		{
			std::vector <int>triangle_vertex_indices(3), texture_vertex_indices(3), normal_vertex_indices(3);
			memset((void*)face_tokens, 0, NR_FACE_TOKENS);
			nr_tokens = 0;
			while (token = strtok(NULL, sep_space))
			{
				if (strlen(token) < 3)
					break;
				face_tokens[nr_tokens] = token;
				nr_tokens++;

			}

			for (int i = 0;i != NR_FACE_TOKENS;++i)
			{
				token_vertex_index = strtok(face_tokens[i], sep_fslash);
				token_texture_index = strtok(NULL, sep_fslash);
				token_normal_index = strtok(NULL, sep_fslash);

				triangle_vertex_indices[i] = atoi(token_vertex_index);
				texture_vertex_indices[i] = atoi(token_texture_index);
				normal_vertex_indices[i] = atoi(token_normal_index);
			}

			g_face_tri.push_back(triangle_vertex_indices);
			g_face_texture.push_back(texture_vertex_indices);
			g_face_normals.push_back(normal_vertex_indices);
		}
		//good practice
		memset((void*)line, (int)'\0', BUFFER_SIZE);
	}

	fclose(g_fp_meshfile);
	g_fp_meshfile = NULL;
//	fprintf(g_fp_meshfile, "global_vector_vertices: %u global_vector_texture: %u global_vector_normal :%u g_face_tri : %u \n", global_vector_vertices.size(), global_vector_texture.size(), global_vector_normal.size(), g_face_tri.size());

}


void display(void)
{

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	//0,0,-6
	glTranslatef(MONKEYHEAD_X_TRANSLATE, MONKEYHEAD_Y_TRANSLATE, MONKEYHEAD_Z_TRANSLATE);
	
	glRotatef(global_rotate, 0.0f, 1.0f, 0.0f);
	
	//1.5,1.5,1.5
	glScalef(MONKEYHEAD_X_SCALE_FACTOR, MONKEYHEAD_Y_SCALE_FACTOR, MONKEYHEAD_Z_SCALE_FACTOR);

	glFrontFace(GL_CCW);
	glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);//

		//face component madhali kuthlihi alue apn size sathi use karu shakato
		//
	for (int i = 0;i != g_face_tri.size();i++)
	{
		glBegin(GL_TRIANGLES);
		for (int j = 0;j != g_face_tri[i].size();j++)
		{
			int vi = g_face_tri[i][j] - 1;
				//actaul drawing of vertices..triangle
			glVertex3f(global_vector_vertices[vi][0], global_vector_vertices[vi][1], global_vector_vertices[vi][2]);
		}
		glEnd();
	}

	SwapBuffers(ghdc);
}

void update(void)
{
	global_rotate = global_rotate + MONKEYHEAD_ANGLE_INCREMENT;
	if (global_rotate >= END_ANGLE_POS) //360
		global_rotate = START_ANGLE_POS; //0
}

void resize(int width, int height)
{
	//code
	if (height == 0)
		height = 1;

	glViewport(VIEWPORT_BOTTOMLEFT_X, VIEWPORT_BOTTOMLEFT_Y, (GLsizei)width, (GLsizei)height);

	glMatrixMode(GL_PROJECTION);

	glLoadIdentity();

	gluPerspective(FOY_ANGLE, (GLfloat)width / (GLfloat)height, ZNEAR, ZFAR);               //(Left, Right, Bottom, Top, Near, Far)

}

void uninitialize(void)
{

	if (gbFullscreen == true)
	{
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED);

		ShowCursor(TRUE);

	}

	wglMakeCurrent(NULL, NULL);

	wglDeleteContext(ghrc);
	ghrc = NULL;

	ReleaseDC(ghwnd, ghdc);
	ghdc = NULL;

	DestroyWindow(ghwnd);
	ghwnd = NULL;

	fclose(g_fp_logfile);
	g_fp_logfile = NULL;
	//gluDeleteQuadric(quadric);
}

