#pragma once
//This IS header files for Array used in program: Light-Material etc.
#include <gl\glew.h> // for GLSL extensions IMPORTANT : This Line Should Be Before #include<gl\gl.h> And #include<gl\glu.h>

#include <gl/GL.h>

#include "vmath.h"

GLfloat lightAmbient[] = { 0.0f, 0.0f, 0.0f, 1.0f };  //increasing this value will increase rightness in material
GLfloat lightDiffuse[] = { 1.0f, 1.0f, 1.0f, 1.0f };
GLfloat lightSpecular[] = { 1.0f, 1.0f, 1.0f ,1.0f };
GLfloat lightPosition[] = { 0.0f, 0.0f, 0.0f, 0.0f };// changing in display function..hence keeping 0 here

													 //light1
GLfloat lightAmbient1[] = { 0.0f,0.0f,0.0f,1.0f };
GLfloat lightDiffuse1[] = { 1.0f,1.0f,1.0f,1.0f };
GLfloat lightSpecular1[] = { 1.0f,1.0f,1.0f,1.0f };
GLfloat lightPosition1[] = { 0,0.0f,0.0f,0.0f };// changing in display function..hence keeping 0 here


												//light2
GLfloat lightAmbient2[] = { 0.0f,0.0f,0.0f,1.0f };
GLfloat lightDiffuse2[] = { 1.0f,1.0f,1.0f,1.0f };
GLfloat lightSpecular2[] = { 1.0f,1.0f,1.0f,1.0f };
GLfloat lightPosition2[] = { 0.0f,0.0f,0.0f,0.0f };// changing in display function..hence keeping 0 here

												   //long turn first..... used 24 variable for each adsp
												   //reducing to 2d matrix form
//Material 24
GLfloat Material_Ambient_Array[24][4] = {
	//row 1
	{ 0.0215f, 0.1745f, 0.0215f, 1.0f },{ 0.135f , 0.225f , 0.1575f, 1.0f },
	{ 0.05375f , 0.05f , 0.06625f, 1.0f },{ 0.25f , 0.20725f , 0.20725f, 1.0f },
	{ 0.1745f , 0.01175f , 0.01175f, 1.0f },{ 0.1f , 0.18725f , 0.1745f, 1.0f },

	//row 2

	{ 0.329412f , 0.223529f , 0.027451f, 1.0f },//column 1
	{ 0.2125f , 0.1275f , 0.054f, 1.0f },//column 2
	{ 0.25f , 0.25f , 0.25f, 1.0f },//column 3
	{ 0.19125f , 0.0735f , 0.0225f, 1.0f },//column 4
	{ 0.24725f , 0.1995f , 0.0745f, 1.0f },//column 5
	{ 0.19225f , 0.19225f , 0.19225f, 1.0f },//column 6
											 //row 3

	{ 0.0f , 0.0f , 0.0f, 1.0f },{ 0.0f , 0.1f , 0.06f, 1.0f },
	{ 0.0f , 0.0f , 0.0f, 1.0f },{ 0.0f , 0.0f , 0.0f, 1.0f },
	{ 0.0f , 0.0f , 0.0f, 1.0f },{ 0.0f , 0.0f , 0.0f, 1.0f },

	//row 4

	{ 0.02f , 0.02f , 0.02f, 1.0f },{ 0.0f , 0.05f , 0.05f, 1.0f },
	{ 0.0f , 0.05f , 0.0f, 1.0f },{ 0.05f , 0.0f , 0.0f, 1.0f },
	{ 0.05f , 0.05f , 0.05f, 1.0f },{ 0.05f , 0.05f , 0.0f, 1.0f }
};

GLfloat Material_Diffuse_Array[24][4] =
{
	//row 1
	{ 0.07568f, 0.61424f, 0.07568f, 1.0f },{ 0.54f , 0.89f , 0.63f , 1.0f },
	{ 0.18275f , 0.17f , 0.22525f , 1.0f },{ 1.0f , 0.829f , 0.829f , 1.0f },
	{ 0.61424f , 0.04136f , 0.04136f , 1.0f },{ 0.396f , 0.74151f , 0.69102f , 1.0f },

	//row 2
	{ 0.780392f , 0.568627f , 0.113725f , 1.0f },{ 0.714f , 0.4284f , 0.18144f , 1.0f },
	{ 0.4f , 0.4f , 0.4f , 1.0f },{ 0.7038f , 0.27048f , 0.0828f , 1.0f },
	{ 0.75164f , 0.60648f , 0.22648f , 1.0f },{ 0.50754f , 0.50754f , 0.50754f , 1.0f },

	//row 3
	{ 0.01f , 0.01f , 0.01f , 1.0f },{ 0.0f , 0.0520980392f ,  0.50980392f , 1.0f },
	{ 0.1f , 0.35f , 0.2f , 1.0f },{ 0.5f , 0.0f , 0.0f , 1.0f },
	{ 0.55f , 0.55f , 0.55f , 1.0f },{ 0.5f , 0.5f , 0.0f , 1.0f },

	//row 4
	{ 0.01f , 0.01f , 0.01f , 1.0f },{ 0.4f , 0.5f , 0.5f , 1.0f },
	{ 0.4f , 0.5f , 0.4f , 1.0f },{ 0.5f , 0.4f , 0.4f , 1.0f },
	{ 0.5f , 0.5f , 0.5f , 1.0f },{ 0.5f , 0.5f , 0.4f , 1.0f }

};

GLfloat Material_Specular_Array[24][4] =
{//row 1
 //

	{ 0.633f , 0.727811f , 0.633f , 1.0f },{ 0.316228f , 0.316228f , 0.316228f , 1.0f },
	{ 0.332741f , 0.328634f , 0.346435f , 1.0f },{ 0.296648f , 0.296648f , 0.296648f , 1.0f },
	{ 0.727811f , 0.626959f , 0.626959f , 1.0f },{ 0.297254f , 0.30829f , 0.306678f , 1.0f },

	//row 2

	{ 0.992157f , 0.941176f , 0.807843f , 1.0f },{ 0.393548f , 0.271906f , 0.166721f , 1.0f },
	{ 0.774597f , 0.774597f , 0.774597f , 1.0f },{ 0.256777f , 0.137622f , 0.086014f , 1.0f },
	{ 0.628281f , 0.555802f , 0.366065f , 1.0f },{ 0.508273f , 0.508273f , 0.508273f , 1.0f },

	//row 3
	{ 0.5f , 0.5f , 0.5f , 1.0f },{ 0.50196078f , 0.50196078f , 0.50196078f , 1.0f },
	{ 0.45f , 0.5f , 0.45f , 1.0f },{ 0.7f , 0.6f , 0.6f , 1.0f },
	{ 0.7f , 0.7f , 0.7f , 1.0f },{ 0.6f , 0.6f , 0.5f , 1.0f },

	//row 4

	{ 0.4f , 0.4f , 0.4f , 1.0f },{ 0.04f , 0.7f , 0.7f , 1.0f },
	{ 0.04f , 0.7f , 0.04f , 1.0f },{ 0.7f , 0.04f , 0.04f , 1.0f },
	{ 0.7f , 0.7f , 0.7f , 1.0f },{ 0.7f , 0.7f , 0.04f , 1.0f }
};

GLfloat Material_Shine_Array[24] =
{
	//row 1
	{ 0.6f * 128 },{ 0.1f * 128 },//column 2
	{ 0.3f * 128 },{ 0.088f * 128 },//column 4
	{ 0.6f * 128 },{ 0.1f * 128 },//column 6

								  //row 2

	{ 0.21794872f * 128 },{ 0.2f * 128 },
	{ 0.6f * 128 },{ 0.1f * 128 },
	{ 0.4f * 128 },{ 0.4f * 128 },

	//row 3

	{ 0.25f * 128 },{ 0.25f * 128 },
	{ 0.25f * 128 },{ 0.25f * 128 },
	{ 0.25f * 128 },{ 0.25f * 128 },

	//row 4

	{ 0.078125f * 128 },{ 0.078125f * 128 },
	{ 0.078125f * 128 },{ 0.078125f * 128 },
	{ 0.078125f * 128 },{ 0.078125f * 128 }
};

