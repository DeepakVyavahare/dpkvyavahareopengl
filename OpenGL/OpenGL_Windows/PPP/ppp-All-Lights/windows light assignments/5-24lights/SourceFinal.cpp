
//24 material + 3 lights position x y z
//using per fragment shader....

#include <windows.h>
#include <stdio.h> // for FILE I/O

#include <gl\glew.h> // for GLSL extensions IMPORTANT : This Line Should Be Before #include<gl\gl.h> And #include<gl\glu.h>

#include <gl/GL.h>

#include "vmath.h"

#include "Sphere.h" //model given by Sir...we r including here also properties->add existing this file
#include "MaterialCoordinatesArray.h" // created header file for all array of material and light

#pragma comment(lib,"glew32.lib") //lib linking glew32 
									// opengl extension wrangler library..cross platform lib..runtime
#pragma comment(lib,"opengl32.lib")//lib linking fo opengl

#pragma comment(lib,"Sphere.lib") //lib inking for sphere

//for window height n width
#define WIN_WIDTH 800
#define WIN_HEIGHT 600

GLfloat angleCube = 0.0f;
using namespace vmath;

//variables for sphere
float sphere_vertices[1146];
float sphere_normals[1146];
float sphere_textures[764];
unsigned short sphere_elements[2280];

enum
{
	VDG_ATTRIBUTE_VERTEX = 0,
	VDG_ATTRIBUTE_COLOR,
	VDG_ATTRIBUTE_NORMAL,
	VDG_ATTRIBUTE_TEXTURE0,
};

//Prototype Of WndProc() declared Globally
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

FILE *gpFile = NULL;


//Global variable declarations
HWND ghwnd = NULL;
HDC ghdc = NULL;
HGLRC ghrc = NULL;

DWORD dwStyle;
WINDOWPLACEMENT wpPrev = { sizeof(WINDOWPLACEMENT) };

bool gbActiveWindow = false;
bool gbEscapeKeyIsPressed = false;
bool gbFullscreen = false;

mat4 gPerspectiveProjectionMatrix;

bool gbLight = true;
GLfloat angleRotation = 0.0f;


float gNumVertices = 0;
float gNumElements = 0;



GLuint gVertexShaderObject;
GLuint gFragmentShaderObject;
GLuint gShaderProgramObject;

GLuint gVao_sphere;
GLuint gVbo_sphere_position;
GLuint gVbo_sphere_normal;
GLuint gVbo_sphere_element;

GLuint model_matrix_uniform, view_matrix_uniform, projection_matrix_uniform;

GLuint L_KeyPressed_uniform;

//variable for 3 lights
GLfloat globalXPress = true;
GLfloat globalYPress = false;
GLfloat globalZPress = false;

//using single light with changing position at 3 locations x y z
//no need of taking more variables ..each for ech position..

GLuint Light_ambient_uniform;
GLuint Light_diffuse_uniform;
GLuint Light_specular_uniform;
GLuint light_position_uniform;

GLuint Material_ambient_uniform;
GLuint Material_diffuse_uniform;
GLuint Material_specular_uniform;
GLuint material_shininess_uniform;

//light0

//main()
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
	//function prototype
	void initialize(void);
	void uninitialize(void);
	void display(void);
	void update(void);

	//variable declaration
	WNDCLASSEX wndclass;
	HWND hwnd;
	MSG msg;
	TCHAR szClassName[] = TEXT("1234");
	bool bDone = false;

	if (fopen_s(&gpFile, "Log.txt", "w") != 0)
	{
		MessageBox(NULL, TEXT("Log File can not be Created\n"), TEXT("Error"), MB_OK | MB_TOPMOST | MB_ICONSTOP);
		exit(0);
	}
	fprintf(gpFile, "Log File is successfully opened. \n");

	int width = 0, height = 0;
	width = GetSystemMetrics(SM_CXSCREEN);
	height = GetSystemMetrics(SM_CYSCREEN);

	//code
	//initializing members of struct WNDCLASSEX
	wndclass.cbSize = sizeof(WNDCLASSEX);
	wndclass.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
	wndclass.cbClsExtra = 0;
	wndclass.cbWndExtra = 0;
	wndclass.hInstance = hInstance;
	wndclass.lpfnWndProc = WndProc;
	wndclass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndclass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndclass.lpszClassName = szClassName;
	wndclass.lpszMenuName = NULL;
	wndclass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);

	//Registering Class
	RegisterClassEx(&wndclass);

	//Create Window
	hwnd = CreateWindowEx(WS_EX_APPWINDOW,
		szClassName,
		TEXT("OpenGL Programmable Pipleline-24 Materials"),
		WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_CLIPSIBLINGS | WS_VISIBLE,
		(width - WIN_WIDTH) / 2,
		(height - WIN_HEIGHT) / 2,
		WIN_WIDTH,
		WIN_HEIGHT,
		NULL,
		NULL,
		hInstance,
		NULL);

	ghwnd = hwnd;

	//initialize
	initialize();

	ShowWindow(hwnd, SW_SHOW);
	SetForegroundWindow(hwnd);
	SetFocus(hwnd);

	//Message Loop
	while (bDone == false)
	{
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			if (msg.message == WM_QUIT)
				bDone = true;
			else
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
		else
		{
			if (gbActiveWindow == true)
			{
				if (gbEscapeKeyIsPressed == true)
					bDone = true;
			}

			//display
			display();
			update();
		}
	}

	uninitialize();
	return((int)msg.wParam);
}

//WndProc()
LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	//function prototype
	void resize(int, int);
	void ToggleFullscreen(void);
	void uninitialize(void);

	static bool bIsAKeyPressed = false;
	static bool bIsLKeyPressed = false;

	//code
	switch (iMsg)
	{
	case WM_ACTIVATE:
		if (HIWORD(wParam) == 0)
			gbActiveWindow = true;
		else
			gbActiveWindow = false;
		break;
	case WM_ERASEBKGND:
		return(0);
	case WM_SIZE:
		resize(LOWORD(lParam), HIWORD(lParam));
		break;
	case WM_KEYDOWN:
		switch (wParam)
		{
		case VK_ESCAPE:
			if (gbEscapeKeyIsPressed == false)
				gbEscapeKeyIsPressed = true;
			break;

		case 0x58://X
			if (gbLight = true)
			{
				globalXPress = true;
				globalYPress = false;
				globalZPress = false;
			}
			else
			{
				globalXPress = false;
				globalYPress = false;
				globalZPress = false;

			}
			break;

		case 0x59://Y
			if (gbLight = true)
			{
				globalXPress = false;
				globalYPress = true;
				globalZPress = false;
			}
			else
			{
				globalXPress = false;
				globalYPress = false;
				globalZPress = false;

			}
			break;

		case 0x5A://Z
			if (gbLight = true)
			{
				globalXPress = false;
				globalYPress = false;
				globalZPress = true;
			}
			else
			{
				globalXPress = false;
				globalYPress = false;
				globalZPress = false;

			}
			break;


		case 0x46: //for 'f' or 'F'
			if (gbFullscreen == false)
			{
				ToggleFullscreen();
				gbFullscreen = true;
			}
			else
			{
				ToggleFullscreen();
				gbFullscreen = false;
			}
			break;
		case 0x4C://L or l
			if (bIsLKeyPressed == false)
			{
				gbLight = true;
				bIsLKeyPressed = true;
			}
			else
			{
				gbLight = false;
				bIsLKeyPressed = false;
			}
			break;
		default:
			break;
		}
		break;
	case WM_LBUTTONDOWN:
		break;
	case WM_CLOSE:
		uninitialize();
		break;
	case WM_DESTROY:
		PostQuitMessage(0);
		break;
	default:
		break;
	}
	return(DefWindowProc(hwnd, iMsg, wParam, lParam));
}

void ToggleFullscreen(void)
{
	//variable declarations
	MONITORINFO mi;

	//code
	if (gbFullscreen == false)
	{
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);
		if (dwStyle & WS_OVERLAPPEDWINDOW)
		{
			mi = { sizeof(MONITORINFO) };
			if (GetWindowPlacement(ghwnd, &wpPrev) && GetMonitorInfo(MonitorFromWindow(ghwnd, MONITORINFOF_PRIMARY), &mi))
			{
				SetWindowLong(ghwnd, GWL_STYLE, dwStyle & ~WS_OVERLAPPEDWINDOW);
				SetWindowPos(ghwnd, HWND_TOP, mi.rcMonitor.left, mi.rcMonitor.top, mi.rcMonitor.right - mi.rcMonitor.left, mi.rcMonitor.bottom - mi.rcMonitor.top, SWP_NOZORDER | SWP_FRAMECHANGED);
			}
		}
		ShowCursor(FALSE);
	}

	else
	{
		//code
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED);

		ShowCursor(TRUE);
	}
}

void initialize(void)
{
	void uninitialize();
	void resize(int, int);

	getSphereVertexData(sphere_vertices, sphere_normals, sphere_textures, sphere_elements);
	gNumVertices = getNumberOfSphereVertices();
	gNumElements = getNumberOfSphereElements();

	//variable declarations
	PIXELFORMATDESCRIPTOR pfd;
	int iPixelFormatIndex;

	//code
	ZeroMemory(&pfd, sizeof(PIXELFORMATDESCRIPTOR));

	//Initialization of structure 'PIXELFORMATDESCRIPTOR'
	pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	pfd.nVersion = 1;
	pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
	pfd.iPixelType = PFD_TYPE_RGBA;
	pfd.cColorBits = 32;
	pfd.cRedBits = 8;
	pfd.cGreenBits = 8;
	pfd.cBlueBits = 8;
	pfd.cAlphaBits = 8;
	pfd.cDepthBits = 24;

	ghdc = GetDC(ghwnd);

	iPixelFormatIndex = ChoosePixelFormat(ghdc, &pfd);
	if (iPixelFormatIndex == 0)
	{
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	if (SetPixelFormat(ghdc, iPixelFormatIndex, &pfd) == false)
	{
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	ghrc = wglCreateContext(ghdc);
	if (ghrc == NULL)
	{
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}


	if (wglMakeCurrent(ghdc, ghrc) == false)
	{
		wglDeleteContext(ghrc);
		ghrc = NULL;
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	GLenum glew_error = glewInit();
	if (glew_error != GLEW_OK)
	{
		wglDeleteContext(ghrc);
		ghrc = NULL;
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	/*** VERTEX SHADER ***/
	//create shader
	gVertexShaderObject = glCreateShader(GL_VERTEX_SHADER);

	//remove unused material variables..keeping for reference
	const GLchar *vertexShaderSourceCode =
		"#version 450" \
		"\n" \
		"in vec4 vPosition;" \
		"in vec4 vPosition2;" \
		"in vec3 vNormal;" \
		"uniform mat4 u_model_matrix;" \
		"uniform mat4 u_view_matrix;" \
		"uniform mat4 u_projection_matrix;" \
		"uniform int u_lighting_enabled;" \
		"uniform vec3 u_La;" \
		"uniform vec3 u_Ld;" \
		"uniform vec3 u_Ls;" \
		"uniform vec4 u_light_position;" \
		"uniform vec3 u_La2;" \
		"uniform vec3 u_Ld2;" \
		"uniform vec3 u_Ls2;" \
		"uniform vec4 u_light_position2;" \
		"uniform vec3 u_La3;" \
		"uniform vec3 u_Ld3;" \
		"uniform vec3 u_Ls3;" \
		"uniform vec4 u_light_position3;" \
		"uniform vec3 u_Ka;" \
		"uniform vec3 u_Kd;" \
		"uniform vec3 u_Ks;" \
		"uniform float u_material_shininess;" \
		"uniform vec3 u_Ka2;" \
		"uniform vec3 u_Kd2;" \
		"uniform vec3 u_Ks2;" \
		"uniform float u_material_shininess2;" \
		"uniform vec3 u_Ka3;" \
		"uniform vec3 u_Kd3;" \
		"uniform vec3 u_Ks3;" \
		"uniform float u_material_shininess3;" \
		"out vec3 transformed_normals;"\
		"out vec3 light_direction;"	\
		"out vec3 viewer_vector;"\
		"out vec3 phong_ads_color;" \
		"out vec3 phong_ads_color2;" \
		"out vec3 phong_ads_color3;" \
	
		"void main(void)"\
		"{"\
		"if(u_lighting_enabled == 1)"\
		"{"\
		"vec4 eye_coordinates = u_view_matrix * u_model_matrix * vPosition;"\
		"transformed_normals = mat3(u_view_matrix * u_model_matrix) * vNormal;"			\
		"light_direction = vec3(u_light_position) - eye_coordinates.xyz;"\
		"viewer_vector = -eye_coordinates.xyz;"\
		"}"\
		"gl_Position = u_projection_matrix * u_view_matrix * u_model_matrix * vPosition;"	\
		"}";

	glShaderSource(gVertexShaderObject, 1, (const GLchar **)&vertexShaderSourceCode, NULL);

	//compile shader
	glCompileShader(gVertexShaderObject);
	GLint iInfoLogLength = 0;
	GLint iShaderCompiledStatus = 0;
	char *szInfoLog = NULL;

	glGetShaderiv(gVertexShaderObject, GL_COMPILE_STATUS, &iShaderCompiledStatus);
	if (iShaderCompiledStatus == GL_FALSE)
	{
		glGetShaderiv(gVertexShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (char*)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(gVertexShaderObject, iInfoLogLength, &written, szInfoLog);
				fprintf(gpFile, "Vertex shader compilation log: %s\n", szInfoLog);
				free(szInfoLog);
				uninitialize();
				exit(0);
			}
		}
	}

	/*** FRAGMENT SHADER***/
	//create shader
	gFragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);

	//provide source code to shader
	const GLchar *fragmentShaderSourceCode =
		"#version 450"\
		"\n"\
		"in vec3 transformed_normals;"\
		"in vec3 light_direction;"\
		"in vec3 viewer_vector;"\
		"out vec4 FragColor;"\
		"uniform vec3 u_La;"\
		"uniform vec3 u_Ld;"\
		"uniform vec3 u_Ls;"\
		"uniform vec3 u_Ka;"\
		"uniform vec3 u_Kd;"\
		"uniform vec3 u_Ks;"\
		"uniform float u_material_shininess;"\
		"uniform int u_lighting_enabled;"\
		"void main(void)"\
		"{"\
		"vec3 phong_ads_color;"\
		"if(u_lighting_enabled == 1)"\
		"{"\
		"vec3 normalized_transformed_normals = normalize(transformed_normals);"\
		"vec3 normalize_light_direction = normalize(light_direction);"\
		"vec3 normalized_viewer_vector = normalize(viewer_vector);"\
		"vec3 ambient = u_La * u_Ka;"\
		"float tn_dot_ld = max(dot(normalized_transformed_normals, normalize_light_direction), 0.0);"				\
		"vec3 diffuse = u_Ld * u_Kd * tn_dot_ld;"\
		"vec3 reflection_vector = reflect(-normalize_light_direction, normalized_transformed_normals);"				\
		"vec3 specular = u_Ls * u_Ks * pow(max(dot(reflection_vector, normalized_viewer_vector), 0.0), u_material_shininess);"\
		"phong_ads_color = ambient + diffuse + specular;"\
		"}"\
		"else"\
		"{"\
		"phong_ads_color = vec3(1.0, 1.0, 1.0);"\
		"}"\
		"FragColor = vec4(phong_ads_color, 1.0);"\
		"}";

	glShaderSource(gFragmentShaderObject, 1, (const GLchar **)&fragmentShaderSourceCode, NULL);

	//compile shader
	glCompileShader(gFragmentShaderObject);
	glGetShaderiv(gFragmentShaderObject, GL_COMPILE_STATUS, &iShaderCompiledStatus);
	if (iShaderCompiledStatus == GL_FALSE)
	{
		glGetShaderiv(gFragmentShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (char*)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(gFragmentShaderObject, iInfoLogLength, &written, szInfoLog);
				fprintf(gpFile, "Fragment Shader Compilation Log: %s\n", szInfoLog);
				free(szInfoLog);
				uninitialize();
				exit(0);
			}
		}
	}

	/**** SHADER PROGRAM ****/
	//create
	gShaderProgramObject = glCreateProgram();

	// attach vertex shader to shader program
	glAttachShader(gShaderProgramObject, gVertexShaderObject);
	glAttachShader(gShaderProgramObject, gFragmentShaderObject);

	glBindAttribLocation(gShaderProgramObject, VDG_ATTRIBUTE_VERTEX, "vPosition");
	glBindAttribLocation(gShaderProgramObject, VDG_ATTRIBUTE_NORMAL, "vNormal");

	//link shader
	glLinkProgram(gShaderProgramObject);
	GLint iShaderProgramLinkStatus = 0;
	glGetProgramiv(gShaderProgramObject, GL_LINK_STATUS, &iShaderProgramLinkStatus);
	if (iShaderProgramLinkStatus == GL_FALSE)
	{
		glGetProgramiv(gShaderProgramObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (char*)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetProgramInfoLog(gShaderProgramObject, iInfoLogLength, &written, szInfoLog);
				fprintf(gpFile, "Shader Program Link Log: %s\n", szInfoLog);
				free(szInfoLog);
				uninitialize();
				exit(0);
			}
		}
	}

	model_matrix_uniform = glGetUniformLocation(gShaderProgramObject, "u_model_matrix");
	view_matrix_uniform = glGetUniformLocation(gShaderProgramObject, "u_view_matrix");
	projection_matrix_uniform = glGetUniformLocation(gShaderProgramObject, "u_projection_matrix");

	L_KeyPressed_uniform = glGetUniformLocation(gShaderProgramObject, "u_lighting_enabled");

	Light_ambient_uniform = glGetUniformLocation(gShaderProgramObject, "u_La");
	Light_diffuse_uniform = glGetUniformLocation(gShaderProgramObject, "u_Ld");
	Light_specular_uniform = glGetUniformLocation(gShaderProgramObject, "u_Ls");

	light_position_uniform = glGetUniformLocation(gShaderProgramObject, "u_light_position");

	Material_ambient_uniform = glGetUniformLocation(gShaderProgramObject, "u_Ka");
	Material_diffuse_uniform = glGetUniformLocation(gShaderProgramObject, "u_Kd");
	Material_specular_uniform = glGetUniformLocation(gShaderProgramObject, "u_Ks");
	material_shininess_uniform = glGetUniformLocation(gShaderProgramObject, "u_material_shininess");

	getSphereVertexData(sphere_vertices, sphere_normals, sphere_textures, sphere_elements);
	gNumVertices = getNumberOfSphereVertices();
	gNumElements = getNumberOfSphereElements();

	glGenVertexArrays(1, &gVao_sphere);

	glBindVertexArray(gVao_sphere);

	glGenBuffers(1, &gVbo_sphere_position);
	glBindBuffer(GL_ARRAY_BUFFER, gVbo_sphere_position);
	glBufferData(GL_ARRAY_BUFFER, sizeof(sphere_vertices), sphere_vertices, GL_STATIC_DRAW);

	glVertexAttribPointer(VDG_ATTRIBUTE_VERTEX, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(VDG_ATTRIBUTE_VERTEX);

	glBindBuffer(GL_ARRAY_BUFFER, 0);


	glGenBuffers(1, &gVbo_sphere_normal);
	glBindBuffer(GL_ARRAY_BUFFER, gVbo_sphere_normal);
	glBufferData(GL_ARRAY_BUFFER, sizeof(sphere_normals), sphere_normals, GL_STATIC_DRAW);

	glVertexAttribPointer(VDG_ATTRIBUTE_NORMAL, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(VDG_ATTRIBUTE_NORMAL);

	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glGenBuffers(1, &gVbo_sphere_element);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(sphere_elements), sphere_elements, GL_STATIC_DRAW);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
	glBindVertexArray(0);

	//clear color
	glClearColor(0.05f, 0.05f, 0.05f, 0.0f);

	//shade model
	glShadeModel(GL_SMOOTH);
	//depth
	glClearDepth(1.0f);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);
	glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);
	//glEnable(GL_CULL_FACE);

	gPerspectiveProjectionMatrix = mat4::identity();

	resize(WIN_WIDTH, WIN_HEIGHT);
}

void resize(int width, int height)
{
	//code
	if (height == 0)
		height = 1;
	glViewport(0, 0, (GLsizei)width, (GLsizei)height);

	gPerspectiveProjectionMatrix = perspective(45.0f, ((GLfloat)width / (GLfloat)height), 0.1f, 100.0f);
}

void update()
{

	if (angleRotation < 2 * 3.145)
	{
		angleRotation += 0.001f;

	}
	else 
		angleRotation = 0.0f;

}

void display(void)
{

	//longer process....earlier kept all 24 draw in these 3 conditions...
	//but then combined all 24 draw and kept 3 conditions for x y z...
	//learning...keep smaller conditions to avoid confusion later on...
	/*
	if (gbLight == true)

	{
		glUniform1i(L_KeyPressed_uniform, 1);

		glUniform3fv(Light_ambient_uniform, 1, lightAmbient);
		glUniform3fv(Light_diffuse_uniform, 1, lightDiffuse);
		glUniform3fv(Light_specular_uniform, 1, lightSpecular);
		glUniform4fv(light_position_uniform, 1, lightPosition);
	*/

	if (globalXPress)
		{

			lightPosition[1] = 50.0f * cos(angleRotation); //y
			lightPosition[2] = 50.0f * sin(angleRotation);//z
			lightPosition[0] = 0.0f;//x
		}
	
	else if (globalYPress)
	{
		lightPosition[0] = 50.0f * cos(angleRotation);//x
		lightPosition[2] = 50.0f * sin(angleRotation);//z
		lightPosition[1] = 0.0f;//y
	}
	else if (globalZPress)
	{
		lightPosition[0] = 50.0f * cos(angleRotation);//x
		lightPosition[1] = 50.0f * sin(angleRotation);//y
		lightPosition[2] = 0.0f;//z
	}




	int i = 0;
	int y = 0;
	if (y > 24)    // as we 24 materials
		y = 0;

	//code
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		glUseProgram(gShaderProgramObject);

		mat4 modelMatrix = mat4::identity();
		mat4 viewMatrix = mat4::identity();

		{
	
			
			if (gbLight == true)
			{
				glUniform1i(L_KeyPressed_uniform, 1);

				glUniform3fv(Light_ambient_uniform, 1, lightAmbient);
				glUniform3fv(Light_diffuse_uniform, 1, lightDiffuse);
				glUniform3fv(Light_specular_uniform, 1, lightSpecular);
				glUniform4fv(light_position_uniform, 1, lightPosition);

			}
			else
			{
				glUniform1i(L_KeyPressed_uniform, 0);
			}

			modelMatrix = mat4::identity();
			viewMatrix = mat4::identity();

			modelMatrix = translate(3.5f, 3.0f, -12.0f);

			glUniformMatrix4fv(model_matrix_uniform, 1, GL_FALSE, modelMatrix);
			glUniformMatrix4fv(view_matrix_uniform, 1, GL_FALSE, viewMatrix);
			glUniformMatrix4fv(projection_matrix_uniform, 1, GL_FALSE, gPerspectiveProjectionMatrix);

			glBindVertexArray(gVao_sphere);
			glUniform3fv(Material_ambient_uniform, 1, Material_Ambient_Array[y]);
			glUniform3fv(Material_diffuse_uniform, 1, Material_Diffuse_Array[y]);
			glUniform3fv(Material_specular_uniform, 1, Material_Specular_Array[y]);
			glUniform1f(material_shininess_uniform, Material_Shine_Array[y]);
			glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
			glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);
			glBindVertexArray(0);


			//		modelMatrix = mat4::identity();
			//		viewMatrix = mat4::identity();
			modelMatrix = translate(1.0f, 3.0f, -12.0f);
			glUniformMatrix4fv(model_matrix_uniform, 1, GL_FALSE, modelMatrix);
			glUniformMatrix4fv(view_matrix_uniform, 1, GL_FALSE, viewMatrix);
			glUniformMatrix4fv(projection_matrix_uniform, 1, GL_FALSE, gPerspectiveProjectionMatrix);

			glBindVertexArray(gVao_sphere);
			glUniform3fv(Material_ambient_uniform, 1, Material_Ambient_Array[y + 1]);
			glUniform3fv(Material_diffuse_uniform, 1, Material_Diffuse_Array[y + 1]);
			glUniform3fv(Material_specular_uniform, 1, Material_Specular_Array[y + 1]);
			glUniform1f(material_shininess_uniform, Material_Shine_Array[y + 1]);
			glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
			glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);
			glBindVertexArray(0);



			modelMatrix = translate(-3.5f, 3.0f, -12.0f);
			glUniformMatrix4fv(model_matrix_uniform, 1, GL_FALSE, modelMatrix);
			glUniformMatrix4fv(view_matrix_uniform, 1, GL_FALSE, viewMatrix);
			glUniformMatrix4fv(projection_matrix_uniform, 1, GL_FALSE, gPerspectiveProjectionMatrix);

			glBindVertexArray(gVao_sphere);
			glUniform3fv(Material_ambient_uniform, 1, Material_Ambient_Array[y + 2]);
			glUniform3fv(Material_diffuse_uniform, 1, Material_Diffuse_Array[y + 2]);
			glUniform3fv(Material_specular_uniform, 1, Material_Specular_Array[y + 2]);
			glUniform1f(material_shininess_uniform, Material_Shine_Array[y + 2]);
			glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
			glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);
			glBindVertexArray(0);



			modelMatrix = translate(-1.0f, 3.0f, -12.0f);
			glUniformMatrix4fv(model_matrix_uniform, 1, GL_FALSE, modelMatrix);
			glUniformMatrix4fv(view_matrix_uniform, 1, GL_FALSE, viewMatrix);
			glUniformMatrix4fv(projection_matrix_uniform, 1, GL_FALSE, gPerspectiveProjectionMatrix);

			glBindVertexArray(gVao_sphere);
			glUniform3fv(Material_ambient_uniform, 1, Material_Ambient_Array[y + 3]);
			glUniform3fv(Material_diffuse_uniform, 1, Material_Diffuse_Array[y + 3]);
			glUniform3fv(Material_specular_uniform, 1, Material_Specular_Array[y + 3]);
			glUniform1f(material_shininess_uniform, Material_Shine_Array[y + 3]);
			glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
			glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);
			glBindVertexArray(0);

			//end of first row....4 sphere

			modelMatrix = translate(3.5f, 2.0f, -12.0f);
			glUniformMatrix4fv(model_matrix_uniform, 1, GL_FALSE, modelMatrix);
			glUniformMatrix4fv(view_matrix_uniform, 1, GL_FALSE, viewMatrix);
			glUniformMatrix4fv(projection_matrix_uniform, 1, GL_FALSE, gPerspectiveProjectionMatrix);

			glBindVertexArray(gVao_sphere);
			glUniform3fv(Material_ambient_uniform, 1, Material_Ambient_Array[y + 4]);
			glUniform3fv(Material_diffuse_uniform, 1, Material_Diffuse_Array[y + 4]);
			glUniform3fv(Material_specular_uniform, 1, Material_Specular_Array[y + 4]);
			glUniform1f(material_shininess_uniform, Material_Shine_Array[y + 4]);
			glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
			glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);
			glBindVertexArray(0);


			modelMatrix = translate(1.0f, 2.0f, -12.0f);
			glUniformMatrix4fv(model_matrix_uniform, 1, GL_FALSE, modelMatrix);
			glUniformMatrix4fv(view_matrix_uniform, 1, GL_FALSE, viewMatrix);
			glUniformMatrix4fv(projection_matrix_uniform, 1, GL_FALSE, gPerspectiveProjectionMatrix);

			glBindVertexArray(gVao_sphere);
			glUniform3fv(Material_ambient_uniform, 1, Material_Ambient_Array[y + 5]);
			glUniform3fv(Material_diffuse_uniform, 1, Material_Diffuse_Array[y + 5]);
			glUniform3fv(Material_specular_uniform, 1, Material_Specular_Array[y + 5]);
			glUniform1f(material_shininess_uniform, Material_Shine_Array[y + 5]);
			glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
			glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);
			glBindVertexArray(0);


			modelMatrix = translate(-1.0f, 2.0f, -12.0f);
			glUniformMatrix4fv(model_matrix_uniform, 1, GL_FALSE, modelMatrix);
			glUniformMatrix4fv(view_matrix_uniform, 1, GL_FALSE, viewMatrix);
			glUniformMatrix4fv(projection_matrix_uniform, 1, GL_FALSE, gPerspectiveProjectionMatrix);

			glBindVertexArray(gVao_sphere);
			glUniform3fv(Material_ambient_uniform, 1, Material_Ambient_Array[y + 6]);
			glUniform3fv(Material_diffuse_uniform, 1, Material_Diffuse_Array[y + 6]);
			glUniform3fv(Material_specular_uniform, 1, Material_Specular_Array[y + 6]);
			glUniform1f(material_shininess_uniform, Material_Shine_Array[y + 6]);
			glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
			glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);
			glBindVertexArray(0);


			modelMatrix = translate(-3.5f, 2.0f, -12.0f);
			glUniformMatrix4fv(model_matrix_uniform, 1, GL_FALSE, modelMatrix);
			glUniformMatrix4fv(view_matrix_uniform, 1, GL_FALSE, viewMatrix);
			glUniformMatrix4fv(projection_matrix_uniform, 1, GL_FALSE, gPerspectiveProjectionMatrix);

			glBindVertexArray(gVao_sphere);
			glUniform3fv(Material_ambient_uniform, 1, Material_Ambient_Array[y + 7]);
			glUniform3fv(Material_diffuse_uniform, 1, Material_Diffuse_Array[y + 7]);
			glUniform3fv(Material_specular_uniform, 1, Material_Specular_Array[y + 7]);
			glUniform1f(material_shininess_uniform, Material_Shine_Array[y + 7]);
			glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
			glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);
			glBindVertexArray(0);

			//end of second row
			modelMatrix = translate(3.5f, 1.0f, -12.0f);
			glUniformMatrix4fv(model_matrix_uniform, 1, GL_FALSE, modelMatrix);
			glUniformMatrix4fv(view_matrix_uniform, 1, GL_FALSE, viewMatrix);
			glUniformMatrix4fv(projection_matrix_uniform, 1, GL_FALSE, gPerspectiveProjectionMatrix);

			glBindVertexArray(gVao_sphere);
			glUniform3fv(Material_ambient_uniform, 1, Material_Ambient_Array[y + 8]);
			glUniform3fv(Material_diffuse_uniform, 1, Material_Diffuse_Array[y + 8]);
			glUniform3fv(Material_specular_uniform, 1, Material_Specular_Array[y + 8]);
			glUniform1f(material_shininess_uniform, Material_Shine_Array[y + 8]);
			glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
			glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);
			glBindVertexArray(0);


			modelMatrix = translate(1.0f, 1.0f, -12.0f);
			glUniformMatrix4fv(model_matrix_uniform, 1, GL_FALSE, modelMatrix);
			glUniformMatrix4fv(view_matrix_uniform, 1, GL_FALSE, viewMatrix);
			glUniformMatrix4fv(projection_matrix_uniform, 1, GL_FALSE, gPerspectiveProjectionMatrix);

			glBindVertexArray(gVao_sphere);
			glUniform3fv(Material_ambient_uniform, 1, Material_Ambient_Array[y + 9]);
			glUniform3fv(Material_diffuse_uniform, 1, Material_Diffuse_Array[y + 9]);
			glUniform3fv(Material_specular_uniform, 1, Material_Specular_Array[y + 9]);
			glUniform1f(material_shininess_uniform, Material_Shine_Array[y + 9]);
			glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
			glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);
			glBindVertexArray(0);


			modelMatrix = translate(-3.5f, 1.0f, -12.0f);
			glUniformMatrix4fv(model_matrix_uniform, 1, GL_FALSE, modelMatrix);
			glUniformMatrix4fv(view_matrix_uniform, 1, GL_FALSE, viewMatrix);
			glUniformMatrix4fv(projection_matrix_uniform, 1, GL_FALSE, gPerspectiveProjectionMatrix);

			glBindVertexArray(gVao_sphere);
			glUniform3fv(Material_ambient_uniform, 1, Material_Ambient_Array[y + 10]);
			glUniform3fv(Material_diffuse_uniform, 1, Material_Diffuse_Array[y + 10]);
			glUniform3fv(Material_specular_uniform, 1, Material_Specular_Array[y + 10]);
			glUniform1f(material_shininess_uniform, Material_Shine_Array[y + 10]);
			glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
			glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);
			glBindVertexArray(0);


			modelMatrix = translate(-1.0f, 1.0f, -12.0f);
			glUniformMatrix4fv(model_matrix_uniform, 1, GL_FALSE, modelMatrix);
			glUniformMatrix4fv(view_matrix_uniform, 1, GL_FALSE, viewMatrix);
			glUniformMatrix4fv(projection_matrix_uniform, 1, GL_FALSE, gPerspectiveProjectionMatrix);

			glBindVertexArray(gVao_sphere);
			glUniform3fv(Material_ambient_uniform, 1, Material_Ambient_Array[y + 11]);
			glUniform3fv(Material_diffuse_uniform, 1, Material_Diffuse_Array[y + 11]);
			glUniform3fv(Material_specular_uniform, 1, Material_Specular_Array[y + 11]);
			glUniform1f(material_shininess_uniform, Material_Shine_Array[y + 11]);
			glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
			glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);
			glBindVertexArray(0);
			// end of 3rd row


			//end of 12 draw calls

			modelMatrix = translate(3.5f, 0.0f, -12.0f);

			glUniformMatrix4fv(model_matrix_uniform, 1, GL_FALSE, modelMatrix);
			glUniformMatrix4fv(view_matrix_uniform, 1, GL_FALSE, viewMatrix);
			glUniformMatrix4fv(projection_matrix_uniform, 1, GL_FALSE, gPerspectiveProjectionMatrix);

			glBindVertexArray(gVao_sphere);
			glUniform3fv(Material_ambient_uniform, 1, Material_Ambient_Array[y + 12]);
			glUniform3fv(Material_diffuse_uniform, 1, Material_Diffuse_Array[y + 12]);
			glUniform3fv(Material_specular_uniform, 1, Material_Specular_Array[y + 12]);
			glUniform1f(material_shininess_uniform, Material_Shine_Array[y + 12]);
			glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
			glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);
			glBindVertexArray(0);


			//		modelMatrix = mat4::identity();
			//		viewMatrix = mat4::identity();
			modelMatrix = translate(1.0f, 0.0f, -12.0f);
			glUniformMatrix4fv(model_matrix_uniform, 1, GL_FALSE, modelMatrix);
			glUniformMatrix4fv(view_matrix_uniform, 1, GL_FALSE, viewMatrix);
			glUniformMatrix4fv(projection_matrix_uniform, 1, GL_FALSE, gPerspectiveProjectionMatrix);

			glBindVertexArray(gVao_sphere);
			glUniform3fv(Material_ambient_uniform, 1, Material_Ambient_Array[y + 13]);
			glUniform3fv(Material_diffuse_uniform, 1, Material_Diffuse_Array[y + 13]);
			glUniform3fv(Material_specular_uniform, 1, Material_Specular_Array[y + 13]);
			glUniform1f(material_shininess_uniform, Material_Shine_Array[y + 13]);
			glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
			glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);
			glBindVertexArray(0);



			modelMatrix = translate(-3.5f, 0.0f, -12.0f);
			glUniformMatrix4fv(model_matrix_uniform, 1, GL_FALSE, modelMatrix);
			glUniformMatrix4fv(view_matrix_uniform, 1, GL_FALSE, viewMatrix);
			glUniformMatrix4fv(projection_matrix_uniform, 1, GL_FALSE, gPerspectiveProjectionMatrix);

			glBindVertexArray(gVao_sphere);
			glUniform3fv(Material_ambient_uniform, 1, Material_Ambient_Array[y + 14]);
			glUniform3fv(Material_diffuse_uniform, 1, Material_Diffuse_Array[y + 14]);
			glUniform3fv(Material_specular_uniform, 1, Material_Specular_Array[y + 14]);
			glUniform1f(material_shininess_uniform, Material_Shine_Array[y + 14]);
			glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
			glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);
			glBindVertexArray(0);



			modelMatrix = translate(-1.0f, 0.0f, -12.0f);
			glUniformMatrix4fv(model_matrix_uniform, 1, GL_FALSE, modelMatrix);
			glUniformMatrix4fv(view_matrix_uniform, 1, GL_FALSE, viewMatrix);
			glUniformMatrix4fv(projection_matrix_uniform, 1, GL_FALSE, gPerspectiveProjectionMatrix);

			glBindVertexArray(gVao_sphere);
			glUniform3fv(Material_ambient_uniform, 1, Material_Ambient_Array[y + 15]);
			glUniform3fv(Material_diffuse_uniform, 1, Material_Diffuse_Array[y + 15]);
			glUniform3fv(Material_specular_uniform, 1, Material_Specular_Array[y + 15]);
			glUniform1f(material_shininess_uniform, Material_Shine_Array[y + 15]);
			glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
			glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);
			glBindVertexArray(0);

			//end of fourth row....4 sphere

			modelMatrix = translate(3.5f, -1.0f, -12.0f);
			glUniformMatrix4fv(model_matrix_uniform, 1, GL_FALSE, modelMatrix);
			glUniformMatrix4fv(view_matrix_uniform, 1, GL_FALSE, viewMatrix);
			glUniformMatrix4fv(projection_matrix_uniform, 1, GL_FALSE, gPerspectiveProjectionMatrix);

			glBindVertexArray(gVao_sphere);
			glUniform3fv(Material_ambient_uniform, 1, Material_Ambient_Array[y + 16]);
			glUniform3fv(Material_diffuse_uniform, 1, Material_Diffuse_Array[y + 16]);
			glUniform3fv(Material_specular_uniform, 1, Material_Specular_Array[y + 16]);
			glUniform1f(material_shininess_uniform, Material_Shine_Array[y + 16]);
			glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
			glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);
			glBindVertexArray(0);


			modelMatrix = translate(1.0f, -1.0f, -12.0f);
			glUniformMatrix4fv(model_matrix_uniform, 1, GL_FALSE, modelMatrix);
			glUniformMatrix4fv(view_matrix_uniform, 1, GL_FALSE, viewMatrix);
			glUniformMatrix4fv(projection_matrix_uniform, 1, GL_FALSE, gPerspectiveProjectionMatrix);

			glBindVertexArray(gVao_sphere);
			glUniform3fv(Material_ambient_uniform, 1, Material_Ambient_Array[y + 17]);
			glUniform3fv(Material_diffuse_uniform, 1, Material_Diffuse_Array[y + 17]);
			glUniform3fv(Material_specular_uniform, 1, Material_Specular_Array[y + 17]);
			glUniform1f(material_shininess_uniform, Material_Shine_Array[y + 17]);
			glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
			glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);
			glBindVertexArray(0);


			modelMatrix = translate(-1.0f, -1.0f, -12.0f);
			glUniformMatrix4fv(model_matrix_uniform, 1, GL_FALSE, modelMatrix);
			glUniformMatrix4fv(view_matrix_uniform, 1, GL_FALSE, viewMatrix);
			glUniformMatrix4fv(projection_matrix_uniform, 1, GL_FALSE, gPerspectiveProjectionMatrix);

			glBindVertexArray(gVao_sphere);
			glUniform3fv(Material_ambient_uniform, 1, Material_Ambient_Array[y + 18]);
			glUniform3fv(Material_diffuse_uniform, 1, Material_Diffuse_Array[y + 18]);
			glUniform3fv(Material_specular_uniform, 1, Material_Specular_Array[y + 18]);
			glUniform1f(material_shininess_uniform, Material_Shine_Array[y + 18]);
			glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
			glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);
			glBindVertexArray(0);


			modelMatrix = translate(-3.5f, -1.0f, -12.0f);
			glUniformMatrix4fv(model_matrix_uniform, 1, GL_FALSE, modelMatrix);
			glUniformMatrix4fv(view_matrix_uniform, 1, GL_FALSE, viewMatrix);
			glUniformMatrix4fv(projection_matrix_uniform, 1, GL_FALSE, gPerspectiveProjectionMatrix);

			glBindVertexArray(gVao_sphere);
			glUniform3fv(Material_ambient_uniform, 1, Material_Ambient_Array[y + 19]);
			glUniform3fv(Material_diffuse_uniform, 1, Material_Diffuse_Array[y + 19]);
			glUniform3fv(Material_specular_uniform, 1, Material_Specular_Array[y + 19]);
			glUniform1f(material_shininess_uniform, Material_Shine_Array[y + 19]);
			glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
			glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);
			glBindVertexArray(0);

			//end of 5th row

			modelMatrix = translate(3.5f, -2.0f, -12.0f);
			glUniformMatrix4fv(model_matrix_uniform, 1, GL_FALSE, modelMatrix);
			glUniformMatrix4fv(view_matrix_uniform, 1, GL_FALSE, viewMatrix);
			glUniformMatrix4fv(projection_matrix_uniform, 1, GL_FALSE, gPerspectiveProjectionMatrix);

			glBindVertexArray(gVao_sphere);
			glUniform3fv(Material_ambient_uniform, 1, Material_Ambient_Array[y + 20]);
			glUniform3fv(Material_diffuse_uniform, 1, Material_Diffuse_Array[y + 20]);
			glUniform3fv(Material_specular_uniform, 1, Material_Specular_Array[y + 20]);
			glUniform1f(material_shininess_uniform, Material_Shine_Array[y + 20]);
			glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
			glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);
			glBindVertexArray(0);


			modelMatrix = translate(1.0f, -2.0f, -12.0f);
			glUniformMatrix4fv(model_matrix_uniform, 1, GL_FALSE, modelMatrix);
			glUniformMatrix4fv(view_matrix_uniform, 1, GL_FALSE, viewMatrix);
			glUniformMatrix4fv(projection_matrix_uniform, 1, GL_FALSE, gPerspectiveProjectionMatrix);

			glBindVertexArray(gVao_sphere);
			glUniform3fv(Material_ambient_uniform, 1, Material_Ambient_Array[y + 21]);
			glUniform3fv(Material_diffuse_uniform, 1, Material_Diffuse_Array[y + 21]);
			glUniform3fv(Material_specular_uniform, 1, Material_Specular_Array[y + 21]);
			glUniform1f(material_shininess_uniform, Material_Shine_Array[y + 21]);
			glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
			glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);
			glBindVertexArray(0);


			modelMatrix = translate(-3.5f, -2.0f, -12.0f);
			glUniformMatrix4fv(model_matrix_uniform, 1, GL_FALSE, modelMatrix);
			glUniformMatrix4fv(view_matrix_uniform, 1, GL_FALSE, viewMatrix);
			glUniformMatrix4fv(projection_matrix_uniform, 1, GL_FALSE, gPerspectiveProjectionMatrix);

			glBindVertexArray(gVao_sphere);
			glUniform3fv(Material_ambient_uniform, 1, Material_Ambient_Array[y + 22]);
			glUniform3fv(Material_diffuse_uniform, 1, Material_Diffuse_Array[y + 22]);
			glUniform3fv(Material_specular_uniform, 1, Material_Specular_Array[y + 22]);
			glUniform1f(material_shininess_uniform, Material_Shine_Array[y + 22]);
			glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
			glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);
			glBindVertexArray(0);


			modelMatrix = translate(-1.0f, -2.0f, -12.0f);
			glUniformMatrix4fv(model_matrix_uniform, 1, GL_FALSE, modelMatrix);
			glUniformMatrix4fv(view_matrix_uniform, 1, GL_FALSE, viewMatrix);
			glUniformMatrix4fv(projection_matrix_uniform, 1, GL_FALSE, gPerspectiveProjectionMatrix);

			glBindVertexArray(gVao_sphere);
			glUniform3fv(Material_ambient_uniform, 1, Material_Ambient_Array[y + 23]);
			glUniform3fv(Material_diffuse_uniform, 1, Material_Diffuse_Array[y + 23]);
			glUniform3fv(Material_specular_uniform, 1, Material_Specular_Array[y + 23]);
			glUniform1f(material_shininess_uniform, Material_Shine_Array[y + 23]);
			glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
			glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);
			glBindVertexArray(0);

		}
	

	glUseProgram(0);
	SwapBuffers(ghdc);
}

void uninitialize(void)
{
	//UNINITIALIZATION CODE
	if (gbFullscreen == true)
	{
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED);

		ShowCursor(TRUE);

	}

	if (gVao_sphere)
	{
		glDeleteVertexArrays(1, &gVao_sphere);
		gVao_sphere = 0;
	}

	if (gVbo_sphere_position)
	{
		glDeleteBuffers(1, &gVbo_sphere_position);
		gVbo_sphere_position = 0;
	}


	if (gVbo_sphere_normal)
	{
		glDeleteBuffers(1, &gVbo_sphere_normal);
		gVbo_sphere_normal = 0;
	}

	if (gVbo_sphere_element)
	{
		glDeleteBuffers(1, &gVbo_sphere_element);
		gVbo_sphere_element = 0;
	}

	glDetachShader(gShaderProgramObject, gVertexShaderObject);
	glDetachShader(gShaderProgramObject, gFragmentShaderObject);

	glDeleteShader(gVertexShaderObject);
	gVertexShaderObject = 0;

	glDeleteShader(gFragmentShaderObject);
	gFragmentShaderObject = 0;

	glDeleteProgram(gShaderProgramObject);
	gShaderProgramObject = 0;

	glUseProgram(0);

	wglMakeCurrent(NULL, NULL);

	wglDeleteContext(ghrc);
	ghrc = NULL;

	ReleaseDC(ghwnd, ghdc);
	ghdc = NULL;

	DestroyWindow(ghwnd);
	ghwnd = NULL;

	if (gpFile)
	{
		fprintf(gpFile, "Log File is Successfully closed.");
		fclose(gpFile);
		gpFile = NULL;
	}
}