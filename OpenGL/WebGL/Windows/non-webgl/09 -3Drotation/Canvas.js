//3D Rotation...
//add more vertices n colors
//draw seperate color for cube..instead vertexAttrib3
//silly mistake...rotation rotateX, rotateY n rotateZ are different functions :P

// global variables
var canvas=null;
var gl=null; // webgl context
var bFullscreen=false;
var canvas_original_width;
var canvas_original_height;

const WebGLMacros= // when whole 'WebGLMacros' is 'const', all inside it are automatically 'const'
{
dpk_ATTRIBUTE_VERTEX:0,
dpk_ATTRIBUTE_COLOR:1,
dpk_ATTRIBUTE_NORMAL:2,
dpk_ATTRIBUTE_TEXTURE0:3,
};

var vertexShaderObject;
var fragmentShaderObject;
var shaderProgramObject;
//triangle
var vao;
var vbo;
var vbocolor
	
	//cube
	var vaoCube;
	var vboCube;
	var vboCubeColor;

var mvpUniform;

var orthographicProjectionMatrix;
var perspectiveProjectionMatrix;

var T=0.0;
var C=0.0;

var degrees;

// To start animation : To have requestAnimationFrame() to be called "cross-browser" compatible
var requestAnimationFrame =
window.requestAnimationFrame ||
window.webkitRequestAnimationFrame ||
window.mozRequestAnimationFrame ||
window.oRequestAnimationFrame ||
window.msRequestAnimationFrame;

// To stop animation : To have cancelAnimationFrame() to be called "cross-browser" compatible
var cancelAnimationFrame =
window.cancelAnimationFrame ||
window.webkitCancelRequestAnimationFrame || window.webkitCancelAnimationFrame ||
window.mozCancelRequestAnimationFrame || window.mozCancelAnimationFrame ||
window.oCancelRequestAnimationFrame || window.oCancelAnimationFrame ||
window.msCancelRequestAnimationFrame || window.msCancelAnimationFrame;

								// onload function
								function main()
								{
												    // get <canvas> element
												    canvas = document.getElementById("AMC");
												    if(!canvas)
												        console.log("Obtaining Canvas Failed\n");
												    else
												        console.log("Obtaining Canvas Succeeded\n");
												    canvas_original_width=canvas.width;
												    canvas_original_height=canvas.height;
												    
												    // register keyboard's keydown event handler
												    window.addEventListener("keydown", keyDown, false);
												    window.addEventListener("click", mouseDown, false);
												    window.addEventListener("resize", resize, false);

												    // initialize WebGL
												    init();
												    
												    // start drawing here as warming-up
												    resize();
												    draw();
								}

function toggleFullScreen()
{
					    // code
					    var fullscreen_element =
					    document.fullscreenElement ||
					    document.webkitFullscreenElement ||
					    document.mozFullScreenElement ||
					    document.msFullscreenElement ||
					    null;

					    // if not fullscreen
					    if(fullscreen_element==null)
					    {
					        if(canvas.requestFullscreen)
					            canvas.requestFullscreen();
					        else if(canvas.mozRequestFullScreen) // S is capital
					            canvas.mozRequestFullScreen();
					        else if(canvas.webkitRequestFullscreen)
					            canvas.webkitRequestFullscreen();
					        else if(canvas.msRequestFullscreen)
					            canvas.msRequestFullscreen();
					        bFullscreen=true;
					    }
					    else // if already fullscreen
					    {
					        if(document.exitFullscreen)
					            document.exitFullscreen();
					        else if(document.mozCancelFullScreen)
					            document.mozCancelFullScreen();
					        else if(document.webkitExitFullscreen)
					            document.webkitExitFullscreen();
					        else if(document.msExitFullscreen)
					            document.msExitFullscreen();
					        bFullscreen=false;
					    }
}

function init()
{
			    // get WebGL 2.0 context
			    gl = canvas.getContext("webgl2");
			    if(gl==null) // failed to get context
			    {
			        console.log("Failed to get the rendering context for WebGL");
			        return;
			    }
			    gl.viewportWidth = canvas.width;
			    gl.viewportHeight = canvas.height;
    
														    // vertex shader
														//source
															var vertexShaderSourceCode=
														    "#version 300 es"+
														    "\n"+
														    "in vec4 vPosition;"+
														    "in vec4 vColor;"+
															"out vec4 fragColor;"+
																"uniform mat4 u_mvp_matrix;"+
														    "void main(void)"+
														    "{"+
														    "gl_Position = u_mvp_matrix * vPosition;"+
																"fragColor=vColor;"+
														    "}";
															//create
														    vertexShaderObject=gl.createShader(gl.VERTEX_SHADER);
														    gl.shaderSource(vertexShaderObject,vertexShaderSourceCode);
														    //compile
															gl.compileShader(vertexShaderObject);
														    //error checking
															if(gl.getShaderParameter(vertexShaderObject,gl.COMPILE_STATUS)==false)
														    {
														        var error=gl.getShaderInfoLog(vertexShaderObject);
														        if(error.length > 0)
														        {
														            alert(error);
														            uninitialize();
														        }
														    }
														    
																									    // fragment shader
																									    var fragmentShaderSourceCode=
																									    "#version 300 es"+
																									    "\n"+
																									    "precision highp float;"+
																									    "in vec4 fragColor;"+
																											"out vec4 FragColor;"+
																									    "void main(void)"+
																									    "{"+
																									    "FragColor = fragColor;"+
																									    "}"
																									    fragmentShaderObject=gl.createShader(gl.FRAGMENT_SHADER);
																									    gl.shaderSource(fragmentShaderObject,fragmentShaderSourceCode);
																									    gl.compileShader(fragmentShaderObject);
																									    if(gl.getShaderParameter(fragmentShaderObject,gl.COMPILE_STATUS)==false)
																									    {
																									        var error=gl.getShaderInfoLog(fragmentShaderObject);
																									        if(error.length > 0)
																									        {
																									            alert(error);
																									            uninitialize();
																									        }
																									    }
    
									    // shader program
									    shaderProgramObject=gl.createProgram();
									    gl.attachShader(shaderProgramObject,vertexShaderObject);
									    gl.attachShader(shaderProgramObject,fragmentShaderObject);
									    
									    // pre-link binding of shader program object with vertex shader attributes
									    gl.bindAttribLocation(shaderProgramObject,WebGLMacros.dpk_ATTRIBUTE_VERTEX,"vPosition");
									  gl.bindAttribLocation(shaderProgramObject,WebGLMacros.dpk_ATTRIBUTE_COLOR,"vColor");
									    
									    // linking
									    gl.linkProgram(shaderProgramObject);
									    if (!gl.getProgramParameter(shaderProgramObject, gl.LINK_STATUS))
									    {
									        var error=gl.getProgramInfoLog(shaderProgramObject);
									        if(error.length > 0)
									        {
									            alert(error);
									            uninitialize();
									        }
									    }

    // get MVP uniform location
    mvpUniform=gl.getUniformLocation(shaderProgramObject,"u_mvp_matrix");
    
    // *** vertices, colors, shader attribs, vbo, vao initializations ***
    var triangleVertices=new Float32Array([
                                           0.0,  1.0, 0.0,   // appex
                                           -1.0, -1.0, 1.0, // left-bottom
                                           1.0, -1.0, 1.0,   // right-bottom
																									0.0,1.0,0.0,
																									1.0,-1.0,1.0,
																									1.0,-1.0,-1.0,
																									
														0.0,1.0,0.0,
														1.0,-1.0,-1.0,
														-1.0,-1.0,-1.0,
														
										0.0,1.0,0.0,
										-1.0,-1.0,-1.0,
										-1.0,-1.0,1.0
										
										]);

	    var triangleColor=new Float32Array([
                              
																			1.0, 0.0, 0.0,
																			0.0, 1.0, 0.0,
																			0.0, 0.0, 1.0,

																						1.0, 0.0, 0.0,
																						0.0, 0.0,1.0,
																						0.0, 1.0, 0.0,

																								1.0, 0.0, 0.0,
																								0.0, 1.0, 0.0,
																								0.0, 0.0, 1.0,

																																	1.0, 0.0, 0.0,
																																	0.0, 0.0,1.0,
																																	0.0, 1.0, 0.0

		]);

//riangle vao
															    vao=gl.createVertexArray();
															    gl.bindVertexArray(vao);
															    
															    vbo = gl.createBuffer();
															    gl.bindBuffer(gl.ARRAY_BUFFER,vbo);
															    gl.bufferData(gl.ARRAY_BUFFER,triangleVertices,gl.STATIC_DRAW);
															    gl.vertexAttribPointer(WebGLMacros.dpk_ATTRIBUTE_VERTEX,
															                           3, // 3 is for X,Y,Z co-ordinates in our triangleVertices array
															                           gl.FLOAT,
															                           false,0,0);
															    gl.enableVertexAttribArray(WebGLMacros.dpk_ATTRIBUTE_VERTEX);
															    gl.bindBuffer(gl.ARRAY_BUFFER,null);
																//gl.bindVertexArray(null);

//color
																    vbocolor = gl.createBuffer();
															    gl.bindBuffer(gl.ARRAY_BUFFER,vbocolor);
															    gl.bufferData(gl.ARRAY_BUFFER,triangleColor,gl.STATIC_DRAW);
															    gl.vertexAttribPointer(WebGLMacros.dpk_ATTRIBUTE_COLOR,
															                           3, // 3 is for X,Y,Z co-ordinates in our triangleVertices array
															                           gl.FLOAT,
															                           false,0,0);
															    gl.enableVertexAttribArray(WebGLMacros.dpk_ATTRIBUTE_COLOR);
															    gl.bindBuffer(gl.ARRAY_BUFFER,null);
															//gl.bindVertexArray(null);

//cube
							var cubeVertices=new Float32Array([
									// top surface
								1.0 , 1.0 , -1.0 ,  // top-right o  top
								-1.0 , 1.0 , -1.0 , // top-le t o  top
								-1.0 , 1.0 , 1.0 , // bottom-le t o  top
								1.0 , 1.0 , 1.0 ,  // bottom-right o  top

												   // bottom sur ace
												   1.0 , -1.0 , 1.0 ,  // top-right o  bottom
												   -1.0 , -1.0 , 1.0 , // top-le t o  bottom
												   -1.0 , -1.0 , -1.0 , // bottom-le t o  bottom
												   1.0 , -1.0 , -1.0 ,  // bottom-right o  bottom

																		//  ront sur ace
																		1.0 , 1.0 , 1.0 ,  // top-right o   ront
																		-1.0 , 1.0 , 1.0 , // top-le t o   ront
																		-1.0 , -1.0 , 1.0 , // bottom-le t o   ront
																		1.0 , -1.0 , 1.0 ,  // bottom-right o   ront

																							// back sur ace
																							1.0 , -1.0 , -1.0 ,  // top-right o  back
																							-1.0 , -1.0 , -1.0 , // top-le t o  back
																							-1.0 , 1.0 , -1.0 , // bottom-le t o back
																							1.0 , 1.0 , -1.0 ,  // bottom-right o back

																												// le t sur ace
																												-1.0 , 1.0 , 1.0 , // top-right o le t
																												-1.0 , 1.0 , -1.0 , // top-left of le t
																												-1.0 , -1.0 , -1.0 , // bottom-le t o  le t
																												-1.0 , -1.0 , 1.0 , // bottom-right o  le t

																																	// right sur ace
																																	1.0 , 1.0 , -1.0 ,  // top-right o  right
																																	1.0 , 1.0 , 1.0 ,  // top-le t o  right
																																	1.0 , -1.0 , 1.0 ,  // bottom-le t o  right
																																	1.0 , -1.0 , -1.0 ,  // bottom-right of right

								]);
							
							var cubeColor=new Float32Array([
							1.0, 0.0, 0.0,  1.0, 0.0, 0.0,  1.0, 0.0, 0.0,  1.0, 0.0, 0.0,//t
							0.0, 1.0, 0.0,  0.0, 1.0, 0.0,  0.0, 1.0, 0.0,  0.0, 1.0, 0.0,//f
							0.0, 0.0, 1.0,   0.0, 0.0, 1.0,  0.0, 0.0, 1.0,  0.0, 0.0, 1.0,//l
							 1.0, 1.0, 0.0,  1.0, 1.0, 0.0,  1.0, 1.0, 0.0,  1.0, 1.0, 0.0,//r
							  0.0, 1.0, 1.0,  0.0, 1.0, 1.0,  0.0, 1.0, 1.0,  0.0, 1.0, 1.0,//t
							1.0, 0.0, 1.0,   1.0, 0.0, 1.0,  1.0, 0.0, 1.0,  1.0, 0.0, 1.0,//b
										
								]);
							
							
							vaoCube=gl.createVertexArray();
							gl.bindVertexArray(vaoCube);
							
							
							vboCube=gl.createBuffer();
							gl.bindBuffer(gl.ARRAY_BUFFER,vboCube);
							gl.bufferData(gl.ARRAY_BUFFER,cubeVertices,gl.STATIC_DRAW);
							gl.vertexAttribPointer(WebGLMacros.dpk_ATTRIBUTE_VERTEX,
						                           3, // 3 is for X,Y,Z co-ordinates in our triangleVertices array
						                           gl.FLOAT,
						                           false,0,0);
						    gl.enableVertexAttribArray(WebGLMacros.dpk_ATTRIBUTE_VERTEX);
						    gl.bindBuffer(gl.ARRAY_BUFFER,null);
							
							//cube color
						//	gl.vertexAttrib3f(WebGLMacros.dpk_ATTRIBUTE_COLOR,0.392,0.584,0.929);
								//color
							    vboCubeColor = gl.createBuffer();
						    gl.bindBuffer(gl.ARRAY_BUFFER,vboCubeColor);
						    gl.bufferData(gl.ARRAY_BUFFER,cubeColor,gl.STATIC_DRAW);
						    gl.vertexAttribPointer(WebGLMacros.dpk_ATTRIBUTE_COLOR,
						                           3, // 3 is for X,Y,Z co-ordinates in our triangleVertices array
						                           gl.FLOAT,
						                           false,0,0);
						    gl.enableVertexAttribArray(WebGLMacros.dpk_ATTRIBUTE_COLOR);
						    gl.bindBuffer(gl.ARRAY_BUFFER,null);

							//
						gl.bindVertexArray(null);

	//required...otherwise shows transperent cubes
gl.clearDepth(1.0);
	gl.enable(gl.DEPTH_TEST);
	//gl.depthFunc(gl.LEQUAL);
	
	// set clear color
    gl.clearColor(0.0, 0.0, 0.0, 1.0); // blue
    
    // initialize projection matrix
    orthographicProjectionMatrix=mat4.create();
	perspectiveProjectionMatrix=mat4.create();
}

function resize()
{
    // code
    if(bFullscreen==true)
    {
        canvas.width=window.innerWidth;
        canvas.height=window.innerHeight;
    }
    else
    {
        canvas.width=canvas_original_width;
        canvas.height=canvas_original_height;
    }
   
    // set the viewport to match
    gl.viewport(0, 0, canvas.width, canvas.height);
    
    // Orthographic Projection => left,right,bottom,top,near,far
    if (canvas.width <= canvas.height)
			mat4.perspective(perspectiveProjectionMatrix,45,(parseFloat)(canvas.height)/(parseFloat)(canvas/width),0.1,100.0);   
    else
		mat4.perspective(perspectiveProjectionMatrix,45,(parseFloat)(canvas.width)/(parseFloat)(canvas.height),0.1,100.0);   
			
}

function degreeToRadian()
{
				T=T+0.05;
				if(T>=360.0)
					T=0;
				
				C=C+0.05;
				if(C>=360.0)
					C=0;
	}

													function draw()
													{
													    // code
																    gl.clear(gl.COLOR_BUFFER_BIT);
																    
																    gl.useProgram(shaderProgramObject);
																    
																    var modelViewMatrix=mat4.create();
																    var modelViewProjectionMatrix=mat4.create();
																	var translateMatrix=mat4.create();
																	var rotateMatrix=mat4.create();

																	
																												mat4.identity(modelViewMatrix);
																											mat4.identity(modelViewProjectionMatrix);
																											mat4.identity(translateMatrix);
																											mat4.identity(rotateMatrix);

																											//	translate triangle
																											mat4.translate(modelViewMatrix,modelViewMatrix,[-1.5,0.0,-5.0]);
																											mat4.multiply(modelViewMatrix,modelViewMatrix,translateMatrix);

																										//rotate
																											
																											mat4.rotateY(rotateMatrix,rotateMatrix,T,[1.0,1.0,1.0]);
																											mat4.rotateX(rotateMatrix,rotateMatrix,T,[1.0,1.0,1.0]);
																											mat4.rotateZ(rotateMatrix,rotateMatrix,T,[1.0,1.0,1.0]);

																											mat4.multiply(modelViewMatrix,modelViewMatrix,rotateMatrix);

																												mat4.multiply(modelViewProjectionMatrix,perspectiveProjectionMatrix,modelViewMatrix);
																											gl.uniformMatrix4fv(mvpUniform,false,modelViewProjectionMatrix);

																										    gl.bindVertexArray(vao);
																										    gl.drawArrays(gl.TRIANGLES,0,3);
																										    gl.drawArrays(gl.TRIANGLES,3,3);
																										    gl.drawArrays(gl.TRIANGLES,6,3);
																										    gl.drawArrays(gl.TRIANGLES,9,3);

																											
																											gl.bindVertexArray(null);
																										  
																//cube bind
																	mat4.identity(modelViewMatrix);
																	mat4.identity(modelViewProjectionMatrix);
																	mat4.identity(translateMatrix);
																	mat4.identity(rotateMatrix);

																		mat4.translate(modelViewMatrix,modelViewMatrix,[1.0,0.0,-5.0]);
																	mat4.multiply(modelViewMatrix,modelViewMatrix,translateMatrix);

																//rotate
																	mat4.rotateX(rotateMatrix,rotateMatrix,C,[0.0,0.0,1.0]);
																	//mat4.rotateY(rotateMatrix,rotateMatrix,C,[0.0,1.0,0.0]);

																	mat4.multiply(modelViewMatrix,modelViewMatrix,rotateMatrix);

																	mat4.multiply(modelViewProjectionMatrix,perspectiveProjectionMatrix,modelViewMatrix);
																	gl.uniformMatrix4fv(mvpUniform,false,modelViewProjectionMatrix);

																	gl.bindVertexArray(vaoCube);
																    gl.drawArrays(gl.TRIANGLE_FAN,0,4);
																    gl.drawArrays(gl.TRIANGLE_FAN,4,4);
																    gl.drawArrays(gl.TRIANGLE_FAN,8,4);
																    gl.drawArrays(gl.TRIANGLE_FAN,12,4);
																    gl.drawArrays(gl.TRIANGLE_FAN,16,4);
																    gl.drawArrays(gl.TRIANGLE_FAN,24,4);

																	gl.bindVertexArray(null);

															
																	gl.useProgram(null);
																    
																	degreeToRadian();
																    // animation loop
																    requestAnimationFrame(draw, canvas);
													}

function uninitialize()
{
    // code
    if(vao)
    {
        gl.deleteVertexArray(vao);
        vao=null;
    }
    
    if(vbo)
    {
        gl.deleteBuffer(vbo);
        vbo=null;
    }
    
    if(shaderProgramObject)
    {
        if(fragmentShaderObject)
        {
            gl.detachShader(shaderProgramObject,fragmentShaderObject);
            gl.deleteShader(fragmentShaderObject);
            fragmentShaderObject=null;
        }
        
        if(vertexShaderObject)
        {
            gl.detachShader(shaderProgramObject,vertexShaderObject);
            gl.deleteShader(vertexShaderObject);
            vertexShaderObject=null;
        }
        
        gl.deleteProgram(shaderProgramObject);
        shaderProgramObject=null;
    }
}

function keyDown(event)
{
    // code
    switch(event.keyCode)
    {
        case 27: // Escape
            // uninitialize
            uninitialize();
            // close our application's tab
            window.close(); // may not work in Firefox but works in Safari and chrome
            break;
        case 70: // for 'F' or 'f'
            toggleFullScreen();
            break;
    }
}

function mouseDown()
{
    // code
}
