//
//  MyView.m
//  Material-Properties
//
//  Created by Manish Baing on 18/06/18.
//
#import <OpenGLES/ES3/gl.h>
#import <OpenGLES/ES3/glext.h>

#import "vmath.h"

#import "MyView.h"

#import "sphere.h"
#import "material.h"

#define PI            (3.14151415f)
//24 Sphere drawing margins and gap
#define columnNumber     4
#define rowNumber     6

enum
{
    VDG_ATTRIBUTE_POSITION = 0,
    VDG_ATTRIBUTE_COLOR,
    VDG_ATTRIBUTE_NORMAL,
    VDG_ATTRIBUTE_TEXTURE0,
};

GLfloat gbAngle = 0.0f;

//light
GLfloat lightAmbient00[] = { 0.0f, 0.0f, 0.0f, 1.0f };
GLfloat lightDiffuse00[] = { 1.0f, 1.0f, 1.0f, 1.0f };
GLfloat lightSpec00[] = { 1.0f, 1.0f, 1.0f, 1.0f };
GLfloat lightPosition00[] = { 0.0f, 0.0f, 0.0f, 0.0f };

//material default
GLfloat materialAmbient[] = { 0.0f, 0.0f,0.0f,1.0f };
GLfloat materialDiffuse[] = { 1.0f,1.0f,1.0f,1.0f };
GLfloat materialSpecular[] = { 1.0f, 1.0f,1.0f,1.0f };
GLfloat materialShininess = 50.0f;


//Sphere declarations
GLuint gNumElements;
GLuint gNumVertices;
GLfloat sphere_vertices[1146];
GLfloat sphere_normals[1146];
GLfloat sphere_textures[764];
GLshort sphere_elements[2280];

@implementation MyView
{
@private
    EAGLContext *eaglContext;
    
    GLuint defaultFrameBuffer;
    GLuint colorRenderbuffer;
    GLuint depthRenderbuffer;
    
    id displayLink;
    NSInteger animationFrameInterval;
    BOOL isAnimating;
    
    Sphere *sphereObject;
    
    //your shader variables
    GLuint perFragVertexShaderObject;
    GLuint perFragFragmentShaderObject;
    GLuint perFragshaderProgramObject;
    
    /*************
     Dont Keep Un-used enums in this code,
     else vao, vbo, uniform arrays will be populated unnecessarly
     ***************/
    enum eShapes            // <--- Modify as per requirement
    {
        SPHERE = 0,
        NR_SHAPES,
    };
    enum eShape_Attr
    {
        POSITION = 0,
        NORMAL,
        ELEMENTS,
        NR_SHAPE_ATTRS,
    };
    enum eUniforms
    {
        MV = 0,
        PROJECTION,
        u_lightDiffuse,
        u_lightAmbient,
        u_lightSpecular,
        KA,
        KD,
        KS,
        SHININESS,
        LIGHT_POS,
        LIGHT_EN,
        NR_UNIFORMS,
    };
    
    GLuint vao[NR_SHAPES];
    GLuint vbo[NR_SHAPES][NR_SHAPE_ATTRS];
    GLuint uniform[NR_UNIFORMS];
    
    BOOL bLighting_enable;
    
    BOOL xRotation;
    BOOL yRotation;
    BOOL zRotation;
    
    vmath::mat4 projectonMatrix;
}
/*
 // Only override drawRect: if you perform custom drawing.
 // An empty implementation adversely affects performance during animation.
 - (void)drawRect:(CGRect)rect {
 // Drawing code
 }
 */

-(id)initWithFrame:(CGRect)frame
{
    //code
    self = [super initWithFrame:frame];
    
    if(self)
    {
        CAEAGLLayer *eaglLayer = (CAEAGLLayer*)super.layer;
        
        eaglLayer.opaque = YES;
        eaglLayer.drawableProperties = [NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithBool:FALSE], kEAGLDrawablePropertyRetainedBacking, kEAGLColorFormatRGBA8, kEAGLDrawablePropertyColorFormat, nil];
        
        eaglContext = [[EAGLContext alloc] initWithAPI:kEAGLRenderingAPIOpenGLES3]; //EGL 3.0
        if(eaglContext == nil)
        {
            [self release];
            return (nil);
        }
        
        [EAGLContext setCurrentContext:eaglContext];
        
        glGenFramebuffers(1, &defaultFrameBuffer);
        glGenRenderbuffers(1, &colorRenderbuffer);
        glBindFramebuffer(GL_FRAMEBUFFER, defaultFrameBuffer);
        glBindRenderbuffer(GL_RENDERBUFFER, colorRenderbuffer);
        
        [eaglContext renderbufferStorage:GL_RENDERBUFFER fromDrawable:eaglLayer];
        
        glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_RENDERBUFFER, colorRenderbuffer);
        
        GLint backingWidth;
        GLint backingHeight;
        glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_WIDTH, &backingWidth);
        glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_HEIGHT, &backingHeight);
        
        glGenRenderbuffers(1, &depthRenderbuffer);
        glBindRenderbuffer(GL_RENDERBUFFER, depthRenderbuffer);
        glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT16, backingWidth, backingHeight);
        glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, depthRenderbuffer);
        
        if(glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
        {
            printf("initWithFrame: Failed To Create Complete Framebuffer Object 0x%x\n", glCheckFramebufferStatus(GL_FRAMEBUFFER));
            glDeleteFramebuffers(1, &defaultFrameBuffer);
            glDeleteRenderbuffers(1, &colorRenderbuffer);
            glDeleteRenderbuffers(1, &depthRenderbuffer);
            [self release];
            return (nil);
        }
        
        printf("Renderer : %s | GL Version : %s | GLSL Version : %s\n", glGetString(GL_RENDERER), glGetString(GL_VERSION), glGetString(GL_SHADING_LANGUAGE_VERSION));
        
        //hard coded initializations
        isAnimating = NO;
        animationFrameInterval = 60;    //its default since iOS 8.2
        //=================
        
        //Do OpenGL Initialization here
  
        
        /*****VERTEX SHADER********/
        perFragVertexShaderObject = glCreateShader(GL_VERTEX_SHADER);
        const GLchar* vsSource =
        "#version 300 es" \
        "\n" \
        "in vec4 vPosition;" \
        "in vec3 vNormal;" \
        "uniform mat4 u_model_view_matrix;" \
        "uniform mat4 u_projection_matrix;" \
        "uniform mediump int u_light_enabled;" \
        "uniform vec4 u_light_position;" \
        "out vec3 transformed_normals;" \
        "out vec3 light_direction;" \
        "out vec3 viewer_vector;" \
        "void main(void)" \
        "{" \
        "if(u_light_enabled==1)" \
        "{" \
        "vec4 eyeCoordinates = u_model_view_matrix * vPosition;" \
        "transformed_normals = mat3(u_model_view_matrix) * vNormal;" \
        "light_direction = vec3(u_light_position) - eyeCoordinates.xyz;" \
        "viewer_vector = -eyeCoordinates.xyz;" \
        "}"\
        "gl_Position = u_projection_matrix * u_model_view_matrix * vPosition;" \
        "}";
        
        
        glShaderSource(perFragVertexShaderObject, 1, (const GLchar**)&vsSource, NULL);
        glCompileShader(perFragVertexShaderObject);
        GLint iShaderCompileStatus = 0;
        glGetShaderiv(perFragVertexShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);
        if(GL_FALSE == iShaderCompileStatus)
        {
            GLsizei iLogLen = 0;
            glGetShaderiv(perFragVertexShaderObject, GL_INFO_LOG_LENGTH, &iLogLen);
            if(iLogLen)
            {
                GLchar* szLog = (GLchar*)malloc(iLogLen);
                if(szLog)
                {
                    GLsizei written;
                    glGetShaderInfoLog(perFragVertexShaderObject, iLogLen, &written, szLog);
                    printf("Vertex Shader Compile Info: %s\n", szLog);
                    free(szLog);
                    [self release];
                    
                }
            }
        }//================
        
        /*****FRAGMENT SHADER********/
        perFragFragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);
        const GLchar* fsSource =
        "#version 300 es" \
        "\n" \
        "precision highp float;"
        "in vec3 transformed_normals;" \
        "in vec3 light_direction;" \
        "in vec3 viewer_vector;" \
        "uniform vec3 u_La;" \
        "uniform vec3 u_Ld;" \
        "uniform vec3 u_Ls;" \
        "uniform int u_light_enabled;" \
        "uniform vec3 u_Ka;" \
        "uniform vec3 u_Kd;" \
        "uniform vec3 u_Ks;" \
        "uniform float u_material_shininess;" \
        "out vec4 FragColor;" \
        "void main(void)" \
        "{" \
        "vec3 phong_ads_color; " \
        "if(u_light_enabled == 1)" \
        "{" \
        "vec3 normalized_transformed_normals = normalize(transformed_normals);" \
        "vec3 normalized_light_direction = normalize(light_direction);" \
        "vec3 normalized_viewer_vector = normalize(viewer_vector);" \
        "float tn_dot_ld = max(dot(normalized_transformed_normals, normalized_light_direction), 0.0);" \
        "vec3 ambient = u_La * u_Ka;" \
        "vec3 diffuse = u_Ld * u_Kd * tn_dot_ld;" \
        "vec3 reflection_vector = reflect(-normalized_light_direction, normalized_transformed_normals);" \
        "float r_dot_v = max(dot(reflection_vector, normalized_viewer_vector), 0.0);" \
        "vec3 specular = u_Ls * u_Ks * pow(r_dot_v, u_material_shininess);" \
        "phong_ads_color = ambient + diffuse + specular;" \
        "}" \
        "else" \
        "{" \
        "phong_ads_color = vec3(1.0, 1.0, 1.0);" \
        "}" \
        "FragColor = vec4(phong_ads_color, 1.0);" \
        "}";
        
        glShaderSource(perFragFragmentShaderObject, 1, (const GLchar**)&fsSource, NULL);
        glCompileShader(perFragFragmentShaderObject);
        iShaderCompileStatus = 0;
        glGetShaderiv(perFragFragmentShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);
        if(GL_FALSE == iShaderCompileStatus)
        {
            GLsizei iLogLen = 0;
            glGetShaderiv(perFragFragmentShaderObject, GL_INFO_LOG_LENGTH, &iLogLen);
            if(iLogLen)
            {
                GLchar* szLog = (GLchar*)malloc(iLogLen);
                if(szLog)
                {
                    GLsizei written;
                    glGetShaderInfoLog(perFragFragmentShaderObject, iLogLen, &written, szLog);
                    printf("Fragment Shader Compile Info: %s\n", szLog);
                    free(szLog);
                    [self release];
                    
                }
            }
        }//===================
        
        /*****SHADER PROGRAM********/
        perFragshaderProgramObject = glCreateProgram();
        //attach vs and fs to shader program
        glAttachShader(perFragshaderProgramObject, perFragVertexShaderObject);
        glAttachShader(perFragshaderProgramObject, perFragFragmentShaderObject);
        
        //pre-link binding of vertex attributes
        glBindAttribLocation(perFragshaderProgramObject, VDG_ATTRIBUTE_POSITION, "vPosition");     // <--- Modify/add as per requirement
        glBindAttribLocation(perFragshaderProgramObject, VDG_ATTRIBUTE_NORMAL, "vNormal");
        
        //link program
        glLinkProgram(perFragshaderProgramObject);
        
        //post-link, get uniform locations from shaders
        uniform[MV] = glGetUniformLocation(perFragshaderProgramObject, "u_model_view_matrix");           // <--- Modify/add as per requirement
        uniform[PROJECTION] = glGetUniformLocation(perFragshaderProgramObject, "u_projection_matrix");
        uniform[u_lightDiffuse] = glGetUniformLocation(perFragshaderProgramObject, "u_La");
        uniform[u_lightAmbient] = glGetUniformLocation(perFragshaderProgramObject, "u_Ld");
        uniform[u_lightSpecular] = glGetUniformLocation(perFragshaderProgramObject, "u_Ls");
        uniform[KA] = glGetUniformLocation(perFragshaderProgramObject, "u_Ka");
        uniform[KD] = glGetUniformLocation(perFragshaderProgramObject, "u_Kd");
        uniform[KS] = glGetUniformLocation(perFragshaderProgramObject, "u_Ks");
        uniform[SHININESS] = glGetUniformLocation(perFragshaderProgramObject, "u_material_shininess");
        uniform[LIGHT_POS] = glGetUniformLocation(perFragshaderProgramObject, "u_light_position");
        uniform[LIGHT_EN] = glGetUniformLocation(perFragshaderProgramObject, "u_light_enabled");

        //load textures, if any
        //============
        
        /******* Vertices, colors, normals, vao, vbo initialization**********/
        //Sphere: objective c
        sphereObject = [[Sphere alloc] init];
        [sphereObject getSphereVertexData:sphere_vertices :sphere_normals :sphere_textures :sphere_elements];
        gNumVertices = [sphereObject getNumberOfSphereVertices];
        gNumElements = [sphereObject getNumberOfSphereElements];
        
        printf("nr vertices:%d\n", gNumVertices);
        printf("nr elements:%d\n", gNumElements);
        
        //VAO:Sphere
        glGenVertexArrays(1, &vao[SPHERE]);
        glBindVertexArray(vao[SPHERE]);
      
            glGenBuffers(1, &vbo[SPHERE][POSITION]);
            glBindBuffer(GL_ARRAY_BUFFER, vbo[SPHERE][POSITION]);
            glBufferData(GL_ARRAY_BUFFER, sizeof(sphere_vertices), sphere_vertices, GL_STATIC_DRAW);
            glVertexAttribPointer(VDG_ATTRIBUTE_POSITION,
                                  3, //xyz
                                  GL_FLOAT,
                                  GL_FALSE, //non-normalize
                                  0, NULL); //0=stride
            glEnableVertexAttribArray(VDG_ATTRIBUTE_POSITION);
            glBindBuffer(GL_ARRAY_BUFFER, 0);   //unbind
      
        glGenBuffers(1, &vbo[SPHERE][NORMAL]);
            glBindBuffer(GL_ARRAY_BUFFER, vbo[SPHERE][NORMAL]);
            glBufferData(GL_ARRAY_BUFFER, sizeof(sphere_normals), sphere_normals, GL_STATIC_DRAW);
            glVertexAttribPointer(VDG_ATTRIBUTE_NORMAL,
                                  3, //nx, ny, nz
                                  GL_FLOAT,
                                  GL_FALSE, //non-normalize
                                  0, NULL); //0=stride
            glEnableVertexAttribArray(VDG_ATTRIBUTE_NORMAL);
            glBindBuffer(GL_ARRAY_BUFFER, 0);   //unbind
       
        glGenBuffers(1, &vbo[SPHERE][ELEMENTS]);
            glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo[SPHERE][ELEMENTS]);
            
            glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(sphere_elements), sphere_elements, GL_STATIC_DRAW);
            
            glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);   //unbind
       
        glBindVertexArray(0);   //unbind
        //==============
        
        //Other OpenGL settings
        glEnable(GL_DEPTH_TEST);    //enable depth testing
        glDepthFunc(GL_LEQUAL);     //depth test to do
        
        //glEnable(GL_CULL_FACE);     //We will always cull back faces for better performance
        
        //set background clearing color
        glClearColor(0.25f, 0.25f, 0.25f, 1.0f);   //gray
        
        //initialize ortho projectio matrix to identity
        projectonMatrix = vmath::mat4::identity();
        
        bLighting_enable = YES; //default light enabled
        xRotation = YES;   //default: light will rotate around x axis
        yRotation = NO;
        zRotation = NO;
        //================
        
        //Gesture Recognizer
        //Tap Gesture code
      
        
        UITapGestureRecognizer *singleTapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onSingleTap:)];
        [singleTapGestureRecognizer setNumberOfTapsRequired:1];
        
        [singleTapGestureRecognizer setNumberOfTouchesRequired:1];
        
        [singleTapGestureRecognizer setDelegate:self];
        [self addGestureRecognizer:singleTapGestureRecognizer];
        
      
        UITapGestureRecognizer *doubleTapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onDouleTap:)];
        [doubleTapGestureRecognizer setNumberOfTapsRequired:2];
        [doubleTapGestureRecognizer setNumberOfTouchesRequired:1];
        
        [doubleTapGestureRecognizer setDelegate:self];
        [self addGestureRecognizer:doubleTapGestureRecognizer];
        
      
        [singleTapGestureRecognizer requireGestureRecognizerToFail:doubleTapGestureRecognizer];
        

        UISwipeGestureRecognizer *swipeGestureRecognizer = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(onSwipe:)];
        [self addGestureRecognizer:swipeGestureRecognizer];
        
      
        UILongPressGestureRecognizer *longPressGestureRecognizer = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(onLongPress:)];
        [self addGestureRecognizer:longPressGestureRecognizer];
    }
    return (self);
}/*initWithFrame*/

-(void)initializePFShaderProgram
{
    }

+(Class)layerClass      //static method
{
    //code
    return ([CAEAGLLayer class]);   //return class which does animation
}

-(void)drawView:(id)sender  //display
{
    //code
    static GLfloat index = 0.0f;
    static GLint cicle_points = 10000;    //360
    
    [EAGLContext setCurrentContext:eaglContext];
    
    glBindFramebuffer(GL_FRAMEBUFFER, defaultFrameBuffer);
    
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);
    
    /*Your Drawing*/        // <--- Modify as per requirement
    
    glUseProgram(perFragshaderProgramObject);
    
    //SEND LIGHTING VALUES TO SHADER
    if(bLighting_enable == YES)
    {
        glUniform1i(uniform[LIGHT_EN], 1);
        
        //Red Light
        glUniform3fv(uniform[u_lightDiffuse], 1, lightAmbient00);
        glUniform3fv(uniform[u_lightAmbient], 1, lightDiffuse00);
        glUniform3fv(uniform[u_lightSpecular], 1, lightSpec00);
        
        if (xRotation == YES)
        {
            //Light move around x-axis
            lightPosition00[0] = 0.0f;
            lightPosition00[1] = 200.0f*cos(gbAngle);
            lightPosition00[2] = 200.0f*sin(gbAngle);
            glUniform4fv(uniform[LIGHT_POS], 1, lightPosition00);
        }
        else if(yRotation == YES)
        {
            //Light move around y-axis
            lightPosition00[0] = 200.0f*cos(gbAngle);
            lightPosition00[1] = 0.0f;
            lightPosition00[2] = 200.0f*sin(gbAngle);
            glUniform4fv(uniform[LIGHT_POS], 1, lightPosition00);
        }
        else
        {
            //Light move around z-axis
            lightPosition00[0] = 200.0f*cos(gbAngle);
            lightPosition00[1] = 200.0f*sin(gbAngle);
            lightPosition00[2] = 0.0f;
            glUniform4fv(uniform[LIGHT_POS], 1, lightPosition00);
        }
        
        glUniform3fv(uniform[KA], 1, materialAmbient);
        glUniform3fv(uniform[KD], 1, materialDiffuse);
        glUniform3fv(uniform[KS], 1, materialSpecular);
        glUniform1f(uniform[SHININESS], materialShininess);
    }
    else
    {
        glUniform1i(uniform[LIGHT_EN], 0);
    }
    //==========
    
    for (int cl = 0; cl < columnNumber; cl++)
    {
        for (int rw = 0; rw < rowNumber; rw++)
        {
            //01-Set ModelVeiw and ModelViewProjection matrices to identity
            vmath::mat4 modelViewMatrix = vmath::mat4::identity();
            
            //Keeping each sphere in 24 separate viewports
            GLint width;
            GLint height;
            
            glBindRenderbuffer(GL_RENDERBUFFER, colorRenderbuffer);
            [eaglContext renderbufferStorage:GL_RENDERBUFFER fromDrawable:(CAEAGLLayer*)self.layer];
            glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_WIDTH, &width);
            glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_HEIGHT, &height);
            
            glGenRenderbuffers(1, &depthRenderbuffer);
            glBindRenderbuffer(1, depthRenderbuffer);
            glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT16, width, height);
            glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, depthRenderbuffer);
            
            glViewport(0, 0, width, height);
            
            GLfloat fwidth = (GLfloat)width;
            GLfloat fheight = (GLfloat)height;
            
           
            GLfloat vWidth = (fwidth/columnNumber);
            GLfloat vHeight = (fheight/rowNumber);
            
            GLfloat xPosition = cl * vWidth;
            GLfloat yPosition = rw * vHeight;
            glViewport(xPosition, yPosition, vWidth, vHeight);
          
            projectonMatrix = vmath::perspective(45.0f, vWidth/vHeight, 0.1f, 100.0f);
            
            
            modelViewMatrix = vmath::translate(0.0f, 0.0f, -2.0f);
            
            
            glUniformMatrix4fv(uniform[MV], 1, GL_FALSE, modelViewMatrix);
            glUniformMatrix4fv(uniform[PROJECTION], 1, GL_FALSE, projectonMatrix);
            
            glUniform3fv(uniform[KA], 1, material_ambient[cl][rw]);
            glUniform3fv(uniform[KD], 1, material_diffuse[cl][rw]);
            glUniform3fv(uniform[KS], 1, material_specular[cl][rw]);
            glUniform1f(uniform[SHININESS], material_shininess[cl][rw]);
            

            glBindVertexArray(vao[SPHERE]);
            
      
            glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo[SPHERE][ELEMENTS]);
            glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);
            glBindVertexArray(0);   //unbind
        }
    }
    
    glUseProgram(0);    //un-use program
    //=============
    
    glBindRenderbuffer(GL_RENDERBUFFER, colorRenderbuffer);
    [eaglContext presentRenderbuffer:GL_RENDERBUFFER];
    
    //light angle
    index = index + 30.0f;
    if (index >= cicle_points)
        index = 0;
    gbAngle = (2 * 3.14f*index) / cicle_points;
}

-(void)layoutSubviews   //resize
{
    //code
    GLint width;
    GLint height;
    
    glBindRenderbuffer(GL_RENDERBUFFER, colorRenderbuffer);
    [eaglContext renderbufferStorage:GL_RENDERBUFFER fromDrawable:(CAEAGLLayer*)self.layer];
    glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_WIDTH, &width);
    glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_HEIGHT, &height);
    
    glGenRenderbuffers(1, &depthRenderbuffer);
    glBindRenderbuffer(1, depthRenderbuffer);
    glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT16, width, height);
    glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, depthRenderbuffer);
    
    glViewport(0, 0, width, height);
    
    GLfloat fwidth = (GLfloat)width;
    GLfloat fheight = (GLfloat)height;
    
    ///perspective projection-> (fovy, AR, near, far)
    projectonMatrix = vmath::perspective(45.0f, fwidth/fheight, 0.1f, 100.0f);
    
    if(glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
    {
        printf("layoutSubviews: Failed to Create Complete Framebuffer Object 0x%x\n", glCheckFramebufferStatus(GL_FRAMEBUFFER));
    }
    
    [self drawView:nil];
}

-(void)startAnimation
{
    printf("startAnimation\n");
    if(!isAnimating)
    {
        displayLink = [NSClassFromString(@"CADisplayLink")displayLinkWithTarget:self selector:@selector(drawView:)];
        [displayLink setPreferredFramesPerSecond:animationFrameInterval];
        [displayLink addToRunLoop:[NSRunLoop currentRunLoop] forMode:NSDefaultRunLoopMode];
        
        isAnimating = YES;
    }
}

-(void)stopAnimation
{
    printf("stopAnimation\n");
    if(isAnimating)
    {
        [displayLink invalidate];
        displayLink = nil;
        
        isAnimating = NO;
    }
}

//to became first responder
-(BOOL)acceptsFirstResponder
{
    //code
    return (YES);
}

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent*)event
{
    
}

-(void)onSingleTap:(UITapGestureRecognizer*)gr
{
    static int count = 1;   //default on x-axix rotation
    if(++count>3)
        count = 0;
    
    if(count == 1)
    {
        xRotation = YES;
        yRotation = NO;
        zRotation = NO;
    }
    else if(count == 2)
    {
        xRotation = NO;
        yRotation = YES;
        zRotation = NO;
    }
    else if(count == 3)
    {
        xRotation = NO;
        yRotation = NO;
        zRotation = YES;
    }
}

-(void)onDouleTap:(UITapGestureRecognizer*)gr
{
    if(bLighting_enable == NO)
        bLighting_enable = YES;
    else
        bLighting_enable = NO;
}

-(void)onSwipe:(UISwipeGestureRecognizer*)gr
{
    //code
    printf("onSwipe: Exiting...\n");
    [self release];
    exit(0);    //this wrong in iOS app which is to be placed on Play Store
}

-(void)onLongPress:(UILongPressGestureRecognizer*)gr
{
    
}

-(void)dealloc
{
    //code
    
    if(perFragshaderProgramObject)
    {
        if(perFragFragmentShaderObject)
        {
            glDetachShader(perFragshaderProgramObject, perFragFragmentShaderObject);
            glDeleteShader(perFragFragmentShaderObject);
            perFragFragmentShaderObject = 0;
        }
        if(perFragVertexShaderObject)
        {
            glDetachShader(perFragshaderProgramObject, perFragVertexShaderObject);
            glDeleteShader(perFragVertexShaderObject);
            perFragVertexShaderObject = 0;
        }
        
        glDeleteProgram(perFragshaderProgramObject);
        perFragshaderProgramObject = 0;
    }
    
    if(depthRenderbuffer)
    {
        glDeleteRenderbuffers(1, &depthRenderbuffer);
        depthRenderbuffer = 0;
    }
    
    if(colorRenderbuffer)
    {
        glDeleteRenderbuffers(1, &colorRenderbuffer);
        colorRenderbuffer = 0;
    }
    
    if(defaultFrameBuffer)
    {
        glDeleteFramebuffers(1, &defaultFrameBuffer);
        defaultFrameBuffer = 0;
    }
    
    if([EAGLContext currentContext] == eaglContext)
    {
        [EAGLContext setCurrentContext:nil];
    }
    [eaglContext release];
    eaglContext = nil;
    
    [sphereObject release];
    [super dealloc];
}

@end
