#ifndef _MATERIAL_PROPERTIES_H_
#define _MATERIAL_PROPERTIES_H_

//#include <gl/GL.h>
//1st sphere on 1st column, emerald =======
    mbient material
GLfloat     One_ambient[] = {
    0.0215f,
    0.1745f,
    0.0215f,
    1.0f
};

// difuse material
GLfloat     One_diffuse[] = {
    0.07568f,
    0.61424f,
    0.07568f,
    1.0f
};

// specular material
GLfloat     One_specular[] = {
    0.633f,
    0.727811f,
    0.633f,
    1.0f
};

// shininess
GLfloat     One_shininess = 0.6f * 128;

//2nd sphere on 1st column, jade =======
    mbient material
GLfloat     two_ambient[] = {
    0.135f,
    0.2225f,
    0.1575f,
    1.0f
};

// difuse material
GLfloat     two_diffuse[] = {
    0.54f,
    0.89f,
    0.63f,
    1.0f
};

// specular material
GLfloat     two_specular[] = {
    0.316228f,
    0.316228f,
    0.316228f,
    1.0f
};

// shininess
GLfloat     two_shininess = 0.1f * 128;

//3rd sphere on 1st column, obsidian =======
    mbient material
GLfloat     two_ambient[] = {
    0.05375f,
    0.05f,
    0.06625f,
    1.0f
};

// difuse material
GLfloat     two_diffuse[] = {
    0.18275f,
    0.17f,
    0.22525f,
    1.0f
};
// specular material
GLfloat     two_specular[] = {
    0.332741f,
    0.328634f,
    0.346435f,
    1.0f
};
// shininess
GLfloat     two_shininess = 0.3f * 128;

//4th sphere on 1st column, pearl =======
    mbient material
GLfloat     five_ambient[] = {
    0.25f,
    0.20725f,
    0.20725f,
    1.0f
};

// difuse material
GLfloat     five_diffuse[] = {
    1.0f,
    0.829f,
    0.829f,
    1.0f
};

// specular material
GLfloat     five_specular[] = {
    0.296648f,
    0.296648f,
    0.296648f,
    1.0f
};
// shininess
GLfloat     five_shininess = 0.088f * 128;

//5th sphere on 1st column, ruby =======
    mbient material
GLfloat     six_ambient[] = {
    0.1745f,
    0.01175f,
    0.01175f,
    1.0f
};

// difuse material
GLfloat     six_diffuse[] = {
    0.61424f,
    0.04136f,
    0.04136f,
    1.0f
};

// specular material
GLfloat     six_specular[] = {
    0.727811f,
    0.626959f,
    0.626959f,
    1.0f
};
// shininess
GLfloat     six_shininess = 0.6f * 128;

//6th sphere on 1st column, turquoise =======
    mbient material
GLfloat     seven_ambient[] = {
    0.1f,
    0.18725f,
    0.1745f,
    1.0f
};

// difuse material
GLfloat     seven_diffuse[] = {
    0.396f,
    0.74151f,
    0.69102f,
    1.0f
};

// specular material
GLfloat     seven_specular[] = {
    0.297254f,
    0.30829f,
    0.306678f,
    1.0f
};
// shininess
GLfloat     seven_shininess = 0.1f * 128;

//1st sphere on 2nd column, brass =======
    mbient material
GLfloat     eight_ambient[] = {
    0.329412f,
    0.223529f,
    0.027451f,
    1.0f
};

// difuse material
GLfloat     eight_diffuse[] = {
    0.780392f,
    0.568627f,
    0.113725f,
    1.0f
};

// specular material
GLfloat     eight_specular[] = {
    0.992157f,
    0.941176f,
    0.807843f,
    1.0f
};
// shininess
GLfloat     eight_shininess = 0.21794872f * 128;

//2nd sphere on 2nd column, bronze =======
    mbient material
GLfloat     nine_ambient[] = {
    0.2125f,
    0.1275f,
    0.054f,
    1.0f
};

// difuse material
GLfloat     nine_diffuse[] = {
    0.714f,
    0.4284f,
    0.18144f,
    1.0f
};

// specular material
GLfloat     nine_specular[] = {
    0.393548f,
    0.271906f,
    0.166721f,
    1.0f
};
// shininess
GLfloat     nine_shininess = 0.2f * 128;

//3rd sphere on 2nd column, chrome =======
    mbient material
GLfloat     twn_ambient[] = {
    0.25f,
    0.25f,
    0.25f,
    1.0f
};

// difuse material
GLfloat     twn_diffuse[] = {
    0.4f,
    0.4f,
    0.4f,
    1.0f
};

// specular material
GLfloat     twn_specular[] = {
    0.774597f,
    0.774597f,
    0.774597f,
    1.0f
};
// shininess
GLfloat     twn_shininess = 0.6f * 128;

//4th sphere on 2nd column, copper =======
    mbient material
GLfloat     eleven_ambient[] = {
    0.19125f,
    0.0735f,
    0.0225f,
    1.0f
};

// difuse material
GLfloat     eleven_diffuse[] = {
    0.7038f,
    0.27048f,
    0.0828f,
    1.0f
};

// specular material
GLfloat     eleven_specular[] = {
    0.256777f,
    0.137622f,
    0.086014f,
    1.0f
};
// shininess
GLfloat     eleven_shininess = 0.1f * 128;

//5th sphere on 2nd column, gold =======
    mbient material
GLfloat     twelve_ambient[] = {
    0.24725f,
    0.1995f,
    0.0745f,
    1.0f
};

// difuse material
GLfloat     twelve_diffuse[] = {
    0.75164f,
    0.60648f,
    0.22648f,
    1.0f
};

// specular material
GLfloat     twelve_specular[] = {
    0.628281f,
    0.555802f,
    0.366065f,
    1.0f
};
// shininess
GLfloat     twelve_shininess = 0.4f * 128;


//6th sphere on 2nd column, silver =======
    mbient material
GLfloat thirteen_ambient[] = {
    0.19225f,
    0.19225f,
    0.19225f,
    1.0f
};

// difuse material
GLfloat thirteen_diffuse[] = {
    0.50754f,
    0.50754f,
    0.50754f,
    1.0f
};

// specular material
GLfloat thirteen_specular[] = {
    0.508273f,
    0.508273f,
    0.508273f,
    1.0f
};
// shininess
GLfloat thirteen_shininess = 0.4f * 128;


//1st sphere on 3rd column, black =======
    mbient material
GLfloat fourteen_ambient[] = {
    0.0f,
    0.0f,
    0.0f,
    1.0f
};

// difuse material
GLfloat fourteen_diffuse[] = {
    0.01f,
    0.01f,
    0.01f,
    1.0f
};

// specular material
GLfloat fourteen_specular[] = {
    0.50f,
    0.50f,
    0.50f,
    1.0f
};
// shininess
GLfloat fourteen_shininess = 0.25f * 128;


//2nd sphere on 3rd column, cyan =======
    mbient material
GLfloat fifteen_ambient[] = {
    0.0f,
    0.1f,
    0.06f,
    1.0f
};

// difuse material
GLfloat fifteen_diffuse[] = {
    0.0f,
    0.50980392f,
    0.50980392f,
    1.0f
};

// specular material
GLfloat fifteen_specular[] = {
    0.50196078f,
    0.50196078f,
    0.50196078f,
    1.0f
};
// shininess
GLfloat fifteen_shininess = 0.25f * 128;

//3rd sphere on 2nd column, green =======
    mbient material
GLfloat green1_mat_ambient[] = {
    0.0f,
    0.0f,
    0.0f,
    1.0f
};

// difuse material
GLfloat green1_mat_diffuse[] = {
    0.1f,
    0.35f,
    0.1f,
    1.0f
};

// specular material
GLfloat green1_mat_specular[] = {
    0.45f,
    0.55f,
    0.45f,
    1.0f
};
// shininess
GLfloat green1_mat_shininess = 0.25f * 128;

//4th sphere on 3rd column, red =======
    mbient material
GLfloat sixteen_ambient[] = {
    0.0f,
    0.0f,
    0.0f,
    1.0f
};

// difuse material
GLfloat sixteen_diffuse[] = {
    0.5f,
    0.0f,
    0.0f,
    1.0f
};

// specular material
GLfloat sixteen_specular[] = {
    0.7f,
    0.6f,
    0.6f,
    1.0f
};

// shininess
GLfloat sixteen_shininess = 0.25f * 128;

//5th sphere on 3rd column, white =======
    mbient material
GLfloat seventeen_ambient[] = {
    0.0f,
    0.0f,
    0.0f,
    1.0f
};

// difuse material
GLfloat seventeen_diffuse[] = {
    0.55f,
    0.55f,
    0.55f,
    1.0f
};

// specular material
GLfloat seventeen_specular[] = {
    0.70f,
    0.70f,
    0.70f,
    1.0f
};
// shininess
GLfloat seventeen_shininess = 0.25f * 128;

//6th sphere on 3rd column, yellow plastic =======
    mbient material
GLfloat eighteen_ambient[] = {
    0.0f,
    0.0f,
    0.0f,
    1.0f
};

// difuse material
GLfloat eighteen_diffuse[] = {
    0.5f,
    0.5f,
    0.0f,
    1.0f
};

// specular material
GLfloat eighteen_specular[] = {
    0.60f,
    0.60f,
    0.50f,
    1.0f
};
// shininess
GLfloat eighteen_shininess = 0.25f * 128;

//1st sphere on 4th column, black =======
    mbient material
GLfloat ninteen_ambient[] = {
    0.02f,
    0.02f,
    0.02f,
    1.0f
};

// difuse material
GLfloat ninteen_diffuse[] = {
    0.01f,
    0.01f,
    0.01f,
    1.0f
};

// specular material
GLfloat ninteen_specular[] = {
    0.4f,
    0.4f,
    0.4f,
    1.0f
};
// shininess
GLfloat ninteen_shininess = 0.078125f * 128;

//2nd sphere on 4th column, cyan =======
    mbient material
GLfloat twenty_ambient[] = {
    0.0f,
    0.05f,
    0.05f,
    1.0f
};

// difuse material
GLfloat twenty_diffuse[] = {
    0.4f,
    0.5f,
    0.5f,
    1.0f
};

// specular material
GLfloat twenty_specular[] = {
    0.04f,
    0.7f,
    0.7f,
    1.0f
};
// shininess
GLfloat twenty_shininess = 0.078125f * 128;


//3rd sphere on 4th column, green =======
    mbient material
GLfloat twenty1_ambient[] = {
    0.0f,
    0.05f,
    0.0f,
    1.0f
};

// difuse material
GLfloat twenty1_diffuse[] = {
    0.4f,
    0.5f,
    0.4f,
    1.0f
};

// specular material
GLfloat twenty1_specular[] = {
    0.04f,
    0.7f,
    0.04f,
    1.0f
};
// shininess
GLfloat twenty1_shininess = 0.078125f * 128;

//4th sphere on 4th column, red =======
    mbient material
GLfloat twenty2_ambient[] = {
    0.05f,
    0.0f,
    0.0f,
    1.0f
};

// difuse material
GLfloat twenty2_diffuse[] = {
    0.5f,
    0.4f,
    0.4f,
    1.0f
};

// specular material
GLfloat twenty2_specular[] = {
    0.7f,
    0.04f,
    0.04f,
    1.0f
};
// shininess
GLfloat twenty2_shininess = 0.078125f * 128;


//5th sphere on 4th column, white =======
    mbient material
GLfloat twenty3_ambient[] = {
    0.05f,
    0.05f,
    0.05f,
    1.0f
};

// difuse material
GLfloat twenty3_diffuse[] = {
    0.5f,
    0.5f,
    0.5f,
    1.0f
};

// specular material
GLfloat twenty3_specular[] = {
    0.7f,
    0.7f,
    0.7f,
    1.0f
};
// shininess
GLfloat twenty3_shininess = 0.078125f * 128;

//6th sphere on 4th column, yellow rubber =======
    mbient material
GLfloat     twentyFour_ambient[] = {
    0.05f,
    0.05f,
    0.0f,
    1.0f
};

// difuse material
GLfloat     twentyFour_diffuse[] = {
    0.5f,
    0.5f,
    0.4f,
    1.0f
};

// specular material
GLfloat     twentyFour_specular[] = {
    0.7f,
    0.7f,
    0.04f,
    1.0f
};
// shininess
GLfloat     twentyFour_shininess = 0.078125f * 128;


//now 2d array for these matrials...
//c  r
GLfloat* material_ambient[4][6] = {
    {    One_ambient,     two_ambient,         two_ambient,     five_ambient,     six_ambient,     seven_ambient},
    {    eight_ambient,     nine_ambient,         twn_ambient,     eleven_ambient,     twelve_ambient, thirteen_ambient },
    {fourteen_ambient, fifteen_ambient, green1_mat_ambient, sixteen_ambient, seventeen_ambient, eighteen_ambient},
    {ninteen_ambient, twenty_ambient, twenty1_ambient, twenty2_ambient, twenty3_ambient,     twentyFour_ambient}
};

//c  r
GLfloat* material_diffuse[4][6] = {
    {     One_diffuse,     two_diffuse,         two_diffuse,     five_diffuse,     six_diffuse,     seven_diffuse },
    {     eight_diffuse,     nine_diffuse,         twn_diffuse,     eleven_diffuse,     twelve_diffuse, thirteen_diffuse },
    { fourteen_diffuse, fifteen_diffuse, green1_mat_diffuse, sixteen_diffuse, seventeen_diffuse, eighteen_diffuse },
    { ninteen_diffuse, twenty_diffuse, twenty1_diffuse, twenty2_diffuse, twenty3_diffuse,     twentyFour_diffuse }
};

//c  r
GLfloat* material_specular[4][6] = {
    {     One_specular,     two_specular,         two_specular,     five_specular,     six_specular,     seven_specular },
    {     eight_specular,     nine_specular,         twn_specular,     eleven_specular,     twelve_specular, thirteen_specular },
    { fourteen_specular, fifteen_specular, green1_mat_specular, sixteen_specular, seventeen_specular, eighteen_specular },
    { ninteen_specular, twenty_specular, twenty1_specular, twenty2_specular, twenty3_specular,     twentyFour_specular }
};

//c  r
GLfloat material_shininess[4][6] = {
    {     One_shininess,     two_shininess,         two_shininess,     five_shininess,     six_shininess,     seven_shininess },
    {     eight_shininess,     nine_shininess,         twn_shininess,     eleven_shininess,     twelve_shininess, thirteen_shininess },
    { fourteen_shininess, fifteen_shininess, green1_mat_shininess, sixteen_shininess, seventeen_shininess, eighteen_shininess },
    { ninteen_shininess, twenty_shininess, twenty1_shininess, twenty2_shininess, twenty3_shininess,     twentyFour_shininess }
};
#endif 
