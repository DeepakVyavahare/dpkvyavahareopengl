//
//  MyView.m
//  Texture-Checkerboard
//
//  Created by Manish Baing on 17/06/18.
//
#import <OpenGLES/ES3/gl.h>
#import <OpenGLES/ES3/glext.h>

#import "vmath.h"

#import "MyView.h"

enum
{
    VDG_ATTRIBUTE_POSITION,
    VDG_ATTRIBUTE_COLOR,
    VDG_ATTRIBUTE_NORMAL,
    VDG_ATTRIBUTE_TEXTURE0,
};

@implementation MyView
{
@private
    EAGLContext *eaglContext;
    
    GLuint defaultFrameBuffer;
    GLuint colorRenderbuffer;
    GLuint depthRenderbuffer;
    
    id displayLink;
    NSInteger animationFrameInterval;
    BOOL isAnimating;
    
    GLuint vertexShaderObject;
    GLuint fragmentShaderObject;
    GLuint shaderProgramObject;
    
    /*************
     Dont Keep Un-used enums in this code,
     else vao, vbo, uniform arrays will be populated unnecessarly
     ***************/
    enum eShapes            // <--- Modify as per requirement
    {
        QUAD=0,
        NR_SHAPES,
    };
    enum eShape_Attr
    {
        POSITION = 0,
        TEXCOORDS,
        NR_SHAPE_ATTRS,
    };
    enum eUniforms
    {
        MVP = 0,
        TEXTURE0_SAMPLER_2D,
        NR_UNIFORMS,
    };
    
    GLuint vao[NR_SHAPES];
    GLuint vbo[NR_SHAPES][NR_SHAPE_ATTRS];
    GLuint uniform[NR_UNIFORMS];
    
    GLuint texture_checkerboard;
    
    vmath::mat4 projectonMatrix;
    
#define CHECKERBOARD_IMAGE_WIDTH     64
#define CHECKERBOARD_IMAGE_HEIGHT    64
    GLubyte checkerboardImage[CHECKERBOARD_IMAGE_WIDTH][CHECKERBOARD_IMAGE_HEIGHT][4];
}
/*
 // Only override drawRect: if you perform custom drawing.
 // An empty implementation adversely affects performance during animation.
 - (void)drawRect:(CGRect)rect {
 // Drawing code
 }
 */

-(id)initWithFrame:(CGRect)frame
{
    //code
    self = [super initWithFrame:frame];
    
    if(self)
    {
        CAEAGLLayer *eaglLayer = (CAEAGLLayer*)super.layer;
        
        eaglLayer.opaque = YES;
        eaglLayer.drawableProperties = [NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithBool:FALSE], kEAGLDrawablePropertyRetainedBacking, kEAGLColorFormatRGBA8, kEAGLDrawablePropertyColorFormat, nil];
        
        eaglContext = [[EAGLContext alloc] initWithAPI:kEAGLRenderingAPIOpenGLES3]; //EGL 3.0
        if(eaglContext == nil)
        {
            [self release];
            return (nil);
        }
        
        [EAGLContext setCurrentContext:eaglContext];
        
        glGenFramebuffers(1, &defaultFrameBuffer);
        glGenRenderbuffers(1, &colorRenderbuffer);
        glBindFramebuffer(GL_FRAMEBUFFER, defaultFrameBuffer);
        glBindRenderbuffer(GL_RENDERBUFFER, colorRenderbuffer);
        
        [eaglContext renderbufferStorage:GL_RENDERBUFFER fromDrawable:eaglLayer];
        
        glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_RENDERBUFFER, colorRenderbuffer);
        
        GLint backingWidth;
        GLint backingHeight;
        glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_WIDTH, &backingWidth);
        glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_HEIGHT, &backingHeight);
        
        glGenRenderbuffers(1, &depthRenderbuffer);
        glBindRenderbuffer(GL_RENDERBUFFER, depthRenderbuffer);
        glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT16, backingWidth, backingHeight);
        glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, depthRenderbuffer);
        
        if(glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
        {
            printf("initWithFrame: Failed To Create Complete Framebuffer Object 0x%x\n", glCheckFramebufferStatus(GL_FRAMEBUFFER));
            glDeleteFramebuffers(1, &defaultFrameBuffer);
            glDeleteRenderbuffers(1, &colorRenderbuffer);
            glDeleteRenderbuffers(1, &depthRenderbuffer);
            [self release];
            return (nil);
        }
        
        printf("Renderer : %s | GL Version : %s | GLSL Version : %s\n", glGetString(GL_RENDERER), glGetString(GL_VERSION), glGetString(GL_SHADING_LANGUAGE_VERSION));
        
        //hard coded initializations
        isAnimating = NO;
        animationFrameInterval = 60;    //its default since iOS 8.2
        //=================
        
        //Do OpenGL Initialization here
        /********VERTEX SHADER**********/
        vertexShaderObject = glCreateShader(GL_VERTEX_SHADER);
        const GLchar* vertexShaderSourceCode =
        "#version 300 es"\
        "\n" \
        "in vec4 vPosition;" \
        "in vec2 vTexture0_Coord;" \
        "uniform mat4 u_mvp_matrix;" \
        "out vec2 outTex0Coords;" \
        "void main(void)" \
        "{" \
        "gl_Position = u_mvp_matrix * vPosition;" \
        "outTex0Coords = vTexture0_Coord;" \
        "}";
        
        glShaderSource(vertexShaderObject, 1, (const GLchar **)&vertexShaderSourceCode, NULL);
        glCompileShader(vertexShaderObject);
        //error handling
        GLint iShaderCompileStatus = 0;
        glGetShaderiv(vertexShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);
        if(iShaderCompileStatus == GL_FALSE)
        {
            GLint iShaderInfoLogLen=0;
            glGetShaderiv(vertexShaderObject, GL_INFO_LOG_LENGTH, &iShaderInfoLogLen);
            char *szInfoLog = (char*)malloc(iShaderInfoLogLen);
            if(szInfoLog)
            {
                GLint written;
                glGetShaderInfoLog(vertexShaderObject, iShaderInfoLogLen, &written, szInfoLog);
                printf( "Vertex Shader Compile Info: %s\n", szInfoLog);
                free(szInfoLog);
                [self release];
                
            }
        }
        
        
        /********FRAGMENT SHADER**********/
        fragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);
        const GLchar* fragmentShaderSourceCode =
        "#version 300 es" \
        "\n" \
        "precision highp float;" \
        "in vec2 outTex0Coords;" \
        "uniform sampler2D u_texture0_sampler;" \
        "out vec4 FragColor;" \
        "void main(void)" \
        "{" \
        "vec3 tex = vec3(texture(u_texture0_sampler, outTex0Coords));" \
        "FragColor = vec4(tex, 1.0f);" \
        "}";
        glShaderSource(fragmentShaderObject,1, (GLchar**)&fragmentShaderSourceCode, NULL);
        glCompileShader(fragmentShaderObject);
        iShaderCompileStatus = 0;
        glGetShaderiv(fragmentShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);
        if(iShaderCompileStatus == GL_FALSE)
        {
            GLint iShaderInfoLogLen = 0;
            glGetShaderiv(fragmentShaderObject, GL_INFO_LOG_LENGTH, &iShaderInfoLogLen);
            char *szInfoLog = (char*)malloc(iShaderInfoLogLen);
            if(szInfoLog)
            {
                GLint written;
                glGetShaderInfoLog(fragmentShaderObject, iShaderInfoLogLen, &written, szInfoLog);
                printf( "Fragment Shader Compile Info: %s\n", szInfoLog);
                free(szInfoLog);
                [self release];
                
            }
        }
        
        /********SHADER PROGRAM**********/
        shaderProgramObject = glCreateProgram();
        glAttachShader(shaderProgramObject, vertexShaderObject);
        glAttachShader(shaderProgramObject, fragmentShaderObject);
        
        //pre-link binding of
        glBindAttribLocation(shaderProgramObject, VDG_ATTRIBUTE_POSITION, "vPosition");
        glBindAttribLocation(shaderProgramObject, VDG_ATTRIBUTE_TEXTURE0, "vTexture0_Coord");
        
        glLinkProgram(shaderProgramObject);
        GLint iProgramLinkStatus = 0;
        glGetProgramiv(shaderProgramObject, GL_LINK_STATUS, &iProgramLinkStatus);
        if(iProgramLinkStatus == GL_FALSE)
        {
            GLint iProgramInfoLogLen = 0;
            glGetProgramiv(shaderProgramObject, GL_INFO_LOG_LENGTH, &iProgramInfoLogLen);
            char *szInfoLog = (char*)malloc(iProgramInfoLogLen);
            if(szInfoLog)
            {
                GLint written;
                glGetProgramInfoLog(shaderProgramObject, iProgramInfoLogLen, &written, szInfoLog);
                printf( "Program Link Status Info: %s\n", szInfoLog);
                free(szInfoLog);
                [self release];
                
            }
        }
        
        //Get Uniform locations
        uniform[MVP] = glGetUniformLocation(shaderProgramObject, "u_mvp_matrix");           // <--- Modify/add as per requirement
        uniform[TEXTURE0_SAMPLER_2D] = glGetUniformLocation(shaderProgramObject, "u_texture0_sampler");
        
        //load textures
        texture_checkerboard = [self loadCheckerboardImage];
        printf("texture_checkerboard = 0x%x\n", texture_checkerboard);
        //============
        
        /******* Vertices, colors, normals, vao, vbo initialization**********/
        //x, y, z
        GLfloat quadVertices [] =
        {
            /*Q1*/  1.0f,   1.0f,   0.0f,   /*Q2*/  -1.0f,  1.0f,   0.0f,   /*Q3*/  -1.0f,  -1.0f,  0.0f,   /*Q4*/  1.0f,   -1.0f,  0.0f
        };
        
        //s,t
        GLfloat quadTexCoords [] =
        {
            /*Q1*/  1.0f,   0.0f,   /*Q2*/  0.0f,  0.0f,   /*Q3*/  0.0f,  1.0f,  /*Q4*/  1.0f,   1.0f,
        };
        
        //create vao from quad
        glGenVertexArrays(1, &vao[QUAD]);
        glBindVertexArray(vao[QUAD]);
        //vbo: position
        {
            glGenBuffers(1, &vbo[QUAD][POSITION]);
            glBindBuffer(GL_ARRAY_BUFFER, vbo[QUAD][POSITION]);
            glBufferData(GL_ARRAY_BUFFER, sizeof(quadVertices), quadVertices, GL_STATIC_DRAW);
            
            glVertexAttribPointer(VDG_ATTRIBUTE_POSITION,
                                  3,    //x,y,z
                                  GL_FLOAT,
                                  GL_FALSE,
                                  0, NULL); //0-stride, no offset
            glEnableVertexAttribArray(VDG_ATTRIBUTE_POSITION);
            
            glBindBuffer(GL_ARRAY_BUFFER, 0);   //unbind
        }
        //vbo:texture coordinates
        {
            glGenBuffers(1, &vbo[QUAD][TEXCOORDS]);
            glBindBuffer(GL_ARRAY_BUFFER, vbo[QUAD][TEXCOORDS]);
            glBufferData(GL_ARRAY_BUFFER, sizeof(quadTexCoords), quadTexCoords, GL_STATIC_DRAW);
            
            glVertexAttribPointer(VDG_ATTRIBUTE_TEXTURE0,
                                  2,    //s, t
                                  GL_FLOAT,
                                  GL_FALSE,
                                  0, NULL); //0-stride, no offset
            glEnableVertexAttribArray(VDG_ATTRIBUTE_TEXTURE0);
            
            glBindBuffer(GL_ARRAY_BUFFER, 0);   //unbind
        }
        glBindVertexArray(0);   //unbind
        //==============
        
        //Other OpenGL settings
        glEnable(GL_DEPTH_TEST);    //enable depth testing
        glDepthFunc(GL_LEQUAL);     //depth test to do
        
        //glEnable(GL_CULL_FACE);     //We will always cull back faces for better performance
        
        //set background clearing color
        glClearColor(0.0f, 0.0f, 0.0f, 1.0f);   //black
        
        //initialize ortho projectio matrix to identity
        projectonMatrix = vmath::mat4::identity();
        //================
        
        //Gesture Recognizer
        //Tap Gesture code
        //01-single tap gesture
        UITapGestureRecognizer *singleTapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onSingleTap:)];
        [singleTapGestureRecognizer setNumberOfTapsRequired:1]; //single tap to recognize singleTap event
        [singleTapGestureRecognizer setNumberOfTouchesRequired:1];  //one finger touch is required to recognize single tap
        [singleTapGestureRecognizer setDelegate:self];
        [self addGestureRecognizer:singleTapGestureRecognizer];
        
        //02-double tap gesture
        UITapGestureRecognizer *doubleTapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onDouleTap:)];
        [doubleTapGestureRecognizer setNumberOfTapsRequired:2]; //two taps required to detect double tap
        [doubleTapGestureRecognizer setNumberOfTouchesRequired:1]; //one finger touch
        [doubleTapGestureRecognizer setDelegate:self];
        [self addGestureRecognizer:doubleTapGestureRecognizer];
        
        //this will allow to differentiate between single tap and double tap
        [singleTapGestureRecognizer requireGestureRecognizerToFail:doubleTapGestureRecognizer];
        
        //03-swipe gesture
        UISwipeGestureRecognizer *swipeGestureRecognizer = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(onSwipe:)];
        [self addGestureRecognizer:swipeGestureRecognizer];
        
        //04-Long press gesture
        UILongPressGestureRecognizer *longPressGestureRecognizer = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(onLongPress:)];
        [self addGestureRecognizer:longPressGestureRecognizer];
    }
    return (self);
}/*initWithFrame*/

-(void) makeCheckerboardTexture
{
    int i, j, c;
    
    for (i = 0; i<CHECKERBOARD_IMAGE_HEIGHT; i++)
    {
        for (j = 0; j<CHECKERBOARD_IMAGE_WIDTH; j++)
        {
            c = (((i & 0x08) == 0) ^ ((j & 0x08) == 0)) * 255;    // 0-7 --> 255 and 8-F --> 0
            checkerboardImage[i][j][0] = (GLubyte)c;    //Red    present
            checkerboardImage[i][j][1] = (GLubyte)c;    //Green present
            checkerboardImage[i][j][2] = (GLubyte)c;    //Blue  present
            checkerboardImage[i][j][3] = (GLubyte)0xFF;    //Alpha present
        }
    }
}

-(GLuint)loadCheckerboardImage
{
    [self makeCheckerboardTexture];
    
    GLuint checkerboardTexture;
    
    glGenTextures(1, &checkerboardTexture);
    glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
    glBindTexture(GL_TEXTURE_2D, checkerboardTexture);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexImage2D(GL_TEXTURE_2D,        //TARGET
                 0,                    //lod
                 GL_RGBA,            //INTERNAL IMAGE FORMAT
                 CHECKERBOARD_IMAGE_WIDTH,
                 CHECKERBOARD_IMAGE_HEIGHT,
                 0,                    //NO BORDER
                 GL_RGBA,            //PIXEL FORMAT
                 GL_UNSIGNED_BYTE,    //DATA TYPE
                 checkerboardImage);        //DATA
    return(checkerboardTexture);
}

+(Class)layerClass      //static method
{
    //code
    return ([CAEAGLLayer class]);   //return class which does animation
}

-(void)drawView:(id)sender  //display
{
    //code
    [EAGLContext setCurrentContext:eaglContext];
    
    glBindFramebuffer(GL_FRAMEBUFFER, defaultFrameBuffer);
    
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);
    
    /*Your Drawing*/        // <--- Modify as per requirement
    glUseProgram(shaderProgramObject);
    
    vmath::mat4 modelViewMatrix = vmath::mat4::identity();
    vmath::mat4 translateMatrix = vmath::mat4::identity();
    vmath::mat4 modelViewProjectionMatrix = vmath::mat4::identity();
    
    translateMatrix = vmath::translate(0.0f, 0.0f, -3.6f);
    
    //MV = T * MV
    modelViewMatrix = translateMatrix * modelViewMatrix;
    
    //MVP = P * MV
    modelViewProjectionMatrix = projectonMatrix * modelViewMatrix;
    
    //send MVP to shader
    glUniformMatrix4fv(uniform[MVP], 1, GL_FALSE, modelViewProjectionMatrix);
    
    //*** bind texture ***
    glBindTexture(GL_TEXTURE_2D, texture_checkerboard);
    
    //*** bind vao ***
    glBindVertexArray(vao[QUAD]);
    {
        //plane quad
        GLfloat quadVertices1[] = {/*Q1*/0.0f, 1.0f, 0.0f, /*Q2*/-2.0f, 1.0f, 0.0f, /*Q3*/-2.0f, -1.0f, 0.0f, /*Q4*/0.0f, -1.0f, 0.0f };
        glBindBuffer(GL_ARRAY_BUFFER, vbo[QUAD][POSITION]);
        glBufferData(GL_ARRAY_BUFFER, sizeof(quadVertices1), quadVertices1, GL_DYNAMIC_DRAW);
        glBindBuffer(GL_ARRAY_BUFFER, 0);   //unbind
        
        //Draw/render using glDrawArrays
        glDrawArrays(GL_TRIANGLE_FAN, 0, 4);    //T1(0,1,2) and  T2(0,2,3)
        //==========
        
        
        //tilted quad
        GLfloat quadVertices2[] = {/*Q1*/2.41421f, 1.0f, -1.41421f, /*Q2*/1.0f, 1.0f, 0.0f, /*Q3*/1.0f, -1.0f, 0.0f, /*Q4*/2.41421f, -1.0f, -1.41421f, };
        glBindBuffer(GL_ARRAY_BUFFER, vbo[QUAD][POSITION]);
        glBufferData(GL_ARRAY_BUFFER, sizeof(quadVertices2), quadVertices2, GL_DYNAMIC_DRAW);
        glBindBuffer(GL_ARRAY_BUFFER, 0);   //unbind
        
        //Draw/render using glDrawArrays
        glDrawArrays(GL_TRIANGLE_FAN, 0, 4);    //T1(0,1,2) and  T2(0,2,3)
        //========
    }
    
    glUseProgram(0);    //un-use program
    //=============
    
    glBindRenderbuffer(GL_RENDERBUFFER, colorRenderbuffer);
    [eaglContext presentRenderbuffer:GL_RENDERBUFFER];
    
    [self spin];
}

//our method
-(void)spin
{
    
}

-(void)layoutSubviews   //resize
{
    //code
    GLint width;
    GLint height;
    
    glBindRenderbuffer(GL_RENDERBUFFER, colorRenderbuffer);
    [eaglContext renderbufferStorage:GL_RENDERBUFFER fromDrawable:(CAEAGLLayer*)self.layer];
    glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_WIDTH, &width);
    glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_HEIGHT, &height);
    
    glGenRenderbuffers(1, &depthRenderbuffer);
    glBindRenderbuffer(1, depthRenderbuffer);
    glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT16, width, height);
    glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, depthRenderbuffer);
    
    glViewport(0, 0, width, height);
    
    //glOrtho(left, right, bottom, top, near, far)
    GLfloat fwidth = (GLfloat)width;
    GLfloat fheight = (GLfloat)height;
    
    //set perspective projection--> (fovy, AR, near, far)
    projectonMatrix = vmath::perspective(60.0f, fwidth/fheight, 1.0f, 30.0f);        // <--- Modify as per requirement
    
    if(glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
    {
        printf("layoutSubviews: Failed to Create Complete Framebuffer Object 0x%x\n", glCheckFramebufferStatus(GL_FRAMEBUFFER));
    }
    
    [self drawView:nil];
}

-(void)startAnimation
{
    printf("startAnimation\n");
    if(!isAnimating)
    {
        displayLink = [NSClassFromString(@"CADisplayLink")displayLinkWithTarget:self selector:@selector(drawView:)];
        [displayLink setPreferredFramesPerSecond:animationFrameInterval];
        [displayLink addToRunLoop:[NSRunLoop currentRunLoop] forMode:NSDefaultRunLoopMode];
        
        isAnimating = YES;
    }
}

-(void)stopAnimation
{
    printf("stopAnimation\n");
    if(isAnimating)
    {
        [displayLink invalidate];
        displayLink = nil;
        
        isAnimating = NO;
    }
}

//to became first responder
-(BOOL)acceptsFirstResponder
{
    //code
    return (YES);
}

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent*)event
{
    
}

-(void)onSingleTap:(UITapGestureRecognizer*)gr
{
    
}

-(void)onDouleTap:(UITapGestureRecognizer*)gr
{
    
}

-(void)onSwipe:(UISwipeGestureRecognizer*)gr
{
    //code
    printf("onSwipe: Exiting...\n");
    [self release];
    exit(0);    //this wrong in iOS app which is to be placed on Play Store
}

-(void)onLongPress:(UILongPressGestureRecognizer*)gr
{
    
}

-(void)dealloc
{
    //code
    //Release vao
    for(int i=NR_SHAPES-1; i>=0; i--)
    {
        if(vao[i])
        {
            glDeleteVertexArrays(1, &vao[i]);
            vao[i] = 0;
        }
    }
    glDeleteVertexArrays(NR_SHAPES, vao);
    
    //Release vbo, As I have array of vao objects, i would have released this in 'glDeleteVertexArrays(NR_SHAPES, vao)' way.
    //But at this point of time, i am not sure, all declared vao objects have been initialized or not.
    //So its good to check and release vao objects one by one, though it looks funny :-)
    for(int i=NR_SHAPES-1; i>=0; i--)
    {
        for(int j=NR_SHAPE_ATTRS-1; i>=0; i--)
        {
            if(vbo[i][j])
            {
                glDeleteBuffers(1, &vbo[i][j]);
                vbo[i][j] = 0;
            }
        }
    }
    
    if(shaderProgramObject)
    {
        if(fragmentShaderObject)
        {
            glDetachShader(shaderProgramObject, fragmentShaderObject);
            glDeleteShader(fragmentShaderObject);
            fragmentShaderObject = 0;
        }
        if(vertexShaderObject)
        {
            glDetachShader(shaderProgramObject, vertexShaderObject);
            glDeleteShader(vertexShaderObject);
            vertexShaderObject = 0;
        }
        glDeleteProgram(shaderProgramObject);
        shaderProgramObject = 0;
    }
    
    if(depthRenderbuffer)
    {
        glDeleteRenderbuffers(1, &depthRenderbuffer);
        depthRenderbuffer = 0;
    }
    
    if(colorRenderbuffer)
    {
        glDeleteRenderbuffers(1, &colorRenderbuffer);
        colorRenderbuffer = 0;
    }
    
    if(defaultFrameBuffer)
    {
        glDeleteFramebuffers(1, &defaultFrameBuffer);
        defaultFrameBuffer = 0;
    }
    
    if([EAGLContext currentContext] == eaglContext)
    {
        [EAGLContext setCurrentContext:nil];
    }
    [eaglContext release];
    eaglContext = nil;
    
    [super dealloc];
}

@end
