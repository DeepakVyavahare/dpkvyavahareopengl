//
//  AppDelegate.h
//  window
//
//  Created by nvidia on 7/28/18.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

