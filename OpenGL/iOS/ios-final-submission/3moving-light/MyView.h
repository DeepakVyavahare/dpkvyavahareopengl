//
//  MyView.h
//  window
//
//  Created by nvidia on 7/29/18.
//

#import <UIKit/UIKit.h>

@interface MyView : UIView <UIGestureRecognizerDelegate>
-(void)startAnimation;
-(void)stopAnimation;
@end
