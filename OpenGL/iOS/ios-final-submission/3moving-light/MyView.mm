
//
#import <OpenGLES/ES3/gl.h>
#import <OpenGLES/ES3/glext.h>

#import "vmath.h"

#import "MyView.h"
//#include "sphere.h"
#import "sphere.h"



enum
{
    VDG_ATTRIBUTE_POSITION = 0,
    VDG_ATTRIBUTE_COLOR,
    VDG_ATTRIBUTE_NORMAL,
    VDG_ATTRIBUTE_TEXTURE0,
};
#define COL_MAX     4
#define ROW_MAX     6
#define DEEPNESS    -1.5f

GLfloat gAngle = 0.0f;

// light0
GLfloat lightAmbient00[] = { 0.0f, 0.0f, 0.0f, 1.0f };
GLfloat lightDiffuse00[] = { 1.0f, 0.0f, 0.0f, 1.0f };
GLfloat lightSpecular00[] = { 1.0f, 0.0f, 0.0f, 1.0f };
GLfloat lightPosition00[] = { 0.0f, 0.0f, 0.0f, 0.0f };

//light1
GLfloat lightAmbient01[] = { 0.0f, 0.0f, 0.0f, 1.0f };
GLfloat lightDiff01[] = { 0.0f, 1.0f, 0.0f, 1.0f };
GLfloat lightSpec01[] = { 0.0f, 1.0f, 0.0f, 1.0f };
GLfloat lightPosition01[] = { 0.0f, 0.0f, 0.0f, 0.0f };

// light2
GLfloat lightAmbient02[] = { 0.0f, 0.0f, 0.0f, 1.0f };
GLfloat lightDiff02[] = { 0.0f, 0.0f, 1.0f, 1.0f };
GLfloat lightSpec02[] = { 0.0f, 0.0f, 1.0f, 1.0f };
GLfloat lightPosition02[] = { 0.0f, 0.0f, 0.0f, 0.0f };

GLfloat materialAmbient[] = { 0.0f, 0.0f,0.0f,1.0f };
GLfloat materialDiffuse[] = { 1.0f,1.0f,1.0f,1.0f };
GLfloat materialSpecular[] = { 1.0f, 1.0f,1.0f,1.0f };
GLfloat materialShininess = 50.0f;


//Sphere declarations
GLuint gNumElements;
GLuint gNumVertices;
GLfloat sphere_vertices[1146];
GLfloat sphere_normals[1146];
GLfloat sphere_textures[764];
GLshort sphere_elements[2280];

@implementation MyView
{
@private
    EAGLContext *eaglContext;
    
    GLuint defaultFrameBuffer;
    GLuint colorRenderbuffer;
    GLuint depthRenderbuffer;
    
    id displayLink;
    NSInteger animationFrameInterval;
    BOOL isAnimating;
    
    //your shader variables
    GLuint PerVertexVertexShaderObject;
    GLuint PerVertexFragmentShaderObject;
    GLuint PerVertexShaderProgramObject;
    
    GLuint PerFragVertexShaderObject;
    GLuint PerFragFragmentShaderObject;
    GLuint PerFragShaderProgramObject;
    
    Sphere *sphereObject;
    
  
    enum eShapes
    {
        SPHERE = 0,
        NR_SHAPES,
    };
    enum eShape_Attr
    {
        POSITION = 0,
        NORMAL,
        ELEMENTS,
        NR_SHAPE_ATTRS,
    };
    enum eUniforms
    {
        MV = 0,
        PROJECTION,
        u_LightAmbient,
        u_LightDiffuse,
        u_LightSpecular,
        
        u_LightAmbient1,
        u_LightDiffuse1,
        u_LightSpecular1,
        
        u_LightAmbient2,
        u_LightDiffuse2,
        u_LightSpecular2,
        
        KA,
        KD,
        KS,
        SHININESS,
        
        u_LIGHT_POSITION,
        u_LIGHT_POSITION1,
        u_LIGHT_POSITION2,
        
        LIGHT_ENABLE,
        NR_UNIFORMS,
    };
    
    GLuint vao[NR_SHAPES];
    GLuint vbo[NR_SHAPES][NR_SHAPE_ATTRS];
   GLuint uniform[NR_UNIFORMS];
    
    BOOL bLightEnable;
    BOOL bLightEnablePerVertex;
    BOOL bLightEnablePerFragment;
    
    vmath::mat4 projectonMatrix;
}


-(id)initWithFrame:(CGRect)frame
{
    //code
    self = [super initWithFrame:frame];
    
    if(self)
    {
        CAEAGLLayer *eaglLayer = (CAEAGLLayer*)super.layer;
        
        eaglLayer.opaque = YES;
        eaglLayer.drawableProperties = [NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithBool:FALSE], kEAGLDrawablePropertyRetainedBacking, kEAGLColorFormatRGBA8, kEAGLDrawablePropertyColorFormat, nil];
        
        eaglContext = [[EAGLContext alloc] initWithAPI:kEAGLRenderingAPIOpenGLES3]; //EGL 3.0
        if(eaglContext == nil)
        {
            [self release];
            return (nil);
        }
        
        [EAGLContext setCurrentContext:eaglContext];
        
        glGenFramebuffers(1, &defaultFrameBuffer);
        glGenRenderbuffers(1, &colorRenderbuffer);
        glBindFramebuffer(GL_FRAMEBUFFER, defaultFrameBuffer);
        glBindRenderbuffer(GL_RENDERBUFFER, colorRenderbuffer);
        
        [eaglContext renderbufferStorage:GL_RENDERBUFFER fromDrawable:eaglLayer];
        
        glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_RENDERBUFFER, colorRenderbuffer);
        
        GLint backingWidth;
        GLint backingHeight;
        glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_WIDTH, &backingWidth);
        glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_HEIGHT, &backingHeight);
        
        glGenRenderbuffers(1, &depthRenderbuffer);
        glBindRenderbuffer(GL_RENDERBUFFER, depthRenderbuffer);
        glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT16, backingWidth, backingHeight);
        glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, depthRenderbuffer);
        
        if(glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
        {
            printf("initWithFrame: Failed To Create Complete Framebuffer Object 0x%x\n", glCheckFramebufferStatus(GL_FRAMEBUFFER));
            glDeleteFramebuffers(1, &defaultFrameBuffer);
            glDeleteRenderbuffers(1, &colorRenderbuffer);
            glDeleteRenderbuffers(1, &depthRenderbuffer);
            [self release];
            return (nil);
        }
        
        printf("Renderer : %s | GL Version : %s | GLSL Version : %s\n", glGetString(GL_RENDERER), glGetString(GL_VERSION), glGetString(GL_SHADING_LANGUAGE_VERSION));
        
        //hard coded initializations
        isAnimating = NO;
        animationFrameInterval = 60;    //its default since iOS 8.2
        
        //Do OpenGL Initialization here
    
        
        
// A ---------per fragmment shader......
        /*****VERTEX SHADER********/
        PerFragVertexShaderObject = glCreateShader(GL_VERTEX_SHADER);
        const GLchar* vsSource =
        "#version 300 es" \
        "\n" \
        "in vec4 vPosition;" \
        "in vec3 vNormal;" \
        "uniform mat4 u_model_view_matrix;" \
        "uniform mat4 u_projection_matrix;" \
        "uniform mediump int u_light_enabled;" \
        "uniform vec4 u_light_position;" \
        "uniform vec4 u_light_position1;" \
        "uniform vec4 u_light_position2;" \
        "out vec3 transformed_normals;" \
        "out vec3 light_direction;" \
        "out vec3 light_direction1;" \
        "out vec3 light_direction2;" \
        "out vec3 viewer_vector;" \
        "void main(void)" \
        "{" \
        "if(u_light_enabled==1)" \
        "{" \
        "vec4 eyeCoordinates = u_model_view_matrix * vPosition;" \
        "transformed_normals = mat3(u_model_view_matrix) * vNormal;" \
        "light_direction = vec3(u_light_position) - eyeCoordinates.xyz;" \
        "light_direction1 = vec3(u_light_position1) - eyeCoordinates.xyz;" \
        "light_direction2 = vec3(u_light_position2) - eyeCoordinates.xyz;" \
        "viewer_vector = -eyeCoordinates.xyz;" \
        "}"\
        "gl_Position = u_projection_matrix * u_model_view_matrix * vPosition;" \
        "}";
        
        
        glShaderSource(PerFragVertexShaderObject, 1, (const GLchar**)&vsSource, NULL);
        glCompileShader(PerFragVertexShaderObject);
        GLint iShaderCompileStatus = 0;
        glGetShaderiv(PerFragVertexShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);
        if(GL_FALSE == iShaderCompileStatus)
        {
            GLsizei iLogLen = 0;
            glGetShaderiv(PerFragVertexShaderObject, GL_INFO_LOG_LENGTH, &iLogLen);
            if(iLogLen)
            {
                GLchar* szLog = (GLchar*)malloc(iLogLen);
                if(szLog)
                {
                    GLsizei written;
                    glGetShaderInfoLog(PerFragVertexShaderObject, iLogLen, &written, szLog);
                    printf("Vertex Shader Compile Info: %s\n", szLog);
                    free(szLog);
                    [self release];
                    
                }
            }
        }//================
        
        /*****FRAGMENT SHADER********/
        PerFragFragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);
        const GLchar* fsSource =
        "#version 300 es" \
        "\n" \
        "precision highp float;"
        "in vec3 transformed_normals;" \
        "in vec3 light_direction;" \
        "in vec3 light_direction1;" \
        "in vec3 light_direction2;" \
        "in vec3 viewer_vector;" \
        "uniform vec3 u_La;" \
        "uniform vec3 u_Ld;" \
        "uniform vec3 u_Ls;" \
        "uniform vec3 u_La1;" \
        "uniform vec3 u_Ld1;" \
        "uniform vec3 u_Ls1;" \
        "uniform vec3 u_La2;" \
        "uniform vec3 u_Ld2;" \
        "uniform vec3 u_Ls2;" \
        "uniform int u_light_enabled;" \
        "uniform vec3 u_Ka;" \
        "uniform vec3 u_Kd;" \
        "uniform vec3 u_Ks;" \
        "uniform float u_material_shininess;" \
        "out vec4 FragColor;" \
        "void main(void)" \
        "{" \
        "vec3 phong_ads_color; " \
        "if(u_light_enabled == 1)" \
        "{" \
        "vec3 normalized_transformed_normals = normalize(transformed_normals);" \
        "vec3 normalized_light_direction = normalize(light_direction);" \
        "vec3 normalized_viewer_vector = normalize(viewer_vector);" \
        "float tn_dot_ld = max(dot(normalized_transformed_normals, normalized_light_direction), 0.0);" \
        "vec3 ambient = u_La * u_Ka;" \
        "vec3 diffuse = u_Ld * u_Kd * tn_dot_ld;" \
        "vec3 reflection_vector = reflect(-normalized_light_direction, normalized_transformed_normals);" \
        "float r_dot_v = max(dot(reflection_vector, normalized_viewer_vector), 0.0);" \
        "vec3 specular = u_Ls * u_Ks * pow(r_dot_v, u_material_shininess);" \
        "phong_ads_color = ambient + diffuse + specular;" \
        
        "normalized_light_direction = normalize(light_direction1);" \
        "tn_dot_ld = max(dot(normalized_transformed_normals, normalized_light_direction), 0.0);" \
        "ambient = u_La1 * u_Ka;" \
        "diffuse = u_Ld1 * u_Kd * tn_dot_ld;" \
        "reflection_vector = reflect(-normalized_light_direction, normalized_transformed_normals);" \
        "r_dot_v = max(dot(reflection_vector, normalized_viewer_vector), 0.0);" \
        "specular = u_Ls1 * u_Ks * pow(r_dot_v, u_material_shininess);" \
        "phong_ads_color = phong_ads_color + ambient + diffuse + specular;" \
        
        "normalized_light_direction = normalize(light_direction2);" \
        "tn_dot_ld = max(dot(normalized_transformed_normals, normalized_light_direction), 0.0);" \
        "ambient = u_La2 * u_Ka;" \
        "diffuse = u_Ld2 * u_Kd * tn_dot_ld;" \
        "reflection_vector = reflect(-normalized_light_direction, normalized_transformed_normals);" \
        "r_dot_v = max(dot(reflection_vector, normalized_viewer_vector), 0.0);" \
        "specular = u_Ls2 * u_Ks * pow(r_dot_v, u_material_shininess);" \
        "phong_ads_color = phong_ads_color + ambient + diffuse + specular;" \
        "}" \
        "else" \
        "{" \
        "phong_ads_color = vec3(1.0, 1.0, 1.0);" \
        "}" \
        "FragColor = vec4(phong_ads_color, 1.0);" \
        "}";
        
        glShaderSource(PerFragFragmentShaderObject, 1, (const GLchar**)&fsSource, NULL);
        glCompileShader(PerFragFragmentShaderObject);
        iShaderCompileStatus = 0;
        glGetShaderiv(PerFragFragmentShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);
        if(GL_FALSE == iShaderCompileStatus)
        {
            GLsizei iLogLen = 0;
            glGetShaderiv(PerFragFragmentShaderObject, GL_INFO_LOG_LENGTH, &iLogLen);
            if(iLogLen)
            {
                GLchar* szLog = (GLchar*)malloc(iLogLen);
                if(szLog)
                {
                    GLsizei written;
                    glGetShaderInfoLog(PerFragFragmentShaderObject, iLogLen, &written, szLog);
                    printf("Fragment Shader Compile Info: %s\n", szLog);
                    free(szLog);
                    [self release];
                    
                }
            }
        }//===================
        
        /*****SHADER PROGRAM********/
        PerFragShaderProgramObject = glCreateProgram();
        //attach vs and fs to shader program
        glAttachShader(PerFragShaderProgramObject, PerFragVertexShaderObject);
        glAttachShader(PerFragShaderProgramObject, PerFragFragmentShaderObject);
        
        //pre-link binding of vertex attributes
        glBindAttribLocation(PerFragShaderProgramObject, VDG_ATTRIBUTE_POSITION, "vPosition");     // <--- Modify/add as per requirement
        glBindAttribLocation(PerFragShaderProgramObject, VDG_ATTRIBUTE_NORMAL, "vNormal");
        
        //link program
        glLinkProgram(PerFragShaderProgramObject);
        
        //post-link, get uniform locations from shaders
        uniform[MV] = glGetUniformLocation(PerFragShaderProgramObject, "u_model_view_matrix");           // <--- Modify/add as per requirement
        uniform[PROJECTION] = glGetUniformLocation(PerFragShaderProgramObject, "u_projection_matrix");
        uniform[u_LightAmbient] = glGetUniformLocation(PerFragShaderProgramObject, "u_La");
        uniform[u_LightDiffuse] = glGetUniformLocation(PerFragShaderProgramObject, "u_Ld");
        uniform[u_LightSpecular] = glGetUniformLocation(PerFragShaderProgramObject, "u_Ls");
        uniform[u_LightAmbient1] = glGetUniformLocation(PerFragShaderProgramObject, "u_La1");
        uniform[u_LightDiffuse1] = glGetUniformLocation(PerFragShaderProgramObject, "u_Ld1");
        uniform[u_LightSpecular1] = glGetUniformLocation(PerFragShaderProgramObject, "u_Ls1");
        uniform[u_LightAmbient2] = glGetUniformLocation(PerFragShaderProgramObject, "u_La2");
        uniform[u_LightDiffuse2] = glGetUniformLocation(PerFragShaderProgramObject, "u_Ld2");
        uniform[u_LightSpecular2] = glGetUniformLocation(PerFragShaderProgramObject, "u_Ls2");
        uniform[KA] = glGetUniformLocation(PerFragShaderProgramObject, "u_Ka");
        uniform[KD] = glGetUniformLocation(PerFragShaderProgramObject, "u_Kd");
        uniform[KS] = glGetUniformLocation(PerFragShaderProgramObject, "u_Ks");
        uniform[SHININESS] = glGetUniformLocation(PerFragShaderProgramObject, "u_material_shininess");
        uniform[u_LIGHT_POSITION] = glGetUniformLocation(PerFragShaderProgramObject, "u_light_position");
        uniform[u_LIGHT_POSITION1] = glGetUniformLocation(PerFragShaderProgramObject, "u_light_position1");
        uniform[u_LIGHT_POSITION2] = glGetUniformLocation(PerFragShaderProgramObject, "u_light_position2");
        uniform[LIGHT_ENABLE] = glGetUniformLocation(PerFragShaderProgramObject, "u_light_enabled");

//B ---------per vertex shader......
        /*****VERTEX SHADER********/
        PerVertexVertexShaderObject = glCreateShader(GL_VERTEX_SHADER);
        const GLchar* vvsSource =
        "#version 300 es" \
        "\n" \
        "in vec4 vPosition;" \
        "in vec3 vNormal;" \
        "uniform mat4 u_model_view_matrix;" \
        "uniform mat4 u_projection_matrix;" \
        "uniform int u_light_enabled;" \
        "uniform vec3 u_La;" \
        "uniform vec3 u_Ld;" \
        "uniform vec3 u_Ls;" \
        "uniform vec4 u_light_position;" \
        
        "uniform vec3 u_La1;" \
        "uniform vec3 u_Ld1;" \
        "uniform vec3 u_Ls1;" \
        "uniform vec4 u_light_position1;" \
        
        "uniform vec3 u_La2;" \
        "uniform vec3 u_Ld2;" \
        "uniform vec3 u_Ls2;" \
        "uniform vec4 u_light_position2;" \
        "uniform vec3 u_Ka;" \
        "uniform vec3 u_Kd;" \
        "uniform vec3 u_Ks;" \
        "uniform float u_material_shininess;" \
        "out vec3 phong_ads_color;" \
        "void main(void)" \
        "{" \
        "if(u_light_enabled==1)" \
        "{" \
        "vec3 ambient = u_La * u_Ka;" \
        "vec4 eyeCoordinates = u_model_view_matrix * vPosition;" \
        "vec3 transformed_normals = normalize(mat3(u_model_view_matrix) * vNormal);" \
        "vec3 light_direction = normalize(vec3(u_light_position) - eyeCoordinates.xyz);" \
        "float tn_dot_ld = max(dot(transformed_normals, light_direction), 0.0);" \
        "vec3 diffuse = u_Ld * u_Kd * tn_dot_ld;" \
        "vec3 reflection_vector = reflect(-light_direction, transformed_normals);" \
        "vec3 viewer_vector = normalize(-eyeCoordinates.xyz);" \
        "float r_dot_v = max(dot(reflection_vector, viewer_vector), 0.0);" \
        "vec3 specular = u_Ls * u_Ks * pow(r_dot_v, u_material_shininess);" \
        "phong_ads_color = ambient + diffuse + specular;" \
        
        "light_direction = normalize(vec3(u_light_position1) - eyeCoordinates.xyz);" \
        "tn_dot_ld = max(dot(transformed_normals, light_direction), 0.0);" \
        "ambient = u_La1 * u_Ka;" \
        "diffuse = u_Ld1 * u_Kd * tn_dot_ld;" \
        "reflection_vector = reflect(-light_direction, transformed_normals);" \
        "r_dot_v = max(dot(reflection_vector, viewer_vector), 0.0);" \
        "specular = u_Ls1 * u_Ks * pow(r_dot_v, u_material_shininess);" \
        "phong_ads_color = phong_ads_color + ambient + diffuse + specular;" \
        
        "light_direction = normalize(vec3(u_light_position2) - eyeCoordinates.xyz);" \
        "tn_dot_ld = max(dot(transformed_normals, light_direction), 0.0);" \
        "ambient = u_La2 * u_Ka;" \
        "diffuse = u_Ld2 * u_Kd * tn_dot_ld;" \
        "reflection_vector = reflect(-light_direction, transformed_normals);" \
        "r_dot_v = max(dot(reflection_vector, viewer_vector), 0.0);" \
        "specular = u_Ls2 * u_Ks * pow(r_dot_v, u_material_shininess);" \
        "phong_ads_color = phong_ads_color + ambient + diffuse + specular;" \
        
        "}"\
        "else" \
        "{" \
        "phong_ads_color = vec3(1.0, 1.0, 1.0);" \
        "}" \
        "gl_Position = u_projection_matrix * u_model_view_matrix * vPosition;" \
        "}";
        
        
        glShaderSource(PerVertexVertexShaderObject, 1, (const GLchar**)&vvsSource, NULL);
        glCompileShader(PerVertexVertexShaderObject);
        GLint iiShaderCompileStatus = 0;
        glGetShaderiv(PerVertexVertexShaderObject, GL_COMPILE_STATUS, &iiShaderCompileStatus);
        if(GL_FALSE == iiShaderCompileStatus)
        {
            GLsizei iLogLen = 0;
            glGetShaderiv(PerVertexVertexShaderObject, GL_INFO_LOG_LENGTH, &iLogLen);
            if(iLogLen)
            {
                GLchar* szLog = (GLchar*)malloc(iLogLen);
                if(szLog)
                {
                    GLsizei written;
                    glGetShaderInfoLog(PerVertexVertexShaderObject, iLogLen, &written, szLog);
                    printf("PV: Vertex Shader Compile Info: %s\n", szLog);
                    free(szLog);
                    [self release];
                    
                }
            }
        }//================
        
        /*****FRAGMENT SHADER********/
        PerVertexFragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);
        const GLchar* ffsSource =
        "#version 300 es" \
        "\n" \
        "precision highp float;"
        "in vec3 phong_ads_color;" \
        "out vec4 FragColor;" \
        "void main(void)" \
        "{" \
        "FragColor = vec4(phong_ads_color, 1.0);" \
        "}";
        
        glShaderSource(PerVertexFragmentShaderObject, 1, (const GLchar**)&ffsSource, NULL);
        glCompileShader(PerVertexFragmentShaderObject);
        iiShaderCompileStatus = 0;
        glGetShaderiv(PerVertexFragmentShaderObject, GL_COMPILE_STATUS, &iiShaderCompileStatus);
        if(GL_FALSE == iiShaderCompileStatus)
        {
            GLsizei iLogLen = 0;
            glGetShaderiv(PerVertexFragmentShaderObject, GL_INFO_LOG_LENGTH, &iLogLen);
            if(iLogLen)
            {
                GLchar* szLog = (GLchar*)malloc(iLogLen);
                if(szLog)
                {
                    GLsizei written;
                    glGetShaderInfoLog(PerVertexFragmentShaderObject, iLogLen, &written, szLog);
                    printf("PV: Fragment Shader Compile Info: %s\n", szLog);
                    free(szLog);
                    [self release];
                }
            }
        }//===================
        
        /*****SHADER PROGRAM********/
        PerVertexShaderProgramObject = glCreateProgram();
        //attach vs and fs to shader program
        glAttachShader(PerVertexShaderProgramObject, PerVertexVertexShaderObject);
        glAttachShader(PerVertexShaderProgramObject, PerVertexFragmentShaderObject);
        
        //pre-link binding of vertex attributes
        glBindAttribLocation(PerVertexShaderProgramObject, VDG_ATTRIBUTE_POSITION, "vPosition");     // <--- Modify/add as per requirement
        glBindAttribLocation(PerVertexShaderProgramObject, VDG_ATTRIBUTE_NORMAL, "vNormal");
        
        //link program
        glLinkProgram(PerVertexShaderProgramObject);
        
        //post-link, get uniform locations from shaders
        uniform[MV] = glGetUniformLocation(PerVertexShaderProgramObject, "u_model_view_matrix");           // <--- Modify/add as per requirement
        uniform[PROJECTION] = glGetUniformLocation(PerVertexShaderProgramObject, "u_projection_matrix");
        uniform[u_LightAmbient] = glGetUniformLocation(PerVertexShaderProgramObject, "u_La");
        uniform[u_LightDiffuse] = glGetUniformLocation(PerVertexShaderProgramObject, "u_Ld");
        uniform[u_LightSpecular] = glGetUniformLocation(PerVertexShaderProgramObject, "u_Ls");
        uniform[u_LightAmbient1] = glGetUniformLocation(PerVertexShaderProgramObject, "u_La1");
        uniform[u_LightDiffuse1] = glGetUniformLocation(PerVertexShaderProgramObject, "u_Ld1");
        uniform[u_LightSpecular1] = glGetUniformLocation(PerVertexShaderProgramObject, "u_Ls1");
        uniform[u_LightAmbient2] = glGetUniformLocation(PerVertexShaderProgramObject, "u_La2");
        uniform[u_LightDiffuse2] = glGetUniformLocation(PerVertexShaderProgramObject, "u_Ld2");
        uniform[u_LightSpecular2] = glGetUniformLocation(PerVertexShaderProgramObject, "u_Ls2");
        uniform[KA] = glGetUniformLocation(PerVertexShaderProgramObject, "u_Ka");
        uniform[KD] = glGetUniformLocation(PerVertexShaderProgramObject, "u_Kd");
        uniform[KS] = glGetUniformLocation(PerVertexShaderProgramObject, "u_Ks");
        uniform[SHININESS] = glGetUniformLocation(PerVertexShaderProgramObject, "u_material_shininess");
        uniform[u_LIGHT_POSITION] = glGetUniformLocation(PerVertexShaderProgramObject, "u_light_position");
        uniform[u_LIGHT_POSITION1] = glGetUniformLocation(PerVertexShaderProgramObject, "u_light_position1");
        uniform[u_LIGHT_POSITION2] = glGetUniformLocation(PerVertexShaderProgramObject, "u_light_position2");
        uniform[LIGHT_ENABLE] = glGetUniformLocation(PerVertexShaderProgramObject, "u_light_enabled");

        //load textures, if any
        //============
        
        /******* Vertices, colors, normals, vao, vbo initialization**********/
        // Sphere
        sphereObject = [[Sphere alloc] init];
        [sphereObject getSphereVertexData:sphere_vertices :sphere_normals :sphere_textures :sphere_elements];
        gNumVertices = [sphereObject getNumberOfSphereVertices];
        gNumElements = [sphereObject getNumberOfSphereElements];
        
        printf("nr vertices:%d\n", gNumVertices);
        printf("nr elements:%d\n", gNumElements);
        
        //VAO:Sphere
        glGenVertexArrays(1, &vao[SPHERE]);
        glBindVertexArray(vao[SPHERE]);
        
        
        //position vbo
            glGenBuffers(1, &vbo[SPHERE][POSITION]);
            glBindBuffer(GL_ARRAY_BUFFER, vbo[SPHERE][POSITION]);
            glBufferData(GL_ARRAY_BUFFER, sizeof(sphere_vertices), sphere_vertices, GL_STATIC_DRAW);
            glVertexAttribPointer(VDG_ATTRIBUTE_POSITION,
                                  3, //xyz
                                  GL_FLOAT,
                                  GL_FALSE, //non-normalize
                                  0, NULL); //0=stride
            glEnableVertexAttribArray(VDG_ATTRIBUTE_POSITION);
            glBindBuffer(GL_ARRAY_BUFFER, 0);   //unbind
    
        
        
        //normal vbo
            glGenBuffers(1, &vbo[SPHERE][NORMAL]);
            glBindBuffer(GL_ARRAY_BUFFER, vbo[SPHERE][NORMAL]);
            glBufferData(GL_ARRAY_BUFFER, sizeof(sphere_normals), sphere_normals, GL_STATIC_DRAW);
            glVertexAttribPointer(VDG_ATTRIBUTE_NORMAL,
                                  3, //nx, ny, nz
                                  GL_FLOAT,
                                  GL_FALSE, //non-normalize
                                  0, NULL); //0=stride
            glEnableVertexAttribArray(VDG_ATTRIBUTE_NORMAL);
            glBindBuffer(GL_ARRAY_BUFFER, 0);   //unbind
       
        //element vbo
        
        glGenBuffers(1, &vbo[SPHERE][ELEMENTS]);
            glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo[SPHERE][ELEMENTS]);
            
            glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(sphere_elements), sphere_elements, GL_STATIC_DRAW);
            
            glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);   //unbind

        glBindVertexArray(0);   //unbind
        //==============
        
        //Other OpenGL settings
        glEnable(GL_DEPTH_TEST);    //enable depth testing
        glDepthFunc(GL_LEQUAL);     //depth test to do
        
        //glEnable(GL_CULL_FACE);     //We will always cull back faces for better performance
        
        //set background clearing color
        glClearColor(0.0f, 0.0f, 0.0f, 1.0f);   //black
        
        //initialize ortho projectio matrix to identity
        projectonMatrix = vmath::mat4::identity();
        
        bLightEnable = YES; //default light enabled
        bLightEnablePerVertex = YES;   //by default per vertex lighting
        bLightEnablePerFragment = NO;
        //================
        
        //Gesture Recognizer
        //Tap Gesture code
        UITapGestureRecognizer *singleTapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onSingleTap:)];
        [singleTapGestureRecognizer setNumberOfTapsRequired:1]; //single tap to recognize singleTap event
        [singleTapGestureRecognizer setNumberOfTouchesRequired:1];  //one finger touch is required to recognize single tap
        [singleTapGestureRecognizer setDelegate:self];
        [self addGestureRecognizer:singleTapGestureRecognizer];
        
        
        UITapGestureRecognizer *doubleTapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onDouleTap:)];
        [doubleTapGestureRecognizer setNumberOfTapsRequired:2]; //two taps required to detect double tap
        [doubleTapGestureRecognizer setNumberOfTouchesRequired:1]; //one finger touch
        [doubleTapGestureRecognizer setDelegate:self];
        [self addGestureRecognizer:doubleTapGestureRecognizer];
        
        //this will allow to differentiate between single tap and double tap
        [singleTapGestureRecognizer requireGestureRecognizerToFail:doubleTapGestureRecognizer];
        
        
        
        UISwipeGestureRecognizer *swipeGestureRecognizer = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(onSwipe:)];
        [self addGestureRecognizer:swipeGestureRecognizer];
        
        
        UILongPressGestureRecognizer *longPressGestureRecognizer = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(onLongPress:)];
        [self addGestureRecognizer:longPressGestureRecognizer];
    }
    return (self);
}/*initWithFrame*/



+(Class)layerClass      //static method
{
    //code
    return ([CAEAGLLayer class]);   //return class which does animation
}

-(void)drawView:(id)sender  //display
{
    //code
    static GLfloat index = 0.0f;
    static GLint lightAngleRotate = 5000;    //360
    
    [EAGLContext setCurrentContext:eaglContext];
    
    glBindFramebuffer(GL_FRAMEBUFFER, defaultFrameBuffer);
    
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);
    
    /*Your Drawing*/        // <--- Modify as per requirement
    
    if(bLightEnablePerVertex == YES)
    {
        //get uniform locations from shaders
        uniform[MV] = glGetUniformLocation(PerVertexShaderProgramObject, "u_model_view_matrix");
        uniform[PROJECTION] = glGetUniformLocation(PerVertexShaderProgramObject, "u_projection_matrix");
        uniform[u_LightAmbient] = glGetUniformLocation(PerVertexShaderProgramObject, "u_La");
        uniform[u_LightDiffuse] = glGetUniformLocation(PerVertexShaderProgramObject, "u_Ld");
        uniform[u_LightSpecular] = glGetUniformLocation(PerVertexShaderProgramObject, "u_Ls");
        uniform[u_LightAmbient1] = glGetUniformLocation(PerVertexShaderProgramObject, "u_La1");
        uniform[u_LightDiffuse1] = glGetUniformLocation(PerVertexShaderProgramObject, "u_Ld1");
        uniform[u_LightSpecular1] = glGetUniformLocation(PerVertexShaderProgramObject, "u_Ls1");
        uniform[u_LightAmbient2] = glGetUniformLocation(PerVertexShaderProgramObject, "u_La2");
        uniform[u_LightDiffuse2] = glGetUniformLocation(PerVertexShaderProgramObject, "u_Ld2");
        uniform[u_LightSpecular2] = glGetUniformLocation(PerVertexShaderProgramObject, "u_Ls2");
        uniform[KA] = glGetUniformLocation(PerVertexShaderProgramObject, "u_Ka");
        uniform[KD] = glGetUniformLocation(PerVertexShaderProgramObject, "u_Kd");
        uniform[KS] = glGetUniformLocation(PerVertexShaderProgramObject, "u_Ks");
        uniform[SHININESS] = glGetUniformLocation(PerVertexShaderProgramObject, "u_material_shininess");
        uniform[u_LIGHT_POSITION] = glGetUniformLocation(PerVertexShaderProgramObject, "u_light_position");
        uniform[u_LIGHT_POSITION1] = glGetUniformLocation(PerVertexShaderProgramObject, "u_light_position1");
        uniform[u_LIGHT_POSITION2] = glGetUniformLocation(PerVertexShaderProgramObject, "u_light_position2");
        uniform[LIGHT_ENABLE] = glGetUniformLocation(PerVertexShaderProgramObject, "u_light_enabled");
    }
    else
    {//per fragment
        //get uniform locations from shaders
        uniform[MV] = glGetUniformLocation(PerFragShaderProgramObject, "u_model_view_matrix");
        uniform[PROJECTION] = glGetUniformLocation(PerFragShaderProgramObject, "u_projection_matrix");
        uniform[u_LightAmbient] = glGetUniformLocation(PerFragShaderProgramObject, "u_La");
        uniform[u_LightDiffuse] = glGetUniformLocation(PerFragShaderProgramObject, "u_Ld");
        uniform[u_LightSpecular] = glGetUniformLocation(PerFragShaderProgramObject, "u_Ls");
        uniform[u_LightAmbient1] = glGetUniformLocation(PerFragShaderProgramObject, "u_La1");
        uniform[u_LightDiffuse1] = glGetUniformLocation(PerFragShaderProgramObject, "u_Ld1");
        uniform[u_LightSpecular1] = glGetUniformLocation(PerFragShaderProgramObject, "u_Ls1");
        uniform[u_LightAmbient2] = glGetUniformLocation(PerFragShaderProgramObject, "u_La2");
        uniform[u_LightDiffuse2] = glGetUniformLocation(PerFragShaderProgramObject, "u_Ld2");
        uniform[u_LightSpecular2] = glGetUniformLocation(PerFragShaderProgramObject, "u_Ls2");
        uniform[KA] = glGetUniformLocation(PerFragShaderProgramObject, "u_Ka");
        uniform[KD] = glGetUniformLocation(PerFragShaderProgramObject, "u_Kd");
        uniform[KS] = glGetUniformLocation(PerFragShaderProgramObject, "u_Ks");
        uniform[SHININESS] = glGetUniformLocation(PerFragShaderProgramObject, "u_material_shininess");
        uniform[u_LIGHT_POSITION] = glGetUniformLocation(PerFragShaderProgramObject, "u_light_position");
        uniform[u_LIGHT_POSITION1] = glGetUniformLocation(PerFragShaderProgramObject, "u_light_position1");
        uniform[u_LIGHT_POSITION2] = glGetUniformLocation(PerFragShaderProgramObject, "u_light_position2");
        uniform[LIGHT_ENABLE] = glGetUniformLocation(PerFragShaderProgramObject, "u_light_enabled");
    }
    
    if(bLightEnablePerVertex == YES)
        glUseProgram(PerVertexShaderProgramObject);
    else
        glUseProgram(PerFragShaderProgramObject);
    
    //SEND LIGHTING VALUES TO SHADER
    if(bLightEnable == YES)
    {
        glUniform1i(uniform[LIGHT_ENABLE], 1);
        
        //L0
        glUniform3fv(uniform[u_LightAmbient], 1, lightAmbient00);
        glUniform3fv(uniform[u_LightDiffuse], 1, lightDiffuse00);
        glUniform3fv(uniform[u_LightSpecular], 1, lightSpecular00);
       
        
        lightPosition00[0] = 0.0f;
        lightPosition00[1] = 10.0f*cos(gAngle);
        lightPosition00[2] = 10.0f*sin(gAngle);
        glUniform4fv(uniform[u_LIGHT_POSITION], 1, lightPosition00);
        
        
        //L1
        glUniform3fv(uniform[u_LightAmbient1], 1, lightAmbient01);
        glUniform3fv(uniform[u_LightDiffuse1], 1, lightDiff01);
        glUniform3fv(uniform[u_LightSpecular1], 1, lightSpec01);
       
        
        lightPosition01[0] = 10.0f*cos(gAngle);
        lightPosition01[1] = 0.0f;
        lightPosition01[2] = 10.0f*sin(gAngle);
        glUniform4fv(uniform[u_LIGHT_POSITION1], 1, lightPosition01);
        
       //L2
        glUniform3fv(uniform[u_LightAmbient2], 1, lightAmbient02);
        glUniform3fv(uniform[u_LightDiffuse2], 1, lightDiff02);
        glUniform3fv(uniform[u_LightSpecular2], 1, lightSpec02);
      
        
        lightPosition02[0] = 10.0f*cos(gAngle);
        lightPosition02[1] = 10.0f*sin(gAngle);
        lightPosition02[2] = 0.0f;
        glUniform4fv(uniform[u_LIGHT_POSITION2], 1, lightPosition02);
        
        //Material
        glUniform3fv(uniform[KA], 1, materialAmbient);
        glUniform3fv(uniform[KD], 1, materialDiffuse);
        glUniform3fv(uniform[KS], 1, materialSpecular);
        glUniform1f(uniform[SHININESS], materialShininess);
    }
    else
    {
        glUniform1i(uniform[LIGHT_ENABLE], 0);
    }
    //==========
    
    //Sphere
    vmath::mat4 modelViewMatrix = vmath::mat4::identity();
    vmath::mat4 translateMatrix = vmath::mat4::identity();
    
    
    translateMatrix = vmath::translate(0.0f, 0.0f, -1.5f);
    //MV = T * MV
    modelViewMatrix = translateMatrix * modelViewMatrix;
    
    //send MV to shader
    glUniformMatrix4fv(uniform[MV], 1, GL_FALSE, modelViewMatrix);
    
    //send Projection matrix to shader
    glUniformMatrix4fv(uniform[PROJECTION], 1, GL_FALSE, projectonMatrix);
    
    //bind sphere vao
    glBindVertexArray(vao[SPHERE]);
    //draw by elements
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo[SPHERE][ELEMENTS]);
    glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);
    glBindVertexArray(0);   //unbind
    
    glUseProgram(0);
    //=============
    
    glBindRenderbuffer(GL_RENDERBUFFER, colorRenderbuffer);
    [eaglContext presentRenderbuffer:GL_RENDERBUFFER];
    
    //light angle
    index = index + 30.0f;
    if (index >= lightAngleRotate)
        index = 0;
    gAngle = (2 * 3.14f*index) / lightAngleRotate;
}

-(void)layoutSubviews   //resize
{
    //code
    GLint width;
    GLint height;
    
    glBindRenderbuffer(GL_RENDERBUFFER, colorRenderbuffer);
    [eaglContext renderbufferStorage:GL_RENDERBUFFER fromDrawable:(CAEAGLLayer*)self.layer];
    glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_WIDTH, &width);
    glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_HEIGHT, &height);
    
    glGenRenderbuffers(1, &depthRenderbuffer);
    glBindRenderbuffer(1, depthRenderbuffer);
    glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT16, width, height);
    glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, depthRenderbuffer);
    
    glViewport(0, 0, width, height);
    
   
    GLfloat fwidth = (GLfloat)width;
    GLfloat fheight = (GLfloat)height;
    

    projectonMatrix = vmath::perspective(45.0f, fwidth/fheight, 0.1f, 100.0f);
    
    if(glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
    {
        printf("layoutSubviews: Failed to Create Complete Framebuffer Object 0x%x\n", glCheckFramebufferStatus(GL_FRAMEBUFFER));
    }
    
    [self drawView:nil];
}

-(void)startAnimation
{
    printf("startAnimation\n");
    if(!isAnimating)
    {
        displayLink = [NSClassFromString(@"CADisplayLink")displayLinkWithTarget:self selector:@selector(drawView:)];
        [displayLink setPreferredFramesPerSecond:animationFrameInterval];
        [displayLink addToRunLoop:[NSRunLoop currentRunLoop] forMode:NSDefaultRunLoopMode];
        
        isAnimating = YES;
    }
}

-(void)stopAnimation
{
    printf("stopAnimation\n");
    if(isAnimating)
    {
        [displayLink invalidate];
        displayLink = nil;
        
        isAnimating = NO;
    }
}

//to became first responder
-(BOOL)acceptsFirstResponder
{
    //code
    return (YES);
}

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent*)event
{
    
}

-(void)onSingleTap:(UITapGestureRecognizer*)gr
{
    static int count=1; //in PerVertex mode
    if(++count > 2)
        count = 1;
    
    if(count == 1)
    {
        bLightEnablePerVertex = YES;
        bLightEnablePerFragment = NO;
    }
    if(count == 2)
    {
        bLightEnablePerVertex = NO;
        bLightEnablePerFragment = YES;
    }
    
    
}

-(void)onDouleTap:(UITapGestureRecognizer*)gr
{
    if(bLightEnable == NO)
        bLightEnable = YES;
    else
        bLightEnable = NO;
}

-(void)onSwipe:(UISwipeGestureRecognizer*)gr
{
    //code
    printf("onSwipe: Exiting...\n");
    [self release];
    exit(0);
}

-(void)onLongPress:(UILongPressGestureRecognizer*)gr
{
    
}

-(void)dealloc
{
    
    
    //Release Shader objects
    if(PerFragShaderProgramObject)
    {
        if(PerFragFragmentShaderObject)
        {
            glDetachShader(PerFragShaderProgramObject, PerFragFragmentShaderObject);
            glDeleteShader(PerFragFragmentShaderObject);
            PerFragFragmentShaderObject = 0;
        }
        if(PerFragVertexShaderObject)
        {
            glDetachShader(PerFragShaderProgramObject, PerFragVertexShaderObject);
            glDeleteShader(PerFragVertexShaderObject);
            PerFragVertexShaderObject = 0;
        }
        
        glDeleteProgram(PerFragShaderProgramObject);
        PerFragShaderProgramObject = 0;
    }
    
    if(PerVertexShaderProgramObject)
    {
        if(PerVertexFragmentShaderObject)
        {
            glDetachShader(PerVertexShaderProgramObject, PerVertexFragmentShaderObject);
            glDeleteShader(PerVertexFragmentShaderObject);
            PerVertexFragmentShaderObject = 0;
        }
        if(PerVertexVertexShaderObject)
        {
            glDetachShader(PerVertexShaderProgramObject, PerVertexVertexShaderObject);
            glDeleteShader(PerVertexVertexShaderObject);
            PerVertexVertexShaderObject = 0;
        }
        
        glDeleteProgram(PerVertexShaderProgramObject);
        PerVertexShaderProgramObject = 0;
    }
    
    if(depthRenderbuffer)
    {
        glDeleteRenderbuffers(1, &depthRenderbuffer);
        depthRenderbuffer = 0;
    }
    
    if(colorRenderbuffer)
    {
        glDeleteRenderbuffers(1, &colorRenderbuffer);
        colorRenderbuffer = 0;
    }
    
    if(defaultFrameBuffer)
    {
        glDeleteFramebuffers(1, &defaultFrameBuffer);
        defaultFrameBuffer = 0;
    }
    
    if([EAGLContext currentContext] == eaglContext)
    {
        [EAGLContext setCurrentContext:nil];
    }
    [eaglContext release];
    eaglContext = nil;
    
    [sphereObject release];
    [super dealloc];
}

@end
