//
//  MyView.h
//  DfirstWindow
//
//  Created by nvidia on 7/27/18.
//

#import <UIKit/UIKit.h>

@interface MyView : UIView <UIGestureRecognizerDelegate>
-(void)startAnimation;
-(void)stopAnimation;
@end
