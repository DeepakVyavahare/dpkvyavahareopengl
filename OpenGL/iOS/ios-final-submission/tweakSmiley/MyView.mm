//  MyView.m
//
//Tweak-Smiley
//
//  Created by nvidia on 30/07/18.
//
#import <OpenGLES/ES3/gl.h>
#import <OpenGLES/ES3/glext.h>

#import "vmath.h"

#import "MyView.h"

enum
{
    VDG_ATTRIBUTE_POSITION,
    VDG_ATTRIBUTE_COLOR,
    VDG_ATTRIBUTE_NORMAL,
    VDG_ATTRIBUTE_TEXTURE0,
};

@implementation MyView
{
@private
    EAGLContext *eaglContext;
    
    GLuint defaultFrameBuffer;
    GLuint colorRenderbuffer;
    GLuint depthRenderbuffer;
    
    id displayLink;
    NSInteger animationFrameInterval;
    BOOL isAnimating;
    
    GLuint vertexShaderObject;
    GLuint fragmentShaderObject;
    GLuint shaderProgramObject;
    
    /*************
     Dont Keep Un-used enums in this code,
     else vao, vbo, uniform arrays will be populated unnecessarly
     ***************/
    enum eShapes            // <--- Modify as per requirement
    {
        CUBE=0,
        NR_SHAPES,
    };
    enum eShape_Attr
    {
        POSITION = 0,
        TEXCOORDS,
        NR_SHAPE_ATTRS,
    };
    enum eUniforms
    {
        MVP = 0,
        TEXTURE_SAMPLER_2D,
        NR_UNIFORMS,
    };
    
    GLuint vao[NR_SHAPES];
    GLuint vbo[NR_SHAPES][NR_SHAPE_ATTRS];
    GLuint uniform[NR_UNIFORMS];
    
    GLuint texture_cube;
    GLuint proceduralTextureObject;
    GLuint keyPress;
    
    vmath::mat4 projectonMatrix;
 
    GLubyte procImageArray[64][64][4];
}
/*
 // Only override drawRect: if you perform custom drawing.
 // An empty implementation adversely affects performance during animation.
 - (void)drawRect:(CGRect)rect {
 // Drawing code
 }
 */

-(id)initWithFrame:(CGRect)frame
{
    //code
    self = [super initWithFrame:frame];
    
    if(self)
    {
        CAEAGLLayer *eaglLayer = (CAEAGLLayer*)super.layer;
        
        eaglLayer.opaque = YES;
        eaglLayer.drawableProperties = [NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithBool:FALSE], kEAGLDrawablePropertyRetainedBacking, kEAGLColorFormatRGBA8, kEAGLDrawablePropertyColorFormat, nil];
        
        eaglContext = [[EAGLContext alloc] initWithAPI:kEAGLRenderingAPIOpenGLES3]; //EGL 3.0
        if(eaglContext == nil)
        {
            [self release];
            return (nil);
        }
        
        [EAGLContext setCurrentContext:eaglContext];
        
        glGenFramebuffers(1, &defaultFrameBuffer);
        glGenRenderbuffers(1, &colorRenderbuffer);
        glBindFramebuffer(GL_FRAMEBUFFER, defaultFrameBuffer);
        glBindRenderbuffer(GL_RENDERBUFFER, colorRenderbuffer);
        
        [eaglContext renderbufferStorage:GL_RENDERBUFFER fromDrawable:eaglLayer];
        
        glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_RENDERBUFFER, colorRenderbuffer);
        
        GLint backingWidth;
        GLint backingHeight;
        glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_WIDTH, &backingWidth);
        glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_HEIGHT, &backingHeight);
        
        glGenRenderbuffers(1, &depthRenderbuffer);
        glBindRenderbuffer(GL_RENDERBUFFER, depthRenderbuffer);
        glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT16, backingWidth, backingHeight);
        glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, depthRenderbuffer);
        
        if(glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
        {
            printf("initWithFrame: Failed To Create Complete Framebuffer Object 0x%x\n", glCheckFramebufferStatus(GL_FRAMEBUFFER));
            glDeleteFramebuffers(1, &defaultFrameBuffer);
            glDeleteRenderbuffers(1, &colorRenderbuffer);
            glDeleteRenderbuffers(1, &depthRenderbuffer);
            [self release];
            return (nil);
        }
        
        printf("Renderer : %s | GL Version : %s | GLSL Version : %s\n", glGetString(GL_RENDERER), glGetString(GL_VERSION), glGetString(GL_SHADING_LANGUAGE_VERSION));
        
        //hard coded initializations
        isAnimating = NO;
        animationFrameInterval = 60;
        //=================
        
        //Do OpenGL Initialization here
        /********VERTEX SHADER**********/
        vertexShaderObject = glCreateShader(GL_VERTEX_SHADER);
        const GLchar* vertexShaderSourceCode =
        "#version 300 es"\
        "\n" \
        "in vec4 vPosition;" \
        "in vec2 vTexture0_Coord;" \
        "uniform mat4 u_mvp_matrix;" \
        "out vec2 outTextureCoord;" \
        "void main(void)" \
        "{" \
        "gl_Position = u_mvp_matrix * vPosition;" \
        "outTextureCoord = vTexture0_Coord;" \
        "}";
        
        glShaderSource(vertexShaderObject, 1, (const GLchar **)&vertexShaderSourceCode, NULL);
        glCompileShader(vertexShaderObject);
        //error handling
        GLint iShaderCompileStatus = 0;
        glGetShaderiv(vertexShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);
        if(iShaderCompileStatus == GL_FALSE)
        {
            GLint iShaderInfoLogLen=0;
            glGetShaderiv(vertexShaderObject, GL_INFO_LOG_LENGTH, &iShaderInfoLogLen);
            char *szInfoLog = (char*)malloc(iShaderInfoLogLen);
            if(szInfoLog)
            {
                GLint written;
                glGetShaderInfoLog(vertexShaderObject, iShaderInfoLogLen, &written, szInfoLog);
                printf( "Vertex Shader Compile Info: %s\n", szInfoLog);
                free(szInfoLog);
                [self release];
                
            }
        }
        
        
        /********FRAGMENT SHADER**********/
        fragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);
        const GLchar* fragmentShaderSourceCode =
        "#version 300 es" \
        "\n" \
        "precision highp float;" \
        "in vec2 outTextureCoord;" \
        "uniform sampler2D u_Texture_sampler2d;" \
        "out vec4 FragColor;" \
        "void main(void)" \
        "{" \
        "vec3 tex = vec3(texture(u_Texture_sampler2d, outTextureCoord));" \
        "FragColor = vec4(tex, 1.0f);" \
        "}";
        glShaderSource(fragmentShaderObject,1, (GLchar**)&fragmentShaderSourceCode, NULL);
        glCompileShader(fragmentShaderObject);
        iShaderCompileStatus = 0;
        glGetShaderiv(fragmentShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);
        if(iShaderCompileStatus == GL_FALSE)
        {
            GLint iShaderInfoLogLen = 0;
            glGetShaderiv(fragmentShaderObject, GL_INFO_LOG_LENGTH, &iShaderInfoLogLen);
            char *szInfoLog = (char*)malloc(iShaderInfoLogLen);
            if(szInfoLog)
            {
                GLint written;
                glGetShaderInfoLog(fragmentShaderObject, iShaderInfoLogLen, &written, szInfoLog);
                printf( "Fragment Shader Compile Info: %s\n", szInfoLog);
                free(szInfoLog);
                [self release];
                
            }
        }
        
        /********SHADER PROGRAM**********/
        shaderProgramObject = glCreateProgram();
        glAttachShader(shaderProgramObject, vertexShaderObject);
        glAttachShader(shaderProgramObject, fragmentShaderObject);
        
        //pre-link binding of
        glBindAttribLocation(shaderProgramObject, VDG_ATTRIBUTE_POSITION, "vPosition");
        glBindAttribLocation(shaderProgramObject, VDG_ATTRIBUTE_TEXTURE0, "vTexture0_Coord");
        
        glLinkProgram(shaderProgramObject);
        GLint iProgramLinkStatus = 0;
        glGetProgramiv(shaderProgramObject, GL_LINK_STATUS, &iProgramLinkStatus);
        if(iProgramLinkStatus == GL_FALSE)
        {
            GLint iProgramInfoLogLen = 0;
            glGetProgramiv(shaderProgramObject, GL_INFO_LOG_LENGTH, &iProgramInfoLogLen);
            char *szInfoLog = (char*)malloc(iProgramInfoLogLen);
            if(szInfoLog)
            {
                GLint written;
                glGetProgramInfoLog(shaderProgramObject, iProgramInfoLogLen, &written, szInfoLog);
                printf( "Program Link Status Info: %s\n", szInfoLog);
                free(szInfoLog);
                [self release];
                
            }
        }
        
        //Get Uniform locations
        uniform[MVP] = glGetUniformLocation(shaderProgramObject, "u_mvp_matrix");
        uniform[TEXTURE_SAMPLER_2D] = glGetUniformLocation(shaderProgramObject, "u_Texture_sampler2d");
        
        //load textures
        texture_cube = [self loadTextureFromBMPFile:@"Vijay_Kundali" :@"bmp"];
        printf("texture_cube = 0x%x\n", texture_cube);
        proceduralTextureObject = [self loadprocImageArray];
        printf("texture_cube = 0x%x\n", proceduralTextureObject);
        //============
        
        /******* Vertices, colors, normals, vao, vbo initialization**********/

        GLfloat quadVertices [] =
        {
             1.0f,   1.0f,   0.0f,
            -1.0f,  1.0f,   0.0f,
            -1.0f,  -1.0f,  0.0f,
            1.0f,   -1.0f,  0.0f
        };
        //texture s t variables
 
        GLfloat quadTexCoords [] =
        {
          
            1.0f,   0.0f,
            0.0f,  0.0f,
            0.0f,  1.0f,
            1.0f,   1.0f,
        };
        
        //create vao from quad
        glGenVertexArrays(1, &vao[CUBE]);
        glBindVertexArray(vao[CUBE]);
      //cube vbo
            glGenBuffers(1, &vbo[CUBE][POSITION]);
            glBindBuffer(GL_ARRAY_BUFFER, vbo[CUBE][POSITION]);
            glBufferData(GL_ARRAY_BUFFER, sizeof(quadVertices), quadVertices, GL_STATIC_DRAW);
            
            glVertexAttribPointer(VDG_ATTRIBUTE_POSITION,
                                  3,    //x,y,z
                                  GL_FLOAT,
                                  GL_FALSE,
                                  0, NULL); //0-stride, no offset
            glEnableVertexAttribArray(VDG_ATTRIBUTE_POSITION);
            
            glBindBuffer(GL_ARRAY_BUFFER, 0);   //unbind
      //texture vbo
            glGenBuffers(1, &vbo[CUBE][TEXCOORDS]);
            glBindBuffer(GL_ARRAY_BUFFER, vbo[CUBE][TEXCOORDS]);
            glBufferData(GL_ARRAY_BUFFER, sizeof(quadTexCoords), quadTexCoords, GL_DYNAMIC_DRAW);   //going to change in drawView
            
            glVertexAttribPointer(VDG_ATTRIBUTE_TEXTURE0,
                                  2,
                                  GL_FLOAT,
                                  GL_FALSE,
                                  0, NULL);
            glEnableVertexAttribArray(VDG_ATTRIBUTE_TEXTURE0);
            
            glBindBuffer(GL_ARRAY_BUFFER, 0);
       
        glBindVertexArray(0);
        
        //Other OpenGL settings
        glEnable(GL_DEPTH_TEST);    //enable depth testing
        glDepthFunc(GL_LEQUAL);     //depth test to do
        
        //glEnable(GL_CULL_FACE);     //We will always cull back faces for better performance
        
        //set background clearing color
        glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
        keyPress = 0;
        
        //initialize ortho projectio matrix to identity
        projectonMatrix = vmath::mat4::identity();
  
        
        //Gesture Recognizer
        //Tap Gesture code
        
        UITapGestureRecognizer *singleTapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onSingleTap:)];
        [singleTapGestureRecognizer setNumberOfTapsRequired:1]; //single tap to recognize singleTap event
        [singleTapGestureRecognizer setNumberOfTouchesRequired:1];  //one finger touch is required to recognize single tap
        [singleTapGestureRecognizer setDelegate:self];
        [self addGestureRecognizer:singleTapGestureRecognizer];
        
      
        UITapGestureRecognizer *doubleTapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onDouleTap:)];
        [doubleTapGestureRecognizer setNumberOfTapsRequired:2]; //two taps required to detect double tap
        [doubleTapGestureRecognizer setNumberOfTouchesRequired:1]; //one finger touch
        [doubleTapGestureRecognizer setDelegate:self];
        [self addGestureRecognizer:doubleTapGestureRecognizer];
        
        //this will allow to differentiate between single tap and double tap
        [singleTapGestureRecognizer requireGestureRecognizerToFail:doubleTapGestureRecognizer];
        
      
        UISwipeGestureRecognizer *swipeGestureRecognizer = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(onSwipe:)];
        [self addGestureRecognizer:swipeGestureRecognizer];
        
     
        UILongPressGestureRecognizer *longPressGestureRecognizer = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(onLongPress:)];
        [self addGestureRecognizer:longPressGestureRecognizer];
    }
    return (self);
}/*initWithFrame*/

-(GLuint)loadTextureFromBMPFile:(NSString *)texFileName :(NSString*)extenstion
{
    NSString *textureFileNameWithPath = [[NSBundle mainBundle] pathForResource:texFileName ofType:extenstion];
    
    UIImage *bmpImage = [[UIImage alloc] initWithContentsOfFile:textureFileNameWithPath];
    if(bmpImage == nil)
    {
        NSLog(@"Can Not Find %@", textureFileNameWithPath);
        return (0);
    }
    
    CGImageRef cgImage = bmpImage.CGImage;
    int w = (int)CGImageGetWidth(cgImage);
    int h = (int)CGImageGetHeight(cgImage);
    printf("Image: %u x %u\n", w, h);
    CFDataRef imageData = CGDataProviderCopyData(CGImageGetDataProvider(cgImage));
    void* pixels = (void*)CFDataGetBytePtr(imageData);
    
    
    GLuint bmpTextureVariable;
    glGenTextures(1, &bmpTextureVariable);
    glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
    glBindTexture(GL_TEXTURE_2D, bmpTextureVariable);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexImage2D(GL_TEXTURE_2D,
                 0,
                 GL_RGBA,
                 w,h,
                 0,
                 GL_RGBA,
                 GL_UNSIGNED_BYTE,
                 pixels);
    
    //create mip maps for this texture for better image quality
    glGenerateMipmap(GL_TEXTURE_2D);
    
    //release
    CFRelease(imageData);
    return (bmpTextureVariable);
}


-(GLuint)loadprocImageArray
{
    [self makeproceduralDeafultTexture];
    
    GLuint proceduralDeafultTexture;
    
    glGenTextures(1, &proceduralDeafultTexture);
    glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
    glBindTexture(GL_TEXTURE_2D, proceduralDeafultTexture);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexImage2D(GL_TEXTURE_2D,
                 0,
                 GL_RGBA,
                 64, 64,
                 0,
                 GL_RGBA,
                 GL_UNSIGNED_BYTE,
                 procImageArray);
    return(proceduralDeafultTexture);
}


-(void) makeproceduralDeafultTexture
{
    int i, j;
    
    for (i = 0; i<64; i++)
    {
        for (j = 0; j<64; j++)
        {
            
            procImageArray[i][j][0] = 0xFF;
            procImageArray[i][j][1] = 0xFF;
            procImageArray[i][j][2] = 0xFF;
            procImageArray[i][j][3] = 0xFF;
        }
    }
}
+(Class)layerClass
{
    //code
    return ([CAEAGLLayer class]);
}

-(void)drawView:(id)sender  //display
{
    //code
    [EAGLContext setCurrentContext:eaglContext];
    
    glBindFramebuffer(GL_FRAMEBUFFER, defaultFrameBuffer);
    
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);
    
    //draw  display
    glUseProgram(shaderProgramObject);
    
    vmath::mat4 modelViewMatrix = vmath::mat4::identity();
    vmath::mat4 translateMatrix = vmath::mat4::identity();
    vmath::mat4 modelViewProjectionMatrix = vmath::mat4::identity();
    
    translateMatrix = vmath::translate(0.0f, 0.0f, -5.0f);
    
    //MV = T * MV
    modelViewMatrix = translateMatrix * modelViewMatrix;
    
    //MVP = P * MV
    modelViewProjectionMatrix = projectonMatrix * modelViewMatrix;
    
    //send MVP to shader
    glUniformMatrix4fv(uniform[MVP], 1, GL_FALSE, modelViewProjectionMatrix);
    
    //*** bind texture ***
    GLuint texture = texture_cube;
    GLfloat texCoords [8];
    switch (keyPress)
    {
        
            
        case 0:
        default:
            texture = proceduralTextureObject;
        case 1:
            texCoords[0] = 0.5f;    texCoords[1] = 0.5f;
            texCoords[2] = 0.0f;    texCoords[3] = 0.5f;
            texCoords[4] = 0.0f;    texCoords[5] = 1.0f;
            texCoords[6] = 0.5f;    texCoords[7] = 1.0f;
            break;
        case 2:
            texCoords[0] = 1.0f;    texCoords[1] = 0.0f;
            texCoords[2] = 0.0f;    texCoords[3] = 0.0f;
            texCoords[4] = 0.0f;    texCoords[5] = 1.0f;
            texCoords[6] = 1.0f;    texCoords[7] = 1.0f;
            break;
            
        case 3://4 images
            texCoords[0] = 2.0f;    texCoords[1] = 0.0f;
            texCoords[2] = 0.0f;    texCoords[3] = 0.0f;
            texCoords[4] = 0.0f;    texCoords[5] = 2.0f;
            texCoords[6] = 2.0f;    texCoords[7] = 2.0f;
            break;
            
        case 4: //at center
            texCoords[0] = 0.5f;    texCoords[1] = 0.5f;
            texCoords[2] = 0.5f;    texCoords[3] = 0.5f;
            texCoords[4] = 0.5f;    texCoords[5] = 0.5f;
            texCoords[6] = 0.5f;    texCoords[7] = 0.5f;
            break;
    }
    glBindTexture(GL_TEXTURE_2D, texture);
    
    //*** bind vao ***
    glBindVertexArray(vao[CUBE]);
    {
        glBindBuffer(GL_ARRAY_BUFFER, vbo[CUBE][TEXCOORDS]);
        glBufferData(GL_ARRAY_BUFFER, sizeof(texCoords), texCoords, GL_DYNAMIC_DRAW);
        glBindBuffer(GL_ARRAY_BUFFER, 0);
        
        //Draw/render using glDrawArrays
        glDrawArrays(GL_TRIANGLE_FAN, 0, 4);
    }
    glBindVertexArray(0);   //unbind
    
    glUseProgram(0);    //un-use program
    //=============
    
    glBindRenderbuffer(GL_RENDERBUFFER, colorRenderbuffer);
    [eaglContext presentRenderbuffer:GL_RENDERBUFFER];
    
//   [self spin];
}



-(void)layoutSubviews   //resize
{
    //code
    GLint width;
    GLint height;
    
    glBindRenderbuffer(GL_RENDERBUFFER, colorRenderbuffer);
    [eaglContext renderbufferStorage:GL_RENDERBUFFER fromDrawable:(CAEAGLLayer*)self.layer];
    glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_WIDTH, &width);
    glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_HEIGHT, &height);
    
    glGenRenderbuffers(1, &depthRenderbuffer);
    glBindRenderbuffer(1, depthRenderbuffer);
    glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT16, width, height);
    glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, depthRenderbuffer);
    
    glViewport(0, 0, width, height);
    
    //glOrtho(left, right, bottom, top, near, far)
    GLfloat fwidth = (GLfloat)width;
    GLfloat fheight = (GLfloat)height;
    
    ///perspective projection-> (fovy, AR, near, far)
    projectonMatrix = vmath::perspective(45.0f, fwidth/fheight, 0.1f, 100.0f);
    
    if(glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
    {
        printf("layoutSubviews: Failed to Create Complete Framebuffer Object 0x%x\n", glCheckFramebufferStatus(GL_FRAMEBUFFER));
    }
    
    [self drawView:nil];
}

-(void)startAnimation
{
    printf("startAnimation\n");
    if(!isAnimating)
    {
        displayLink = [NSClassFromString(@"CADisplayLink")displayLinkWithTarget:self selector:@selector(drawView:)];
        [displayLink setPreferredFramesPerSecond:animationFrameInterval];
        [displayLink addToRunLoop:[NSRunLoop currentRunLoop] forMode:NSDefaultRunLoopMode];
        
        isAnimating = YES;
    }
}

-(void)stopAnimation
{
    printf("stopAnimation\n");
    if(isAnimating)
    {
        [displayLink invalidate];
        displayLink = nil;
        
        isAnimating = NO;
    }
}

//to became first responder
-(BOOL)acceptsFirstResponder
{
    //code
    return (YES);
}

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent*)event
{
    
}

-(void)onSingleTap:(UITapGestureRecognizer*)gr
{
    keyPress++;
    if(keyPress>4)
        keyPress = 0;
    printf("keyPress = %d\n", keyPress);
}

-(void)onDouleTap:(UITapGestureRecognizer*)gr
{
    
}

-(void)onSwipe:(UISwipeGestureRecognizer*)gr
{
    //code
    printf("onSwipe: Exiting...\n");
    [self release];
    exit(0);    //this wrong in iOS app which is to be placed on Play Store
}

-(void)onLongPress:(UILongPressGestureRecognizer*)gr
{
    
}

-(void)dealloc
{
    //code
    
    
    if(shaderProgramObject)
    {
        if(fragmentShaderObject)
        {
            glDetachShader(shaderProgramObject, fragmentShaderObject);
            glDeleteShader(fragmentShaderObject);
            fragmentShaderObject = 0;
        }
        if(vertexShaderObject)
        {
            glDetachShader(shaderProgramObject, vertexShaderObject);
            glDeleteShader(vertexShaderObject);
            vertexShaderObject = 0;
        }
        glDeleteProgram(shaderProgramObject);
        shaderProgramObject = 0;
    }
    
    if(depthRenderbuffer)
    {
        glDeleteRenderbuffers(1, &depthRenderbuffer);
        depthRenderbuffer = 0;
    }
    
    if(colorRenderbuffer)
    {
        glDeleteRenderbuffers(1, &colorRenderbuffer);
        colorRenderbuffer = 0;
    }
    
    if(defaultFrameBuffer)
    {
        glDeleteFramebuffers(1, &defaultFrameBuffer);
        defaultFrameBuffer = 0;
    }
    
    if([EAGLContext currentContext] == eaglContext)
    {
        [EAGLContext setCurrentContext:nil];
    }
    [eaglContext release];
    eaglContext = nil;
    
    [super dealloc];
}

@end
